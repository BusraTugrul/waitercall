﻿namespace cevircagirserverdeneme
{
    partial class frmMasterSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMasterSettings));
            this.panel3 = new System.Windows.Forms.Panel();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.lblgarsonbaslik = new System.Windows.Forms.Label();
            this.lblformname = new System.Windows.Forms.Label();
            this.pnl1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnPnl1Next = new DevExpress.XtraEditors.SimpleButton();
            this.pnl2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.btnPnl2Next = new DevExpress.XtraEditors.SimpleButton();
            this.pnl4 = new System.Windows.Forms.Panel();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnPnl4Next = new DevExpress.XtraEditors.SimpleButton();
            this.pnl3 = new System.Windows.Forms.Panel();
            this.lblerror = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmbssid = new System.Windows.Forms.ComboBox();
            this.btnHelp = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.btnMasterSend = new DevExpress.XtraEditors.SimpleButton();
            this.label18 = new System.Windows.Forms.Label();
            this.txtPass = new System.Windows.Forms.TextBox();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.btnPnl3Next = new DevExpress.XtraEditors.SimpleButton();
            this.panel3.SuspendLayout();
            this.pnl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.pnl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.pnl4.SuspendLayout();
            this.pnl3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Orange;
            this.panel3.Controls.Add(this.simpleButton1);
            this.panel3.Controls.Add(this.lblgarsonbaslik);
            this.panel3.Controls.Add(this.lblformname);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1100, 40);
            this.panel3.TabIndex = 20;
            // 
            // simpleButton1
            // 
            this.simpleButton1.AllowFocus = false;
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.simpleButton1.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton1.Image = global::cevircagirserverdeneme.Properties.Resources.icons8_Close_Window_Filled_30;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(1056, 4);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 2, 11, 2);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(33, 30);
            this.simpleButton1.TabIndex = 29;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // lblgarsonbaslik
            // 
            this.lblgarsonbaslik.AutoSize = true;
            this.lblgarsonbaslik.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold);
            this.lblgarsonbaslik.ForeColor = System.Drawing.Color.Orange;
            this.lblgarsonbaslik.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblgarsonbaslik.Location = new System.Drawing.Point(12, 10);
            this.lblgarsonbaslik.Name = "lblgarsonbaslik";
            this.lblgarsonbaslik.Size = new System.Drawing.Size(0, 19);
            this.lblgarsonbaslik.TabIndex = 13;
            // 
            // lblformname
            // 
            this.lblformname.AccessibleName = "";
            this.lblformname.AutoSize = true;
            this.lblformname.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblformname.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold);
            this.lblformname.ForeColor = System.Drawing.Color.White;
            this.lblformname.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblformname.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblformname.Location = new System.Drawing.Point(4, 3);
            this.lblformname.Name = "lblformname";
            this.lblformname.Size = new System.Drawing.Size(260, 32);
            this.lblformname.TabIndex = 12;
            this.lblformname.Text = "Erişim Noktası Ayarları";
            this.lblformname.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // pnl1
            // 
            this.pnl1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnl1.Controls.Add(this.pictureBox1);
            this.pnl1.Controls.Add(this.labelControl1);
            this.pnl1.Controls.Add(this.btnPnl1Next);
            this.pnl1.Location = new System.Drawing.Point(0, 40);
            this.pnl1.Name = "pnl1";
            this.pnl1.Size = new System.Drawing.Size(550, 305);
            this.pnl1.TabIndex = 21;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Image = global::cevircagirserverdeneme.Properties.Resources.modemakesiz;
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(3, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(304, 296);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 28;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl1.LineVisible = true;
            this.labelControl1.Location = new System.Drawing.Point(309, 5);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(238, 253);
            this.labelControl1.TabIndex = 27;
            this.labelControl1.Text = resources.GetString("labelControl1.Text");
            this.labelControl1.Click += new System.EventHandler(this.labelControl1_Click);
            // 
            // btnPnl1Next
            // 
            this.btnPnl1Next.Appearance.BackColor = System.Drawing.Color.Orange;
            this.btnPnl1Next.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.btnPnl1Next.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnPnl1Next.Appearance.Options.UseBackColor = true;
            this.btnPnl1Next.Appearance.Options.UseFont = true;
            this.btnPnl1Next.Appearance.Options.UseForeColor = true;
            this.btnPnl1Next.AppearanceHovered.BackColor = System.Drawing.Color.White;
            this.btnPnl1Next.AppearanceHovered.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.btnPnl1Next.AppearanceHovered.ForeColor = System.Drawing.Color.DimGray;
            this.btnPnl1Next.AppearanceHovered.Options.UseBackColor = true;
            this.btnPnl1Next.AppearanceHovered.Options.UseFont = true;
            this.btnPnl1Next.AppearanceHovered.Options.UseForeColor = true;
            this.btnPnl1Next.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPnl1Next.Location = new System.Drawing.Point(443, 264);
            this.btnPnl1Next.Name = "btnPnl1Next";
            this.btnPnl1Next.Size = new System.Drawing.Size(84, 32);
            this.btnPnl1Next.TabIndex = 26;
            this.btnPnl1Next.Text = "İleri";
            this.btnPnl1Next.Click += new System.EventHandler(this.btnPnl1Next_Click);
            // 
            // pnl2
            // 
            this.pnl2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnl2.Controls.Add(this.pictureBox2);
            this.pnl2.Controls.Add(this.labelControl2);
            this.pnl2.Controls.Add(this.btnPnl2Next);
            this.pnl2.Location = new System.Drawing.Point(550, 40);
            this.pnl2.Name = "pnl2";
            this.pnl2.Size = new System.Drawing.Size(550, 305);
            this.pnl2.TabIndex = 22;
            this.pnl2.Visible = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox2.Image = global::cevircagirserverdeneme.Properties.Resources.ccesp2;
            this.pictureBox2.InitialImage = null;
            this.pictureBox2.Location = new System.Drawing.Point(12, 3);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(225, 299);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 29;
            this.pictureBox2.TabStop = false;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl2.LineVisible = true;
            this.labelControl2.Location = new System.Drawing.Point(250, 5);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(280, 207);
            this.labelControl2.TabIndex = 29;
            this.labelControl2.Text = resources.GetString("labelControl2.Text");
            // 
            // btnPnl2Next
            // 
            this.btnPnl2Next.Appearance.BackColor = System.Drawing.Color.Orange;
            this.btnPnl2Next.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.btnPnl2Next.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnPnl2Next.Appearance.Options.UseBackColor = true;
            this.btnPnl2Next.Appearance.Options.UseFont = true;
            this.btnPnl2Next.Appearance.Options.UseForeColor = true;
            this.btnPnl2Next.AppearanceHovered.BackColor = System.Drawing.Color.White;
            this.btnPnl2Next.AppearanceHovered.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.btnPnl2Next.AppearanceHovered.ForeColor = System.Drawing.Color.DimGray;
            this.btnPnl2Next.AppearanceHovered.Options.UseBackColor = true;
            this.btnPnl2Next.AppearanceHovered.Options.UseFont = true;
            this.btnPnl2Next.AppearanceHovered.Options.UseForeColor = true;
            this.btnPnl2Next.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPnl2Next.Location = new System.Drawing.Point(453, 264);
            this.btnPnl2Next.Name = "btnPnl2Next";
            this.btnPnl2Next.Size = new System.Drawing.Size(84, 32);
            this.btnPnl2Next.TabIndex = 27;
            this.btnPnl2Next.Text = "İleri";
            this.btnPnl2Next.Click += new System.EventHandler(this.btnPnl2Next_Click);
            // 
            // pnl4
            // 
            this.pnl4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnl4.Controls.Add(this.labelControl4);
            this.pnl4.Controls.Add(this.btnPnl4Next);
            this.pnl4.Location = new System.Drawing.Point(550, 345);
            this.pnl4.Name = "pnl4";
            this.pnl4.Size = new System.Drawing.Size(550, 305);
            this.pnl4.TabIndex = 24;
            this.pnl4.Visible = false;
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl4.LineVisible = true;
            this.labelControl4.Location = new System.Drawing.Point(104, 51);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(357, 161);
            this.labelControl4.TabIndex = 75;
            this.labelControl4.Text = resources.GetString("labelControl4.Text");
            // 
            // btnPnl4Next
            // 
            this.btnPnl4Next.Appearance.BackColor = System.Drawing.Color.Orange;
            this.btnPnl4Next.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.btnPnl4Next.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnPnl4Next.Appearance.Options.UseBackColor = true;
            this.btnPnl4Next.Appearance.Options.UseFont = true;
            this.btnPnl4Next.Appearance.Options.UseForeColor = true;
            this.btnPnl4Next.AppearanceHovered.BackColor = System.Drawing.Color.White;
            this.btnPnl4Next.AppearanceHovered.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.btnPnl4Next.AppearanceHovered.ForeColor = System.Drawing.Color.DimGray;
            this.btnPnl4Next.AppearanceHovered.Options.UseBackColor = true;
            this.btnPnl4Next.AppearanceHovered.Options.UseFont = true;
            this.btnPnl4Next.AppearanceHovered.Options.UseForeColor = true;
            this.btnPnl4Next.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPnl4Next.Location = new System.Drawing.Point(453, 263);
            this.btnPnl4Next.Name = "btnPnl4Next";
            this.btnPnl4Next.Size = new System.Drawing.Size(84, 32);
            this.btnPnl4Next.TabIndex = 29;
            this.btnPnl4Next.Text = "İleri";
            this.btnPnl4Next.Click += new System.EventHandler(this.btnPnl4Next_Click);
            // 
            // pnl3
            // 
            this.pnl3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pnl3.Controls.Add(this.lblerror);
            this.pnl3.Controls.Add(this.panel1);
            this.pnl3.Controls.Add(this.labelControl3);
            this.pnl3.Controls.Add(this.btnPnl3Next);
            this.pnl3.Location = new System.Drawing.Point(0, 345);
            this.pnl3.Name = "pnl3";
            this.pnl3.Size = new System.Drawing.Size(550, 305);
            this.pnl3.TabIndex = 23;
            this.pnl3.Visible = false;
            // 
            // lblerror
            // 
            this.lblerror.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.lblerror.ForeColor = System.Drawing.Color.Red;
            this.lblerror.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblerror.Location = new System.Drawing.Point(12, 233);
            this.lblerror.Name = "lblerror";
            this.lblerror.Size = new System.Drawing.Size(272, 62);
            this.lblerror.TabIndex = 75;
            this.lblerror.Text = "Erişim noktası cihazına bağlı değilsiniz. Önceki adımları tekrar edin!";
            this.lblerror.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmbssid);
            this.panel1.Controls.Add(this.btnHelp);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.btnMasterSend);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.txtPass);
            this.panel1.Location = new System.Drawing.Point(3, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(245, 225);
            this.panel1.TabIndex = 74;
            // 
            // cmbssid
            // 
            this.cmbssid.Font = new System.Drawing.Font("Segoe UI", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cmbssid.ForeColor = System.Drawing.Color.DimGray;
            this.cmbssid.FormattingEnabled = true;
            this.cmbssid.Location = new System.Drawing.Point(14, 53);
            this.cmbssid.Name = "cmbssid";
            this.cmbssid.Size = new System.Drawing.Size(216, 31);
            this.cmbssid.TabIndex = 73;
            this.cmbssid.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.cmbssid.DropDownClosed += new System.EventHandler(this.cmbssid_DropDownClosed);
            // 
            // btnHelp
            // 
            this.btnHelp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnHelp.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnHelp.FlatAppearance.BorderSize = 0;
            this.btnHelp.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnHelp.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnHelp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnHelp.ForeColor = System.Drawing.Color.White;
            this.btnHelp.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.btnHelp.Location = new System.Drawing.Point(335, 84);
            this.btnHelp.Name = "btnHelp";
            this.btnHelp.Size = new System.Drawing.Size(52, 46);
            this.btnHelp.TabIndex = 72;
            this.btnHelp.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.DimGray;
            this.label17.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label17.Location = new System.Drawing.Point(10, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 20);
            this.label17.TabIndex = 67;
            this.label17.Text = "Ağ İsmi";
            // 
            // btnMasterSend
            // 
            this.btnMasterSend.AllowFocus = false;
            this.btnMasterSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMasterSend.Appearance.BackColor = System.Drawing.Color.Orange;
            this.btnMasterSend.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.btnMasterSend.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnMasterSend.Appearance.Options.UseBackColor = true;
            this.btnMasterSend.Appearance.Options.UseFont = true;
            this.btnMasterSend.Appearance.Options.UseForeColor = true;
            this.btnMasterSend.AppearanceHovered.BackColor = System.Drawing.Color.White;
            this.btnMasterSend.AppearanceHovered.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.btnMasterSend.AppearanceHovered.ForeColor = System.Drawing.Color.DimGray;
            this.btnMasterSend.AppearanceHovered.Options.UseBackColor = true;
            this.btnMasterSend.AppearanceHovered.Options.UseFont = true;
            this.btnMasterSend.AppearanceHovered.Options.UseForeColor = true;
            this.btnMasterSend.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnMasterSend.Location = new System.Drawing.Point(14, 180);
            this.btnMasterSend.Name = "btnMasterSend";
            this.btnMasterSend.Size = new System.Drawing.Size(179, 32);
            this.btnMasterSend.TabIndex = 71;
            this.btnMasterSend.Text = "Gönder";
            this.btnMasterSend.Click += new System.EventHandler(this.btnMasterSend_Click);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.label18.ForeColor = System.Drawing.Color.DimGray;
            this.label18.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label18.Location = new System.Drawing.Point(10, 96);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(53, 20);
            this.label18.TabIndex = 69;
            this.label18.Text = "Parola";
            // 
            // txtPass
            // 
            this.txtPass.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.txtPass.ForeColor = System.Drawing.Color.DimGray;
            this.txtPass.Location = new System.Drawing.Point(14, 125);
            this.txtPass.Multiline = true;
            this.txtPass.Name = "txtPass";
            this.txtPass.Size = new System.Drawing.Size(216, 36);
            this.txtPass.TabIndex = 70;
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl3.LineVisible = true;
            this.labelControl3.Location = new System.Drawing.Point(291, 3);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(254, 253);
            this.labelControl3.TabIndex = 29;
            this.labelControl3.Text = resources.GetString("labelControl3.Text");
            // 
            // btnPnl3Next
            // 
            this.btnPnl3Next.Appearance.BackColor = System.Drawing.Color.Orange;
            this.btnPnl3Next.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.btnPnl3Next.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnPnl3Next.Appearance.Options.UseBackColor = true;
            this.btnPnl3Next.Appearance.Options.UseFont = true;
            this.btnPnl3Next.Appearance.Options.UseForeColor = true;
            this.btnPnl3Next.AppearanceHovered.BackColor = System.Drawing.Color.White;
            this.btnPnl3Next.AppearanceHovered.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.btnPnl3Next.AppearanceHovered.ForeColor = System.Drawing.Color.DimGray;
            this.btnPnl3Next.AppearanceHovered.Options.UseBackColor = true;
            this.btnPnl3Next.AppearanceHovered.Options.UseFont = true;
            this.btnPnl3Next.AppearanceHovered.Options.UseForeColor = true;
            this.btnPnl3Next.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPnl3Next.Location = new System.Drawing.Point(443, 263);
            this.btnPnl3Next.Name = "btnPnl3Next";
            this.btnPnl3Next.Size = new System.Drawing.Size(84, 32);
            this.btnPnl3Next.TabIndex = 28;
            this.btnPnl3Next.Text = "İleri";
            this.btnPnl3Next.Click += new System.EventHandler(this.btnPnl3Next_Click);
            // 
            // frmMasterSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1100, 650);
            this.Controls.Add(this.pnl4);
            this.Controls.Add(this.pnl3);
            this.Controls.Add(this.pnl2);
            this.Controls.Add(this.pnl1);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmMasterSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMasterSettings";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmMasterSettings_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.pnl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.pnl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.pnl4.ResumeLayout(false);
            this.pnl3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Label lblgarsonbaslik;
        public System.Windows.Forms.Label lblformname;
        private System.Windows.Forms.Panel pnl1;
        private System.Windows.Forms.Panel pnl2;
        private System.Windows.Forms.Panel pnl4;
        private System.Windows.Forms.Panel pnl3;
        private DevExpress.XtraEditors.SimpleButton btnPnl1Next;
        private DevExpress.XtraEditors.SimpleButton btnPnl2Next;
        private DevExpress.XtraEditors.SimpleButton btnPnl4Next;
        private DevExpress.XtraEditors.SimpleButton btnPnl3Next;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnHelp;
        private System.Windows.Forms.Label label17;
        public DevExpress.XtraEditors.SimpleButton btnMasterSend;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtPass;
        private System.Windows.Forms.ComboBox cmbssid;
        private System.Windows.Forms.Label lblerror;
    }
}