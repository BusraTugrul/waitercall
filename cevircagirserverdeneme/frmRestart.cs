﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cevircagirserverdeneme
{
    public partial class frmRestart : Form
    {
        public frmRestart()
        {
            InitializeComponent();
            localizationset();
        }

        private void btnLater_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        public void localizationset()
        {
            btnLater.Text = Localization.btnlater;
            btnRestart.Text = Localization.btnrestart;
            labelControl4.Text = Localization.restartinfo;
        }

        private void labelControl4_Click(object sender, EventArgs e)
        {

        }

        private void frmRestart_Load(object sender, EventArgs e)
        {
            Screen screen = Screen.FromControl(this);
            int x = screen.WorkingArea.X - screen.Bounds.X;
            int y = screen.WorkingArea.Y - screen.Bounds.Y;
            this.MaximizedBounds = new Rectangle(x, y,
                screen.WorkingArea.Width, screen.WorkingArea.Height);
            this.MaximumSize = screen.WorkingArea.Size;
            this.WindowState = FormWindowState.Maximized;
        }
    }
}
