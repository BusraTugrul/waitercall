﻿/*
*Bu ekranın kullanım amacı; kullanıcının ap cihazların bağlı olduğu ağı seçmesini sağlamaktır.
*Bu ekranın açılacağı durumlar şu şekildedir;
    *Uygulamaya ilk defa girilmişse, 
    *Daha önce seçilen ağ bulunamamışsa,
    * Hiçbir ağ bulunamamışsa.
*Settings de "manualSelectedIP" değeri seçilen ağ kartının mac adresini tutuyor.
*Settings de "udptcpIP" ise mac adresine karşılık gelen ip değerini tutuyor.
*Bu ekranda seçilen ağ kartının mac adresi next butonu ile manualSelectedIP ye atanıyor ve uygulama yeniden başlatılıyor. 
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.NetworkInformation;
using System.Text.RegularExpressions;

namespace cevircagirserverdeneme
{
    public partial class frmManualNetworkSelection : Form
    {
        string manualSelectedNetwork = "";
        UdpBroadcast udpclass;
        string macip = "";
        public frmManualNetworkSelection()
        {
            InitializeComponent();
        }

        private void frmManualNetworkSelection_Load(object sender, EventArgs e)
        {
            Screen screen = Screen.FromControl(this);
            int x = screen.WorkingArea.X - screen.Bounds.X;
            int y = screen.WorkingArea.Y - screen.Bounds.Y;
            this.MaximizedBounds = new Rectangle(x, y,
                screen.WorkingArea.Width, screen.WorkingArea.Height);
            this.MaximumSize = screen.WorkingArea.Size;
            this.WindowState = FormWindowState.Maximized;

            if(Settings1.Default.serialorwifi == 0)   //uygulama wifi alıcı ile çalışıyorsa
            {
                chkwifi.Checked = true;
                pnlforwifi.Visible = true;
                cmbNetwork.Text = "";
                cmbNetwork.Properties.Items.Clear();
                int i = 0;
                NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
                if (adapters == null || adapters.Length < 1)
                {
                    Settings1.Default.manualSelectedIP = "0";
                    Settings1.Default.Save();
                    label1.Visible = true;
                    label1.Text = Localization.nothingfound;
                }
                else
                {
                    foreach (NetworkInterface adapter in adapters.Where(a => a.OperationalStatus == OperationalStatus.Up))
                    {
                        if (adapter.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || adapter.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                        {
                            foreach (UnicastIPAddressInformation ip in adapter.GetIPProperties().UnicastAddresses)
                            {
                                if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                                {
                                    //Console.WriteLine(adapter.Name);
                                    //Console.WriteLine(adapter.GetPhysicalAddress().ToString());
                                    manualSelectedNetwork = adapter.Name;
                                    if (i == 0)
                                    {
                                        i++;
                                        cmbNetwork.Text = manualSelectedNetwork;
                                    }
                                    cmbNetwork.Properties.Items.Add(manualSelectedNetwork);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                chkSerial.Checked = true;
                pnlforwifi.Visible = false;
            }

            //label17.Text = Localization.choosenet;
            //lblformname.Text = Localization.infonet;
            //btnPnl2Next.Text = Localization.next;
            
        }

        private void btnPnl2Next_Click(object sender, EventArgs e)
        {
            if (chkwifi.Checked)
            {
                if (cmbNetwork.Properties.Items.Count >= 1)
                {
                    getip(cmbNetwork.Text);
                    Settings1.Default.serialorwifi = 0;
                    Settings1.Default.Save();
                    //Settings1.Default.manualSelectedIP =Regex.Split(cmbNetwork.Text, " <----> ")[1];
                    //Console.WriteLine("----"+Settings1.Default.manualSelectedIP+"----------"+cmbNetwork.Text);
                    //udpclass = UdpBroadcast.singletonudpobject;
                    //udpclass.startUdpBroadcasting();
                    frmRestart frmrst = new frmRestart();
                    frmrst.StartPosition = FormStartPosition.Manual;
                    frmrst.Left = frmManualNetworkSelection.ActiveForm.Left;
                    frmrst.Top = frmManualNetworkSelection.ActiveForm.Top;
                    frmrst.Show();
                    this.Close();
                    //frmAccessPointConnection frmapc = new frmAccessPointConnection();
                    //frmapc.StartPosition = FormStartPosition.Manual;
                    //frmapc.Left = this.Left;
                    //frmapc.Top = this.Top;
                    //frmapc.Show();
                }
            }
            else
            {
                Settings1.Default.serialorwifi = 1;
                Settings1.Default.Save();
                frmRestart frmrst = new frmRestart();
                frmrst.StartPosition = FormStartPosition.Manual;
                frmrst.Left = frmManualNetworkSelection.ActiveForm.Left;
                frmrst.Top = frmManualNetworkSelection.ActiveForm.Top;
                frmrst.Show();
                this.Close();
            }
        }
        public void getip(string interfacename)    //interface ismine göre mac adress karşılığını bulur ve settings e kaydeder
        {
            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            if (adapters == null || adapters.Length < 1)
            {
                Settings1.Default.manualSelectedIP = "0";
                Settings1.Default.Save();
            }
            else
            {
                foreach (NetworkInterface adapter in adapters)
                {
                    if(adapter.Name == interfacename)
                    {
                        Settings1.Default.manualSelectedIP = adapter.GetPhysicalAddress().ToString();
                        //Console.WriteLine(Settings1.Default.manualSelectedIP);
                        Settings1.Default.Save();
                    }
                }
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
            System.Windows.Forms.Application.Exit();
        }

        private void chkwifi_CheckedChanged(object sender, EventArgs e)
        {
            if (chkwifi.Checked)
            {
                chkSerial.Checked = false;
                pnlforwifi.Visible = true;
                cmbNetwork.Text = "";
                cmbNetwork.Properties.Items.Clear();
                int i = 0;
                NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
                if (adapters == null || adapters.Length < 1)
                {
                    Settings1.Default.manualSelectedIP = "0";
                    Settings1.Default.Save();
                    label1.Visible = true;
                    label1.Text = Localization.nothingfound;
                }
                else
                {
                    foreach (NetworkInterface adapter in adapters.Where(a => a.OperationalStatus == OperationalStatus.Up))
                    {
                        if (adapter.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 || adapter.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                        {
                            foreach (UnicastIPAddressInformation ip in adapter.GetIPProperties().UnicastAddresses)
                            {
                                if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                                {
                                    //Console.WriteLine(adapter.Name);
                                    //Console.WriteLine(adapter.GetPhysicalAddress().ToString());
                                    manualSelectedNetwork = adapter.Name;
                                    if (i == 0)
                                    {
                                        i++;
                                        cmbNetwork.Text = manualSelectedNetwork;
                                    }
                                    cmbNetwork.Properties.Items.Add(manualSelectedNetwork);
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                chkSerial.Checked = true;
                pnlforwifi.Visible = false;
            }
        }

        private void chkSerial_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSerial.Checked)
            {
                chkwifi.Checked = false;
                pnlforwifi.Visible = false;
            }
            else
            {
                chkwifi.Checked = true;
                pnlforwifi.Visible = true;
            }
        }
    }
}
