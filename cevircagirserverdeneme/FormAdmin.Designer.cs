﻿namespace cevircagirserverdeneme
{
    partial class frmadmin
    {
        /// <summary>
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer üretilen kod

        /// <summary>
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmadmin));
            this.btncihazekle = new DevExpress.XtraEditors.SimpleButton();
            this.btntanimlar = new DevExpress.XtraEditors.SimpleButton();
            this.btnmasaraporlar = new DevExpress.XtraEditors.SimpleButton();
            this.btnadmin = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabadmin = new DevExpress.XtraTab.XtraTabPage();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btncihazekle
            // 
            this.btncihazekle.AllowFocus = false;
            this.btncihazekle.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btncihazekle.Appearance.BackColor")));
            this.btncihazekle.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btncihazekle.Appearance.Font")));
            this.btncihazekle.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btncihazekle.Appearance.ForeColor")));
            this.btncihazekle.Appearance.Options.UseBackColor = true;
            this.btncihazekle.Appearance.Options.UseFont = true;
            this.btncihazekle.Appearance.Options.UseForeColor = true;
            this.btncihazekle.Appearance.Options.UseTextOptions = true;
            this.btncihazekle.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btncihazekle.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btncihazekle.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btncihazekle.AppearanceHovered.BackColor")));
            this.btncihazekle.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btncihazekle.AppearanceHovered.Font")));
            this.btncihazekle.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btncihazekle.AppearanceHovered.ForeColor")));
            this.btncihazekle.AppearanceHovered.Options.UseBackColor = true;
            this.btncihazekle.AppearanceHovered.Options.UseFont = true;
            this.btncihazekle.AppearanceHovered.Options.UseForeColor = true;
            this.btncihazekle.AppearanceHovered.Options.UseTextOptions = true;
            this.btncihazekle.AppearanceHovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btncihazekle.AppearanceHovered.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            resources.ApplyResources(this.btncihazekle, "btncihazekle");
            this.btncihazekle.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btncihazekle.Image = global::cevircagirserverdeneme.Properties.Resources.icons8_Services_25;
            this.btncihazekle.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btncihazekle.ImageToTextIndent = 5;
            this.btncihazekle.Name = "btncihazekle";
            this.btncihazekle.Click += new System.EventHandler(this.btncihazekle_Click);
            // 
            // btntanimlar
            // 
            this.btntanimlar.AllowFocus = false;
            this.btntanimlar.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btntanimlar.Appearance.BackColor")));
            this.btntanimlar.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btntanimlar.Appearance.Font")));
            this.btntanimlar.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btntanimlar.Appearance.ForeColor")));
            this.btntanimlar.Appearance.Options.UseBackColor = true;
            this.btntanimlar.Appearance.Options.UseFont = true;
            this.btntanimlar.Appearance.Options.UseForeColor = true;
            this.btntanimlar.Appearance.Options.UseTextOptions = true;
            this.btntanimlar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btntanimlar.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btntanimlar.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btntanimlar.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btntanimlar.AppearanceHovered.BackColor")));
            this.btntanimlar.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btntanimlar.AppearanceHovered.Font")));
            this.btntanimlar.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btntanimlar.AppearanceHovered.ForeColor")));
            this.btntanimlar.AppearanceHovered.Options.UseBackColor = true;
            this.btntanimlar.AppearanceHovered.Options.UseFont = true;
            this.btntanimlar.AppearanceHovered.Options.UseForeColor = true;
            this.btntanimlar.AppearanceHovered.Options.UseTextOptions = true;
            this.btntanimlar.AppearanceHovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btntanimlar.AppearanceHovered.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btntanimlar.AppearanceHovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.NoWrap;
            resources.ApplyResources(this.btntanimlar, "btntanimlar");
            this.btntanimlar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btntanimlar.Image = global::cevircagirserverdeneme.Properties.Resources.icons8_Groom_Filled_25;
            this.btntanimlar.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btntanimlar.ImageToTextIndent = 5;
            this.btntanimlar.Name = "btntanimlar";
            this.btntanimlar.Click += new System.EventHandler(this.btntanimlar_Click);
            // 
            // btnmasaraporlar
            // 
            this.btnmasaraporlar.AllowFocus = false;
            this.btnmasaraporlar.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnmasaraporlar.Appearance.BackColor")));
            this.btnmasaraporlar.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnmasaraporlar.Appearance.Font")));
            this.btnmasaraporlar.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnmasaraporlar.Appearance.ForeColor")));
            this.btnmasaraporlar.Appearance.Options.UseBackColor = true;
            this.btnmasaraporlar.Appearance.Options.UseFont = true;
            this.btnmasaraporlar.Appearance.Options.UseForeColor = true;
            this.btnmasaraporlar.Appearance.Options.UseTextOptions = true;
            this.btnmasaraporlar.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnmasaraporlar.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btnmasaraporlar.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnmasaraporlar.AppearanceHovered.BackColor")));
            this.btnmasaraporlar.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnmasaraporlar.AppearanceHovered.Font")));
            this.btnmasaraporlar.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnmasaraporlar.AppearanceHovered.ForeColor")));
            this.btnmasaraporlar.AppearanceHovered.Options.UseBackColor = true;
            this.btnmasaraporlar.AppearanceHovered.Options.UseFont = true;
            this.btnmasaraporlar.AppearanceHovered.Options.UseForeColor = true;
            this.btnmasaraporlar.AppearanceHovered.Options.UseTextOptions = true;
            this.btnmasaraporlar.AppearanceHovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnmasaraporlar.AppearanceHovered.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            resources.ApplyResources(this.btnmasaraporlar, "btnmasaraporlar");
            this.btnmasaraporlar.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnmasaraporlar.Image = global::cevircagirserverdeneme.Properties.Resources.icons8_Combo_Chart_Filled_25;
            this.btnmasaraporlar.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnmasaraporlar.ImageToTextIndent = 5;
            this.btnmasaraporlar.Name = "btnmasaraporlar";
            this.btnmasaraporlar.Click += new System.EventHandler(this.btnmasaraporlar_Click);
            // 
            // btnadmin
            // 
            this.btnadmin.AllowFocus = false;
            this.btnadmin.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnadmin.Appearance.BackColor")));
            this.btnadmin.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnadmin.Appearance.Font")));
            this.btnadmin.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnadmin.Appearance.ForeColor")));
            this.btnadmin.Appearance.Options.UseBackColor = true;
            this.btnadmin.Appearance.Options.UseFont = true;
            this.btnadmin.Appearance.Options.UseForeColor = true;
            this.btnadmin.Appearance.Options.UseTextOptions = true;
            this.btnadmin.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnadmin.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btnadmin.Appearance.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.btnadmin.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnadmin.AppearanceHovered.BackColor")));
            this.btnadmin.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnadmin.AppearanceHovered.Font")));
            this.btnadmin.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnadmin.AppearanceHovered.ForeColor")));
            this.btnadmin.AppearanceHovered.Options.UseBackColor = true;
            this.btnadmin.AppearanceHovered.Options.UseFont = true;
            this.btnadmin.AppearanceHovered.Options.UseForeColor = true;
            this.btnadmin.AppearanceHovered.Options.UseTextOptions = true;
            this.btnadmin.AppearanceHovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.btnadmin.AppearanceHovered.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btnadmin.AppearanceHovered.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            resources.ApplyResources(this.btnadmin, "btnadmin");
            this.btnadmin.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnadmin.Image = global::cevircagirserverdeneme.Properties.Resources.icons8_Admin_Settings_Male_Filled_25;
            this.btnadmin.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.btnadmin.ImageToTextIndent = 5;
            this.btnadmin.Name = "btnadmin";
            this.btnadmin.Click += new System.EventHandler(this.btnadmin_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.AllowFocus = false;
            resources.ApplyResources(this.simpleButton1, "simpleButton1");
            this.simpleButton1.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton1.Appearance.BackColor")));
            this.simpleButton1.Appearance.BackColor2 = ((System.Drawing.Color)(resources.GetObject("simpleButton1.Appearance.BackColor2")));
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton1.Image = global::cevircagirserverdeneme.Properties.Resources.icons8_Close_Window_Filled_30;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("panelControl1.Appearance.BackColor")));
            this.panelControl1.Appearance.BackColor2 = ((System.Drawing.Color)(resources.GetObject("panelControl1.Appearance.BackColor2")));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.simpleButton4);
            this.panelControl1.Controls.Add(this.simpleButton3);
            this.panelControl1.Controls.Add(this.simpleButton2);
            this.panelControl1.Controls.Add(this.simpleButton1);
            this.panelControl1.Controls.Add(this.btncihazekle);
            this.panelControl1.Controls.Add(this.btnadmin);
            this.panelControl1.Controls.Add(this.btnmasaraporlar);
            this.panelControl1.Controls.Add(this.btntanimlar);
            resources.ApplyResources(this.panelControl1, "panelControl1");
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panelControl1_MouseDown);
            this.panelControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panelControl1_MouseMove);
            this.panelControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panelControl1_MouseUp);
            // 
            // simpleButton4
            // 
            this.simpleButton4.AllowFocus = false;
            resources.ApplyResources(this.simpleButton4, "simpleButton4");
            this.simpleButton4.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton4.Appearance.BackColor")));
            this.simpleButton4.Appearance.BackColor2 = ((System.Drawing.Color)(resources.GetObject("simpleButton4.Appearance.BackColor2")));
            this.simpleButton4.Appearance.Options.UseBackColor = true;
            this.simpleButton4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton4.Image = global::cevircagirserverdeneme.Properties.Resources.icons8_minimize_window30;
            this.simpleButton4.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // simpleButton3
            // 
            this.simpleButton3.AllowFocus = false;
            this.simpleButton3.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton3.Appearance.BackColor")));
            this.simpleButton3.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton3.Appearance.Font")));
            this.simpleButton3.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton3.Appearance.ForeColor")));
            this.simpleButton3.Appearance.Options.UseBackColor = true;
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Appearance.Options.UseForeColor = true;
            this.simpleButton3.Appearance.Options.UseTextOptions = true;
            this.simpleButton3.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.simpleButton3.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.simpleButton3.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton3.AppearanceHovered.BackColor")));
            this.simpleButton3.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton3.AppearanceHovered.Font")));
            this.simpleButton3.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton3.AppearanceHovered.ForeColor")));
            this.simpleButton3.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton3.AppearanceHovered.Options.UseFont = true;
            this.simpleButton3.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton3.AppearanceHovered.Options.UseTextOptions = true;
            this.simpleButton3.AppearanceHovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.simpleButton3.AppearanceHovered.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            resources.ApplyResources(this.simpleButton3, "simpleButton3");
            this.simpleButton3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton3.Image = global::cevircagirserverdeneme.Properties.Resources.icons8_Services_25;
            this.simpleButton3.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton3.ImageToTextIndent = 5;
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click_1);
            // 
            // simpleButton2
            // 
            this.simpleButton2.AllowFocus = false;
            this.simpleButton2.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton2.Appearance.BackColor")));
            this.simpleButton2.Appearance.BorderColor = ((System.Drawing.Color)(resources.GetObject("simpleButton2.Appearance.BorderColor")));
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseBorderColor = true;
            this.simpleButton2.Appearance.Options.UseTextOptions = true;
            this.simpleButton2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.simpleButton2.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton2.AppearanceHovered.BackColor")));
            this.simpleButton2.AppearanceHovered.Options.UseBackColor = true;
            resources.ApplyResources(this.simpleButton2, "simpleButton2");
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton2.Image = global::cevircagirserverdeneme.Properties.Resources.icons8_Waiter_Filled_30;
            this.simpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click);
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("xtraTabControl1.Appearance.BackColor")));
            this.xtraTabControl1.Appearance.BackColor2 = ((System.Drawing.Color)(resources.GetObject("xtraTabControl1.Appearance.BackColor2")));
            this.xtraTabControl1.Appearance.BorderColor = ((System.Drawing.Color)(resources.GetObject("xtraTabControl1.Appearance.BorderColor")));
            this.xtraTabControl1.Appearance.Options.UseBackColor = true;
            this.xtraTabControl1.Appearance.Options.UseBorderColor = true;
            this.xtraTabControl1.Appearance.Options.UseTextOptions = true;
            this.xtraTabControl1.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.xtraTabControl1.AppearancePage.Header.Options.UseTextOptions = true;
            this.xtraTabControl1.AppearancePage.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            resources.ApplyResources(this.xtraTabControl1, "xtraTabControl1");
            this.xtraTabControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tabadmin;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabadmin});
            this.xtraTabControl1.CloseButtonClick += new System.EventHandler(this.xtraTabControl1_CloseButtonClick);
            // 
            // tabadmin
            // 
            resources.ApplyResources(this.tabadmin, "tabadmin");
            this.tabadmin.Name = "tabadmin";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 1000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // frmadmin
            // 
            this.AccessibleRole = System.Windows.Forms.AccessibleRole.ScrollBar;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmadmin";
            this.Load += new System.EventHandler(this.frmadmin_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.SimpleButton btnmasaraporlar;
        private DevExpress.XtraEditors.SimpleButton btnadmin;
        private DevExpress.XtraEditors.SimpleButton btntanimlar;
        private DevExpress.XtraEditors.SimpleButton btncihazekle;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraTab.XtraTabPage tabadmin;
        public System.Windows.Forms.Timer timer2;
        public System.Windows.Forms.Timer timer1;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton4;
    }
}

