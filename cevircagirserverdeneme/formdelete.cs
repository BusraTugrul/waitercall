﻿using System;
using System.Windows.Forms;

namespace cevircagirserverdeneme
{
    public partial class formdelete : DevExpress.XtraEditors.XtraForm
    {
        public TextBox txtdlt;
        public DevExpress.XtraEditors.SimpleButton btn;
        public DevExpress.XtraEditors.SimpleButton btnh;
        public formdelete()
        {
            btn = btnevet;
            btnh = btnhyr;
            InitializeComponent();
            txtdlt = txtmsj;
        }

        private void btnhyr_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
        }

        private void formdelete_Load(object sender, EventArgs e)
        {
            btnevet.Text = Localization.btnYes;
            btnhyr.Text = Localization.btnNo;
            txtmsj.Enabled = false;
        }

        public void btnevet_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Yes;
        }
    }
}
