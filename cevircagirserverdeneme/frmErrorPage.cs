﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cevircagirserverdeneme
{
    public partial class frmErrorPage : Form
    {
        private static readonly frmErrorPage singletonfep = new frmErrorPage();
        public bool pageopen = false, pagefirst = true;

        static frmErrorPage()
        {

        }
        private frmErrorPage()
        {
            InitializeComponent();
        }
        public static frmErrorPage singletonep
        {
            get
            {
                return singletonfep;
            }
        }

        private void frmErrorPage_Load(object sender, EventArgs e)
        {
            Screen screen = Screen.FromControl(this);
            int x = screen.WorkingArea.X - screen.Bounds.X;
            int y = screen.WorkingArea.Y - screen.Bounds.Y;
            this.MaximizedBounds = new Rectangle(x, y,
                screen.WorkingArea.Width, screen.WorkingArea.Height);
            this.MaximumSize = screen.WorkingArea.Size;
            this.WindowState = FormWindowState.Maximized;
            pageopen = true;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            pageopen = false;
            pagefirst = false;
            this.Hide();
        }
    }
}
