﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cevircagirserverdeneme
{
    class WhenDataComes
    {
        static string conString = @" Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\cevircagir.mdf';Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        //"Server=.;Database=cevircagir;Uid=sa;Password=bhappy_5;";
        SqlConnection baglanti;
        //TcpIpServer tiswdc;
        String[] split2, split1;
        Thread serialread;
        int statenummasa, statenumgarson;
        SqlCommand komut1,komut2;
        DateTime dt1, dt2;
        int table_id;
        string tableLocalization = Localization.labelControl4;
        TimeSpan sonuc;
        int sn;
        int tab_id = 0;
        string chargeddeviceid = "";
        public string table_name = "";
        formCagrilar frmcagrilarwdc = formCagrilar.singletonfrmcagrilar;   //form çağrılardan singleton instance alınıyor
        formbattery frmbat = formbattery.singletonfrmbattery;
        formCagrilarMonitor frmcagrilarmonitorwdc;
        FormCihazKonfigurasyon frmconfwdc;
        frmTableName frmtname;
        SqlTransaction myTransaction = null,myTransaction2=null;
        public Color colfrm;
        public List<int> tables = new List<int>();
        public WhenDataComes()
        {
            //tiswdc = new TcpIpServer();
            baglanti = new SqlConnection(conString);
            frmtname = frmTableName.singletonfrm;
            //tiswdc.ocd = "$0#";

        }
        public Color colwdc
        {
            get
            {
                return colfrm;
            }
            set
            {
                colfrm = value;
            }
        }
        public void GetData(String data)
        {
            //String a = data;   //clienttan gelen veri
            ////Debug.WriteLine(a);
            //if (a != null)
            //{
            //   // Debug.WriteLine("............"+a+".............");
            //    if (a.Contains("-"))
            //    {
            //        split2 = a.Split('-');
            //        if (split2.Length == 6)
            //        {
            //            if (split2[1] == "1" || split2[1] == "2")    //gelen sinyal : garson çağırma state=2, garson geldi state=1
            //            {
            //                serialread = new Thread(new ParameterizedThreadStart(serialreadthread));
            //                serialread.Start(a);
            //            }
            //        }
            //    }
            //}

            //Console.WriteLine("when data comes");
            ////veri formatı kanal:devid:komut:değer
            String a = data;   //clienttan gelen veri    
            //Console.WriteLine(data);
            if (a != null)
            {
                if (a.Contains(":"))
                {
                    split2 = a.Split(':');
                    if (split2.Length == 3)
                    {
                        if (Convert.ToInt32(split2[1]) == 3 && (Convert.ToInt32(split2[2]) == 2 || Convert.ToInt32(split2[2]) == 1))    //gelen sinyal : garson çağırma state=2, garson geldi state=1
                        {                            
                            serialread = new Thread(new ParameterizedThreadStart(serialreadthread));
                            serialread.Start(a);
                        }
                        if (Convert.ToInt32(split2[1]) == 1)    //batarya durumları için
                        {
                            //serialread = new Thread(new ParameterizedThreadStart(batterythread));
                            //serialread.Start(a);
                        }
                        if(Convert.ToInt32(split2[1]) == 4 && Convert.ToInt32(split2[2]) == 3)   //şarja konuldu bilgisi gelirse
                        {
                            //Console.WriteLine("cihaz şarja konuyor", Convert.ToInt32(split1[0]));
                            serialread = new Thread(new ParameterizedThreadStart(batterythread));
                            serialread.Start(a);
                        }
                    }
                }
                if (a.Contains("sendfail"))
                {
                    Console.WriteLine("hata geldi...");
                }
            }
            else
                Console.WriteLine("deger nulll");
        }
        public void batterythread(object v)
        {
            string veri = v.ToString();
            String[] splitarr = null;
            splitarr = veri.Split(':');
            chargeddeviceid = splitarr[0].ToString();
            clearAllReds(2, "0");
            //string bat = splitarr[2];
            //frmbat.addLabel(macid, bat);
        }

        
        public void serialreadthread(object v)
        {
            //////////   +IPD;0,9:1a:fe:34:a4:35:42-0-0-00-00-00    linkid,kr sayısı,macid,state,pil durumu, counter, sayı,sayı   ///////////
            lock (baglanti)
            {
                string veri = v.ToString();
                split1 = null;
                split1 = veri.Split(':');
                string masamac_id = "--";
                string macid = split1[0];  //macid alır
                string state = split1[2];
                //Console.WriteLine("macid"+macid+state);
                if (baglanti.State != ConnectionState.Open)
                    baglanti.Open();
                string conf = "select * from tables where mac_address=@mac_id";
                SqlCommand sconf = new SqlCommand(conf, baglanti);
                sconf.Parameters.AddWithValue("@mac_id", macid);
                SqlDataReader dr = sconf.ExecuteReader();
                if (!dr.Read() && masamac_id == "--")
                {
                    //Console.WriteLine("veri var mı"+dr.Read());
                    if (baglanti.State == ConnectionState.Open)
                    {
                        baglanti.Close();
                    }
                    //////// buraya kayıt sayfası başka cihaz için açılmış mı kontrolü ekle
                    if (!frmtname.isalive)
                    {
                        masamac_id = macid;
                        Thread masakayit = new Thread(new ParameterizedThreadStart(masailkkayit));
                        masakayit.Start(split1);
                    }
                }
                else
                {
                    if (baglanti.State == ConnectionState.Open)
                    {
                        baglanti.Close();
                    }
                    //if (split1.Length == 6)
                    //{
                    if (Convert.ToInt32(split1[2]) == 2 || Convert.ToInt32(split1[2]) == 1)
                    {
                        kayitYaz(split1);
                        //if (info == 5)    //uyandır butonuna basıldıktan sonra veri geldiyse cihazlar uyanıyor demektir. adminsettingsdeki bilgilendirme text visible false olsun.
                        //{
                        //    frmadminset.tinfo.Visible = false;
                        //}
                    }
                    //}
                }
                if (baglanti.State == ConnectionState.Open)
                {
                    baglanti.Close();
                }
                // }
            }//lock
        }//serialreadthread

        public void kayitYaz(object split)
        {
            //Debug.WriteLine("kayıt yaz fonksiyonu");
            object[] res = split as object[];
            //string mac_id = res[0].ToString().Substring(10);
            string mac_id = res[0].ToString();
            string masa_ad_x = "";
            //Debug.WriteLine("........"+mac_id);
            using (SqlCommand masabul = new SqlCommand("select table_name from tables where mac_address=@mac", baglanti))
            {
                masabul.Parameters.AddWithValue("@mac", mac_id);
                baglanti.Open();
                using (SqlDataReader dread = masabul.ExecuteReader())   ////////////reader bağlantıyı bloke eder.
                {
                    if (dread.Read())
                    {
                        masa_ad_x = dread["table_name"].ToString();
                    }
                    else
                        masa_ad_x = "";
                    dread.Close();
                    baglanti.Close();
                }
                //Debug.WriteLine("xxxxx" + masa_ad_x);
                if (masa_ad_x != "")
                {
                    int tablecount;
                    SqlCommand tableok = new SqlCommand("select count(id) from reports where table_name=@name and statenum=2", baglanti);
                    tableok.Parameters.AddWithValue("@name", masa_ad_x);
                    //Debug.WriteLine("-------"+masa_ad_x);
                    baglanti.Open();
                    tablecount = Convert.ToInt32(tableok.ExecuteScalar());
                    string state_x = res[2].ToString();
                    string saat_x = DateTime.Now.ToString("HH:mm:ss");
                   
                    if (Convert.ToInt32(state_x) == 2 && tablecount == 0)   //garson çağrılmışsa ve sadece ilk çağırma sinyali ise
                    {
                        komut1 = baglanti.CreateCommand();
                        myTransaction = baglanti.BeginTransaction();
                        komut1.Connection = baglanti;
                        komut1.Transaction = myTransaction;
                        //leds = 900;
                        //tis.ocd = "$0#";
                        string masaismi = null;
                        if (masaismi != masa_ad_x)
                        {
                            masaismi = masa_ad_x;
                            //if (xtraTabControl1.SelectedTabPage.Name != "newPageCagrilar")    //masaların yandığı ekran kapalı ise uyarı ver
                            //{
                            //    System.Media.SystemSounds.Beep.Play();  //bip sesi için
                            //}
                        }
                        string todayday = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");  //HH-->24 saatlik format  hh-->12 saatlik format
                        statenummasa = Convert.ToInt32(state_x);
                        if (Settings1.Default.ringnotification)
                        {
                            System.Media.SoundPlayer player = new System.Media.SoundPlayer(Properties.Resources.bell);
                            player.Play();
                        }
                        //komut1.CommandType = CommandType.StoredProcedure;
                        komut1.CommandText="insert into reports(table_id,table_name,req_time,statenum,req_date) select t.table_id ,@mad,@saat_x,@statenummasa,@todayday from tables t where t.table_name = '" + masa_ad_x + "'";
                        komut1.Parameters.AddWithValue("@mad", masa_ad_x);
                        komut1.Parameters.AddWithValue("@saat_x", saat_x);
                        komut1.Parameters.AddWithValue("@statenummasa", 2);
                        komut1.Parameters.AddWithValue("@todayday", todayday);
                        komut1.ExecuteNonQuery();
                        komut1.CommandText="update reports set waiter_name=(select waiter_name from waitersstables where table_name='" + masa_ad_x + "'),waiter_id=(select waiter_id from waitersstables where table_name='" + masa_ad_x + "') where table_name='" + masa_ad_x + "' and statenum=2";
                        komut1.ExecuteNonQuery();
                        myTransaction.Commit();
                        //Debug.WriteLine("Veri tabanı düzenlendi");
                       
                    }
                    if (Convert.ToInt32(state_x) == 1 && tablecount != 0)  //garson işlem görmüşse (yalnızca kırmızı moddan mum moduna dönmüşse yani garson cevap vermişse)
                    {
                        statenumgarson = 1;
                        string kayit = "SELECT req_time from reports where table_name=@masaad and statenum=2 order by id asc";
                        SqlCommand komut = new SqlCommand(kayit, baglanti);
                        komut.Parameters.AddWithValue("@masaad", masa_ad_x);
                        SqlDataAdapter da = new SqlDataAdapter(komut);
                        using (SqlDataReader dr = komut.ExecuteReader())  //select sorgusundan gelen verileri döndürür.
                        {
                            if (dr.Read())//satır varsa
                            {
                                dt1 = Convert.ToDateTime(dr["req_time"].ToString());
                                dt2 = Convert.ToDateTime(saat_x);
                                if (dt2 >= dt1)
                                {
                                    sonuc = dt2 - dt1;
                                    sn = sonuc.Hours * 3600 + sonuc.Minutes * 60 + sonuc.Seconds;
                                }
                                else //eğer ertesi güne geçilmişse
                                {
                                    sonuc = dt1 - dt2;
                                    sn = sonuc.Hours * 3600 + sonuc.Minutes * 60 + sonuc.Seconds;
                                    sn = 86400 - sn;
                                }
                            }
                            dr.Close();
                        }
                        komut2 = baglanti.CreateCommand();
                        myTransaction2 = baglanti.BeginTransaction();
                        komut2.Connection = baglanti;
                        komut2.Transaction = myTransaction2;
                        komut2.CommandText="update reports set reply_time='" + saat_x + "',reply_duration=" + sn + ",statenum=" + statenumgarson + " where table_name='" + masa_ad_x + "' and statenum=2";
                        komut2.ExecuteNonQuery();
                        myTransaction2.Commit();
                    }
                    
                    if ((Convert.ToInt32(state_x) == 2 && tablecount == 0) || (Convert.ToInt32(state_x) == 1 && tablecount != 0))   //  ilk çağırma sinyali için
                    {
                        SqlCommand refreshgrid = new SqlCommand("SELECT table_name,req_time,waiter_name from reports where statenum=@snum order by id asc", baglanti);
                        refreshgrid.Parameters.AddWithValue("@snum", 2);
                        SqlDataAdapter dataad = new SqlDataAdapter(refreshgrid);
                        DataSet refresh = new DataSet();
                        dataad.Fill(refresh);
                        SqlCommand refreshgridmon = new SqlCommand("SELECT table_id,req_time,waiter_name from reports where statenum=@snum order by id asc", baglanti);
                        refreshgridmon.Parameters.AddWithValue("@snum", 2);
                        SqlDataAdapter dataadmon = new SqlDataAdapter(refreshgridmon);
                        DataSet refreshmon = new DataSet();
                        dataadmon.Fill(refreshmon);
                        lock (frmcagrilarwdc.gridcagrilar)
                        {
                            frmcagrilarwdc.gridcagrilar.BeginUpdate();
                            try
                            {
                                frmcagrilarwdc.gridcagrilar.DataSource = null;
                                frmcagrilarwdc.gridcagrilar.DataSource = refresh.Tables[0];
                                frmcagrilarwdc.gridcagrilar.ViewCollection.Clear();
                                frmcagrilarwdc.gridcagrilar.ViewCollection.Add(frmcagrilarwdc.tvmainv);
                                frmcagrilarwdc.gridcagrilar.MainView = frmcagrilarwdc.tvmainv;
                                frmcagrilarwdc.gridcagrilar.RefreshDataSource();
                                //Debug.WriteLine("grid düzenlendi");
                            }
                            catch (Exception Ex2)
                            {
                                Debug.WriteLine(Ex2.Message);
                            }
                            finally
                            {
                                frmcagrilarwdc.gridcagrilar.EndUpdate();
                            }
                        }//lock
                        //foreach (Form form in Application.OpenForms)
                        //{
                        //    if (form.Name.Equals("formCagrilarMonitor"))
                        //    {
                        //        frmcagrilarmonitorwdc = formCagrilarMonitor.singletonfrmcagrilarm;
                        //        lock (frmcagrilarmonitorwdc.gridcagrilarmonitor)
                        //        {
                        //            frmcagrilarmonitorwdc.gridcagrilarmonitor.BeginUpdate();
                        //            try
                        //            {
                        //                frmcagrilarmonitorwdc.gridcagrilarmonitor.DataSource = null;
                        //                frmcagrilarmonitorwdc.gridcagrilarmonitor.DataSource = refreshmon.Tables[0];
                        //                frmcagrilarmonitorwdc.gridcagrilarmonitor.ViewCollection.Clear();
                        //                frmcagrilarmonitorwdc.gridcagrilarmonitor.ViewCollection.Add(frmcagrilarmonitorwdc.tvmainvmonitor);
                        //                frmcagrilarmonitorwdc.gridcagrilarmonitor.MainView = frmcagrilarmonitorwdc.tvmainvmonitor;
                        //                frmcagrilarmonitorwdc.gridcagrilarmonitor.RefreshDataSource();
                        //            }
                        //            catch (Exception Ex2)
                        //            {
                        //                Debug.WriteLine(Ex2.Message);
                        //            }
                        //            finally
                        //            {
                        //                frmcagrilarmonitorwdc.gridcagrilarmonitor.EndUpdate();
                        //            }
                        //        }//lock
                        //        break;
                        //    }
                        //}
                    }
                    if (baglanti.State != ConnectionState.Closed && baglanti != null)
                        baglanti.Close();
                }
            }
        }//kayitYaz

        public void clearAllReds(int command,string a)
        {
            baglanti.Open();
            if (command == 1)  //tüm ekranı temizle komutu 
            {
                SqlCommand loadcommand = new SqlCommand("select table_id from tables where mac_address like @mac", baglanti);
                loadcommand.Parameters.AddWithValue("@mac", a + "%");
                SqlDataAdapter da = new SqlDataAdapter(loadcommand);
                using (SqlDataReader dr = loadcommand.ExecuteReader())  //select sorgusundan gelen verileri döndürür.
                {
                    while (dr.Read())//satır varsa
                    {
                        tables.Add(Convert.ToInt32(dr["table_id"]));                     
                    }
                }
                if (tables.Count > 0)
                {
                    for (int i = 0; i < tables.Count; i++)
                    {
                        loadcommand = new SqlCommand("delete from reports where statenum = 2 and table_id = @tid", baglanti);
                        loadcommand.Parameters.AddWithValue("tid", tables[i]);
                        loadcommand.ExecuteNonQuery();
                    }
                }
                tables.Clear();
            }
            if(command == 2)
            {
                SqlCommand loadcommand = new SqlCommand("select table_id from tables where mac_address = @mac", baglanti);
                loadcommand.Parameters.AddWithValue("@mac",chargeddeviceid);
                SqlDataAdapter da = new SqlDataAdapter(loadcommand);
                using (SqlDataReader dr = loadcommand.ExecuteReader())  //select sorgusundan gelen verileri döndürür.
                {
                    if (dr.Read())//satır varsa
                    {
                        tab_id = Convert.ToInt32(dr["table_id"]);
                    }
                }
                if (tab_id != 0)
                {
                    loadcommand = new SqlCommand("delete from reports where statenum = 2 and table_id = @tid", baglanti);
                    loadcommand.Parameters.AddWithValue("tid", tab_id);
                    loadcommand.ExecuteNonQuery();
                    tab_id = 0;
                }
            }
            SqlCommand refreshgrid = new SqlCommand("SELECT table_name,req_time,waiter_name from reports where statenum=@snum order by id asc", baglanti);
            refreshgrid.Parameters.AddWithValue("@snum", 2);
            SqlDataAdapter dataad = new SqlDataAdapter(refreshgrid);
            DataSet refresh = new DataSet();
            dataad.Fill(refresh);
            SqlCommand refreshgridmon = new SqlCommand("SELECT table_id,req_time,waiter_name from reports where statenum=@snum order by id asc", baglanti);
            refreshgridmon.Parameters.AddWithValue("@snum", 2);
            SqlDataAdapter dataadmon = new SqlDataAdapter(refreshgridmon);
            DataSet refreshmon = new DataSet();
            dataadmon.Fill(refreshmon);
            lock (frmcagrilarwdc.gridcagrilar)
            {
                frmcagrilarwdc.gridcagrilar.BeginUpdate();
                try
                {
                    frmcagrilarwdc.gridcagrilar.DataSource = null;
                    frmcagrilarwdc.gridcagrilar.DataSource = refresh.Tables[0];
                    frmcagrilarwdc.gridcagrilar.ViewCollection.Clear();
                    frmcagrilarwdc.gridcagrilar.ViewCollection.Add(frmcagrilarwdc.tvmainv);
                    frmcagrilarwdc.gridcagrilar.MainView = frmcagrilarwdc.tvmainv;
                    frmcagrilarwdc.gridcagrilar.RefreshDataSource();
                    //Debug.WriteLine("grid düzenlendi");
                }
                catch (Exception Ex2)
                {
                    Debug.WriteLine(Ex2.Message);
                }
                finally
                {
                    frmcagrilarwdc.gridcagrilar.EndUpdate();
                }
            }//lock
            //foreach (Form form in Application.OpenForms)
            //{
            //    if (form.Name.Equals("formCagrilarMonitor"))
            //    {
            //        frmcagrilarmonitorwdc = formCagrilarMonitor.singletonfrmcagrilarm;
            //        lock (frmcagrilarmonitorwdc.gridcagrilarmonitor)
            //        {
            //            frmcagrilarmonitorwdc.gridcagrilarmonitor.BeginUpdate();
            //            try
            //            {
            //                frmcagrilarmonitorwdc.gridcagrilarmonitor.DataSource = null;
            //                frmcagrilarmonitorwdc.gridcagrilarmonitor.DataSource = refreshmon.Tables[0];
            //                frmcagrilarmonitorwdc.gridcagrilarmonitor.ViewCollection.Clear();
            //                frmcagrilarmonitorwdc.gridcagrilarmonitor.ViewCollection.Add(frmcagrilarmonitorwdc.tvmainvmonitor);
            //                frmcagrilarmonitorwdc.gridcagrilarmonitor.MainView = frmcagrilarmonitorwdc.tvmainvmonitor;
            //                frmcagrilarmonitorwdc.gridcagrilarmonitor.RefreshDataSource();
            //            }
            //            catch (Exception Ex2)
            //            {
            //                Debug.WriteLine(Ex2.Message);
            //            }
            //            finally
            //            {
            //                frmcagrilarmonitorwdc.gridcagrilarmonitor.EndUpdate();
            //            }
            //        }//lock
            //        break;
            //    }
            //}
        
        if (baglanti.State != ConnectionState.Closed && baglanti != null)
            baglanti.Close();
        }
        
        public void masailkkayit(object obj)
        {
            //frmconfwdc = FormCihazKonfigurasyon.singletonfrmdevconf;
            //frmconfwdc.masaekle(obj.ToString());
            if (!frmtname.isalive)
            {
                frmtname.ShowDialog();
                table_name = frmtname.tname;
            }
            if (table_name != "" && table_name != null)    //yeni açılan pencereden isim girilmişse
            {
                object[] res = obj as object[];
                //string mac_id = res[0].ToString().Substring(10);
                string mac_id = res[0].ToString();
                if (baglanti.State != ConnectionState.Open)
                    baglanti.Open();
                string idal = "SELECT TOP 1 table_id FROM tables ORDER BY table_id desc";
                SqlCommand command = new SqlCommand(idal, baglanti);
                SqlDataReader sread = command.ExecuteReader();
                if (sread.Read())
                {
                    table_id = Convert.ToInt32(sread["table_id"].ToString());
                    table_id += 1;
                }
                else
                {
                    table_id = 1;
                }
                //Console.WriteLine("table id"+table_id);
                sread.Close();
                SqlCommand scmasaekle = new SqlCommand("insert into tables(table_id,table_name,mac_address,isselected,isactive) values(@t_id,@t_name,@m_id,@s,@a)", baglanti);
                scmasaekle.Parameters.AddWithValue("@t_id", table_id);
                scmasaekle.Parameters.AddWithValue("@t_name", table_name);   // tableLocalization + " " + table_id
                scmasaekle.Parameters.AddWithValue("@m_id", mac_id);
                scmasaekle.Parameters.AddWithValue("@s", 0);
                scmasaekle.Parameters.AddWithValue("@a", 1);
                scmasaekle.ExecuteNonQuery();
                baglanti.Close();
                table_name = "";
                frmtname.tname = "";
                kayitYaz(obj);
            }
            else
            {
                //////// hata formu yaz kaydedilemedi desin
            }
            
        }
    }
}
