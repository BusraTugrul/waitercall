﻿using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Tile;
using DevExpress.XtraGrid.Views.Tile.ViewInfo;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace cevircagirserverdeneme
{
    public partial class FormCihazKonfigurasyon : Form
    {
        static string conString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\cevircagir.mdf';Integrated Security = True; Connect Timeout = 30";
        SqlConnection baglanti;
        SqlCommand command;
        int table_id;
        ArrayList madaktif = new ArrayList();
        ArrayList madpasif = new ArrayList();
        int sayac = 0;
        string aktif="", pasif="";
        formdelete frmdlt;
        frmTableName frmtname;
        string tableLocalization=Localization.labelControl4;
        string switchActive, active, passive;
        string table_name = "";
        public static FormCihazKonfigurasyon singletonfrmconf = new FormCihazKonfigurasyon();

        private FormCihazKonfigurasyon()
        {
            InitializeComponent();
            baglanti = new SqlConnection(conString);
            clisactive.Visible = false;
            
        }
        public static FormCihazKonfigurasyon singletonfrmdevconf
        {
            get
            {
                return singletonfrmconf;
            }
        }
        private void FormCihazKonfigürasyon_Load(object sender, EventArgs e)
        {
            label3.Text = Localization.label3;
            label5.Text = Localization.label5;
            label2.Text = Localization.label2;
            label4.Text = Localization.label4;
            btntemizle.Text = Localization.btntemizle;
            btnkaydet.Text = Localization.btnKaydet;
            switchActive = Localization.switchActive;
            active = Localization.active;
            passive = Localization.passive;
            gridrefresh();
        }
        public void masaekle(string mac_id)
        {
            baglanti.Open();
            string idal = "SELECT TOP 1 table_id FROM tables ORDER BY table_id desc";
            SqlCommand command = new SqlCommand(idal, baglanti);
            SqlDataReader sread = command.ExecuteReader();
            if (sread.Read())
            {
                table_id = Convert.ToInt32(sread["table_id"].ToString());
                table_id += 1;
            }
            else
            {
                table_id = 1;
            }
            sread.Close();
            SqlCommand scmasaekle = new SqlCommand("insert into tables(table_id,table_name,mac_address,isselected,isactive) values(@t_id,@t_name,@m_id,@s,@a)", baglanti);
            scmasaekle.Parameters.AddWithValue("@t_id", table_id);
            scmasaekle.Parameters.AddWithValue("@t_name", tableLocalization + " " + table_id);
            scmasaekle.Parameters.AddWithValue("@m_id", mac_id);
            scmasaekle.Parameters.AddWithValue("@s", 0);
            scmasaekle.Parameters.AddWithValue("@a", 1);
            scmasaekle.ExecuteNonQuery();
            baglanti.Close();
            griddoldur();
        }
        public void griddoldur()
        {
            baglanti.Open();
            string fillgrid = "select table_name,isactive from tables";
            command = new SqlCommand(fillgrid, baglanti);
            command.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gridControl1.BeginInit();
            try
            {
                gridControl1.DataSource = null;
                gridControl1.DataSource = ds.Tables[0];
                gridControl1.ViewCollection.Clear();
                gridControl1.ViewCollection.Add(tileView1);
                gridControl1.MainView = tileView1;
                gridControl1.RefreshDataSource();
            }
            catch (Exception Ex2)
            {
                MessageBox.Show(Ex2.Message);
            }
            finally
            {
                try
                {
                    gridControl1.EndInit();
                }
                catch (Exception Ex)
                {
                    MessageBox.Show(Ex.Message);
                }
            }

            baglanti.Close();
        }
        
        private void tileView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                TileView view = sender as TileView;
                TileViewHitInfo hitInfo = view.CalcHitInfo(e.Location);
                if (hitInfo.InItem)
                {
                    frmtname = frmTableName.singletonfrm;
                    if (!frmtname.isalive)
                    {
                        frmtname.ShowDialog();
                        table_name = frmtname.tname;
                    }
                    if (table_name != "" && table_name != null)
                    {
                        if (baglanti.State != ConnectionState.Open)
                            baglanti.Open();
                        SqlCommand commaktif = new SqlCommand("update tables set table_name=@tname where table_name=@name", baglanti);   //önceden pasif olan
                        commaktif.Parameters.AddWithValue("@tname", table_name);
                        commaktif.Parameters.AddWithValue("@name", tileView1.GetRowCellDisplayText(hitInfo.RowHandle, cltable_id1));
                        //commaktif.Parameters.AddWithValue("@id", pas_id);
                        commaktif.ExecuteNonQuery();
                        baglanti.Close();
                        griddoldur();
                    }
                }
                table_name = "";
                frmtname.tname = "";
            }
           

            //if (e.Button == System.Windows.Forms.MouseButtons.Left)
            //{
            //    if (sayac != 2)
            //    {
            //        TileView view = sender as TileView;
            //        TileViewHitInfo hitInfo = view.CalcHitInfo(e.Location);
            //        if (hitInfo.InItem)
            //        {
            //            if (hitInfo.Item.Elements[0].Appearance.Normal.BackColor == Color.DimGray)
            //            {
            //                int val = 1;
            //                madaktif.Add(tileView1.GetRowCellDisplayText(hitInfo.RowHandle, cltable_id1));
            //                view.SetRowCellValue(hitInfo.RowHandle, clisactive, val);
            //                //griddoldur();
            //                aktif = madaktif[0].ToString();
            //            }
            //            else if (hitInfo.Item.Elements[0].Appearance.Normal.BackColor == Color.Orange)
            //            {
            //                int val = 0;
            //                madpasif.Add(tileView1.GetRowCellDisplayText(hitInfo.RowHandle, cltable_id1));
            //                view.SetRowCellValue(hitInfo.RowHandle, clisactive, val);
            //                //griddoldur();
            //                pasif = madpasif[0].ToString();
            //            }
            //        }
            //    }
            //    if (sayac == 2)
            //    {
            //        aktif = "";
            //        pasif = "";
            //        madaktif.Clear();
            //        madpasif.Clear();
            //        griddoldur();
            //        sayac = -1;
            //    }
            //    sayac++;
            //}
        }
        public void gridrefresh()
        {
            tileView1.Focus();
            tileView1.SortInfo.ClearAndAddRange(new GridColumnSortInfo[] { new GridColumnSortInfo(tileView1.Columns["isactive"], DevExpress.Data.ColumnSortOrder.Ascending) }, 2);
            griddoldur();
        }
        private void btnkaydet_Click(object sender, EventArgs e)
        {
            frmdlt = new formdelete();
            if (aktif != "" && pasif != "")   //eğer karşılıklı seçim yapılmışsa kaydetmesi için 
            {
                //aktif olanın adını pasif olanla değiştir
                //insert into waitersstables(waiter_id,waiter_name,table_id,table_name) select @mid1,@mid2, @tableid,t.table_name from tables t where t.table_id=@tableid
                frmdlt.txtdlt.Text = String.Format(switchActive, aktif, pasif);
                frmdlt.ShowDialog();
                if (frmdlt.DialogResult == DialogResult.Yes)
                {
                    baglanti.Open();

                    int pas_id = 0, ac_id = 0;
                    string pas_name = "", ac_name = "";

                    string kayit = "SELECT id, table_id, table_name,mac_address from tables where table_id=@tid";  //önceden pasif olanın bilgilerini al
                    SqlCommand komut = new SqlCommand(kayit, baglanti);
                    komut.Parameters.AddWithValue("@tid", Convert.ToInt32(aktif));
                    SqlDataAdapter da = new SqlDataAdapter(komut);
                    using (SqlDataReader dr = komut.ExecuteReader())  //select sorgusundan gelen verileri döndürür.
                    {
                        if (dr.Read())//satır varsa
                        {
                            pas_id = Convert.ToInt32(dr["id"]);
                            pas_name = dr["table_name"].ToString();
                            //Console.WriteLine("önceden pasif olan " + dr["id"] + " " + dr["table_id"] + " " + dr["table_name"] + " " + dr["mac_address"]);
                        }
                    }

                    kayit = "SELECT id, table_id, table_name,mac_address from tables where table_id=@tid";  //önceden aktif olanın bilgilerini al
                    komut = new SqlCommand(kayit, baglanti);
                    komut.Parameters.AddWithValue("@tid", Convert.ToInt32(pasif));
                    da = new SqlDataAdapter(komut);
                    using (SqlDataReader dr = komut.ExecuteReader())  //select sorgusundan gelen verileri döndürür.
                    {
                        if (dr.Read())//satır varsa
                        {
                            ac_id = Convert.ToInt32(dr["id"]);
                            ac_name = dr["table_name"].ToString();
                            //Console.WriteLine("önceden aktif olan " + dr["id"] + " " + dr["table_id"] + " " + dr["table_name"] + " " + dr["mac_address"]);
                        }
                    }

                    SqlCommand commaktif = new SqlCommand("update tables set table_id=@tid,table_name=@tname where id=@id", baglanti);   //önceden pasif olan
                    commaktif.Parameters.AddWithValue("@tid", Convert.ToInt32(pasif));
                    commaktif.Parameters.AddWithValue("@tname", ac_name);
                    commaktif.Parameters.AddWithValue("@id", pas_id);
                    commaktif.ExecuteNonQuery();
                    SqlCommand commpasif = new SqlCommand("update tables set table_id=@tid,table_name=@tname where id=@id", baglanti);   //önceden aktif olan
                    commpasif.Parameters.AddWithValue("@tid", Convert.ToInt32(aktif));
                    commpasif.Parameters.AddWithValue("@tname", pas_name);
                    commpasif.Parameters.AddWithValue("@id", ac_id);
                    commpasif.ExecuteNonQuery();
                    baglanti.Close();
                    griddoldur();
                    frmdlt.Close();
                }
                if (frmdlt.DialogResult == DialogResult.No)
                {
                    griddoldur();
                    frmdlt.Close();
                }
            }
            else if (aktif != "" && pasif == "")
            {
                frmdlt.txtdlt.Text = "\r\n" + String.Format(active, aktif);
                frmdlt.ShowDialog();
                if (frmdlt.DialogResult == DialogResult.Yes)
                {
                    baglanti.Open();
                    SqlCommand commaktif = new SqlCommand("update tables set isactive=1 where table_id=@tid", baglanti);
                    commaktif.Parameters.AddWithValue("@tid", Convert.ToInt32(aktif));
                    commaktif.ExecuteNonQuery();
                    baglanti.Close();
                    griddoldur();
                    frmdlt.Close();
                }
                if (frmdlt.DialogResult == DialogResult.No)
                {
                    griddoldur();
                    frmdlt.Close();
                }
            }
            else if (aktif == "" && pasif != "")
            {
                frmdlt.txtdlt.Text = "\r\n" + String.Format(passive, pasif);
                frmdlt.ShowDialog();
                if (frmdlt.DialogResult == DialogResult.Yes)
                {
                    baglanti.Open();
                    SqlCommand commaktif = new SqlCommand("update tables set isactive=0 where table_id=@tid", baglanti);
                    commaktif.Parameters.AddWithValue("@tid", Convert.ToInt32(pasif));
                    commaktif.ExecuteNonQuery();
                    baglanti.Close();
                    griddoldur();
                    frmdlt.Close();
                }
                if (frmdlt.DialogResult == DialogResult.No)
                {
                    griddoldur();
                    frmdlt.Close();
                }
            }


            //frmdlt = new formdelete();
            //if (aktif != "" && pasif != "")   //eğer karşılıklı seçim yapılmışsa kaydetmesi için 
            //{
            //    frmdlt.txtdlt.Text = String.Format(switchActive,aktif,pasif);
            //    frmdlt.ShowDialog();
            //    if (frmdlt.DialogResult == DialogResult.Yes)
            //    {
            //        baglanti.Open();
            //        SqlCommand commaktif = new SqlCommand("update tables set isactive=1 where table_id=@tid", baglanti);
            //        commaktif.Parameters.AddWithValue("@tid", Convert.ToInt32(aktif));
            //        commaktif.ExecuteNonQuery();
            //        SqlCommand commpasif = new SqlCommand("update tables set isactive=0 where table_id=@tid", baglanti);
            //        commpasif.Parameters.AddWithValue("@tid", Convert.ToInt32(pasif));
            //        commpasif.ExecuteNonQuery();
            //        baglanti.Close();
            //        griddoldur();
            //        frmdlt.Close();
            //    }
            //    if (frmdlt.DialogResult == DialogResult.No)
            //    {
            //        griddoldur();
            //        frmdlt.Close();
            //    }
            //}
            //else if(aktif != "" && pasif == "")
            //{
            //    frmdlt.txtdlt.Text = "\r\n"+String.Format(active,aktif);
            //    frmdlt.ShowDialog();
            //    if (frmdlt.DialogResult == DialogResult.Yes)
            //    {
            //        baglanti.Open();
            //        SqlCommand commaktif = new SqlCommand("update tables set isactive=1 where table_id=@tid", baglanti);
            //        commaktif.Parameters.AddWithValue("@tid", Convert.ToInt32(aktif));
            //        commaktif.ExecuteNonQuery();
            //        baglanti.Close();
            //        griddoldur();
            //        frmdlt.Close();
            //    }
            //    if (frmdlt.DialogResult == DialogResult.No)
            //    {
            //        griddoldur();
            //        frmdlt.Close();
            //    }
            //}
            //else if (aktif == "" && pasif != "")
            //{
            //    frmdlt.txtdlt.Text = "\r\n" +String.Format(passive,pasif);
            //    frmdlt.ShowDialog();
            //    if (frmdlt.DialogResult == DialogResult.Yes)
            //    {
            //        baglanti.Open();
            //        SqlCommand commaktif = new SqlCommand("update tables set isactive=0 where table_id=@tid", baglanti);
            //        commaktif.Parameters.AddWithValue("@tid", Convert.ToInt32(pasif));
            //        commaktif.ExecuteNonQuery();
            //        baglanti.Close();
            //        griddoldur();
            //        frmdlt.Close();
            //    }
            //    if (frmdlt.DialogResult == DialogResult.No)
            //    {
            //        griddoldur();
            //        frmdlt.Close();
            //    }
            //}
        }

        private void tileView1_ItemClick(object sender, TileViewItemClickEventArgs e)
        {
        }

        private void btntemizle_Click(object sender, EventArgs e)
        {
            madaktif.Clear();
            madpasif.Clear();
            aktif = "";
            pasif = "";
            sayac = 0;
            griddoldur();
        }

        private void tileView1_ItemCustomize_1(object sender, TileViewItemCustomizeEventArgs e)
        {
            TileView view = sender as TileView;
            int val = Convert.ToInt32(view.GetRowCellValue(e.RowHandle, clisactive));
            if (val == 0 )
            {
                e.Item.Elements[0].Appearance.Normal.BackColor = Color.DimGray;
            }
            else if (val == 1 )
            {
                e.Item.Elements[0].Appearance.Normal.BackColor = Color.Orange;
            }
        }
    }
}
