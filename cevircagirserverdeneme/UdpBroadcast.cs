﻿/*
 UdpBroadcast sınıfı ağa frmManualNetworkSelection formu ile seçilen ip'yi 
 tanıtmak içindir.
 Bulunduğu ağın 255. adresinden yayın yapar.
 Cihazlara "$UDPSEND#pcip" ile ip gönderir.
 Cihazlardan "$UDPEND#" mesajı bekler.
 Ayarlarda girilen cihaz sayısı kadar "$UDPEND#" mesajı aldığında broadcast yayını bırakır ve tcp classı çağrılır.
 
 */


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using System.Timers;
using System.Net.NetworkInformation;
using NativeWifi;

namespace cevircagirserverdeneme
{
    class UdpBroadcast
    {
        public static int portudp;
        private static readonly UdpBroadcast singletonudp = new UdpBroadcast();
        private UdpClient udp;
        IAsyncResult ar_;
        public static string host;
        public static string pcip = "";
        IPHostEntry ip;
        Timer tmr;
        TcpIpServer tisadmin;
        int forsubnet = 0, subnetjump;
        string broadcastip = "";
        public int devicenum = 0;
        public int devcounter = 0;
        public List<String> lst = new List<String>();
        public string deneme;

        static UdpBroadcast()
        {

        }
        private UdpBroadcast()   //constructor
        {
            devicenum = Settings1.Default.devicenum;
            pcip = "";
            portudp = 8574;
            ar_ = null;
            //udp = new UdpClient(portudp);
            tmr = new Timer();
            tisadmin = TcpIpServer.singletontisobj;

        }
        public static UdpBroadcast singletonudpobject
        {
            get
            {
                return singletonudp;
            }
        }

        public void startUdpBroadcasting()
        {
            ar_ = null;
            udp = new UdpClient(portudp);
            devcounter = 0;
            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            if (adapters == null || adapters.Length < 1)
            {
            }
            else
            {
                foreach (NetworkInterface adapter in adapters)
                {
                    if (adapter.GetPhysicalAddress().ToString() == Settings1.Default.manualSelectedIP)
                    {
                        foreach (UnicastIPAddressInformation ip in adapter.GetIPProperties().UnicastAddresses)
                        {
                            if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                            {
                                pcip = ip.Address.ToString();
                                Settings1.Default.udptcpIP = pcip;
                                Settings1.Default.Save();
                                //Console.WriteLine(pcip + "  " + ip.IPv4Mask);
                                calculateBroadcast(pcip, ip.IPv4Mask.ToString()); //"192.168.1.1", "255.255.252.0"
                            }
                        }
                    }
                }
            }
            if (pcip != "")
            {
                tmr.Interval = 2000;
                tmr.Elapsed += onTimeout;
                tmr.Start();
                StartListening();
            }
        }

        public void calculateBroadcast(string ip, string subnetmask)
        {   
            forsubnet = Convert.ToInt16(ip.Split('.')[2]);
            subnetjump = 256 - Convert.ToInt16(subnetmask.ToString().Split('.')[2]);
            int grup = forsubnet / subnetjump;
            broadcastip = ip.Split('.')[0] + "." + ip.Split('.')[1] + "." + ((grup * subnetjump) + (subnetjump - 1)) + ".255";
            //Console.WriteLine(broadcastip);
        }
        public static string GetStringForSSID(Wlan.Dot11Ssid ssid)
        {
            return Encoding.ASCII.GetString(ssid.SSID, 0, (int)ssid.SSIDLength);
        }

        public string getipfrommac(string mac)
        {

            return null;
        }

        private void StartListening()
        {
            ar_ = udp.BeginReceive(Receive, new object());
        }
        private void Receive(IAsyncResult ar)
        {
            try
            {
                IPEndPoint ip = new IPEndPoint(IPAddress.Any, portudp);
                byte[] bytes = udp.EndReceive(ar, ref ip);
                string message = Encoding.ASCII.GetString(bytes);
                if (lst.Count() == 0 || !lst.Contains(ip.Address.ToString()))
                {
                    lst.Add(ip.Address.ToString());
                    if (message == "$UDPEND#")
                    {
                        devcounter++;
                        //Console.WriteLine("From {0} received: {1} ", ip.Address.ToString(), message);
                        //Console.WriteLine(devcounter + " " + Settings1.Default.devicenum);
                        if (devcounter == Settings1.Default.devicenum)
                        {
                            Stop();
                            //tmr.Stop();
                            tisadmin.starttcp();
                        }
                    }
                }
                StartListening();
            }
            catch(Exception ex)
            {
                //Console.WriteLine("udp end receive hata mesajı");
            }
        }
        public void Stop()
        {
            try
            {
                tmr.Stop();
                udp.Client.Close();
                udp.Close();
                udp = null;
                //Console.WriteLine("Stopped listening");
            }
            catch(Exception ex){
                //Console.WriteLine(ex.Message);
            }

        }
        public void Send(string message)
        {
            //IPEndPoint ip = new IPEndPoint(IPAddress.Broadcast, portudp);
            IPAddress broad = IPAddress.Parse(broadcastip); //pcip.Remove(pcip.LastIndexOf('.')) + ".255"
            IPEndPoint ip = new IPEndPoint(broad, portudp);
            byte[] bytes = Encoding.ASCII.GetBytes(message);
            //deneme = ip.Address + "***" + Settings1.Default.udptcpIP + "***" + Settings1.Default.manualSelectedIP ;
            try
            {
                udp.Send(bytes, bytes.Length, ip);
                //Console.WriteLine("Sent: {0} ",deneme +"****" +ip);
            }
            catch (Exception ex)
            {
                //Console.WriteLine("udp send metodu hata", ex.Message);
            }
        }
        public void onTimeout(object sender, EventArgs e)
        {
            if (devcounter == Settings1.Default.devicenum)
            {
                //Console.WriteLine("dur");
                Stop();
                //tmr.Stop();
                //tisadmin.starttcp();
            }
            Send("$UDPSEND#" + pcip);
        }
    }
}
