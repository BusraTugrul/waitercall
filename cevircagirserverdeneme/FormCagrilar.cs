﻿using DevExpress.XtraGrid.Views.Tile;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using DevExpress.XtraGrid.Views.Base;
using System.Collections;
using System.Diagnostics;
using DevExpress.XtraGrid.Views.Tile.ViewInfo;

namespace cevircagirserverdeneme
{
    public partial class formCagrilar : Form //bu form; ana form load olurken tab olarak gömülen masa çağrılar formudur.
    {
        public DevExpress.XtraGrid.GridControl gridcagrilar;
        public DevExpress.XtraGrid.Views.Tile.TileView tvmainv;
        static string conString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\cevircagir.mdf';Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        SqlConnection baglanti;
        SqlCommand loadcommand,deleteCommand;
        formadminsettings frmset;
        Color r1, r2, r3;
        int sure1, sure2, sure3;
        frmadmin fadmin;
        string val, dk_sn;
        public int forcoloring1, forcoloring2, forcoloring3;
        public ArrayList color = new ArrayList();
        public Label lbl;
        int[] sureler = { 10, 20, 30, 40, 60, 80, 120, 180, 240, 300 };
        Image immorc, imyesilc, imkirmizic;
        public Form fradmin { get; set; }
        formCagrilarMonitor frmcagrilarmonitorwdc;
        private static readonly formCagrilar singletonfrmc = new formCagrilar();   //singleton nesne

        
        static formCagrilar()
        {
           
        }
        private formCagrilar()
        {
            InitializeComponent();
            gridcagrilar = this.gridControl1;
            tvmainv = this.tvMainv;
            frmset = new formadminsettings();
            lbl = lblformname;
        }
        public static formCagrilar singletonfrmcagrilar
        {
            get
            {
                return singletonfrmc;
            }
        }
        private void formCagrilar_Load(object sender, EventArgs e)
        {
            // TODO: Bu kod satırı 'cevircagirDataSet.reports' tablosuna veri yükler. Bunu gerektiği şekilde taşıyabilir, veya kaldırabilirsiniz.
            //this.reportsTableAdapter1.Fill(this.cevircagirDataSet.reports);
            lblformname.Text = Localization.lblformname__;
            baglanti = new SqlConnection(conString);
            veritaboku();
            fadmin = new frmadmin();
            immorc = Properties.Resources.denememor;
            imyesilc = Properties.Resources.denemeyesil;
            imkirmizic = Properties.Resources.denemekirmizi;
            //immorc = Image.FromFile(Environment.CurrentDirectory + @"\masalaricon\denememor.png");
            //imyesilc = Image.FromFile(Environment.CurrentDirectory + @"\masalaricon\denemeyesil.png");
            //imkirmizic = Image.FromFile(Environment.CurrentDirectory + @"\masalaricon\denemekirmizi.png");
        }
        

        public int adminsetingskaydet { get; set; }
       
        public void tvMainv_ItemCustomize(object sender, DevExpress.XtraGrid.Views.Tile.TileViewItemCustomizeEventArgs e)
        {
            int hesaplasn;
            if (adminsetingskaydet == 1) //eğer admin panelindeki ayarlarda bir değişiklik olmuşsa veritabanını oku(sürekli değil)
            {
                veritaboku();
            }
            adminsetingskaydet = 0;
            if (color.Count>=0)
            {
                if(!color.Contains(e))
                    color.Add(e);
            }
            TileView view = sender as TileView;
            if (view.RowCount>0)
            {
                val = Convert.ToString(view.GetRowCellValue(e.RowHandle, clreqtime));
                TimeSpan nownow = DateTime.Now.TimeOfDay;
                TimeSpan ts = new TimeSpan();
                if (nownow >= Convert.ToDateTime(val).TimeOfDay)
                {
                    ts = nownow - Convert.ToDateTime(val).TimeOfDay;
                    hesaplasn = ts.Hours * 3600 + ts.Minutes * 60 + ts.Seconds;
                    dk_sn =ts.Minutes.ToString("D2") + ":" + ts.Seconds.ToString("D2");
                }
                else
                {
                    ts = Convert.ToDateTime(val).TimeOfDay - nownow;
                    hesaplasn = ts.Hours * 3600 + ts.Minutes * 60 + ts.Seconds;
                    hesaplasn = 86400 - hesaplasn;
                    dk_sn = (23-ts.Hours).ToString("D2") + ":" + (59-ts.Minutes).ToString("D2") + ":" + (60-ts.Seconds).ToString("D2");
                }
                e.Item.Elements[2].Text = dk_sn;
                if (sure1 != 0 && r1 != Color.Transparent && hesaplasn >= sure1 && hesaplasn < sure2 && hesaplasn < sure3)
                {
                    e.Item.Elements[3].Image = imyesilc;
                }
                if (sure2 != 0 && r1 != Color.Transparent && hesaplasn >= sure2 && hesaplasn < sure3)
                {
                    e.Item.Elements[3].Image = immorc;
                }
                if (sure3 != 0 && r1 != Color.Transparent && hesaplasn >= sure3)
                {
                    e.Item.Elements[3].Image = imkirmizic;
                }
            }
        }//tvMainv_ItemCustomize

        private void tvMainv_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                TileView view = sender as TileView;
                TileViewHitInfo hitInfo = view.CalcHitInfo(e.Location);
                if (hitInfo.InItem)
                {
                    //Debug.WriteLine("silinmek istenen masa adı ", hitInfo.Item.Elements[0].Text);
                    if (baglanti.State != ConnectionState.Open)
                        baglanti.Open();
                    deleteCommand = new SqlCommand("delete from reports where table_name = @t and statenum = 2", baglanti);
                    deleteCommand.Parameters.AddWithValue("@t", hitInfo.Item.Elements[0].Text);
                    //Console.WriteLine("silinecek masa adı "+ hitInfo.Item.Elements[0].Text);
                    deleteCommand.ExecuteNonQuery();

                    SqlCommand refreshgrid = new SqlCommand("SELECT table_name,req_time,waiter_name from reports where statenum=@snum order by id asc", baglanti);
                    refreshgrid.Parameters.AddWithValue("@snum", 2);
                    SqlDataAdapter dataadmon = new SqlDataAdapter(refreshgrid);
                    DataSet refresh = new DataSet();
                    dataadmon.Fill(refresh);

                    SqlCommand refreshgridmon = new SqlCommand("SELECT table_id,req_time,waiter_name from reports where statenum=@snum order by id asc", baglanti);
                    refreshgridmon.Parameters.AddWithValue("@snum", 2);
                    SqlDataAdapter dataadmon2 = new SqlDataAdapter(refreshgridmon);
                    DataSet refreshmon = new DataSet();
                    dataadmon2.Fill(refreshmon);
                    lock (gridcagrilar)
                    {
                        gridcagrilar.BeginUpdate();
                        try
                        {
                            gridcagrilar.DataSource = null;
                            gridcagrilar.DataSource = refresh.Tables[0];
                            gridcagrilar.ViewCollection.Clear();
                            gridcagrilar.ViewCollection.Add(tvmainv);
                            gridcagrilar.MainView = tvmainv;
                            gridcagrilar.RefreshDataSource();
                            //Debug.WriteLine("grid düzenlendi");
                        }
                        catch (Exception Ex2)
                        {
                            Debug.WriteLine(Ex2.Message);
                        }
                        finally
                        {
                            gridcagrilar.EndUpdate();
                        }
                    }//lock
                    //foreach (Form form in Application.OpenForms)
                    //{
                    //    if (form.Name.Equals("formCagrilarMonitor"))
                    //    {
                    //        frmcagrilarmonitorwdc = formCagrilarMonitor.singletonfrmcagrilarm;
                    //        lock (frmcagrilarmonitorwdc.gridcagrilarmonitor)
                    //        {
                    //            frmcagrilarmonitorwdc.gridcagrilarmonitor.BeginUpdate();
                    //            try
                    //            {
                    //                frmcagrilarmonitorwdc.gridcagrilarmonitor.DataSource = null;
                    //                frmcagrilarmonitorwdc.gridcagrilarmonitor.DataSource = refreshmon.Tables[0];
                    //                frmcagrilarmonitorwdc.gridcagrilarmonitor.ViewCollection.Clear();
                    //                frmcagrilarmonitorwdc.gridcagrilarmonitor.ViewCollection.Add(frmcagrilarmonitorwdc.tvmainvmonitor);
                    //                frmcagrilarmonitorwdc.gridcagrilarmonitor.MainView = frmcagrilarmonitorwdc.tvmainvmonitor;
                    //                frmcagrilarmonitorwdc.gridcagrilarmonitor.RefreshDataSource();
                    //            }
                    //            catch (Exception Ex2)
                    //            {
                    //                Debug.WriteLine(Ex2.Message);
                    //            }
                    //            finally
                    //            {
                    //                frmcagrilarmonitorwdc.gridcagrilarmonitor.EndUpdate();
                    //            }
                    //        }//lock
                    //        break;
                    //    }
                    //}
                }
            }
        }

        public Color coloring()
        {
            int arraycount,notredcount ;
            
            for (arraycount = 0; arraycount <= color.Count-1;)
            {
             //   Debug.WriteLine("tile eleman sayısı"+color.Count.ToString());
                notredcount = arraycount;
                TileViewItemCustomizeEventArgs tl = (TileViewItemCustomizeEventArgs)color[arraycount]; 
                arraycount++;
                if(tl.Item.Elements[3].Image != imkirmizic)  //kırmızı değilse
                {
                    notredcount++;
                }
                if (notredcount < arraycount)  //bir tane bile kırmızı olsa eşitlik bozulacak notredcount küçük olacak
                {
                    lblformname.ForeColor = r3;
                    return r3;
                }
            }
            lblformname.ForeColor = Color.Orange;
            return Color.Orange;
        }

        public void veritaboku()
        {
            if(baglanti.State!=ConnectionState.Open)
            baglanti.Open();
            loadcommand = new SqlCommand("select duration1,duration2,duration3,color1,color2,color3 from adminsettings", baglanti);
            string colorr;
            using (SqlDataReader dr = loadcommand.ExecuteReader())  //select sorgusundan gelen verileri döndürür.
            {
                while (dr.Read())//satır varsa
                {
                    if (dr["duration1"].ToString() != "")
                    {
                        sure1 = sureler[Convert.ToInt16(dr["duration1"])];
                    }
                    else
                        sure1 = 0;
                    if (dr["duration2"].ToString() != "")
                    {
                        sure2 = sureler[Convert.ToInt16(dr["duration2"])];
                    }
                    else
                        sure2 = 0;
                    if (dr["duration3"].ToString() != "")
                    {
                        sure3 = sureler[Convert.ToInt16(dr["duration3"])];
                    }
                    else
                        sure3 = 0;
                    if (dr["color1"].ToString() != "")
                    {
                        colorr = dr["color1"].ToString();
                        r1 = whichcolor(colorr);
                    }
                    if (dr["color2"].ToString() != "")
                    {
                        colorr = dr["color2"].ToString();
                        r2 = whichcolor(colorr);
                    }
                    if (dr["color3"].ToString() != "")
                    {
                        colorr = dr["color3"].ToString();
                        r3 = whichcolor(colorr);
                    }
                }
                dr.Close();
            }
            if(baglanti.State!=ConnectionState.Closed)
            baglanti.Close();
        }
        public Color whichcolor(string renk)
        {
            if (renk == "3")
            {
                return Color.Purple;
            }
            else if (renk == "2")
            {
                return Color.DodgerBlue;
            }
            else if (renk == "5")
            {
                return Color.Red;
            }
            else if (renk == "4")
            {
                return Color.Orange;
            }
            else if (renk == "1")
            {
                return Color.LimeGreen;
            }
            else if (renk == "6")
            {
                return Color.Black;
            }
            else
            {
                return Color.Transparent;
            }
        }
    }
}


