﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cevircagirserverdeneme
{
    class WifiHotSpot
    {
        ProcessStartInfo ps;
        public bool startok = false,setipok=false,admintcp=false, setipok2 = false,whs_setipok=false;
        private string Message = "";
        public bool exitmessage;
        public WifiHotSpot()  //constructor
        {
            Init();
        }
        public void Init()
        {
            ps = new ProcessStartInfo();
            ps.UseShellExecute = false;
            ps.RedirectStandardOutput = true;
            ps.CreateNoWindow = true;
            ps.FileName = "cmd.exe";
        }
        public bool exitmes   //exitmessage get set method
        {
            get
            {
                return exitmessage;
            }
            set
            {
                exitmessage = value;
            }
        }

        public void start()
        {
            ps.Arguments = "/c netsh wlan start hosted network";
            //Debug.WriteLine("HOTSPOT başlatıldı");
            Execute(ps);
        }
        public void stop()
        {
            ps.Arguments = "/c netsh wlan stop hosted network";
            Execute(ps);
        }
        public void create(string ssid, string key)
        {
            ps.Arguments = String.Format("/c netsh wlan set hostednetwork mode=allow ssid={0} key={1}", ssid,key);
            Execute(ps);
        }
        public void setstaticip()
        {
            //exitnetsh();   //netsh dan çıkılması lazım
            ps.Arguments = String.Format("/c netsh interface ip set address \"AKEHOTSPOT\" static 192.168.5.2 255.255.255.0 192.168.5.1");
            if (startok)
            {
                Debug.WriteLine("setip---startok true");
                setipok = true;
                Execute(ps);
                if (setipok2)
                {
                    whs_setipok = true;
                    Debug.WriteLine("ok ok ok ok");
                }
            }
        }

        public void exitnetsh()
        {
            ps.Arguments = String.Format("exit");
            Execute(ps);
        }
        //public bool whs_setipok   //incomedata get set method
        //{
        //    get
        //    {
        //        return admintcp;
        //    }
        //    set
        //    {
        //        //incomedata = value;
        //    }
        //}
        public void Execute(ProcessStartInfo ps)
        {
            bool isExecuted = false;
            try
            {
                //Process p = Process.Start(ps);
                //Process p = new Process();
                //p.StartInfo = ps;
                //p.Start();
                //Debug.WriteLine("TRY İÇİNDEYİM");
                using (Process p = Process.Start(ps))
                {
                    Message += p.StandardOutput.ReadLine();
                    p.WaitForExit();
                    isExecuted = true;
                    Debug.WriteLine("CMD MESAJI" + Message+" "+isExecuted);
                    if(Message.Contains("The hosted network started"))
                    {
                        startok = true;
                    }
                    if (setipok)
                    {
                        setipok2 = true;
                        //Thread.Sleep(2000);
                    }
                }
            }
            catch (Exception e)
            {
                Message = "";
                Message += e.Message;
                isExecuted = false;
                Debug.WriteLine("HATA"+Message+""+isExecuted);
            }
            //return isExecuted;
        }
    }
}
