﻿namespace cevircagirserverdeneme
{
    partial class seriportcihazekle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblmac = new System.Windows.Forms.Label();
            this.txtmacid = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblmac
            // 
            this.lblmac.AutoSize = true;
            this.lblmac.Location = new System.Drawing.Point(23, 9);
            this.lblmac.Name = "lblmac";
            this.lblmac.Size = new System.Drawing.Size(92, 17);
            this.lblmac.TabIndex = 0;
            this.lblmac.Text = "Cihaz Mac id:";
            // 
            // txtmacid
            // 
            this.txtmacid.Enabled = false;
            this.txtmacid.Location = new System.Drawing.Point(131, 6);
            this.txtmacid.Name = "txtmacid";
            this.txtmacid.Size = new System.Drawing.Size(149, 22);
            this.txtmacid.TabIndex = 1;
            // 
            // seriportcihazekle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(292, 345);
            this.Controls.Add(this.txtmacid);
            this.Controls.Add(this.lblmac);
            this.Name = "seriportcihazekle";
            this.Text = "Cihaz Ekle";
            this.Load += new System.EventHandler(this.seriportcihazekle_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblmac;
        private System.Windows.Forms.TextBox txtmacid;
    }
}