﻿namespace cevircagirserverdeneme
{
    partial class FormGarsonlar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGarsonlar));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement1 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement2 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            this.coltable_id = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.colbool = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.tablesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cevircagirDataSet = new cevircagirserverdeneme.cevircagirDataSet();
            this.tileView1 = new DevExpress.XtraGrid.Views.Tile.TileView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.separatorControl1 = new DevExpress.XtraEditors.SeparatorControl();
            this.pnlgarsonduzenle = new System.Windows.Forms.Panel();
            this.cmbgarsonlar = new System.Windows.Forms.ComboBox();
            this.waiterBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.pnlgarsonyeni = new System.Windows.Forms.Panel();
            this.txtgarson_kod = new DevExpress.XtraEditors.TextEdit();
            this.lblad = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtgarson_ad = new DevExpress.XtraEditors.TextEdit();
            this.btnmasasec = new DevExpress.XtraEditors.SimpleButton();
            this.btnKaydet = new DevExpress.XtraEditors.SimpleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.waiterBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tablesTableAdapter = new cevircagirserverdeneme.cevircagirDataSetTableAdapters.tablesTableAdapter();
            this.waiterTableAdapter1 = new cevircagirserverdeneme.cevircagirDataSetTableAdapters.waiterTableAdapter();
            this.sqlDataSource1 = new DevExpress.DataAccess.Sql.SqlDataSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileView1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).BeginInit();
            this.pnlgarsonduzenle.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.waiterBindingSource1)).BeginInit();
            this.pnlgarsonyeni.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtgarson_kod.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgarson_ad.Properties)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.waiterBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // coltable_id
            // 
            this.coltable_id.FieldName = "table_name";
            this.coltable_id.Name = "coltable_id";
            this.coltable_id.OptionsColumn.AllowFocus = false;
            this.coltable_id.OptionsFilter.AllowAutoFilter = false;
            this.coltable_id.OptionsFilter.AllowFilter = false;
            resources.ApplyResources(this.coltable_id, "coltable_id");
            // 
            // colbool
            // 
            resources.ApplyResources(this.colbool, "colbool");
            this.colbool.FieldName = "isselected";
            this.colbool.Name = "colbool";
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("panelControl1.Appearance.BackColor")));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.panel3);
            resources.ApplyResources(this.panelControl1, "panelControl1");
            this.panelControl1.Name = "panelControl1";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.gridControl1);
            this.panel3.Controls.Add(this.panel2);
            this.panel3.Controls.Add(this.panel1);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.tablesBindingSource;
            resources.ApplyResources(this.gridControl1, "gridControl1");
            gridLevelNode1.RelationName = "FK_reports_tables";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.MainView = this.tileView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.tileView1});
            // 
            // tablesBindingSource
            // 
            this.tablesBindingSource.DataMember = "tables";
            this.tablesBindingSource.DataSource = this.cevircagirDataSet;
            // 
            // cevircagirDataSet
            // 
            this.cevircagirDataSet.DataSetName = "cevircagirDataSet";
            this.cevircagirDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tileView1
            // 
            this.tileView1.Appearance.EmptySpace.BackColor = ((System.Drawing.Color)(resources.GetObject("tileView1.Appearance.EmptySpace.BackColor")));
            this.tileView1.Appearance.EmptySpace.Options.UseBackColor = true;
            this.tileView1.Appearance.ItemSelected.Font = ((System.Drawing.Font)(resources.GetObject("tileView1.Appearance.ItemSelected.Font")));
            this.tileView1.Appearance.ItemSelected.Options.UseFont = true;
            this.tileView1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.tileView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.coltable_id,
            this.colbool});
            this.tileView1.GridControl = this.gridControl1;
            this.tileView1.Name = "tileView1";
            this.tileView1.OptionsTiles.ItemSize = new System.Drawing.Size(120, 30);
            this.tileView1.OptionsTiles.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tileView1.OptionsTiles.RowCount = 10;
            this.tileView1.OptionsTiles.ShowGroupText = false;
            tileViewItemElement1.Appearance.Normal.BackColor = ((System.Drawing.Color)(resources.GetObject("resource.BackColor")));
            tileViewItemElement1.Appearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("resource.Font")));
            tileViewItemElement1.Appearance.Normal.ForeColor = ((System.Drawing.Color)(resources.GetObject("resource.ForeColor")));
            tileViewItemElement1.Appearance.Normal.Options.UseBackColor = true;
            tileViewItemElement1.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement1.Appearance.Normal.Options.UseForeColor = true;
            tileViewItemElement1.Column = this.coltable_id;
            tileViewItemElement1.StretchHorizontal = true;
            tileViewItemElement1.StretchVertical = true;
            resources.ApplyResources(tileViewItemElement1, "tileViewItemElement1");
            tileViewItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileViewItemElement2.Column = this.colbool;
            resources.ApplyResources(tileViewItemElement2, "tileViewItemElement2");
            this.tileView1.TileTemplate.Add(tileViewItemElement1);
            this.tileView1.TileTemplate.Add(tileViewItemElement2);
            this.tileView1.ItemCustomize += new DevExpress.XtraGrid.Views.Tile.TileViewItemCustomizeEventHandler(this.tileView1_ItemCustomize);
            this.tileView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tileView1_MouseDown);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.separatorControl1);
            this.panel2.Controls.Add(this.pnlgarsonduzenle);
            this.panel2.Controls.Add(this.pnlgarsonyeni);
            this.panel2.Controls.Add(this.btnmasasec);
            this.panel2.Controls.Add(this.btnKaydet);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // separatorControl1
            // 
            this.separatorControl1.LineAlignment = DevExpress.XtraEditors.Alignment.Near;
            this.separatorControl1.LineColor = System.Drawing.Color.DimGray;
            this.separatorControl1.LineStyle = System.Drawing.Drawing2D.DashStyle.Custom;
            this.separatorControl1.LineThickness = 1;
            resources.ApplyResources(this.separatorControl1, "separatorControl1");
            this.separatorControl1.Name = "separatorControl1";
            // 
            // pnlgarsonduzenle
            // 
            this.pnlgarsonduzenle.Controls.Add(this.cmbgarsonlar);
            this.pnlgarsonduzenle.Controls.Add(this.labelControl2);
            resources.ApplyResources(this.pnlgarsonduzenle, "pnlgarsonduzenle");
            this.pnlgarsonduzenle.Name = "pnlgarsonduzenle";
            // 
            // cmbgarsonlar
            // 
            this.cmbgarsonlar.DataSource = this.waiterBindingSource1;
            this.cmbgarsonlar.DisplayMember = "waiter_name";
            resources.ApplyResources(this.cmbgarsonlar, "cmbgarsonlar");
            this.cmbgarsonlar.ForeColor = System.Drawing.Color.DimGray;
            this.cmbgarsonlar.FormattingEnabled = true;
            this.cmbgarsonlar.Name = "cmbgarsonlar";
            this.cmbgarsonlar.ValueMember = "waiter_name";
            this.cmbgarsonlar.SelectedIndexChanged += new System.EventHandler(this.cmbgarsonlar_SelectedIndexChanged);
            this.cmbgarsonlar.SelectedValueChanged += new System.EventHandler(this.cmbgarsonlar_SelectedValueChanged);
            // 
            // waiterBindingSource1
            // 
            this.waiterBindingSource1.DataMember = "waiter";
            this.waiterBindingSource1.DataSource = this.cevircagirDataSet;
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl2.Appearance.Font")));
            this.labelControl2.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl2.Appearance.ForeColor")));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.labelControl2, "labelControl2");
            this.labelControl2.Name = "labelControl2";
            // 
            // pnlgarsonyeni
            // 
            this.pnlgarsonyeni.Controls.Add(this.txtgarson_kod);
            this.pnlgarsonyeni.Controls.Add(this.lblad);
            this.pnlgarsonyeni.Controls.Add(this.labelControl1);
            this.pnlgarsonyeni.Controls.Add(this.txtgarson_ad);
            resources.ApplyResources(this.pnlgarsonyeni, "pnlgarsonyeni");
            this.pnlgarsonyeni.Name = "pnlgarsonyeni";
            // 
            // txtgarson_kod
            // 
            resources.ApplyResources(this.txtgarson_kod, "txtgarson_kod");
            this.txtgarson_kod.Name = "txtgarson_kod";
            this.txtgarson_kod.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtgarson_kod.Properties.Appearance.Font")));
            this.txtgarson_kod.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtgarson_kod.Properties.Appearance.ForeColor")));
            this.txtgarson_kod.Properties.Appearance.Options.UseFont = true;
            this.txtgarson_kod.Properties.Appearance.Options.UseForeColor = true;
            this.txtgarson_kod.Properties.ReadOnly = true;
            this.txtgarson_kod.Properties.UseReadOnlyAppearance = false;
            // 
            // lblad
            // 
            this.lblad.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("lblad.Appearance.Font")));
            this.lblad.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("lblad.Appearance.ForeColor")));
            this.lblad.Appearance.Options.UseFont = true;
            this.lblad.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.lblad, "lblad");
            this.lblad.Name = "lblad";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl1.Appearance.Font")));
            this.labelControl1.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl1.Appearance.ForeColor")));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.labelControl1, "labelControl1");
            this.labelControl1.Name = "labelControl1";
            // 
            // txtgarson_ad
            // 
            resources.ApplyResources(this.txtgarson_ad, "txtgarson_ad");
            this.txtgarson_ad.Name = "txtgarson_ad";
            this.txtgarson_ad.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtgarson_ad.Properties.Appearance.Font")));
            this.txtgarson_ad.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtgarson_ad.Properties.Appearance.ForeColor")));
            this.txtgarson_ad.Properties.Appearance.Options.UseFont = true;
            this.txtgarson_ad.Properties.Appearance.Options.UseForeColor = true;
            this.txtgarson_ad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtgarson_ad_KeyPress);
            // 
            // btnmasasec
            // 
            this.btnmasasec.AllowFocus = false;
            this.btnmasasec.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnmasasec.Appearance.BackColor")));
            this.btnmasasec.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnmasasec.Appearance.Font")));
            this.btnmasasec.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnmasasec.Appearance.ForeColor")));
            this.btnmasasec.Appearance.Options.UseBackColor = true;
            this.btnmasasec.Appearance.Options.UseFont = true;
            this.btnmasasec.Appearance.Options.UseForeColor = true;
            this.btnmasasec.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnmasasec.AppearanceHovered.BackColor")));
            this.btnmasasec.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnmasasec.AppearanceHovered.Font")));
            this.btnmasasec.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnmasasec.AppearanceHovered.ForeColor")));
            this.btnmasasec.AppearanceHovered.Options.UseBackColor = true;
            this.btnmasasec.AppearanceHovered.Options.UseFont = true;
            this.btnmasasec.AppearanceHovered.Options.UseForeColor = true;
            this.btnmasasec.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnmasasec, "btnmasasec");
            this.btnmasasec.Name = "btnmasasec";
            this.btnmasasec.Click += new System.EventHandler(this.btnmasasec_Click);
            // 
            // btnKaydet
            // 
            this.btnKaydet.AllowFocus = false;
            this.btnKaydet.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnKaydet.Appearance.BackColor")));
            this.btnKaydet.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnKaydet.Appearance.Font")));
            this.btnKaydet.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnKaydet.Appearance.ForeColor")));
            this.btnKaydet.Appearance.Options.UseBackColor = true;
            this.btnKaydet.Appearance.Options.UseFont = true;
            this.btnKaydet.Appearance.Options.UseForeColor = true;
            this.btnKaydet.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnKaydet.AppearanceHovered.BackColor")));
            this.btnKaydet.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnKaydet.AppearanceHovered.Font")));
            this.btnKaydet.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnKaydet.AppearanceHovered.ForeColor")));
            this.btnKaydet.AppearanceHovered.Options.UseBackColor = true;
            this.btnKaydet.AppearanceHovered.Options.UseFont = true;
            this.btnKaydet.AppearanceHovered.Options.UseForeColor = true;
            this.btnKaydet.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnKaydet, "btnKaydet");
            this.btnKaydet.Name = "btnKaydet";
            this.btnKaydet.Click += new System.EventHandler(this.btnKaydet_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.simpleButton1);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // simpleButton1
            // 
            this.simpleButton1.AllowFocus = false;
            this.simpleButton1.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton1.Appearance.BackColor")));
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton1.Image = global::cevircagirserverdeneme.Properties.Resources.icons8_Delete_Filled_20;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            resources.ApplyResources(this.simpleButton1, "simpleButton1");
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // tablesTableAdapter
            // 
            this.tablesTableAdapter.ClearBeforeFill = true;
            // 
            // waiterTableAdapter1
            // 
            this.waiterTableAdapter1.ClearBeforeFill = true;
            // 
            // sqlDataSource1
            // 
            this.sqlDataSource1.Name = "sqlDataSource1";
            // 
            // FormGarsonlar
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormGarsonlar";
            this.Load += new System.EventHandler(this.FormBolumler_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileView1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).EndInit();
            this.pnlgarsonduzenle.ResumeLayout(false);
            this.pnlgarsonduzenle.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.waiterBindingSource1)).EndInit();
            this.pnlgarsonyeni.ResumeLayout(false);
            this.pnlgarsonyeni.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtgarson_kod.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtgarson_ad.Properties)).EndInit();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.waiterBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        ////private cevircagirDataSet5TableAdapters.tablesTableAdapter tablesTableAdapter1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.BindingSource waiterBindingSource;
        //private cevircagirDataSet5TableAdapters.waiterTableAdapter waiterTableAdapter;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Tile.TileView tileView1;
        private DevExpress.XtraGrid.Columns.TileViewColumn coltable_id;
        private DevExpress.XtraGrid.Columns.TileViewColumn colbool;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel pnlgarsonduzenle;
        private System.Windows.Forms.ComboBox cmbgarsonlar;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.Panel pnlgarsonyeni;
        private DevExpress.XtraEditors.TextEdit txtgarson_kod;
        private DevExpress.XtraEditors.LabelControl lblad;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtgarson_ad;
        private DevExpress.XtraEditors.SimpleButton btnmasasec;
        private DevExpress.XtraEditors.SimpleButton btnKaydet;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraEditors.SeparatorControl separatorControl1;
        private cevircagirDataSet cevircagirDataSet;
        private System.Windows.Forms.BindingSource tablesBindingSource;
        private cevircagirDataSetTableAdapters.tablesTableAdapter tablesTableAdapter;
        private System.Windows.Forms.BindingSource waiterBindingSource1;
        private cevircagirDataSetTableAdapters.waiterTableAdapter waiterTableAdapter1;
        private DevExpress.DataAccess.Sql.SqlDataSource sqlDataSource1;
    }
}