﻿using NativeWifi;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace cevircagirserverdeneme
{
    public partial class formadminsettings : Form
    {
        public bool startok = false, setipok = false, admintcp = false, setipok2 = false, whs_setipok = false;
        public bool exitmessage;
        public string s1, s2, s3, r1, r2, r3, cname;
        DevExpress.XtraEditors.ComboBoxEdit colora, colorb, colorc;
        public TextBox tinfo;
        //ArrayList justNumbers=new ArrayList();
        static string conString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\cevircagir.mdf';Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        //"Server=.;Database=cevircagir;Uid=sa;Password=bhappy_5;";
        SqlConnection baglanti;
        SqlCommand loadcommand, admincommand;
        //frmadmin admin;
        frmadmin form1;
        formCagrilar frmcagrilar;
        formCagrilarMonitor frmcagrilarmonitor;
        TcpIpServer tisadmin;
        UdpBroadcast udpset;
        bool help = false;
        Serial seradmin;
        String saveChanges,serverStarted, serverFailed;
        string[] islem = { "0302", "0402", "0303", "0403", "0301", "0401",  "0304", "0404", "0305", "0405", "0408", "0407", "030F", "040F" };   //0101 batarya  "030B", "030C", "030E"  "040B", "040C", "040E"
        string[] sureler = { "10","20","30","40","60","80","120","180","240","300"};
        //public bool whs_setipok { get; set; }
        public formadminsettings()
        {
            InitializeComponent();
            colora = new DevExpress.XtraEditors.ComboBoxEdit();
            colorb = new DevExpress.XtraEditors.ComboBoxEdit();
            colorc = new DevExpress.XtraEditors.ComboBoxEdit();
            colora = cmbrenka;
            colorb = cmbrenkb;
            colorc = cmbrenkc;
            tinfo = txtinfo;
            baglanti = new SqlConnection(conString);
            cmbssid.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            
        }

        enum colors
        {
            gray=0,
            green,
            blue,
            purple,
            orange,
            red,
            black
        }

        enum time
        {
            s10=0,
            s20,
            s30,
            s40,
            s60,
            s80,
            s120,
            s180,
            s240,
            s300
        }
        private void btnredlight_Click(object sender, EventArgs e)    //kırmızı ışık
        {
            sendData("FA06060D");  
        }
        private void btnsimplelight_Click(object sender, EventArgs e)   //mum ışığı
        {
            sendData("FA070B0D");  
        }

        private void btnlightoff_Click(object sender, EventArgs e)   //ledleri söndür
        {
            sendData("FA07010D");  
        }

        private void btnderinuyku_Click(object sender, EventArgs e)
        {
            //form1.leds = 400;
            //Debug.WriteLine(tisadmin.ocd);
            //tisadmin.ocd = "$2#";  //derin uyku
            //Debug.WriteLine(tisadmin.ocd);
        }

        private void btnwakeup_Click(object sender, EventArgs e)
        {
            //form1.leds = 500;
            //Debug.WriteLine(tisadmin.ocd);
            //tisadmin.ocd = "$0#";  //uyandır
            //Debug.WriteLine(tisadmin.ocd);
            //txtinfo.Visible = true;
            //txtinfo.Text = "Cihazlar uyandırılıyor, lütfen bekleyiniz...";
            //tmrtmr.Start();
        }

        private void btncvrcgr_Click(object sender, EventArgs e)
        {
            //form1.leds = 600;
            //tisadmin.ocd = "$0#";   //normal mod
        }

        private void formadminsettings_Load(object sender, EventArgs e)
        {
            localizationSettings();
            label26.Text = Settings1.Default.setupversion;
            form1 = (frmadmin)Application.OpenForms["frmadmin"];
            frmcagrilar = (formCagrilar)Application.OpenForms["formCagrilar"];
            frmcagrilarmonitor = (formCagrilarMonitor)Application.OpenForms["formCagrilarMonitor"];
            formdoldur();
            formdoldur2();
            showConnectedId();
            if (Settings1.Default.serialorwifi == 0)
            {
                tisadmin = TcpIpServer.singletontisobj;
                udpset = UdpBroadcast.singletonudpobject;
            }
            else
            {
                seradmin = Serial.singletonterobj;
            }
            if(Settings1.Default.serialorwifi == 1)
            {
                panel1.Visible = false;
                btnMasterStart.Visible = false;
            }
            if (Settings1.Default.devicenum == 0)
            {
                Console.WriteLine("tanımlı cihaz yok");
                Settings1.Default.devicenum = 1;
                Settings1.Default.Save();
                cmbssid.SelectedIndex = 0;
            }
            cmbssid.SelectedIndex = Settings1.Default.devicenum - 1;
            //label27.Text = udpset.deneme;
            //seradmin = Serial.singletonterobj;
            //foreach (string s in System.IO.Ports.SerialPort.GetPortNames())   //her bir seri port için;
            //{
            //    cmbPort.Properties.Items.Add(s);    //comports bileşenine seri port isimlerini yaz.
            //}
            //cmbPort.SelectedIndex = 0;
        }

        private void btnTr_Click(object sender, EventArgs e)
        {
            cmbSettings.Properties.Items.Clear();
            cmbsurea.Properties.Items.Clear();
            cmbsureb.Properties.Items.Clear();
            cmbsurec.Properties.Items.Clear();
            Localization.Culture = new CultureInfo("");
            localizationSettings();
            Settings1.Default.language = "tr";
            Settings1.Default.Save();
            formdoldur();
            cmbSettings.SelectedIndex = 0;
            baglanti.Open();
            //loadcommand = new SqlCommand("update tables set table_name = replace(table_name,substring(table_name,0, CHARINDEX(' ',table_name)),'Masa')", baglanti);
            //loadcommand.ExecuteNonQuery();
            //loadcommand = new SqlCommand("update reports set table_name = replace(table_name,substring(table_name,0, CHARINDEX(' ',table_name)),'Masa')", baglanti);
            //loadcommand.ExecuteNonQuery();
            //loadcommand = new SqlCommand("update waitersstables set table_name = replace(table_name,substring(table_name,0, CHARINDEX(' ',table_name)),'Masa')", baglanti);
            //loadcommand.ExecuteNonQuery();
            //baglanti.Close();

            frmRestart frmrst = new frmRestart();
            frmrst.StartPosition = FormStartPosition.Manual;
            frmrst.Left = frmadmin.ActiveForm.Left;
            frmrst.Top = frmadmin.ActiveForm.Top;
            frmrst.Show();
        }

        private void btnEn_Click(object sender, EventArgs e)
        {
            cmbSettings.Properties.Items.Clear();
            cmbsurea.Properties.Items.Clear();
            cmbsureb.Properties.Items.Clear();
            cmbsurec.Properties.Items.Clear();
            Localization.Culture = new CultureInfo("en-US");
            localizationSettings();
            Settings1.Default.language = "en";
            Settings1.Default.Save();
            cmbSettings.SelectedIndex = 0;
            formdoldur();
            //baglanti.Open();
            //loadcommand = new SqlCommand("update tables set table_name = replace(table_name,substring(table_name,0, CHARINDEX(' ',table_name)),'Table')", baglanti);
            //loadcommand.ExecuteNonQuery();
            //loadcommand = new SqlCommand("update reports set table_name = replace(table_name,substring(table_name,0, CHARINDEX(' ',table_name)),'Table')", baglanti);
            //loadcommand.ExecuteNonQuery();
            //loadcommand = new SqlCommand("update waitersstables set table_name = replace(table_name,substring(table_name,0, CHARINDEX(' ',table_name)),'Table')", baglanti);
            //loadcommand.ExecuteNonQuery();
            //baglanti.Close();

            frmRestart frmrst = new frmRestart();
            frmrst.StartPosition = FormStartPosition.Manual;
            frmrst.Left = frmadmin.ActiveForm.Left;
            frmrst.Top = frmadmin.ActiveForm.Top;
            frmrst.Show();
        }

        private void btnsimpleFlash_Click(object sender, EventArgs e)   //flash
        {
            sendData("FA07050D");    //fa 07 0E 0D
        }

        private void cmbPort_SelectedIndexChanged(object sender, EventArgs e)
        {
            label1.Focus();
        }

        private void simpleButton2_Click_1(object sender, EventArgs e)   //kırmızı flash
        {
            sendData("FA07040D");    
        }

        private void simpleButton3_Click(object sender, EventArgs e)  //mum ışığı kapat
        {
            sendData("FA07060D");    
        }

        private void simpleButton4_Click(object sender, EventArgs e)    //beyaz ışık
        {
            sendData("FA06010D");   
        }

        private void simpleButton7_Click(object sender, EventArgs e)    //slave
        {
            sendData("FA07090D");   
        }

        private void simpleButton6_Click(object sender, EventArgs e)
        {
            //sendData("001:250:007:014");   
        }

        private void simpleButton5_Click(object sender, EventArgs e)     //uyku
        {
            sendData("FA070E0D");    
        }

        public void sendData(string data)
        {
            if (Settings1.Default.serialorwifi == 0)
            {
                tisadmin.ocd = data;
                tisadmin.sendData();
            }
            else
            {
                seradmin.ocd = data;
                seradmin.sp_SendGlobal();
            }
        }
        public void formdoldur() //veritabanına kaydedilen değişiklikleri formdaki comboboxlara çeker
        {
            baglanti.Open();
            loadcommand = new SqlCommand("select company_name,duration1,duration2,duration3,color1,color2,color3 from adminsettings", baglanti);
            using (SqlDataReader dr = loadcommand.ExecuteReader())  //select sorgusundan gelen verileri döndürür.
            {
                while (dr.Read())//satır varsa
                {
                    if (dr["company_name"].ToString() != "")
                    {
                        txtfirmaad.Text = dr["company_name"].ToString();
                    }
                    if (dr["duration1"].ToString() != "")
                    {
                       // Console.WriteLine("süre1"+ dr["duration1"].ToString());
                        cmbsurea.Text = sureler[Convert.ToInt16(dr["duration1"])] + " " + Localization.label1;
                    }
                    if (dr["duration2"].ToString() != "")
                    {
                        //Console.WriteLine("süre2" + dr["duration2"].ToString());
                        cmbsureb.Text = sureler[Convert.ToInt16(dr["duration2"])] + " " + Localization.label1;
                    }
                    if (dr["duration3"].ToString() != "")
                    {
                        //Console.WriteLine("süre3" + dr["duration3"].ToString());
                        cmbsurec.Text = sureler[Convert.ToInt16(dr["duration3"])] + " " + Localization.label1;
                    }
                    //if (dr["color1"] != null)
                    //{
                    //    comboDefColor(dr["color1"].ToString(), colora);
                    //}
                    //if (dr["color2"] != null)
                    //{
                    //    comboDefColor(dr["color2"].ToString(), colorb);
                    //}
                    //if (dr["color3"] != null)
                    //{
                    //    comboDefColor(dr["color3"].ToString(), colorc);
                    //}
                }
                dr.Close();
            }
            
            baglanti.Close();

            txtrenk1.BackColor = Color.LimeGreen;
            txtrenk1.Text = Localization.green;

            txtrenk2.BackColor = Color.Purple;
            txtrenk2.Text = Localization.purple;

            txtrenk3.BackColor = Color.Red;
            txtrenk3.Text = Localization.red;

            chkYes.Checked = Settings1.Default.ringnotification;


        }

        private void cmbDevNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            label1.Focus();
        }

        private void cmbSettings_SelectedIndexChanged(object sender, EventArgs e)
        {
            label1.Focus();
        }

        private void cmbDevNo_Closed(object sender, DevExpress.XtraEditors.Controls.ClosedEventArgs e)
        {
            label1.Focus();
        }

        private void cmbSettings_Closed(object sender, DevExpress.XtraEditors.Controls.ClosedEventArgs e)
        {
            label1.Focus();
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            String senddata = "";
            if (cmbDevNo.SelectedItem != null)
            {
                baglanti.Open();
                loadcommand = new SqlCommand("select mac_address from tables where table_name = @x", baglanti);
                loadcommand.Parameters.AddWithValue("@x", cmbDevNo.SelectedItem.ToString());
                using (SqlDataReader dr = loadcommand.ExecuteReader())  //select sorgusundan gelen verileri döndürür.
                {
                    while (dr.Read())//satır varsa
                    {
                        senddata = dr["mac_address"].ToString();
                    }
                    dr.Close();
                }
                if (senddata != null)
                {
                    //String[] splitarr = null;
                    //splitarr = senddata.Split('-');
                    //string chanel = splitarr[0];
                    //string id = splitarr[1];           
                    senddata += islem[cmbSettings.SelectedIndex];
                    senddata += "0D";
                    if (Settings1.Default.serialorwifi == 0)
                    {
                        tisadmin.ocd = senddata;
                        tisadmin.sendDatabyChanel();
                    }
                    else
                    {
                        String[] splitarr = null;
                        splitarr = senddata.Split('-');
                        senddata = splitarr[1];
                        seradmin.ocd = senddata;
                        seradmin.sp_SendData();
                    }
                    //sendData(senddata);
                    //Console.WriteLine(senddata);
                }
                baglanti.Close();
            }
        }

        private void btnDefault_Click(object sender, EventArgs e)
        {
            sendData("FA070F0D");
        }

        private void txtrenk1_Click(object sender, EventArgs e)
        {
            label1.Focus();
        }

        private void cmbDevNo_MouseDown(object sender, MouseEventArgs e)
        {
            cmbDevNo.Properties.Items.Clear();
            baglanti.Open();
            loadcommand = new SqlCommand("select table_name from tables", baglanti);   //table_id önceki hali
            using (SqlDataReader dr = loadcommand.ExecuteReader())  //select sorgusundan gelen verileri döndürür.
            {
                while (dr.Read())//satır varsa
                {
                    cmbDevNo.Properties.Items.Add(dr["table_name"].ToString());   //table_id önceki hali
                }
                dr.Close();
            }
            baglanti.Close();
        }

        //private void chkMaster_CheckedChanged(object sender, EventArgs e)
        //{
        //    //if(chkMaster.Checked)
        //    //{
        //    //    label16.Enabled = true;
        //    //    //panel1.Visible = true;
        //    //    udpset.Stop();
        //    //    btnMasterStart.Visible = true;
        //    //}
        //    //else
        //    //{
        //    //    label16.Enabled = false;
        //    //    //panel1.Visible = false;
        //    //    btnMasterStart.Visible = false;
        //    //}
        //}

        private void btnMasterSend_Click(object sender, EventArgs e)
        {
            //if (txtSSID.Text != "" && txtPass.Text != "")
            //{
            //    masterset.SSID = txtSSID.Text;
            //    masterset.pass = txtPass.Text;
            //    //masterset.startClient();
            //}
            //txtSSID.Text = "";
            //txtPass.Text = "";
            //chkMaster.Checked = false;
        }

        private void btnHelp_Click(object sender, EventArgs e)
        {
            //ayrı bir formda anlat
            frmMasterSettings frmMaster = new frmMasterSettings();
            frmMaster.Show();
        }

        private void btnMasterStart_Click(object sender, EventArgs e)
        {
            udpset.Stop();
            frmMasterSettings frmMaster = new frmMasterSettings();
            frmMaster.StartPosition = FormStartPosition.Manual;
            frmMaster.Left = frmadmin.ActiveForm.Left;
            frmMaster.Top = frmadmin.ActiveForm.Top;
            frmMaster.Show();
        }

        private void cmbssid_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Console.WriteLine("cihaz adedi değişti kaydet");
            Settings1.Default.devicenum = Convert.ToInt32(cmbssid.SelectedItem);
            Settings1.Default.Save();
        }

        private void simpleButton9_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton17_Click(object sender, EventArgs e)
        {

        }

        private void simpleButton8_Click(object sender, EventArgs e)
        {
            sendData("FA06020D");
        }

        private void simpleButton11_Click(object sender, EventArgs e)
        {
            sendData("FA07030D");
        }

        private void simpleButton10_Click(object sender, EventArgs e)
        {
            sendData("FA06030D");
        }

        private void simpleButton12_Click(object sender, EventArgs e)
        {
            sendData("FA06040D");
        }

        private void simpleButton16_Click(object sender, EventArgs e)
        {
            sendData("FA06070D");
        }

        private void simpleButton14_Click(object sender, EventArgs e)
        {
            sendData("FA07020D");
        }

        private void simpleButton15_Click(object sender, EventArgs e)
        {
            sendData("FA06080D");
        }

        private void simpleButton13_Click(object sender, EventArgs e)
        {
            sendData("FA06090D");
        }

        private void simpleButton21_Click(object sender, EventArgs e)
        {
            sendData("FA07090D");
        }

        private void simpleButton19_Click(object sender, EventArgs e)
        {
            sendData("FA070A0D");
        }

        private void simpleButton20_Click(object sender, EventArgs e)
        {
            sendData("FA070B0D");
        }

        private void simpleButton18_Click(object sender, EventArgs e)
        {
            sendData("FA070C0D");
        }

        private void simpleButton9_Click_1(object sender, EventArgs e)
        {
            sendData("FA07070D");
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            help = !help;
            if (help)
            {
                lstChn.Items.Clear();
                if (Settings1.Default.serialorwifi == 0)
                {
                    if (tisadmin.getCh.Count() != 0)
                    {
                        foreach (String chn in tisadmin.getCh)    //getch fonksiyonu versiyon dizisi döndürür 
                        {
                            //Console.WriteLine(chn);
                            lstChn.Items.Add(chn);
                        }
                    }
                }
                else
                {
                    if (seradmin.getCh.Count() != 0)
                    {
                        foreach (String chn in seradmin.getCh)    //getch fonksiyonu versiyon dizisi döndürür 
                        {
                            //Console.WriteLine(chn);
                            lstChn.Items.Add(chn);
                        }
                    }
                }
            }
            lstChn.Visible = help;
            //help = !help;
            //labelControl1.Visible = help;
        }

        private void chkYes_CheckedChanged(object sender, EventArgs e)
        {
            Settings1.Default.ringnotification = chkYes.Checked;
            Settings1.Default.Save();
        }

        private void chkNo_CheckedChanged(object sender, EventArgs e)
        {
            //chkYes.Checked = !chkNo.Checked;
            //Settings1.Default.automatch = chkYes.Checked;
            //Console.WriteLine("chkNo " + Settings1.Default.automatch);
        }

        private void chkYes_CheckStateChanged(object sender, EventArgs e)
        {
            //Console.WriteLine("check state yes");
        }

        private void chkNo_CheckStateChanged(object sender, EventArgs e)
        {
           // Console.WriteLine("check state no");
        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void btnnet_Click(object sender, EventArgs e)
        {
            if(udpset!=null)
                udpset.Stop();

            frmManualNetworkSelection frmman = new frmManualNetworkSelection();
            frmman.StartPosition = FormStartPosition.Manual;
            frmman.Left = frmadmin.ActiveForm.Left;
            frmman.Top = frmadmin.ActiveForm.Top;
            frmman.Show();
        }

        public void formdoldur2()
        {
            baglanti.Open();
            loadcommand = new SqlCommand("select table_name from tables", baglanti);
            using (SqlDataReader dr = loadcommand.ExecuteReader())  //select sorgusundan gelen verileri döndürür.
            {
                while (dr.Read())//satır varsa
                {
                    cmbDevNo.Properties.Items.Add(dr["table_name"].ToString());
                }
                dr.Close();
            }
            baglanti.Close();
            if(cmbDevNo.Properties.Items.Count>0)
            {
                cmbDevNo.SelectedIndex = 0;
            }
            cmbSettings.SelectedIndex = 0;
            
        }
        public void comboDefColor(String clr, DevExpress.XtraEditors.ComboBoxEdit cmb)
        {
            if (clr == "3")
            {
                cmb.BackColor = Color.Purple;
                cmb.Text = Localization.purple;
            }
            if (clr == "0")
            {
                cmb.BackColor = Color.DimGray;
                cmb.Text = Localization.gray;
            }
            if (clr == "2")
            {
                cmb.BackColor = Color.DodgerBlue;
                cmb.Text = Localization.blue;
            }
            if (clr == "5")
            {
                cmb.BackColor = Color.Red;
                cmb.Text = Localization.red;
            }
            if (clr == "4")
            {
                cmb.BackColor = Color.Orange;
                cmb.Text = Localization.orange;
            }
            if (clr == "1")
            {
                cmb.BackColor = Color.LimeGreen;
                cmb.Text = Localization.green;
            }
            if (clr == "6")
            {
                cmb.BackColor = Color.Black;
                cmb.Text = Localization.black;
            }
            cmb.ForeColor = Color.White;
            label1.Focus();
        }
        private void simpleButton2_Click(object sender, EventArgs e)   //burada bilgisayar ismi değiştiriliyor, eğer istenen isim değilse uyarı ver her açılışta
        {
            //WifiHotSpot whs = new WifiHotSpot();
            //whs.start();
            //whs.setstaticip();
            //if(whs.whs_setipok)
            //{
            //    lblinfo.Text = "Hotspot başlatıldı..";
            //    tmrtmr.Start();
            //}
            //else
            //{
            //    lblinfo.Text = "Hotspot başlatılamadı, lütfen tekrar deneyin..";
            //    tmrtmr.Start();
            //}
        }

        
    
    private void simpleButton1_Click(object sender, EventArgs e)
        {
            //Console.WriteLine(cmbPort.SelectedItem.ToString());
            //seradmin.portnum = cmbPort.SelectedItem.ToString();
            //seradmin.startSerial();
            //TcpIpServer tis = TcpIpServer.singletontisobj;
            //tis.starttcp();
            //if (tis.tcpstartok)
            //{
            //    lblinfo.Text = String.Format(serverStarted, tis.senin_ip);
            //    tmrtmr.Start();                
            //    baglanti.Open();     //silme işlemi için
            //    loadcommand = new SqlCommand("delete from reports where statenum = 2", baglanti);
            //    loadcommand.ExecuteNonQuery();
            //    baglanti.Close();
            //}
            //else
            //{
            //    lblinfo.Text = serverFailed;
            //    tmrtmr.Start();
            //}
        }

        private void tmrtmr_Tick(object sender, EventArgs e)
        {
            txtinfo.Text = "";
            lblinfo.Text = "";
            label15.Text = "";
            tmrtmr.Stop();
        }

        private void cmbrenkb_SelectedIndexChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cmb2 = new DevExpress.XtraEditors.ComboBoxEdit();
            cmb2 = cmbrenkb;
            comborenklendir(cmb2);
        }
        private void cmbrenkc_SelectedValueChanged_1(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cmb3 = new DevExpress.XtraEditors.ComboBoxEdit();
            cmb3 = cmbrenkc;
            comborenklendir(cmb3);
        }

        private void cmbrenka_SelectedValueChanged(object sender, EventArgs e)
        {
            DevExpress.XtraEditors.ComboBoxEdit cmb1 = new DevExpress.XtraEditors.ComboBoxEdit();
            cmb1 = cmbrenka;
            comborenklendir(cmb1);
        }


        public void comborenklendir(DevExpress.XtraEditors.ComboBoxEdit cmb)  //comboboxlarda şeçilen renge göre combobox arkaplan rengini değiştitir.
        {
            string renk = cmb.SelectedItem.ToString();
            int selIndex = cmb.SelectedIndex;
            //Debug.WriteLine("renk edit numarası",selIndex.ToString());
            //if (selIndex == 0)
            //{
            //    cmb.BackColor = Color.White;
            //    cmb.ForeColor = Color.DimGray;
            //}
            if (selIndex == 3)
            {
                cmb.BackColor = Color.Purple;
            }
            if (selIndex == 0)
            {
                cmb.BackColor = Color.DimGray;
            }
            if (selIndex == 2)
            {
                cmb.BackColor = Color.DodgerBlue;
            }
            if (selIndex == 5)
            {
                cmb.BackColor = Color.Red;
            }
            if (selIndex == 4)
            {
                cmb.BackColor = Color.Orange;
            }
            if (selIndex == 1)
            {
                cmb.BackColor = Color.LimeGreen;
            }
            if (selIndex == 6)
            {
                cmb.BackColor = Color.Black;
            }
            cmb.ForeColor = Color.White;
            label1.Focus();
        }//comborenklendir
        
        private void showConnectedId()
        {
            //int i = 0;
            //WlanClient client = new WlanClient();
            //foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
            //{
            //    // Lists all available networks
            //    Wlan.WlanAvailableNetwork[] networks = wlanIface.GetAvailableNetworkList(0);
            //    foreach (Wlan.WlanAvailableNetwork network in networks)
            //    {
            //        if (i == 0)
            //        {
            //            i++;
            //            cmbssid.Text = GetStringForSSID(network.dot11Ssid);
            //            // Console.WriteLine("Found network with SSID {0}.", GetStringForSSID(network.dot11Ssid));
            //        }
            //        cmbssid.Properties.Items.Add(GetStringForSSID(network.dot11Ssid));
            //    }
            //}
        }
        public static string GetStringForSSID(Wlan.Dot11Ssid ssid)
        {
            return Encoding.ASCII.GetString(ssid.SSID, 0, (int)ssid.SSIDLength);
        }
        
        public void send(string s, string p)
        {
            form1 = (frmadmin)Application.OpenForms["frmadmin"];
            string sp = s + "+" + p;
            form1.getssid = sp;
        }
        private void btnlogosec_Click(object sender, EventArgs e)//logo seçmek için seç butonu
        {
            //OpenFileDialog getimage = new OpenFileDialog();
            //getimage.Filter = "Resim Dosyası |*.jpg;*.png";
            //getimage.Title = "Firma Logosu Seçin";
            //getimage.ShowDialog();
            //string getfolder = getimage.FileName;

            // lblformname.Image = new Bitmap(getfolder);
        }
        private void btnadminkaydet_Click(object sender, EventArgs e) //admin ayarları sayfasında yapılan değişiklikleri veri tabanına kaydeder.
        {
            //string ssid = cmbssid.SelectedItem.ToString();
            //string pass = txtssidparola.Text;
            //send(ssid, pass);
            frmcagrilar.adminsetingskaydet = 1;
            //foreach(Form form in Application.OpenForms)
            //{
            //    if(form.Name.Equals("formCagrilarMonitor"))
            //    {
            //        frmcagrilarmonitor.adminsetingskaydet = 1;
            //    }
            //}
            //gerekli değişiklikleri veri tabanına kaydet veya update et
            //comboboxlar doluysa verileri al ve kaydet.
            if(txtfirmaad.Text != "")
            {
                cname = txtfirmaad.Text;
            }
            else
            {
                cname = "";
            }
            if (cmbsurea.SelectedIndex != -1)
            {
                if (cmbsurea.SelectedItem != null)
                {
                //    justNumbers.Add(new String(cmbsurea.SelectedItem.ToString().Where(Char.IsDigit).ToArray()));
                //    form1.suresn1 = Convert.ToInt32(justNumbers[0]);
                    s1 = cmbsurea.SelectedIndex.ToString();
                }
            }
            else
            {
                //form1.suresn1 = 0;
                s1 = "";
            }
            if (cmbsureb.SelectedIndex != -1)
            {
                if (cmbsureb.SelectedItem != null)
                {
                //    justNumbers.Add(new String(cmbsureb.SelectedItem.ToString().Where(Char.IsDigit).ToArray()));
                //    form1.suresn2 = Convert.ToInt32(justNumbers[1]);
                    s2 = cmbsureb.SelectedIndex.ToString();
                }
            }
            else
            {
                //form1.suresn2 = 0;
                s2 = "";
            }
            if (cmbsurec.SelectedIndex != -1)
            {
                if (cmbsurec.SelectedItem != null)
                {
                    //justNumbers.Add(new String(cmbsurec.SelectedItem.ToString().Where(Char.IsDigit).ToArray()));
                    //form1.suresn3 = Convert.ToInt32(justNumbers[2]);
                    s3 = cmbsurec.SelectedIndex.ToString();
                }
            }
            else
            {
                //form1.suresn3 = 0;
                s3 = "";
            }
            //if (cmbrenka.SelectedIndex != -1)
            //{
            //    if (cmbrenka.SelectedItem != null)
            //    {
            //        //comborenklendir(color1,1);
            //        r1 = cmbrenka.SelectedIndex.ToString();
            //    }
            //}
            //else
            //{
            //    //form1.renk1 = Color.Transparent;
            //    r1 = "0";
            //}
            //if (cmbrenkb.SelectedIndex != -1)
            //{
            //    if (cmbrenkb.SelectedItem != null)
            //    {
            //        //comborenklendir(color2,1);
            //        r2 = cmbrenkb.SelectedIndex.ToString();
            //    }
            //}
            //else
            //{
            //    //form1.renk2 = Color.Transparent;
            //    r2 = "0";
            //}
            //if (cmbrenkc.SelectedIndex != -1)
            //{
            //    if (cmbrenkc.SelectedItem != null)
            //    {
            //        //comborenklendir(color3,1);
            //        r3 = cmbrenkc.SelectedIndex.ToString();
            //    }
            //}
            //else
            //{
            //    //form1.renk3 = Color.Transparent;
            //    r3 = "0";
            //}
            r1 = "1";
            r2 = "3";
            r3 = "5";
            baglanti.Open();
            loadcommand = new SqlCommand("select company_name,duration1,duration2,duration3,color1,color2,color3 from adminsettings", baglanti);
            SqlDataAdapter da = new SqlDataAdapter(loadcommand);
            SqlDataReader dr = loadcommand.ExecuteReader();  //select sorgusundan gelen verileri döndürür.
            if (!dr.Read())
            {
                //Console.WriteLine("veri yok kaydet");
                admincommand = new SqlCommand("insert into adminsettings(company_name,duration1,duration2,duration3,color1,color2,color3) values(@cname,@sur1,@sur2,@sur3,@col1,@col2,@col3)", baglanti);
            }
            else
            {
                //Console.WriteLine("veri update");
                admincommand = new SqlCommand("update adminsettings set company_name=@cname, duration1=@sur1, duration2=@sur2, duration3=@sur3, color1=@col1, color2=@col2, color3=@col3",baglanti);
            }
            dr.Close();
            admincommand.Parameters.AddWithValue("@cname",cname);
            admincommand.Parameters.AddWithValue("@sur1", s1);
            admincommand.Parameters.AddWithValue("@sur2", s2);
            admincommand.Parameters.AddWithValue("@sur3", s3);
            admincommand.Parameters.AddWithValue("@col1", r1);
            admincommand.Parameters.AddWithValue("@col2", r2);
            admincommand.Parameters.AddWithValue("@col3", r3);
            admincommand.ExecuteNonQuery();
            baglanti.Close();
            //justNumbers.Clear();
            label15.Text = saveChanges;
            tmrtmr.Start();
        }
        public void localizationSettings()
        {
            lblformname.Text = Localization.lblformname_;
            label16.Text = Localization.apdevice;
            btnMasterStart.Text = Localization.strt;
            btnDefault.Text = Localization.globalstok;
            label8.Text = Localization.label8;
            label10.Text = Localization.label10;
            label14.Text = Localization.label14;
            btnredlight.Text = Localization.btnredlight;
            btnwakeup.Text = Localization.btnwakeup;
            btnsimpleFlash.Text = Localization.btnflash;
            simpleButton2.Text = Localization.btnredflash;
            simpleButton3.Text = Localization.simplelightoff;
            simpleButton4.Text = Localization.whitelight;
            label11.Text = Localization.label11;
            label12.Text = Localization.label12;
            btnSend.Text = Localization.btnSend;
            btnderinuyku.Text = Localization.btnderinuyku;
            btncvrcgr.Text = Localization.btncvrcgr;
            btnlightoff.Text = Localization.btnlightoff;
            label1.Text = Localization.label1_;
            label2.Text = Localization.level1;
            label3.Text = Localization.level2;
            label4.Text = Localization.level3;
            label5.Text = Localization.color;
            label6.Text = Localization.color;
            label7.Text = Localization.color;
            simpleButton1.Text = Localization.simpleButton1;
            btnadminkaydet.Text = Localization.btnadminkaydet;
            txtrenk1.Text = Localization.green;
            txtrenk2.Text = Localization.purple;
            txtrenk3.Text = Localization.red;
            //colora.Properties.Items.Add(Localization.gray);
            //colora.Properties.Items.Add(Localization.green);
            //colora.Properties.Items.Add(Localization.blue);
            //colora.Properties.Items.Add(Localization.purple);
            //colora.Properties.Items.Add(Localization.orange);
            //colora.Properties.Items.Add(Localization.red);
            //colora.Properties.Items.Add(Localization.black);
            //colorb.Properties.Items.Add(Localization.gray);
            //colorb.Properties.Items.Add(Localization.green);
            //colorb.Properties.Items.Add(Localization.blue);
            //colorb.Properties.Items.Add(Localization.purple);
            //colorb.Properties.Items.Add(Localization.orange);
            //colorb.Properties.Items.Add(Localization.red);
            //colorb.Properties.Items.Add(Localization.black);
            //colorc.Properties.Items.Add(Localization.gray);
            //colorc.Properties.Items.Add(Localization.green);
            //colorc.Properties.Items.Add(Localization.blue);
            //colorc.Properties.Items.Add(Localization.purple);
            //colorc.Properties.Items.Add(Localization.orange);
            //colorc.Properties.Items.Add(Localization.red);
            //colorc.Properties.Items.Add(Localization.black);
            //cmbsurea.Properties.Items.Add("");
            cmbsurea.Properties.Items.Add("10 " + Localization.label1);
            cmbsurea.Properties.Items.Add("20 " + Localization.label1);
            cmbsurea.Properties.Items.Add("30 " + Localization.label1);
            cmbsurea.Properties.Items.Add("40 " + Localization.label1);
            cmbsurea.Properties.Items.Add("60 " + Localization.label1);
            cmbsurea.Properties.Items.Add("80 " + Localization.label1);
            cmbsurea.Properties.Items.Add("120 " + Localization.label1);
            cmbsurea.Properties.Items.Add("180 " + Localization.label1);
            cmbsurea.Properties.Items.Add("240 " + Localization.label1);
            cmbsurea.Properties.Items.Add("300 " + Localization.label1);
            //cmbsureb.Properties.Items.Add("");
            cmbsureb.Properties.Items.Add("10 " + Localization.label1);
            cmbsureb.Properties.Items.Add("20 " + Localization.label1);
            cmbsureb.Properties.Items.Add("30 " + Localization.label1);
            cmbsureb.Properties.Items.Add("40 " + Localization.label1);
            cmbsureb.Properties.Items.Add("60 " + Localization.label1);
            cmbsureb.Properties.Items.Add("80 " + Localization.label1);
            cmbsureb.Properties.Items.Add("120 " + Localization.label1);
            cmbsureb.Properties.Items.Add("180 " + Localization.label1);
            cmbsureb.Properties.Items.Add("240 " + Localization.label1);
            cmbsureb.Properties.Items.Add("300 " + Localization.label1);
            //cmbsurec.Properties.Items.Add("");
            cmbsurec.Properties.Items.Add("10 " + Localization.label1);
            cmbsurec.Properties.Items.Add("20 " + Localization.label1);
            cmbsurec.Properties.Items.Add("30 " + Localization.label1);
            cmbsurec.Properties.Items.Add("40 " + Localization.label1);
            cmbsurec.Properties.Items.Add("60 " + Localization.label1);
            cmbsurec.Properties.Items.Add("80 " + Localization.label1);
            cmbsurec.Properties.Items.Add("120 " + Localization.label1);
            cmbsurec.Properties.Items.Add("180 " + Localization.label1);
            cmbsurec.Properties.Items.Add("240 " + Localization.label1);
            cmbsurec.Properties.Items.Add("300 " + Localization.label1);
            cmbSettings.Properties.Items.Add(Localization.s3);
            cmbSettings.Properties.Items.Add(Localization.s4);
            cmbSettings.Properties.Items.Add(Localization.s1); 
            cmbSettings.Properties.Items.Add(Localization.s2);
            cmbSettings.Properties.Items.Add(Localization.s5);
            cmbSettings.Properties.Items.Add(Localization.s6);
            cmbSettings.Properties.Items.Add(Localization.s7);
            cmbSettings.Properties.Items.Add(Localization.s8);
            cmbSettings.Properties.Items.Add(Localization.s9);
            cmbSettings.Properties.Items.Add(Localization.s10);
            cmbSettings.Properties.Items.Add(Localization.s11);
            cmbSettings.Properties.Items.Add(Localization.s12);
            //cmbSettings.Properties.Items.Add(String.Format(Localization.s13, "1"));
            //cmbSettings.Properties.Items.Add(String.Format(Localization.s13, "2"));
            //cmbSettings.Properties.Items.Add(String.Format(Localization.s13, "4"));
            cmbSettings.Properties.Items.Add(Localization.s13);
            //cmbSettings.Properties.Items.Add(String.Format(Localization.s14, "1"));
            //cmbSettings.Properties.Items.Add(String.Format(Localization.s14, "2"));
            //cmbSettings.Properties.Items.Add(String.Format(Localization.s14, "4"));
            cmbSettings.Properties.Items.Add(Localization.s14);

            saveChanges = Localization.saveChanges;
            serverStarted = Localization.serverStarted;
            serverFailed = Localization.serverFailed;
            chkYes.Text = Localization.btnYes;
            label24.Text = Localization.ring;
            label17.Text = Localization.devicecount;
            simpleButton9.Text = Localization.s11;
            label18.Text = Localization.wbright;
            label19.Text = Localization.rbright;
            label20.Text = Localization.cbright;
            label9.Text = Localization.alldevices;
            btnnet.Text = Localization.infonet;
        }
    }
}
