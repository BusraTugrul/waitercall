﻿using DevExpress.XtraTab;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cevircagirserverdeneme
{
    public partial class FormGarsonIslem : Form
    {
        bool dragging;
        Point offset;
        private frmadmin admin = new frmadmin();
        public int x;
        XtraTabPage newPageGarson;
        public static XtraTabControl tabgarson;
        private static readonly FormGarsonIslem singletonfrmgarsonislem = new FormGarsonIslem();  //singleton nesne
        string waiterTable, listWaiters, editWaiters;

        static FormGarsonIslem()
        {

        }
        private FormGarsonIslem()
        {
            InitializeComponent();
        }
        public static FormGarsonIslem singletonfrmgars
        {
            get
            {
                return singletonfrmgarsonislem;
            }
        }
        private void btnislemclose_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void btnduzenle_Click(object sender, EventArgs e)
        {
            x = 1;
            garsonformu(tabgarson);
            this.Hide();
            //LabelText = 1;
            //admin.Show();
        }

        private void btnyenikayit_Click(object sender, EventArgs e)
        {
            FormGarsonlar frmyeni = new FormGarsonlar();
            frmyeni.pnlduzenle.Visible = false;
            frmyeni.pnlyeni.Visible = true;
            frmyeni.duzenle = 2;
            frmyeni.Show();
            this.Hide();
        }

        private void btnlistele_Click(object sender, EventArgs e)
        {
            x = 2;
            garsonformu(tabgarson);
            this.Hide();
            //admin.Show();
        }
        public void tab(XtraTabControl tabx)
        {
            //Debug.WriteLine("adminden gelen tab"+tabx.SelectedTabPage.Text);
            tabgarson = tabx;  //formadmin den gelen xtratabcontrol tabgarsona atanıyor.
        }
        public void garsonformu(XtraTabControl a)   //garson listele veya garson düzenle ekranını formadminin tabında açıyor.
        {
            FormGarsonListe frmduzen = FormGarsonListe.singletonfrmgar;  //garson listeleme formu
            if (x == 1)   //edit
            {
                //Debug.WriteLine(x);
                frmduzen.lblgarsonbaslik.Text = waiterTable;
                frmduzen.pnlcntrl.Visible = true;
                frmduzen.gview.Columns[2].Visible = true;
                frmduzen.gview.Focus();
                foreach (XtraTabPage page in a.TabPages)   //garson düzenle açılmışsa
                {
                   // Debug.WriteLine(page.Text);
                    if (page.Text.Equals(listWaiters))
                    {
                        a.TabPages.Remove(newPageGarson);
                        break;
                    }
                }
                foreach (XtraTabPage page in a.TabPages)   //tab önceden varsa selected page yap.
                {
                    if (page.Text.Equals(editWaiters))
                    {
                        //garson listele page kapat
                        //Debug.WriteLine("///////////////"+page.Text);
                        a.SelectedTabPage = page;
                        page.Focus();
                        //frmduzen.btnlist.PerformClick();
                    }
                }
                if (!a.SelectedTabPage.Text.Equals(editWaiters))    //tab yeni var edilecekse
                {
                    newPageGarson = new DevExpress.XtraTab.XtraTabPage { Text = editWaiters };  //newpagegarson enable=true olduğu an
                    //Debug.WriteLine("tab yok");
                    a.TabPages.Add(newPageGarson);
                    a.SelectedTabPage = newPageGarson;
                    frmduzen.TopLevel = false;
                    frmduzen.Parent = newPageGarson;
                    frmduzen.Dock = DockStyle.Fill;
                    a.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.DimGray;
                    a.AppearancePage.HeaderActive.Font = new Font("Segoe UI Semibold", 9, FontStyle.Bold);
                    a.AppearancePage.Header.Font = new Font("Segoe UI Semibold", 9, FontStyle.Bold);
                    newPageGarson.Appearance.Header.ForeColor = Color.DimGray;
                    newPageGarson.TabPageWidth = 100;
                    frmduzen.Show();
                }
            }
            if (x == 2)  // list
            {
                //frmduzen.pnlcntrl.Visible = false;
                frmduzen.gview.Columns[2].Visible = false;
                frmduzen.gview.Focus();
                frmduzen.lblgarsonbaslik.Text = waiterTable;

                foreach (XtraTabPage page in a.TabPages)   //garson düzenle açılmışsa
                {
                    if (page.Text.Equals(editWaiters))
                    {
                        a.TabPages.Remove(newPageGarson);
                        break;
                    }
                }
                foreach (XtraTabPage page in a.TabPages)   //tab önceden varsa selected page yap.
                {
                    if (page.Text.Equals(listWaiters))
                    {
                        a.SelectedTabPage = page;
                        page.Focus();
                        //frmduzen.btnlist.PerformClick();
                    }
                }
                if (!a.SelectedTabPage.Text.Equals(listWaiters))    //tab yeni var edilecekse
                {
                    newPageGarson = new DevExpress.XtraTab.XtraTabPage { Text = listWaiters };  //newpagegarson enable=true olduğu an
                    a.TabPages.Add(newPageGarson);
                    a.SelectedTabPage = newPageGarson;
                    frmduzen.TopLevel = false;
                    frmduzen.Parent = newPageGarson;
                    frmduzen.Dock = DockStyle.Fill;
                    a.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.DimGray;
                    a.AppearancePage.HeaderActive.Font = new Font("Segoe UI Semibold", 9, FontStyle.Bold);
                    a.AppearancePage.Header.Font = new Font("Segoe UI Semibold", 9, FontStyle.Bold);
                    newPageGarson.Appearance.Header.ForeColor = Color.DimGray;
                    newPageGarson.TabPageWidth = 100;
                    frmduzen.Show();
                }
            }
        }//garsonformu
        private void btnyenikayit_MouseDown(object sender, MouseEventArgs e)
        {
        }
        
        private void btnduzenle_MouseDown(object sender, MouseEventArgs e)
        {

        }
        private void btnlistele_MouseDown(object sender, MouseEventArgs e)
        {

        }

       
        private void btnyenikayit_MouseHover(object sender, EventArgs e)
        {
            pnlyeni.BackColor = Color.DimGray;
        }
        private void btnyenikayit_MouseLeave(object sender, EventArgs e)
        {
            pnlyeni.BackColor = Color.White;
        }
        private void btnduzenle_MouseHover(object sender, EventArgs e)
        {
            pnlduzenle.BackColor = Color.DimGray;
        }
        private void btnduzenle_MouseLeave(object sender, EventArgs e)
        {
            pnlduzenle.BackColor = Color.White;
        }
        private void btnlistele_MouseHover(object sender, EventArgs e)
        {
            pnlliste.BackColor = Color.DimGray;
        }
        private void btnlistele_MouseLeave(object sender, EventArgs e)
        {
            pnlliste.BackColor = Color.White;
        }

        

        private void btnyeniicon_MouseHover(object sender, EventArgs e)
        {
            pnlyeni.BackColor = Color.DimGray;
        }

        private void btnyeniicon_MouseLeave(object sender, EventArgs e)
        {
            pnlyeni.BackColor = Color.White;
        }

        private void btnediticon_MouseHover(object sender, EventArgs e)
        {
            pnlduzenle.BackColor = Color.DimGray;
        }

        private void btnediticon_MouseLeave(object sender, EventArgs e)
        {
            pnlduzenle.BackColor = Color.White;
        }

        private void FormGarsonIslem_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            offset = e.Location;
        }

        private void FormGarsonIslem_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }

        private void FormGarsonIslem_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point currentScreenPos = PointToScreen(e.Location);
                Location = new
                Point(currentScreenPos.X - offset.X,
                currentScreenPos.Y - offset.Y);
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            offset = e.Location;
        }

        private void FormGarsonIslem_Load(object sender, EventArgs e)
        {
            label1.Text = Localization.btntanimlar;
            waiterTable = Localization.waiterTable;
            listWaiters = Localization.listWaiter;
            editWaiters = Localization.editWaiter;
            btnyenikayit.Text = Localization.btnNew;
            btnduzenle.Text = Localization.editWaiter;
            btnlistele.Text = Localization.listWaiter;
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point currentScreenPos = PointToScreen(e.Location);
                Location = new
                Point(currentScreenPos.X - offset.X,
                currentScreenPos.Y - offset.Y);
            }
        }

        
    }
}
