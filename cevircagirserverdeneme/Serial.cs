﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Ports;
using System.Linq;
using System.Management;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cevircagirserverdeneme
{
    class Serial
    {
        /// <summary>
        /// ///////////    0 ise wifi, 1 ise serial  -->>default ayar wifi
        /// </summary>
        public static String incomedata, outcomedata, portt=null;
        static WhenDataComes wdctip;
        private static readonly Serial singletonser = new Serial();
        private static readonly object _lock = new object();
        private static readonly object _lockflag = new object();
        private static readonly List<String> vers = new List<String>();
        SerialPort _serialPort;
        public System.Timers.Timer tmrSerialFix,tmrwrite;
        public bool writetimer = false , portok = false;
        int bytelength = 0;
        bool globalcommand = false;
        frmErrorPage frmerr;
        //private static readonly List<Socket> clients = new List<Socket>();
        public bool tcpstartok = false;
        static Serial()
        {

        }

        private Serial()   //constructor
        {
            //Console.WriteLine("SERIALPORT CONSTRUCTOR");
            wdctip = new WhenDataComes();
            outcomedata = "";
            tmrwrite = new System.Timers.Timer();
            tmrwrite.Interval = 50;
            tmrwrite.Elapsed += onTimeoutWrite;

        }
        public static Serial singletonterobj
        {
            get
            {
                return singletonser;
            }
        }
        public String ocd  //outcomedata get set method
        {
            get
            {
                return outcomedata;
            }
            set
            {
                outcomedata = value;
            }
        }
        public void startSerial()
        {
            _serialPort = new SerialPort(portt , 9600, Parity.None, 8, StopBits.One);
            _serialPort.Handshake = Handshake.None;
            _serialPort.DataReceived += new SerialDataReceivedEventHandler(sp_DataReceived);
            try
            {
                if (!(_serialPort.IsOpen))
                    _serialPort.Open();
                if(_serialPort.IsOpen)
                    firstmessage();
            }
            catch (Exception ex)
            {
                //Console.WriteLine("seri port hata " + ex.Message);
            }
        }
        public String portnumm  //outcomedata get set method
        {
            get
            {
                return portt;
            }
            set
            {
                portt = value;
            }
        }
        public String[] getCh
        {
            get
            {
                return vers.ToArray();
            }
        }

        public void sp_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            //Console.WriteLine("veri geldi");
            try
            {
                string data = BitConverter.ToString(Encoding.ASCII.GetBytes(_serialPort.ReadTo("\r")));
                data = data.Replace("-", "");
                _serialPort.DiscardInBuffer();

                //Console.WriteLine("dataaa " + data + " ///" + data.Length);
                if (data.Length > 0 || _serialPort.BytesToRead > 0)
                {
                    if (data.Contains("43484e"))  //kanal bilgisi
                    {
                        //kanal bilgisi ile işim yok
                    }
                    if (data.Contains("5252"))   //versiyon bilgisi
                    {
                        vers.Add(data.Substring(4, 4) + "-" + (Convert.ToInt32(data.Substring(8, 2)) - 30) + "-" + data.Substring(11, 1) + "." + data.Substring(13, 1));
                    }
                    else
                    {
                        //gelen veri kanal ve versiyon dışında birşey göndermişse kontrolü eklenecek
                        data = data.Substring(0, 2) + "" + data.Substring(2, 2) + "-" + data.Substring(10, 2) + ":" + data.Substring(12, 2) + ":" + data.Substring(14, 2);
                        wdctip.GetData(data);
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        
        public void sp_SendGlobal()   //global komut gönderme
        {
            globalcommand = true;
            sp_SendData();
        }
        public void sp_SendData()
        {
            try
            {
                string snd = "234324";
                snd += outcomedata;
                byte[] by = new byte[7];
                by = StringToByteArray(snd);
                serialfix();
                if (portok && _serialPort.IsOpen)    //cihaz takılı mı anlamak için
                {
                    if (globalcommand)
                    {
                        frmGlobal frmglo = new frmGlobal();
                        frmglo.StartPosition = FormStartPosition.Manual;
                        frmglo.Left = frmadmin.ActiveForm.Left;
                        frmglo.Top = frmadmin.ActiveForm.Top;
                        frmglo.Show();
                        globalcommand = false;
                    }
                    _serialPort.Write(by, 0, by.Length);  //(Encoding.ASCII.GetBytes(snd), 0, Encoding.ASCII.GetBytes(snd).Length)
                    writetimer = false;
                    tmrwrite.Start();
                }
                else
                {
                    //Console.WriteLine("cihaz takılı değil!!");
                }
                outcomedata = "";
            }
            catch (Exception ex)
            {
               // Console.WriteLine(ex.Message);
            }       
        }

        public void firstmessage()
        {//cihaz bağlandığında(seri port açılınca) bir kerelik kırmızı kim sorgusu at
            outcomedata += "FA070E0D";
            sp_SendData();
            outcomedata = "";
        }

        public void onTimeoutWrite(object sender, EventArgs e)
        {
            if(_serialPort.BytesToWrite == 0 )
            {
                _serialPort.DiscardOutBuffer();
            }
            writetimer = true;
            tmrwrite.Stop();
        }
        
        public void serialfix()
        {
            portok = false;
            ComPortNames();
            if(_serialPort == null && portt != null)    //daha önce port açılmamışsa, mesela uygulama açıldığında cihaz otomatik algılanmamışsa
                _serialPort = new SerialPort(portt, 9600, Parity.None, 8, StopBits.One);
            if (!portok)
            {
                //Console.WriteLine("cihaz takılı değil");
                //uyarı ver
            }
            else if(portok && !_serialPort.IsOpen)
            {
                //Console.WriteLine("cihaz takılı, port açık değil");
                _serialPort = null;
                startSerial();
                //port aç
            }
            else
            {
                //Console.WriteLine("sorun yok devam");
            }

        }
        public static ConnectionOptions ProcessConnectionOptions()
        {
            ConnectionOptions options = new ConnectionOptions();
            options.Impersonation = ImpersonationLevel.Impersonate;
            options.Authentication = AuthenticationLevel.Default;
            options.EnablePrivileges = true;
            return options;
        }

        public static ManagementScope ConnectionScope(string machineName, ConnectionOptions options, string path)
        {
            ManagementScope connectScope = new ManagementScope();
            connectScope.Path = new ManagementPath(@"\\" + machineName + path);
            connectScope.Options = options;
            connectScope.Connect();
            return connectScope;
        }
        public void ComPortNames()
        {
            //List<COMPortInfo> comPortInfoList = new List<COMPortInfo>();
            try
            {

                ConnectionOptions options = ProcessConnectionOptions();
                ManagementScope connectionScope = ConnectionScope(Environment.MachineName, options, @"\root\CIMV2");

                ObjectQuery objectQuery = new ObjectQuery("SELECT * FROM Win32_PnPEntity WHERE ConfigManagerErrorCode = 0");
                ManagementObjectSearcher comPortSearcher = new ManagementObjectSearcher(connectionScope, objectQuery);

                using (comPortSearcher)
                {
                    string caption = null;
                    foreach (ManagementObject obj in comPortSearcher.Get())
                    {
                        if (obj != null)
                        {
                            object captionObj = obj["Caption"];
                            if (captionObj != null)
                            {
                                caption = captionObj.ToString();
                                if (caption.Contains("Silicon"))    //silicon labs ifadesi içeriyorsa
                                {
                                    if (caption.Contains("(COM"))
                                    {
                                        portok = true;
                                        portt = caption.Substring(caption.LastIndexOf("(COM")).Replace("(", string.Empty).Replace(")", string.Empty);
                                        Console.WriteLine(portt);
                                    }

                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {

            }

        }
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            //Console.WriteLine(hex.ToString());
            return hex.ToString();
        }

        public static byte[] StringToByteArray(string hex)
        {
            //hex.Replace("U+00","");
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte (hex.Substring(i, 2),16);
            return bytes;
        }
        public static string DecodeEncodedNonAsciiCharacters(string value)
        {
            return Regex.Replace(
                value,
                @"\\u(?<Value>[a-zA-Z0-9]{4})",
                m => {
                    return ((char)int.Parse(m.Groups["Value"].Value, NumberStyles.HexNumber)).ToString();
                });
        }
        

        public void onTimeout(object sender, EventArgs e)   //gerek kalmamıştı
        {
            //serialfix();
            
        }

    }
}
