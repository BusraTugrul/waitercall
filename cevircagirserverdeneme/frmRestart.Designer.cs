﻿namespace cevircagirserverdeneme
{
    partial class frmRestart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRestart));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnLater = new DevExpress.XtraEditors.SimpleButton();
            this.btnRestart = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.InitialImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(379, 115);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(317, 309);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 29;
            this.pictureBox1.TabStop = false;
            // 
            // labelControl4
            // 
            this.labelControl4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelControl4.Appearance.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Bold);
            this.labelControl4.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            this.labelControl4.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.None;
            this.labelControl4.Location = new System.Drawing.Point(261, 470);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(727, 23);
            this.labelControl4.TabIndex = 76;
            this.labelControl4.Text = "Ayarların gerçekleşmesi için lütfen uygulamayı kapatıp yeniden başlatın.";
            this.labelControl4.Click += new System.EventHandler(this.labelControl4_Click);
            // 
            // btnLater
            // 
            this.btnLater.AllowFocus = false;
            this.btnLater.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLater.Appearance.BackColor = System.Drawing.Color.Orange;
            this.btnLater.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnLater.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnLater.Appearance.Options.UseBackColor = true;
            this.btnLater.Appearance.Options.UseFont = true;
            this.btnLater.Appearance.Options.UseForeColor = true;
            this.btnLater.AppearanceHovered.BackColor = System.Drawing.Color.White;
            this.btnLater.AppearanceHovered.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.btnLater.AppearanceHovered.ForeColor = System.Drawing.Color.DimGray;
            this.btnLater.AppearanceHovered.Options.UseBackColor = true;
            this.btnLater.AppearanceHovered.Options.UseFont = true;
            this.btnLater.AppearanceHovered.Options.UseForeColor = true;
            this.btnLater.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnLater.Location = new System.Drawing.Point(25, 216);
            this.btnLater.Name = "btnLater";
            this.btnLater.Size = new System.Drawing.Size(235, 79);
            this.btnLater.TabIndex = 77;
            this.btnLater.Text = "Daha Sonra";
            this.btnLater.Visible = false;
            this.btnLater.Click += new System.EventHandler(this.btnLater_Click);
            // 
            // btnRestart
            // 
            this.btnRestart.AllowFocus = false;
            this.btnRestart.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnRestart.Appearance.BackColor = System.Drawing.Color.Orange;
            this.btnRestart.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnRestart.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnRestart.Appearance.Options.UseBackColor = true;
            this.btnRestart.Appearance.Options.UseFont = true;
            this.btnRestart.Appearance.Options.UseForeColor = true;
            this.btnRestart.AppearanceHovered.BackColor = System.Drawing.Color.White;
            this.btnRestart.AppearanceHovered.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.btnRestart.AppearanceHovered.ForeColor = System.Drawing.Color.DimGray;
            this.btnRestart.AppearanceHovered.Options.UseBackColor = true;
            this.btnRestart.AppearanceHovered.Options.UseFont = true;
            this.btnRestart.AppearanceHovered.Options.UseForeColor = true;
            this.btnRestart.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnRestart.Location = new System.Drawing.Point(422, 542);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(235, 79);
            this.btnRestart.TabIndex = 78;
            this.btnRestart.Text = "Uygulamayı Kapat";
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // frmRestart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1100, 650);
            this.Controls.Add(this.btnRestart);
            this.Controls.Add(this.btnLater);
            this.Controls.Add(this.labelControl4);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmRestart";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmRestart";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmRestart_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        public DevExpress.XtraEditors.SimpleButton btnLater;
        public DevExpress.XtraEditors.SimpleButton btnRestart;
    }
}