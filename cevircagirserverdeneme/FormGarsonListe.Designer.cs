﻿namespace cevircagirserverdeneme
{
    partial class FormGarsonListe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGarsonListe));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.pnlislem = new DevExpress.XtraEditors.PanelControl();
            this.btnnewrecord = new DevExpress.XtraEditors.SimpleButton();
            this.btnyeniiliski = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.txtmasaad = new System.Windows.Forms.TextBox();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.txtgarson_ad = new System.Windows.Forms.TextBox();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.btnlistele = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.waitersstablesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.cevircagirDataSet = new cevircagirserverdeneme.cevircagirDataSet();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colwaiter_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.coltable_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.delete = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btn_delete = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.lblformname = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblgarsonbaslik = new System.Windows.Forms.Label();
            this.waitersstablesTableAdapter1 = new cevircagirserverdeneme.cevircagirDataSetTableAdapters.waitersstablesTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.pnlislem)).BeginInit();
            this.pnlislem.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.waitersstablesBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_delete)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlislem
            // 
            this.pnlislem.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("pnlislem.Appearance.BackColor")));
            this.pnlislem.Appearance.Options.UseBackColor = true;
            this.pnlislem.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.pnlislem.Controls.Add(this.btnnewrecord);
            this.pnlislem.Controls.Add(this.btnyeniiliski);
            resources.ApplyResources(this.pnlislem, "pnlislem");
            this.pnlislem.Name = "pnlislem";
            // 
            // btnnewrecord
            // 
            this.btnnewrecord.AllowFocus = false;
            resources.ApplyResources(this.btnnewrecord, "btnnewrecord");
            this.btnnewrecord.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnnewrecord.Appearance.BackColor")));
            this.btnnewrecord.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnnewrecord.Appearance.Font")));
            this.btnnewrecord.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnnewrecord.Appearance.ForeColor")));
            this.btnnewrecord.Appearance.Options.UseBackColor = true;
            this.btnnewrecord.Appearance.Options.UseFont = true;
            this.btnnewrecord.Appearance.Options.UseForeColor = true;
            this.btnnewrecord.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnnewrecord.AppearanceHovered.BackColor")));
            this.btnnewrecord.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnnewrecord.AppearanceHovered.Font")));
            this.btnnewrecord.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnnewrecord.AppearanceHovered.ForeColor")));
            this.btnnewrecord.AppearanceHovered.Options.UseBackColor = true;
            this.btnnewrecord.AppearanceHovered.Options.UseFont = true;
            this.btnnewrecord.AppearanceHovered.Options.UseForeColor = true;
            this.btnnewrecord.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnnewrecord.Name = "btnnewrecord";
            this.btnnewrecord.Click += new System.EventHandler(this.btnnewrecord_Click);
            // 
            // btnyeniiliski
            // 
            this.btnyeniiliski.AllowFocus = false;
            resources.ApplyResources(this.btnyeniiliski, "btnyeniiliski");
            this.btnyeniiliski.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnyeniiliski.Appearance.BackColor")));
            this.btnyeniiliski.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnyeniiliski.Appearance.Font")));
            this.btnyeniiliski.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnyeniiliski.Appearance.ForeColor")));
            this.btnyeniiliski.Appearance.Options.UseBackColor = true;
            this.btnyeniiliski.Appearance.Options.UseFont = true;
            this.btnyeniiliski.Appearance.Options.UseForeColor = true;
            this.btnyeniiliski.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnyeniiliski.AppearanceHovered.BackColor")));
            this.btnyeniiliski.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnyeniiliski.AppearanceHovered.Font")));
            this.btnyeniiliski.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnyeniiliski.AppearanceHovered.ForeColor")));
            this.btnyeniiliski.AppearanceHovered.Options.UseBackColor = true;
            this.btnyeniiliski.AppearanceHovered.Options.UseFont = true;
            this.btnyeniiliski.AppearanceHovered.Options.UseForeColor = true;
            this.btnyeniiliski.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnyeniiliski.Name = "btnyeniiliski";
            this.btnyeniiliski.Click += new System.EventHandler(this.btnyeniiliski_Click);
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("panelControl2.Appearance.BackColor")));
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.txtmasaad);
            this.panelControl2.Controls.Add(this.labelControl2);
            this.panelControl2.Controls.Add(this.txtgarson_ad);
            this.panelControl2.Controls.Add(this.labelControl1);
            this.panelControl2.Controls.Add(this.btnlistele);
            resources.ApplyResources(this.panelControl2, "panelControl2");
            this.panelControl2.Name = "panelControl2";
            // 
            // txtmasaad
            // 
            this.txtmasaad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtmasaad, "txtmasaad");
            this.txtmasaad.Name = "txtmasaad";
            this.txtmasaad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmasaad_KeyPress);
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl2.Appearance.Font")));
            this.labelControl2.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl2.Appearance.ForeColor")));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.labelControl2, "labelControl2");
            this.labelControl2.Name = "labelControl2";
            // 
            // txtgarson_ad
            // 
            this.txtgarson_ad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtgarson_ad, "txtgarson_ad");
            this.txtgarson_ad.Name = "txtgarson_ad";
            this.txtgarson_ad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtgarson_ad_KeyPress);
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl1.Appearance.Font")));
            this.labelControl1.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl1.Appearance.ForeColor")));
            this.labelControl1.Appearance.Options.UseFont = true;
            this.labelControl1.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.labelControl1, "labelControl1");
            this.labelControl1.Name = "labelControl1";
            // 
            // btnlistele
            // 
            this.btnlistele.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnlistele.Appearance.BackColor")));
            this.btnlistele.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnlistele.Appearance.Font")));
            this.btnlistele.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnlistele.Appearance.ForeColor")));
            this.btnlistele.Appearance.Options.UseBackColor = true;
            this.btnlistele.Appearance.Options.UseFont = true;
            this.btnlistele.Appearance.Options.UseForeColor = true;
            this.btnlistele.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnlistele.AppearanceHovered.BackColor")));
            this.btnlistele.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnlistele.AppearanceHovered.Font")));
            this.btnlistele.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnlistele.AppearanceHovered.ForeColor")));
            this.btnlistele.AppearanceHovered.Options.UseBackColor = true;
            this.btnlistele.AppearanceHovered.Options.UseFont = true;
            this.btnlistele.AppearanceHovered.Options.UseForeColor = true;
            this.btnlistele.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnlistele, "btnlistele");
            this.btnlistele.Name = "btnlistele";
            this.btnlistele.Click += new System.EventHandler(this.btnlistele_Click);
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.waitersstablesBindingSource1;
            resources.ApplyResources(this.gridControl1, "gridControl1");
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btn_delete});
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // waitersstablesBindingSource1
            // 
            this.waitersstablesBindingSource1.DataMember = "waitersstables";
            this.waitersstablesBindingSource1.DataSource = this.cevircagirDataSet;
            // 
            // cevircagirDataSet
            // 
            this.cevircagirDataSet.DataSetName = "cevircagirDataSet";
            this.cevircagirDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FocusedCell.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.FocusedCell.BackColor")));
            this.gridView1.Appearance.FocusedCell.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.FocusedCell.Font")));
            this.gridView1.Appearance.FocusedCell.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.FocusedCell.ForeColor")));
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.FocusedRow.BackColor")));
            this.gridView1.Appearance.FocusedRow.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.FocusedRow.Font")));
            this.gridView1.Appearance.FocusedRow.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.FocusedRow.ForeColor")));
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.GroupPanel.BackColor")));
            this.gridView1.Appearance.GroupPanel.BackColor2 = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.GroupPanel.BackColor2")));
            this.gridView1.Appearance.GroupPanel.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.GroupPanel.Font")));
            this.gridView1.Appearance.GroupPanel.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.GroupPanel.ForeColor")));
            this.gridView1.Appearance.GroupPanel.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("gridView1.Appearance.GroupPanel.GradientMode")));
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.GroupRow.BackColor")));
            this.gridView1.Appearance.GroupRow.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.GroupRow.Font")));
            this.gridView1.Appearance.GroupRow.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.GroupRow.ForeColor")));
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.HeaderPanel.BackColor")));
            this.gridView1.Appearance.HeaderPanel.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.HeaderPanel.Font")));
            this.gridView1.Appearance.HeaderPanel.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.HeaderPanel.ForeColor")));
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.HideSelectionRow.BackColor")));
            this.gridView1.Appearance.HideSelectionRow.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.HideSelectionRow.Font")));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.HideSelectionRow.ForeColor")));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.Row.Font")));
            this.gridView1.Appearance.Row.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.Row.ForeColor")));
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colwaiter_name,
            this.coltable_name,
            this.delete});
            this.gridView1.GridControl = this.gridControl1;
            resources.ApplyResources(this.gridView1, "gridView1");
            this.gridView1.Name = "gridView1";
            this.gridView1.PaintStyleName = "Web";
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.coltable_name, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colwaiter_name
            // 
            resources.ApplyResources(this.colwaiter_name, "colwaiter_name");
            this.colwaiter_name.FieldName = "waiter_name";
            this.colwaiter_name.Name = "colwaiter_name";
            this.colwaiter_name.OptionsColumn.AllowShowHide = false;
            // 
            // coltable_name
            // 
            resources.ApplyResources(this.coltable_name, "coltable_name");
            this.coltable_name.FieldName = "table_name";
            this.coltable_name.Name = "coltable_name";
            this.coltable_name.OptionsColumn.AllowShowHide = false;
            // 
            // delete
            // 
            this.delete.ColumnEdit = this.btn_delete;
            this.delete.Name = "delete";
            this.delete.OptionsColumn.AllowMove = false;
            this.delete.OptionsColumn.AllowShowHide = false;
            resources.ApplyResources(this.delete, "delete");
            // 
            // btn_delete
            // 
            this.btn_delete.AppearanceFocused.BackColor = ((System.Drawing.Color)(resources.GetObject("btn_delete.AppearanceFocused.BackColor")));
            this.btn_delete.AppearanceFocused.Options.UseBackColor = true;
            resources.ApplyResources(this.btn_delete, "btn_delete");
            resources.ApplyResources(serializableAppearanceObject5, "serializableAppearanceObject5");
            serializableAppearanceObject5.Options.UseBackColor = true;
            resources.ApplyResources(serializableAppearanceObject6, "serializableAppearanceObject6");
            serializableAppearanceObject6.Options.UseBackColor = true;
            serializableAppearanceObject6.Options.UseBorderColor = true;
            resources.ApplyResources(serializableAppearanceObject8, "serializableAppearanceObject8");
            serializableAppearanceObject8.Options.UseBackColor = true;
            serializableAppearanceObject8.Options.UseBorderColor = true;
            this.btn_delete.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("btn_delete.Buttons"))), resources.GetString("btn_delete.Buttons1"), ((int)(resources.GetObject("btn_delete.Buttons2"))), ((bool)(resources.GetObject("btn_delete.Buttons3"))), ((bool)(resources.GetObject("btn_delete.Buttons4"))), ((bool)(resources.GetObject("btn_delete.Buttons5"))), ((DevExpress.XtraEditors.ImageLocation)(resources.GetObject("btn_delete.Buttons6"))), global::cevircagirserverdeneme.Properties.Resources.icons8_Delete_20, resources.GetString("btn_delete.Buttons7"), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, serializableAppearanceObject6, serializableAppearanceObject7, serializableAppearanceObject8, resources.GetString("btn_delete.Buttons8"), ((object)(resources.GetObject("btn_delete.Buttons9"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("btn_delete.Buttons10"))), ((bool)(resources.GetObject("btn_delete.Buttons11"))))});
            this.btn_delete.Name = "btn_delete";
            this.btn_delete.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.HideTextEditor;
            this.btn_delete.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btn_delete_ButtonClick);
            // 
            // lblformname
            // 
            resources.ApplyResources(this.lblformname, "lblformname");
            this.lblformname.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblformname.ForeColor = System.Drawing.Color.DimGray;
            this.lblformname.Name = "lblformname";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.lblgarsonbaslik);
            this.panel1.Controls.Add(this.lblformname);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // lblgarsonbaslik
            // 
            resources.ApplyResources(this.lblgarsonbaslik, "lblgarsonbaslik");
            this.lblgarsonbaslik.ForeColor = System.Drawing.Color.Orange;
            this.lblgarsonbaslik.Name = "lblgarsonbaslik";
            // 
            // waitersstablesTableAdapter1
            // 
            this.waitersstablesTableAdapter1.ClearBeforeFill = true;
            // 
            // FormGarsonListe
            // 
            this.AcceptButton = this.btnlistele;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panelControl2);
            this.Controls.Add(this.pnlislem);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormGarsonListe";
            this.Load += new System.EventHandler(this.FormGarsonListe_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pnlislem)).EndInit();
            this.pnlislem.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.waitersstablesBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btn_delete)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.PanelControl pnlislem;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colwaiter_name;
        private DevExpress.XtraGrid.Columns.GridColumn coltable_name;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraGrid.Columns.GridColumn delete;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btn_delete;
        private DevExpress.XtraEditors.SimpleButton btnyeniiliski;
        private System.Windows.Forms.TextBox txtgarson_ad;
        private System.Windows.Forms.TextBox txtmasaad;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraEditors.SimpleButton btnlistele;
        public System.Windows.Forms.Label lblformname;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Label lblgarsonbaslik;
        private cevircagirDataSet cevircagirDataSet;
        private System.Windows.Forms.BindingSource waitersstablesBindingSource1;
        private cevircagirDataSetTableAdapters.waitersstablesTableAdapter waitersstablesTableAdapter1;
        private DevExpress.XtraEditors.SimpleButton btnnewrecord;
    }
}