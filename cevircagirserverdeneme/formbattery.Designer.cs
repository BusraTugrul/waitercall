﻿namespace cevircagirserverdeneme
{
    partial class formbattery
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblgarsonbaslik = new System.Windows.Forms.Label();
            this.lblformname = new System.Windows.Forms.Label();
            this.pnllayout = new System.Windows.Forms.FlowLayoutPanel();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.lblgarsonbaslik);
            this.panel3.Controls.Add(this.lblformname);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(2);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(825, 27);
            this.panel3.TabIndex = 21;
            // 
            // lblgarsonbaslik
            // 
            this.lblgarsonbaslik.AutoSize = true;
            this.lblgarsonbaslik.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblgarsonbaslik.ForeColor = System.Drawing.Color.Orange;
            this.lblgarsonbaslik.Location = new System.Drawing.Point(9, 8);
            this.lblgarsonbaslik.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblgarsonbaslik.Name = "lblgarsonbaslik";
            this.lblgarsonbaslik.Size = new System.Drawing.Size(0, 13);
            this.lblgarsonbaslik.TabIndex = 13;
            // 
            // lblformname
            // 
            this.lblformname.AccessibleName = "";
            this.lblformname.AutoSize = true;
            this.lblformname.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblformname.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblformname.ForeColor = System.Drawing.Color.Orange;
            this.lblformname.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblformname.Location = new System.Drawing.Point(3, 6);
            this.lblformname.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblformname.Name = "lblformname";
            this.lblformname.Size = new System.Drawing.Size(102, 13);
            this.lblformname.TabIndex = 12;
            this.lblformname.Text = "Cihaz Pil Durumları";
            this.lblformname.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lblformname.Click += new System.EventHandler(this.lblformname_Click);
            // 
            // pnllayout
            // 
            this.pnllayout.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnllayout.Location = new System.Drawing.Point(0, 27);
            this.pnllayout.Margin = new System.Windows.Forms.Padding(2);
            this.pnllayout.Name = "pnllayout";
            this.pnllayout.Size = new System.Drawing.Size(825, 464);
            this.pnllayout.TabIndex = 22;
            // 
            // formbattery
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(825, 491);
            this.Controls.Add(this.pnllayout);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "formbattery";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.formbattery_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Label lblgarsonbaslik;
        public System.Windows.Forms.Label lblformname;
        private System.Windows.Forms.FlowLayoutPanel pnllayout;
    }
}