﻿namespace cevircagirserverdeneme
{
    partial class FormRaporlar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRaporlar));
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.txtgarson = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnsongun = new DevExpress.XtraEditors.SimpleButton();
            this.reportsTableAdapter = new cevircagirserverdeneme.cevircagirDataSet5TableAdapters.reportsTableAdapter();
            this.colreply_duration = new DevExpress.XtraGrid.Columns.GridColumn();
            this.pnlsum = new System.Windows.Forms.Panel();
            this.txtsum = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblgarsonbaslik = new System.Windows.Forms.Label();
            this.lblformname = new System.Windows.Forms.Label();
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.tabraporlar = new DevExpress.XtraTab.XtraTabPage();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.cevircagirDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cevircagirDataSet = new cevircagirserverdeneme.cevircagirDataSet();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.coltable_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colreq_date = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colwaiter_name = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colreq_time = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colreply_time = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colreply_dur = new DevExpress.XtraGrid.Columns.GridColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtraphaftalik = new DevExpress.XtraEditors.SimpleButton();
            this.txtrapaylık = new DevExpress.XtraEditors.SimpleButton();
            this.txtrapyillik = new DevExpress.XtraEditors.SimpleButton();
            this.txtrapgunluk = new DevExpress.XtraEditors.SimpleButton();
            this.tabgrafikler = new DevExpress.XtraTab.XtraTabPage();
            this.chartControl1 = new DevExpress.XtraCharts.ChartControl();
            this.reportsTableAdapter1 = new cevircagirserverdeneme.cevircagirDataSetTableAdapters.reportsTableAdapter();
            this.reportsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel2 = new System.Windows.Forms.Panel();
            this.dateedtgrafik = new DevExpress.XtraEditors.DateEdit();
            this.label3 = new System.Windows.Forms.Label();
            this.txtgrhaftalik = new DevExpress.XtraEditors.SimpleButton();
            this.txtgraylik = new DevExpress.XtraEditors.SimpleButton();
            this.txtgryillik = new DevExpress.XtraEditors.SimpleButton();
            this.txtgrgunluk = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.label1 = new System.Windows.Forms.Label();
            this.txtcevapsure = new System.Windows.Forms.TextBox();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.txtgarsonismi = new System.Windows.Forms.TextBox();
            this.txtmasaismi = new System.Windows.Forms.TextBox();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.btnfiltrele = new DevExpress.XtraEditors.SimpleButton();
            this.edtbitistarih = new DevExpress.XtraEditors.DateEdit();
            this.edtbaslangictarih = new DevExpress.XtraEditors.DateEdit();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            this.pnlsum.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.tabraporlar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.panel1.SuspendLayout();
            this.tabgrafikler.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportsBindingSource)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateedtgrafik.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateedtgrafik.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtbitistarih.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtbitistarih.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtbaslangictarih.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtbaslangictarih.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // panelControl2
            // 
            this.panelControl2.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("panelControl2.Appearance.BackColor")));
            this.panelControl2.Appearance.Options.UseBackColor = true;
            this.panelControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl2.Controls.Add(this.txtgarson);
            this.panelControl2.Controls.Add(this.label2);
            this.panelControl2.Controls.Add(this.simpleButton3);
            this.panelControl2.Controls.Add(this.simpleButton2);
            this.panelControl2.Controls.Add(this.simpleButton1);
            this.panelControl2.Controls.Add(this.btnsongun);
            resources.ApplyResources(this.panelControl2, "panelControl2");
            this.panelControl2.Name = "panelControl2";
            // 
            // txtgarson
            // 
            this.txtgarson.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtgarson, "txtgarson");
            this.txtgarson.Name = "txtgarson";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton3.Appearance.BackColor")));
            this.simpleButton3.Appearance.BorderColor = ((System.Drawing.Color)(resources.GetObject("simpleButton3.Appearance.BorderColor")));
            this.simpleButton3.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton3.Appearance.Font")));
            this.simpleButton3.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton3.Appearance.ForeColor")));
            this.simpleButton3.Appearance.Options.UseBackColor = true;
            this.simpleButton3.Appearance.Options.UseBorderColor = true;
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Appearance.Options.UseForeColor = true;
            this.simpleButton3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton3, "simpleButton3");
            this.simpleButton3.Name = "simpleButton3";
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton2.Appearance.BackColor")));
            this.simpleButton2.Appearance.BorderColor = ((System.Drawing.Color)(resources.GetObject("simpleButton2.Appearance.BorderColor")));
            this.simpleButton2.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton2.Appearance.Font")));
            this.simpleButton2.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton2.Appearance.ForeColor")));
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseBorderColor = true;
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Appearance.Options.UseForeColor = true;
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton2, "simpleButton2");
            this.simpleButton2.Name = "simpleButton2";
            // 
            // simpleButton1
            // 
            this.simpleButton1.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton1.Appearance.BackColor")));
            this.simpleButton1.Appearance.BorderColor = ((System.Drawing.Color)(resources.GetObject("simpleButton1.Appearance.BorderColor")));
            this.simpleButton1.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton1.Appearance.Font")));
            this.simpleButton1.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton1.Appearance.ForeColor")));
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseBorderColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Appearance.Options.UseForeColor = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton1, "simpleButton1");
            this.simpleButton1.Name = "simpleButton1";
            // 
            // btnsongun
            // 
            this.btnsongun.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnsongun.Appearance.BackColor")));
            this.btnsongun.Appearance.BorderColor = ((System.Drawing.Color)(resources.GetObject("btnsongun.Appearance.BorderColor")));
            this.btnsongun.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnsongun.Appearance.Font")));
            this.btnsongun.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnsongun.Appearance.ForeColor")));
            this.btnsongun.Appearance.Options.UseBackColor = true;
            this.btnsongun.Appearance.Options.UseBorderColor = true;
            this.btnsongun.Appearance.Options.UseFont = true;
            this.btnsongun.Appearance.Options.UseForeColor = true;
            this.btnsongun.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnsongun, "btnsongun");
            this.btnsongun.Name = "btnsongun";
            // 
            // reportsTableAdapter
            // 
            this.reportsTableAdapter.ClearBeforeFill = true;
            // 
            // colreply_duration
            // 
            this.colreply_duration.AppearanceCell.Options.UseTextOptions = true;
            this.colreply_duration.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            resources.ApplyResources(this.colreply_duration, "colreply_duration");
            this.colreply_duration.FieldName = "reply_duration";
            this.colreply_duration.GroupFormat.FormatString = "sn";
            this.colreply_duration.GroupFormat.FormatType = DevExpress.Utils.FormatType.Custom;
            this.colreply_duration.Name = "colreply_duration";
            // 
            // pnlsum
            // 
            this.pnlsum.BackColor = System.Drawing.Color.White;
            this.pnlsum.Controls.Add(this.txtsum);
            resources.ApplyResources(this.pnlsum, "pnlsum");
            this.pnlsum.Name = "pnlsum";
            // 
            // txtsum
            // 
            this.txtsum.BackColor = System.Drawing.Color.White;
            this.txtsum.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtsum, "txtsum");
            this.txtsum.ForeColor = System.Drawing.Color.DimGray;
            this.txtsum.Name = "txtsum";
            this.txtsum.ReadOnly = true;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.lblgarsonbaslik);
            this.panel3.Controls.Add(this.lblformname);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // lblgarsonbaslik
            // 
            resources.ApplyResources(this.lblgarsonbaslik, "lblgarsonbaslik");
            this.lblgarsonbaslik.ForeColor = System.Drawing.Color.Orange;
            this.lblgarsonbaslik.Name = "lblgarsonbaslik";
            // 
            // lblformname
            // 
            resources.ApplyResources(this.lblformname, "lblformname");
            this.lblformname.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblformname.ForeColor = System.Drawing.Color.Orange;
            this.lblformname.Name = "lblformname";
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("xtraTabControl1.Appearance.BackColor")));
            this.xtraTabControl1.Appearance.BackColor2 = ((System.Drawing.Color)(resources.GetObject("xtraTabControl1.Appearance.BackColor2")));
            this.xtraTabControl1.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("xtraTabControl1.Appearance.Font")));
            this.xtraTabControl1.Appearance.Options.UseBackColor = true;
            this.xtraTabControl1.Appearance.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.Header.BackColor = ((System.Drawing.Color)(resources.GetObject("xtraTabControl1.AppearancePage.Header.BackColor")));
            this.xtraTabControl1.AppearancePage.Header.BackColor2 = ((System.Drawing.Color)(resources.GetObject("xtraTabControl1.AppearancePage.Header.BackColor2")));
            this.xtraTabControl1.AppearancePage.Header.Font = ((System.Drawing.Font)(resources.GetObject("xtraTabControl1.AppearancePage.Header.Font")));
            this.xtraTabControl1.AppearancePage.Header.ForeColor = ((System.Drawing.Color)(resources.GetObject("xtraTabControl1.AppearancePage.Header.ForeColor")));
            this.xtraTabControl1.AppearancePage.Header.Options.UseBackColor = true;
            this.xtraTabControl1.AppearancePage.Header.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.Header.Options.UseForeColor = true;
            this.xtraTabControl1.AppearancePage.HeaderActive.BackColor = ((System.Drawing.Color)(resources.GetObject("xtraTabControl1.AppearancePage.HeaderActive.BackColor")));
            this.xtraTabControl1.AppearancePage.HeaderActive.Font = ((System.Drawing.Font)(resources.GetObject("xtraTabControl1.AppearancePage.HeaderActive.Font")));
            this.xtraTabControl1.AppearancePage.HeaderActive.ForeColor = ((System.Drawing.Color)(resources.GetObject("xtraTabControl1.AppearancePage.HeaderActive.ForeColor")));
            this.xtraTabControl1.AppearancePage.HeaderActive.Options.UseBackColor = true;
            this.xtraTabControl1.AppearancePage.HeaderActive.Options.UseFont = true;
            this.xtraTabControl1.AppearancePage.HeaderActive.Options.UseForeColor = true;
            resources.ApplyResources(this.xtraTabControl1, "xtraTabControl1");
            this.xtraTabControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Style3D;
            this.xtraTabControl1.LookAndFeel.UseDefaultLookAndFeel = false;
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.tabraporlar;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabraporlar,
            this.tabgrafikler});
            this.xtraTabControl1.SelectedPageChanged += new DevExpress.XtraTab.TabPageChangedEventHandler(this.xtraTabControl1_SelectedPageChanged);
            this.xtraTabControl1.Click += new System.EventHandler(this.xtraTabControl1_Click);
            // 
            // tabraporlar
            // 
            this.tabraporlar.Controls.Add(this.gridControl1);
            this.tabraporlar.Controls.Add(this.panel1);
            this.tabraporlar.Name = "tabraporlar";
            resources.ApplyResources(this.tabraporlar, "tabraporlar");
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.cevircagirDataSetBindingSource;
            resources.ApplyResources(this.gridControl1, "gridControl1");
            this.gridControl1.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // cevircagirDataSetBindingSource
            // 
            this.cevircagirDataSetBindingSource.DataSource = this.cevircagirDataSet;
            this.cevircagirDataSetBindingSource.Position = 0;
            // 
            // cevircagirDataSet
            // 
            this.cevircagirDataSet.DataSetName = "cevircagirDataSet";
            this.cevircagirDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // gridView1
            // 
            this.gridView1.Appearance.FixedLine.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.FixedLine.BackColor")));
            this.gridView1.Appearance.FixedLine.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.FixedLine.Font")));
            this.gridView1.Appearance.FixedLine.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.FixedLine.ForeColor")));
            this.gridView1.Appearance.FixedLine.Options.UseBackColor = true;
            this.gridView1.Appearance.FixedLine.Options.UseFont = true;
            this.gridView1.Appearance.FixedLine.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedCell.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.FocusedCell.BackColor")));
            this.gridView1.Appearance.FocusedCell.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.FocusedCell.Font")));
            this.gridView1.Appearance.FocusedCell.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.FocusedCell.ForeColor")));
            this.gridView1.Appearance.FocusedCell.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedCell.Options.UseFont = true;
            this.gridView1.Appearance.FocusedCell.Options.UseForeColor = true;
            this.gridView1.Appearance.FocusedRow.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.FocusedRow.BackColor")));
            this.gridView1.Appearance.FocusedRow.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.FocusedRow.Font")));
            this.gridView1.Appearance.FocusedRow.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.FocusedRow.ForeColor")));
            this.gridView1.Appearance.FocusedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.FocusedRow.Options.UseFont = true;
            this.gridView1.Appearance.FocusedRow.Options.UseForeColor = true;
            this.gridView1.Appearance.FooterPanel.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.FooterPanel.BackColor")));
            this.gridView1.Appearance.FooterPanel.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.FooterPanel.Font")));
            this.gridView1.Appearance.FooterPanel.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.FooterPanel.ForeColor")));
            this.gridView1.Appearance.FooterPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.FooterPanel.Options.UseFont = true;
            this.gridView1.Appearance.FooterPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupPanel.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.GroupPanel.BackColor")));
            this.gridView1.Appearance.GroupPanel.BackColor2 = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.GroupPanel.BackColor2")));
            this.gridView1.Appearance.GroupPanel.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.GroupPanel.Font")));
            this.gridView1.Appearance.GroupPanel.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.GroupPanel.ForeColor")));
            this.gridView1.Appearance.GroupPanel.GradientMode = ((System.Drawing.Drawing2D.LinearGradientMode)(resources.GetObject("gridView1.Appearance.GroupPanel.GradientMode")));
            this.gridView1.Appearance.GroupPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupPanel.Options.UseFont = true;
            this.gridView1.Appearance.GroupPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.GroupRow.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.GroupRow.BackColor")));
            this.gridView1.Appearance.GroupRow.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.GroupRow.Font")));
            this.gridView1.Appearance.GroupRow.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.GroupRow.ForeColor")));
            this.gridView1.Appearance.GroupRow.Options.UseBackColor = true;
            this.gridView1.Appearance.GroupRow.Options.UseFont = true;
            this.gridView1.Appearance.GroupRow.Options.UseForeColor = true;
            this.gridView1.Appearance.HeaderPanel.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.HeaderPanel.BackColor")));
            this.gridView1.Appearance.HeaderPanel.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.HeaderPanel.Font")));
            this.gridView1.Appearance.HeaderPanel.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.HeaderPanel.ForeColor")));
            this.gridView1.Appearance.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseFont = true;
            this.gridView1.Appearance.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.Appearance.HideSelectionRow.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.HideSelectionRow.BackColor")));
            this.gridView1.Appearance.HideSelectionRow.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.HideSelectionRow.Font")));
            this.gridView1.Appearance.HideSelectionRow.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.HideSelectionRow.ForeColor")));
            this.gridView1.Appearance.HideSelectionRow.Options.UseBackColor = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseFont = true;
            this.gridView1.Appearance.HideSelectionRow.Options.UseForeColor = true;
            this.gridView1.Appearance.Row.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.Row.BackColor")));
            this.gridView1.Appearance.Row.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.Row.Font")));
            this.gridView1.Appearance.Row.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.Row.ForeColor")));
            this.gridView1.Appearance.Row.Options.UseBackColor = true;
            this.gridView1.Appearance.Row.Options.UseFont = true;
            this.gridView1.Appearance.Row.Options.UseForeColor = true;
            this.gridView1.Appearance.SelectedRow.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.SelectedRow.BackColor")));
            this.gridView1.Appearance.SelectedRow.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.Appearance.SelectedRow.Font")));
            this.gridView1.Appearance.SelectedRow.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.Appearance.SelectedRow.ForeColor")));
            this.gridView1.Appearance.SelectedRow.Options.UseBackColor = true;
            this.gridView1.Appearance.SelectedRow.Options.UseFont = true;
            this.gridView1.Appearance.SelectedRow.Options.UseForeColor = true;
            this.gridView1.AppearancePrint.FilterPanel.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.AppearancePrint.FilterPanel.Font")));
            this.gridView1.AppearancePrint.FilterPanel.Options.UseFont = true;
            this.gridView1.AppearancePrint.HeaderPanel.BackColor = ((System.Drawing.Color)(resources.GetObject("gridView1.AppearancePrint.HeaderPanel.BackColor")));
            this.gridView1.AppearancePrint.HeaderPanel.Font = ((System.Drawing.Font)(resources.GetObject("gridView1.AppearancePrint.HeaderPanel.Font")));
            this.gridView1.AppearancePrint.HeaderPanel.ForeColor = ((System.Drawing.Color)(resources.GetObject("gridView1.AppearancePrint.HeaderPanel.ForeColor")));
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseBackColor = true;
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseFont = true;
            this.gridView1.AppearancePrint.HeaderPanel.Options.UseForeColor = true;
            this.gridView1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.coltable_name,
            this.colreq_date,
            this.colwaiter_name,
            this.colreq_time,
            this.colreply_time,
            this.colreply_dur});
            this.gridView1.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.None;
            this.gridView1.GridControl = this.gridControl1;
            resources.ApplyResources(this.gridView1, "gridView1");
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsBehavior.ReadOnly = true;
            this.gridView1.PaintStyleName = "Web";
            // 
            // coltable_name
            // 
            this.coltable_name.AppearanceCell.Options.UseTextOptions = true;
            this.coltable_name.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            resources.ApplyResources(this.coltable_name, "coltable_name");
            this.coltable_name.DisplayFormat.FormatString = "dd.MM.yyyy";
            this.coltable_name.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.coltable_name.FieldName = "table_name";
            this.coltable_name.Name = "coltable_name";
            this.coltable_name.OptionsColumn.AllowShowHide = false;
            // 
            // colreq_date
            // 
            this.colreq_date.AppearanceCell.Options.UseTextOptions = true;
            this.colreq_date.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            resources.ApplyResources(this.colreq_date, "colreq_date");
            this.colreq_date.FieldName = "req_date";
            this.colreq_date.Name = "colreq_date";
            this.colreq_date.OptionsColumn.AllowShowHide = false;
            // 
            // colwaiter_name
            // 
            resources.ApplyResources(this.colwaiter_name, "colwaiter_name");
            this.colwaiter_name.FieldName = "waiter_name";
            this.colwaiter_name.Name = "colwaiter_name";
            this.colwaiter_name.OptionsColumn.AllowShowHide = false;
            // 
            // colreq_time
            // 
            this.colreq_time.AppearanceCell.Options.UseTextOptions = true;
            this.colreq_time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            resources.ApplyResources(this.colreq_time, "colreq_time");
            this.colreq_time.FieldName = "req_time";
            this.colreq_time.Name = "colreq_time";
            this.colreq_time.OptionsColumn.AllowShowHide = false;
            // 
            // colreply_time
            // 
            this.colreply_time.AppearanceCell.Options.UseTextOptions = true;
            this.colreply_time.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            resources.ApplyResources(this.colreply_time, "colreply_time");
            this.colreply_time.FieldName = "reply_time";
            this.colreply_time.Name = "colreply_time";
            this.colreply_time.OptionsColumn.AllowShowHide = false;
            // 
            // colreply_dur
            // 
            this.colreply_dur.AppearanceCell.Options.UseTextOptions = true;
            this.colreply_dur.AppearanceCell.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            resources.ApplyResources(this.colreply_dur, "colreply_dur");
            this.colreply_dur.FieldName = "reply_duration";
            this.colreply_dur.Name = "colreply_dur";
            this.colreply_dur.OptionsColumn.AllowShowHide = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.txtraphaftalik);
            this.panel1.Controls.Add(this.txtrapaylık);
            this.panel1.Controls.Add(this.txtrapyillik);
            this.panel1.Controls.Add(this.txtrapgunluk);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // txtraphaftalik
            // 
            this.txtraphaftalik.AllowFocus = false;
            this.txtraphaftalik.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("txtraphaftalik.Appearance.BackColor")));
            this.txtraphaftalik.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtraphaftalik.Appearance.Font")));
            this.txtraphaftalik.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtraphaftalik.Appearance.ForeColor")));
            this.txtraphaftalik.Appearance.Options.UseBackColor = true;
            this.txtraphaftalik.Appearance.Options.UseFont = true;
            this.txtraphaftalik.Appearance.Options.UseForeColor = true;
            this.txtraphaftalik.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("txtraphaftalik.AppearanceHovered.BackColor")));
            this.txtraphaftalik.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("txtraphaftalik.AppearanceHovered.Font")));
            this.txtraphaftalik.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtraphaftalik.AppearanceHovered.ForeColor")));
            this.txtraphaftalik.AppearanceHovered.Options.UseBackColor = true;
            this.txtraphaftalik.AppearanceHovered.Options.UseFont = true;
            this.txtraphaftalik.AppearanceHovered.Options.UseForeColor = true;
            this.txtraphaftalik.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.txtraphaftalik, "txtraphaftalik");
            this.txtraphaftalik.Name = "txtraphaftalik";
            this.txtraphaftalik.Click += new System.EventHandler(this.txtraphaftalik_Click);
            // 
            // txtrapaylık
            // 
            this.txtrapaylık.AllowFocus = false;
            this.txtrapaylık.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("txtrapaylık.Appearance.BackColor")));
            this.txtrapaylık.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtrapaylık.Appearance.Font")));
            this.txtrapaylık.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtrapaylık.Appearance.ForeColor")));
            this.txtrapaylık.Appearance.Options.UseBackColor = true;
            this.txtrapaylık.Appearance.Options.UseFont = true;
            this.txtrapaylık.Appearance.Options.UseForeColor = true;
            this.txtrapaylık.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("txtrapaylık.AppearanceHovered.BackColor")));
            this.txtrapaylık.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("txtrapaylık.AppearanceHovered.Font")));
            this.txtrapaylık.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtrapaylık.AppearanceHovered.ForeColor")));
            this.txtrapaylık.AppearanceHovered.Options.UseBackColor = true;
            this.txtrapaylık.AppearanceHovered.Options.UseFont = true;
            this.txtrapaylık.AppearanceHovered.Options.UseForeColor = true;
            this.txtrapaylık.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.txtrapaylık, "txtrapaylık");
            this.txtrapaylık.Name = "txtrapaylık";
            this.txtrapaylık.Click += new System.EventHandler(this.txtrapaylık_Click);
            // 
            // txtrapyillik
            // 
            this.txtrapyillik.AllowFocus = false;
            this.txtrapyillik.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("txtrapyillik.Appearance.BackColor")));
            this.txtrapyillik.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtrapyillik.Appearance.Font")));
            this.txtrapyillik.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtrapyillik.Appearance.ForeColor")));
            this.txtrapyillik.Appearance.Options.UseBackColor = true;
            this.txtrapyillik.Appearance.Options.UseFont = true;
            this.txtrapyillik.Appearance.Options.UseForeColor = true;
            this.txtrapyillik.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("txtrapyillik.AppearanceHovered.BackColor")));
            this.txtrapyillik.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("txtrapyillik.AppearanceHovered.Font")));
            this.txtrapyillik.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtrapyillik.AppearanceHovered.ForeColor")));
            this.txtrapyillik.AppearanceHovered.Options.UseBackColor = true;
            this.txtrapyillik.AppearanceHovered.Options.UseFont = true;
            this.txtrapyillik.AppearanceHovered.Options.UseForeColor = true;
            this.txtrapyillik.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.txtrapyillik, "txtrapyillik");
            this.txtrapyillik.Name = "txtrapyillik";
            this.txtrapyillik.Click += new System.EventHandler(this.txtrapyillik_Click);
            // 
            // txtrapgunluk
            // 
            this.txtrapgunluk.AllowFocus = false;
            this.txtrapgunluk.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("txtrapgunluk.Appearance.BackColor")));
            this.txtrapgunluk.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtrapgunluk.Appearance.Font")));
            this.txtrapgunluk.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtrapgunluk.Appearance.ForeColor")));
            this.txtrapgunluk.Appearance.Options.UseBackColor = true;
            this.txtrapgunluk.Appearance.Options.UseFont = true;
            this.txtrapgunluk.Appearance.Options.UseForeColor = true;
            this.txtrapgunluk.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("txtrapgunluk.AppearanceHovered.BackColor")));
            this.txtrapgunluk.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("txtrapgunluk.AppearanceHovered.Font")));
            this.txtrapgunluk.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtrapgunluk.AppearanceHovered.ForeColor")));
            this.txtrapgunluk.AppearanceHovered.Options.UseBackColor = true;
            this.txtrapgunluk.AppearanceHovered.Options.UseFont = true;
            this.txtrapgunluk.AppearanceHovered.Options.UseForeColor = true;
            this.txtrapgunluk.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.txtrapgunluk, "txtrapgunluk");
            this.txtrapgunluk.Name = "txtrapgunluk";
            this.txtrapgunluk.Click += new System.EventHandler(this.txtrapgunluk_Click);
            // 
            // tabgrafikler
            // 
            this.tabgrafikler.Controls.Add(this.chartControl1);
            this.tabgrafikler.Controls.Add(this.panel2);
            this.tabgrafikler.Name = "tabgrafikler";
            resources.ApplyResources(this.tabgrafikler, "tabgrafikler");
            // 
            // chartControl1
            // 
            this.chartControl1.CrosshairOptions.ArgumentLineColor = System.Drawing.Color.Orange;
            this.chartControl1.CrosshairOptions.ArgumentLineStyle.Thickness = 2;
            this.chartControl1.CrosshairOptions.ValueLineColor = System.Drawing.Color.Orange;
            this.chartControl1.DataAdapter = this.reportsTableAdapter1;
            this.chartControl1.DataBindings = null;
            this.chartControl1.DataSource = this.reportsBindingSource;
            resources.ApplyResources(this.chartControl1, "chartControl1");
            this.chartControl1.IndicatorsPaletteName = "Opulent";
            this.chartControl1.Legend.Font = ((System.Drawing.Font)(resources.GetObject("chartControl1.Legend.Font")));
            this.chartControl1.Legend.Name = "Default Legend";
            this.chartControl1.Name = "chartControl1";
            this.chartControl1.PaletteBaseColorNumber = 4;
            this.chartControl1.PaletteName = "Grayscale";
            this.chartControl1.SeriesSerializable = new DevExpress.XtraCharts.Series[0];
            this.chartControl1.CustomDrawCrosshair += new DevExpress.XtraCharts.CustomDrawCrosshairEventHandler(this.chartControl1_CustomDrawCrosshair);
            // 
            // reportsTableAdapter1
            // 
            this.reportsTableAdapter1.ClearBeforeFill = true;
            // 
            // reportsBindingSource
            // 
            this.reportsBindingSource.DataMember = "reports";
            this.reportsBindingSource.DataSource = this.cevircagirDataSet;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.dateedtgrafik);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.txtgrhaftalik);
            this.panel2.Controls.Add(this.txtgraylik);
            this.panel2.Controls.Add(this.txtgryillik);
            this.panel2.Controls.Add(this.txtgrgunluk);
            resources.ApplyResources(this.panel2, "panel2");
            this.panel2.Name = "panel2";
            // 
            // dateedtgrafik
            // 
            resources.ApplyResources(this.dateedtgrafik, "dateedtgrafik");
            this.dateedtgrafik.Name = "dateedtgrafik";
            this.dateedtgrafik.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("dateedtgrafik.Properties.Appearance.ForeColor")));
            this.dateedtgrafik.Properties.Appearance.Options.UseForeColor = true;
            this.dateedtgrafik.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.dateedtgrafik.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dateedtgrafik.Properties.Buttons"))))});
            this.dateedtgrafik.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("dateedtgrafik.Properties.CalendarTimeProperties.Buttons"))))});
            this.dateedtgrafik.EditValueChanged += new System.EventHandler(this.dateedtgrafik_EditValueChanged);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Name = "label3";
            // 
            // txtgrhaftalik
            // 
            this.txtgrhaftalik.AllowFocus = false;
            this.txtgrhaftalik.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("txtgrhaftalik.Appearance.BackColor")));
            this.txtgrhaftalik.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtgrhaftalik.Appearance.Font")));
            this.txtgrhaftalik.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtgrhaftalik.Appearance.ForeColor")));
            this.txtgrhaftalik.Appearance.Options.UseBackColor = true;
            this.txtgrhaftalik.Appearance.Options.UseFont = true;
            this.txtgrhaftalik.Appearance.Options.UseForeColor = true;
            this.txtgrhaftalik.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("txtgrhaftalik.AppearanceHovered.BackColor")));
            this.txtgrhaftalik.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("txtgrhaftalik.AppearanceHovered.Font")));
            this.txtgrhaftalik.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtgrhaftalik.AppearanceHovered.ForeColor")));
            this.txtgrhaftalik.AppearanceHovered.Options.UseBackColor = true;
            this.txtgrhaftalik.AppearanceHovered.Options.UseFont = true;
            this.txtgrhaftalik.AppearanceHovered.Options.UseForeColor = true;
            this.txtgrhaftalik.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.txtgrhaftalik, "txtgrhaftalik");
            this.txtgrhaftalik.Name = "txtgrhaftalik";
            this.txtgrhaftalik.Click += new System.EventHandler(this.txtgrhaftalik_Click);
            // 
            // txtgraylik
            // 
            this.txtgraylik.AllowFocus = false;
            this.txtgraylik.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("txtgraylik.Appearance.BackColor")));
            this.txtgraylik.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtgraylik.Appearance.Font")));
            this.txtgraylik.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtgraylik.Appearance.ForeColor")));
            this.txtgraylik.Appearance.Options.UseBackColor = true;
            this.txtgraylik.Appearance.Options.UseFont = true;
            this.txtgraylik.Appearance.Options.UseForeColor = true;
            this.txtgraylik.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("txtgraylik.AppearanceHovered.BackColor")));
            this.txtgraylik.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("txtgraylik.AppearanceHovered.Font")));
            this.txtgraylik.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtgraylik.AppearanceHovered.ForeColor")));
            this.txtgraylik.AppearanceHovered.Options.UseBackColor = true;
            this.txtgraylik.AppearanceHovered.Options.UseFont = true;
            this.txtgraylik.AppearanceHovered.Options.UseForeColor = true;
            this.txtgraylik.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.txtgraylik, "txtgraylik");
            this.txtgraylik.Name = "txtgraylik";
            this.txtgraylik.Click += new System.EventHandler(this.txtgraylik_Click);
            // 
            // txtgryillik
            // 
            this.txtgryillik.AllowFocus = false;
            this.txtgryillik.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("txtgryillik.Appearance.BackColor")));
            this.txtgryillik.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtgryillik.Appearance.Font")));
            this.txtgryillik.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtgryillik.Appearance.ForeColor")));
            this.txtgryillik.Appearance.Options.UseBackColor = true;
            this.txtgryillik.Appearance.Options.UseFont = true;
            this.txtgryillik.Appearance.Options.UseForeColor = true;
            this.txtgryillik.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("txtgryillik.AppearanceHovered.BackColor")));
            this.txtgryillik.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("txtgryillik.AppearanceHovered.Font")));
            this.txtgryillik.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtgryillik.AppearanceHovered.ForeColor")));
            this.txtgryillik.AppearanceHovered.Options.UseBackColor = true;
            this.txtgryillik.AppearanceHovered.Options.UseFont = true;
            this.txtgryillik.AppearanceHovered.Options.UseForeColor = true;
            this.txtgryillik.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.txtgryillik, "txtgryillik");
            this.txtgryillik.Name = "txtgryillik";
            this.txtgryillik.Click += new System.EventHandler(this.txtgryillik_Click);
            // 
            // txtgrgunluk
            // 
            this.txtgrgunluk.AllowFocus = false;
            this.txtgrgunluk.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("txtgrgunluk.Appearance.BackColor")));
            this.txtgrgunluk.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtgrgunluk.Appearance.Font")));
            this.txtgrgunluk.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtgrgunluk.Appearance.ForeColor")));
            this.txtgrgunluk.Appearance.Options.UseBackColor = true;
            this.txtgrgunluk.Appearance.Options.UseFont = true;
            this.txtgrgunluk.Appearance.Options.UseForeColor = true;
            this.txtgrgunluk.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("txtgrgunluk.AppearanceHovered.BackColor")));
            this.txtgrgunluk.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("txtgrgunluk.AppearanceHovered.Font")));
            this.txtgrgunluk.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtgrgunluk.AppearanceHovered.ForeColor")));
            this.txtgrgunluk.AppearanceHovered.Options.UseBackColor = true;
            this.txtgrgunluk.AppearanceHovered.Options.UseFont = true;
            this.txtgrgunluk.AppearanceHovered.Options.UseForeColor = true;
            this.txtgrgunluk.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.txtgrgunluk, "txtgrgunluk");
            this.txtgrgunluk.Name = "txtgrgunluk";
            this.txtgrgunluk.Click += new System.EventHandler(this.txtgrgunluk_Click);
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("panelControl1.Appearance.BackColor")));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.label1);
            this.panelControl1.Controls.Add(this.txtcevapsure);
            this.panelControl1.Controls.Add(this.labelControl6);
            this.panelControl1.Controls.Add(this.txtgarsonismi);
            this.panelControl1.Controls.Add(this.txtmasaismi);
            this.panelControl1.Controls.Add(this.labelControl5);
            this.panelControl1.Controls.Add(this.labelControl4);
            this.panelControl1.Controls.Add(this.btnfiltrele);
            this.panelControl1.Controls.Add(this.edtbitistarih);
            this.panelControl1.Controls.Add(this.edtbaslangictarih);
            this.panelControl1.Controls.Add(this.labelControl3);
            this.panelControl1.Controls.Add(this.labelControl2);
            resources.ApplyResources(this.panelControl1, "panelControl1");
            this.panelControl1.Name = "panelControl1";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Name = "label1";
            // 
            // txtcevapsure
            // 
            this.txtcevapsure.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtcevapsure, "txtcevapsure");
            this.txtcevapsure.ForeColor = System.Drawing.Color.DimGray;
            this.txtcevapsure.Name = "txtcevapsure";
            this.txtcevapsure.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtcevapsure_KeyPress);
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl6.Appearance.Font")));
            this.labelControl6.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl6.Appearance.ForeColor")));
            this.labelControl6.Appearance.Options.UseFont = true;
            this.labelControl6.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.labelControl6, "labelControl6");
            this.labelControl6.Name = "labelControl6";
            // 
            // txtgarsonismi
            // 
            this.txtgarsonismi.AllowDrop = true;
            this.txtgarsonismi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtgarsonismi, "txtgarsonismi");
            this.txtgarsonismi.ForeColor = System.Drawing.Color.DimGray;
            this.txtgarsonismi.Name = "txtgarsonismi";
            this.txtgarsonismi.TextChanged += new System.EventHandler(this.txtgarsonismi_TextChanged);
            this.txtgarsonismi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtgarsonismi_KeyPress);
            // 
            // txtmasaismi
            // 
            this.txtmasaismi.AllowDrop = true;
            this.txtmasaismi.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            resources.ApplyResources(this.txtmasaismi, "txtmasaismi");
            this.txtmasaismi.ForeColor = System.Drawing.Color.DimGray;
            this.txtmasaismi.Name = "txtmasaismi";
            this.txtmasaismi.TextChanged += new System.EventHandler(this.txtmasaismi_TextChanged);
            this.txtmasaismi.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtmasaismi_KeyPress);
            // 
            // labelControl5
            // 
            this.labelControl5.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl5.Appearance.Font")));
            this.labelControl5.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl5.Appearance.ForeColor")));
            this.labelControl5.Appearance.Options.UseFont = true;
            this.labelControl5.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.labelControl5, "labelControl5");
            this.labelControl5.Name = "labelControl5";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl4.Appearance.Font")));
            this.labelControl4.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl4.Appearance.ForeColor")));
            this.labelControl4.Appearance.Options.UseFont = true;
            this.labelControl4.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.labelControl4, "labelControl4");
            this.labelControl4.Name = "labelControl4";
            // 
            // btnfiltrele
            // 
            this.btnfiltrele.AllowDrop = true;
            this.btnfiltrele.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnfiltrele.Appearance.BackColor")));
            this.btnfiltrele.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnfiltrele.Appearance.Font")));
            this.btnfiltrele.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnfiltrele.Appearance.ForeColor")));
            this.btnfiltrele.Appearance.Options.UseBackColor = true;
            this.btnfiltrele.Appearance.Options.UseFont = true;
            this.btnfiltrele.Appearance.Options.UseForeColor = true;
            this.btnfiltrele.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnfiltrele.AppearanceHovered.BackColor")));
            this.btnfiltrele.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnfiltrele.AppearanceHovered.Font")));
            this.btnfiltrele.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnfiltrele.AppearanceHovered.ForeColor")));
            this.btnfiltrele.AppearanceHovered.Options.UseBackColor = true;
            this.btnfiltrele.AppearanceHovered.Options.UseFont = true;
            this.btnfiltrele.AppearanceHovered.Options.UseForeColor = true;
            this.btnfiltrele.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnfiltrele, "btnfiltrele");
            this.btnfiltrele.Name = "btnfiltrele";
            this.btnfiltrele.Click += new System.EventHandler(this.btnfiltrele_Click);
            // 
            // edtbitistarih
            // 
            resources.ApplyResources(this.edtbitistarih, "edtbitistarih");
            this.edtbitistarih.Name = "edtbitistarih";
            this.edtbitistarih.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("edtbitistarih.Properties.Appearance.Font")));
            this.edtbitistarih.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("edtbitistarih.Properties.Appearance.ForeColor")));
            this.edtbitistarih.Properties.Appearance.Options.UseFont = true;
            this.edtbitistarih.Properties.Appearance.Options.UseForeColor = true;
            this.edtbitistarih.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.edtbitistarih.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("edtbitistarih.Properties.Buttons"))))});
            this.edtbitistarih.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("edtbitistarih.Properties.CalendarTimeProperties.Buttons"))))});
            // 
            // edtbaslangictarih
            // 
            resources.ApplyResources(this.edtbaslangictarih, "edtbaslangictarih");
            this.edtbaslangictarih.Name = "edtbaslangictarih";
            this.edtbaslangictarih.Properties.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("edtbaslangictarih.Properties.Appearance.BackColor")));
            this.edtbaslangictarih.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("edtbaslangictarih.Properties.Appearance.Font")));
            this.edtbaslangictarih.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("edtbaslangictarih.Properties.Appearance.ForeColor")));
            this.edtbaslangictarih.Properties.Appearance.Options.UseBackColor = true;
            this.edtbaslangictarih.Properties.Appearance.Options.UseFont = true;
            this.edtbaslangictarih.Properties.Appearance.Options.UseForeColor = true;
            this.edtbaslangictarih.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.edtbaslangictarih.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("edtbaslangictarih.Properties.Buttons"))))});
            this.edtbaslangictarih.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("edtbaslangictarih.Properties.CalendarTimeProperties.Buttons"))))});
            // 
            // labelControl3
            // 
            this.labelControl3.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl3.Appearance.Font")));
            this.labelControl3.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl3.Appearance.ForeColor")));
            this.labelControl3.Appearance.Options.UseFont = true;
            this.labelControl3.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.labelControl3, "labelControl3");
            this.labelControl3.Name = "labelControl3";
            // 
            // labelControl2
            // 
            this.labelControl2.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("labelControl2.Appearance.Font")));
            this.labelControl2.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("labelControl2.Appearance.ForeColor")));
            this.labelControl2.Appearance.Options.UseFont = true;
            this.labelControl2.Appearance.Options.UseForeColor = true;
            resources.ApplyResources(this.labelControl2, "labelControl2");
            this.labelControl2.Name = "labelControl2";
            // 
            // FormRaporlar
            // 
            this.AcceptButton = this.btnfiltrele;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.xtraTabControl1);
            this.Controls.Add(this.panelControl1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pnlsum);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormRaporlar";
            this.Load += new System.EventHandler(this.FormRaporlar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            this.panelControl2.PerformLayout();
            this.pnlsum.ResumeLayout(false);
            this.pnlsum.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.tabraporlar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.tabgrafikler.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chartControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportsBindingSource)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dateedtgrafik.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dateedtgrafik.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtbitistarih.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtbitistarih.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtbaslangictarih.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtbaslangictarih.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private cevircagirDataSet5TableAdapters.reportsTableAdapter reportsTableAdapter;
        private DevExpress.XtraGrid.Columns.GridColumn colreply_duration;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraEditors.SimpleButton btnsongun;
        private DevExpress.XtraEditors.SimpleButton simpleButton3;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.TextBox txtgarson;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel pnlsum;
        private System.Windows.Forms.TextBox txtsum;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Label lblgarsonbaslik;
        public System.Windows.Forms.Label lblformname;
        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage tabraporlar;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn coltable_name;
        private DevExpress.XtraGrid.Columns.GridColumn colreq_date;
        private DevExpress.XtraGrid.Columns.GridColumn colwaiter_name;
        private DevExpress.XtraGrid.Columns.GridColumn colreq_time;
        private DevExpress.XtraGrid.Columns.GridColumn colreply_time;
        private DevExpress.XtraGrid.Columns.GridColumn colreply_dur;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SimpleButton txtraphaftalik;
        private DevExpress.XtraEditors.SimpleButton txtrapaylık;
        private DevExpress.XtraEditors.SimpleButton txtrapyillik;
        private DevExpress.XtraEditors.SimpleButton txtrapgunluk;
        private DevExpress.XtraTab.XtraTabPage tabgrafikler;
        private DevExpress.XtraCharts.ChartControl chartControl1;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.DateEdit dateedtgrafik;
        private System.Windows.Forms.Label label3;
        private DevExpress.XtraEditors.SimpleButton txtgrhaftalik;
        private DevExpress.XtraEditors.SimpleButton txtgraylik;
        private DevExpress.XtraEditors.SimpleButton txtgryillik;
        private DevExpress.XtraEditors.SimpleButton txtgrgunluk;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtcevapsure;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private System.Windows.Forms.TextBox txtgarsonismi;
        private System.Windows.Forms.TextBox txtmasaismi;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.SimpleButton btnfiltrele;
        private DevExpress.XtraEditors.DateEdit edtbitistarih;
        private DevExpress.XtraEditors.DateEdit edtbaslangictarih;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private System.Windows.Forms.BindingSource cevircagirDataSetBindingSource;
        private cevircagirDataSet cevircagirDataSet;
        private cevircagirDataSetTableAdapters.reportsTableAdapter reportsTableAdapter1;
        private System.Windows.Forms.BindingSource reportsBindingSource;
    }
}