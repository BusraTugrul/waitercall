﻿namespace cevircagirserverdeneme
{
    partial class formdelete
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formdelete));
            this.btnevet = new DevExpress.XtraEditors.SimpleButton();
            this.btnhyr = new DevExpress.XtraEditors.SimpleButton();
            this.txtmsj = new System.Windows.Forms.TextBox();
            this.pnlmsj = new System.Windows.Forms.Panel();
            this.pnlmsj.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnevet
            // 
            this.btnevet.AllowFocus = false;
            this.btnevet.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnevet.Appearance.BackColor")));
            this.btnevet.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnevet.Appearance.Font")));
            this.btnevet.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnevet.Appearance.ForeColor")));
            this.btnevet.Appearance.Options.UseBackColor = true;
            this.btnevet.Appearance.Options.UseFont = true;
            this.btnevet.Appearance.Options.UseForeColor = true;
            this.btnevet.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnevet.AppearanceHovered.BackColor")));
            this.btnevet.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnevet.AppearanceHovered.Font")));
            this.btnevet.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnevet.AppearanceHovered.ForeColor")));
            this.btnevet.AppearanceHovered.Options.UseBackColor = true;
            this.btnevet.AppearanceHovered.Options.UseFont = true;
            this.btnevet.AppearanceHovered.Options.UseForeColor = true;
            this.btnevet.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnevet, "btnevet");
            this.btnevet.Name = "btnevet";
            this.btnevet.Click += new System.EventHandler(this.btnevet_Click);
            // 
            // btnhyr
            // 
            this.btnhyr.AllowFocus = false;
            this.btnhyr.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnhyr.Appearance.BackColor")));
            this.btnhyr.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnhyr.Appearance.Font")));
            this.btnhyr.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnhyr.Appearance.ForeColor")));
            this.btnhyr.Appearance.Options.UseBackColor = true;
            this.btnhyr.Appearance.Options.UseFont = true;
            this.btnhyr.Appearance.Options.UseForeColor = true;
            this.btnhyr.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnhyr.AppearanceHovered.BackColor")));
            this.btnhyr.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnhyr.AppearanceHovered.Font")));
            this.btnhyr.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnhyr.AppearanceHovered.ForeColor")));
            this.btnhyr.AppearanceHovered.Options.UseBackColor = true;
            this.btnhyr.AppearanceHovered.Options.UseFont = true;
            this.btnhyr.AppearanceHovered.Options.UseForeColor = true;
            this.btnhyr.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnhyr, "btnhyr");
            this.btnhyr.Name = "btnhyr";
            this.btnhyr.Click += new System.EventHandler(this.btnhyr_Click);
            // 
            // txtmsj
            // 
            this.txtmsj.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtmsj, "txtmsj");
            this.txtmsj.ForeColor = System.Drawing.Color.DimGray;
            this.txtmsj.Name = "txtmsj";
            // 
            // pnlmsj
            // 
            this.pnlmsj.BackColor = System.Drawing.SystemColors.Control;
            this.pnlmsj.Controls.Add(this.btnhyr);
            this.pnlmsj.Controls.Add(this.txtmsj);
            this.pnlmsj.Controls.Add(this.btnevet);
            resources.ApplyResources(this.pnlmsj, "pnlmsj");
            this.pnlmsj.Name = "pnlmsj";
            // 
            // formdelete
            // 
            this.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("formdelete.Appearance.BackColor")));
            this.Appearance.Options.UseBackColor = true;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlmsj);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "formdelete";
            this.Load += new System.EventHandler(this.formdelete_Load);
            this.pnlmsj.ResumeLayout(false);
            this.pnlmsj.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TextBox txtmsj;
        private System.Windows.Forms.Panel pnlmsj;
        public DevExpress.XtraEditors.SimpleButton btnevet;
        public DevExpress.XtraEditors.SimpleButton btnhyr;
    }
}