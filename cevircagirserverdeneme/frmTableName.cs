﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cevircagirserverdeneme
{
    public partial class frmTableName : Form
    {
        public String table_name;
        WhenDataComes wdt;
        public bool isalive = false;
        private static readonly frmTableName singletonfrmtname = new frmTableName();
        static string conString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\cevircagir.mdf';Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        SqlConnection baglanti;
        SqlCommand loadcommand;

        public String tname  //outcomedata get set method
        {
            get
            {
                return table_name;
            }
            set
            {
                table_name = value;
            }
        }
        static frmTableName()
        {

        }
        private frmTableName()
        {
            InitializeComponent();
        }
        public static frmTableName singletonfrm
        {
            get
            {
                return singletonfrmtname;
            }
        }

        private void frmTableName_Load(object sender, EventArgs e)
        {
            //wdt = new WhenDataComes();
            localization();
            baglanti = new SqlConnection(conString);
            txtTableName.Text = "";
            isalive = true;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtTableName.Text != "")
            {
                table_name = txtTableName.Text;
                baglanti.Open();
                string conf = "select * from tables where table_name=@t_name";
                SqlCommand sconf = new SqlCommand(conf, baglanti);
                sconf.Parameters.AddWithValue("@t_name", table_name);
                SqlDataReader dr = sconf.ExecuteReader();
                if (dr.Read())   //daha önceden o isimle kayıtlı masa varsa hata ver
                {
                    lblerror.Text = Localization.lblerror;
                    lblerror.Visible = true;
                    table_name = "";
                    txtTableName.Text = "";
                }
                else
                {
                    this.Close();
                    isalive = false;
                }
                baglanti.Close();
            }
            else
            {
                lblerror.Visible = true;
                lblerror.Text=Localization.lblerror2;
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
            isalive = false;
        }

        private void frmTableName_FormClosing(object sender, FormClosingEventArgs e)
        {
            txtTableName.Text = "";
            lblerror.Text = "";
            lblerror.Visible = false;
            isalive = false;
        }
        public void localization()
        {
            lblerror.Text = Localization.lblerror;
            label8.Text = Localization.coltable_name_;
            btnSave.Text = Localization.btnKaydet;
        }

        private void txtTableName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtTableName.Text.Length > 30)
            {
                e.Handled = !(e.KeyChar == (char)Keys.Back);
            }
            else
                e.Handled = !(char.IsLetter(e.KeyChar) || char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space);
        }
    }
}
