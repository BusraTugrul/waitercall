﻿namespace cevircagirserverdeneme
{
    partial class formCagrilar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formCagrilar));
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement5 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement6 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement7 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement8 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            this.clMasaAdi = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.clwaiter = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.cldk_sn = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.clreqtime = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.repositoryItemPictureEdit3 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.cevircagirDataSet5 = new cevircagirserverdeneme.cevircagirDataSet5();
            this.cevircagirDataSet5BindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblgarsonbaslik = new System.Windows.Forms.Label();
            this.lblformname = new System.Windows.Forms.Label();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.reportsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.cevircagirDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cevircagirDataSet = new cevircagirserverdeneme.cevircagirDataSet();
            this.tvMainv = new DevExpress.XtraGrid.Views.Tile.TileView();
            this.repositoryItemPictureEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.repositoryItemPictureEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.reportsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.reportsTableAdapter = new cevircagirserverdeneme.cevircagirDataSet5TableAdapters.reportsTableAdapter();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            this.reportsTableAdapter1 = new cevircagirserverdeneme.cevircagirDataSetTableAdapters.reportsTableAdapter();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet5BindingSource)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tvMainv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // clMasaAdi
            // 
            resources.ApplyResources(this.clMasaAdi, "clMasaAdi");
            this.clMasaAdi.FieldName = "table_name";
            this.clMasaAdi.Name = "clMasaAdi";
            // 
            // clwaiter
            // 
            resources.ApplyResources(this.clwaiter, "clwaiter");
            this.clwaiter.FieldName = "waiter_name";
            this.clwaiter.Name = "clwaiter";
            // 
            // cldk_sn
            // 
            resources.ApplyResources(this.cldk_sn, "cldk_sn");
            this.cldk_sn.FieldName = "cldk_sn";
            this.cldk_sn.Name = "cldk_sn";
            this.cldk_sn.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // clreqtime
            // 
            resources.ApplyResources(this.clreqtime, "clreqtime");
            this.clreqtime.FieldName = "req_time";
            this.clreqtime.Image = ((System.Drawing.Image)(resources.GetObject("clreqtime.Image")));
            this.clreqtime.Name = "clreqtime";
            // 
            // repositoryItemPictureEdit3
            // 
            this.repositoryItemPictureEdit3.Name = "repositoryItemPictureEdit3";
            this.repositoryItemPictureEdit3.ZoomAccelerationFactor = 1D;
            // 
            // cevircagirDataSet5
            // 
            this.cevircagirDataSet5.DataSetName = "cevircagirDataSet5";
            this.cevircagirDataSet5.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // cevircagirDataSet5BindingSource
            // 
            this.cevircagirDataSet5BindingSource.DataSource = this.cevircagirDataSet5;
            this.cevircagirDataSet5BindingSource.Position = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.lblgarsonbaslik);
            this.panel3.Controls.Add(this.lblformname);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // lblgarsonbaslik
            // 
            resources.ApplyResources(this.lblgarsonbaslik, "lblgarsonbaslik");
            this.lblgarsonbaslik.ForeColor = System.Drawing.Color.Orange;
            this.lblgarsonbaslik.Name = "lblgarsonbaslik";
            // 
            // lblformname
            // 
            resources.ApplyResources(this.lblformname, "lblformname");
            this.lblformname.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblformname.ForeColor = System.Drawing.Color.Orange;
            this.lblformname.Name = "lblformname";
            // 
            // gridControl1
            // 
            this.gridControl1.AllowRestoreSelectionAndFocusedRow = DevExpress.Utils.DefaultBoolean.False;
            this.gridControl1.DataSource = this.reportsBindingSource1;
            resources.ApplyResources(this.gridControl1, "gridControl1");
            this.gridControl1.MainView = this.tvMainv;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemPictureEdit1,
            this.repositoryItemPictureEdit2,
            this.repositoryItemPictureEdit3});
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.tvMainv,
            this.gridView2,
            this.gridView1});
            // 
            // reportsBindingSource1
            // 
            this.reportsBindingSource1.DataMember = "reports";
            this.reportsBindingSource1.DataSource = this.cevircagirDataSetBindingSource;
            // 
            // cevircagirDataSetBindingSource
            // 
            this.cevircagirDataSetBindingSource.DataSource = this.cevircagirDataSet;
            this.cevircagirDataSetBindingSource.Position = 0;
            // 
            // cevircagirDataSet
            // 
            this.cevircagirDataSet.DataSetName = "cevircagirDataSet";
            this.cevircagirDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tvMainv
            // 
            this.tvMainv.Appearance.EmptySpace.BackColor = ((System.Drawing.Color)(resources.GetObject("tvMainv.Appearance.EmptySpace.BackColor")));
            this.tvMainv.Appearance.EmptySpace.Options.UseBackColor = true;
            this.tvMainv.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.tvMainv.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.cldk_sn,
            this.clMasaAdi,
            this.clreqtime,
            this.clwaiter});
            this.tvMainv.FocusBorderColor = System.Drawing.Color.Transparent;
            this.tvMainv.GridControl = this.gridControl1;
            this.tvMainv.Name = "tvMainv";
            this.tvMainv.OptionsTiles.HorizontalContentAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.tvMainv.OptionsTiles.ItemBorderVisibility = DevExpress.XtraEditors.TileItemBorderVisibility.Never;
            this.tvMainv.OptionsTiles.ItemSize = new System.Drawing.Size(120, 60);
            this.tvMainv.OptionsTiles.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tvMainv.OptionsTiles.Padding = new System.Windows.Forms.Padding(25, 30, 25, 30);
            this.tvMainv.OptionsTiles.RowCount = 7;
            this.tvMainv.OptionsTiles.ShowGroupText = false;
            this.tvMainv.OptionsTiles.VerticalContentAlignment = DevExpress.Utils.VertAlignment.Top;
            tileViewItemElement5.Appearance.Normal.BackColor = ((System.Drawing.Color)(resources.GetObject("resource.BackColor")));
            tileViewItemElement5.Appearance.Normal.BackColor2 = ((System.Drawing.Color)(resources.GetObject("resource.BackColor2")));
            tileViewItemElement5.Appearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("resource.Font")));
            tileViewItemElement5.Appearance.Normal.ForeColor = ((System.Drawing.Color)(resources.GetObject("resource.ForeColor")));
            tileViewItemElement5.Appearance.Normal.Options.UseBackColor = true;
            tileViewItemElement5.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement5.Appearance.Normal.Options.UseForeColor = true;
            tileViewItemElement5.Column = this.clMasaAdi;
            resources.ApplyResources(tileViewItemElement5, "tileViewItemElement5");
            tileViewItemElement5.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopCenter;
            tileViewItemElement5.TextLocation = new System.Drawing.Point(0, -10);
            tileViewItemElement6.Appearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("resource.Font1")));
            tileViewItemElement6.Appearance.Normal.ForeColor = ((System.Drawing.Color)(resources.GetObject("resource.ForeColor1")));
            tileViewItemElement6.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement6.Appearance.Normal.Options.UseForeColor = true;
            tileViewItemElement6.Column = this.clwaiter;
            resources.ApplyResources(tileViewItemElement6, "tileViewItemElement6");
            tileViewItemElement6.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileViewItemElement6.TextLocation = new System.Drawing.Point(0, 2);
            tileViewItemElement7.Appearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("resource.Font2")));
            tileViewItemElement7.Appearance.Normal.ForeColor = ((System.Drawing.Color)(resources.GetObject("resource.ForeColor2")));
            tileViewItemElement7.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement7.Appearance.Normal.Options.UseForeColor = true;
            tileViewItemElement7.Column = this.cldk_sn;
            resources.ApplyResources(tileViewItemElement7, "tileViewItemElement7");
            tileViewItemElement7.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileViewItemElement7.TextLocation = new System.Drawing.Point(0, 9);
            tileViewItemElement8.Image = global::cevircagirserverdeneme.Properties.Resources.deneme2;
            tileViewItemElement8.ImageLocation = new System.Drawing.Point(10, 15);
            tileViewItemElement8.ImageScaleMode = DevExpress.XtraEditors.TileItemImageScaleMode.StretchVertical;
            tileViewItemElement8.ImageSize = new System.Drawing.Size(150, 75);
            resources.ApplyResources(tileViewItemElement8, "tileViewItemElement8");
            this.tvMainv.TileTemplate.Add(tileViewItemElement5);
            this.tvMainv.TileTemplate.Add(tileViewItemElement6);
            this.tvMainv.TileTemplate.Add(tileViewItemElement7);
            this.tvMainv.TileTemplate.Add(tileViewItemElement8);
            this.tvMainv.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tvMainv_MouseDown);
            // 
            // repositoryItemPictureEdit1
            // 
            this.repositoryItemPictureEdit1.Name = "repositoryItemPictureEdit1";
            this.repositoryItemPictureEdit1.ZoomAccelerationFactor = 1D;
            // 
            // repositoryItemPictureEdit2
            // 
            this.repositoryItemPictureEdit2.Name = "repositoryItemPictureEdit2";
            this.repositoryItemPictureEdit2.ZoomAccelerationFactor = 1D;
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // reportsBindingSource
            // 
            this.reportsBindingSource.DataMember = "reports";
            this.reportsBindingSource.DataSource = this.cevircagirDataSet5BindingSource;
            // 
            // reportsTableAdapter
            // 
            this.reportsTableAdapter.ClearBeforeFill = true;
            // 
            // gridSplitContainer1
            // 
            resources.ApplyResources(this.gridSplitContainer1, "gridSplitContainer1");
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            // 
            // reportsTableAdapter1
            // 
            this.reportsTableAdapter1.ClearBeforeFill = true;
            // 
            // formCagrilar
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "formCagrilar";
            this.Load += new System.EventHandler(this.formCagrilar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet5BindingSource)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tvMainv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPictureEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.BindingSource cevircagirDataSet5BindingSource;
        private cevircagirDataSet5 cevircagirDataSet5;
        public System.Windows.Forms.Label lblgarsonbaslik;
        public System.Windows.Forms.Label lblformname;
        private DevExpress.XtraGrid.Columns.TileViewColumn clMasaAdi;
        private DevExpress.XtraGrid.Columns.TileViewColumn clreqtime;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        public DevExpress.XtraGrid.GridControl gridControl1;
        public DevExpress.XtraGrid.Views.Tile.TileView tvMainv;
        public System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.BindingSource reportsBindingSource;
        private cevircagirDataSet5TableAdapters.reportsTableAdapter reportsTableAdapter;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit2;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemPictureEdit repositoryItemPictureEdit3;
        private DevExpress.XtraGrid.Columns.TileViewColumn clwaiter;
        private DevExpress.XtraGrid.Columns.TileViewColumn cldk_sn;
        private System.Windows.Forms.BindingSource cevircagirDataSetBindingSource;
        private cevircagirDataSet cevircagirDataSet;
        private System.Windows.Forms.BindingSource reportsBindingSource1;
        private cevircagirDataSetTableAdapters.reportsTableAdapter reportsTableAdapter1;
    }
}