﻿namespace cevircagirserverdeneme
{
    partial class FormCihazKonfigurasyon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCihazKonfigurasyon));
            DevExpress.XtraGrid.GridLevelNode gridLevelNode1 = new DevExpress.XtraGrid.GridLevelNode();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement1 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement2 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            this.cltable_id1 = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.clisactive = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.tablesBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.panel3 = new System.Windows.Forms.Panel();
            this.btntemizle = new DevExpress.XtraEditors.SimpleButton();
            this.btnkaydet = new DevExpress.XtraEditors.SimpleButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.cevircagirDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cevircagirDataSet = new cevircagirserverdeneme.cevircagirDataSet();
            this.tileView1 = new DevExpress.XtraGrid.Views.Tile.TileView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tablesBindingSource1)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileView1)).BeginInit();
            this.SuspendLayout();
            // 
            // cltable_id1
            // 
            this.cltable_id1.FieldName = "table_name";
            this.cltable_id1.Name = "cltable_id1";
            this.cltable_id1.OptionsColumn.AllowFocus = false;
            resources.ApplyResources(this.cltable_id1, "cltable_id1");
            // 
            // clisactive
            // 
            resources.ApplyResources(this.clisactive, "clisactive");
            this.clisactive.FieldName = "isactive";
            this.clisactive.Name = "clisactive";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label3);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.Orange;
            this.label3.Name = "label3";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btntemizle);
            this.panel3.Controls.Add(this.btnkaydet);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label2);
            this.panel3.Controls.Add(this.button2);
            this.panel3.Controls.Add(this.button1);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // btntemizle
            // 
            resources.ApplyResources(this.btntemizle, "btntemizle");
            this.btntemizle.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btntemizle.Appearance.BackColor")));
            this.btntemizle.Appearance.BackColor2 = ((System.Drawing.Color)(resources.GetObject("btntemizle.Appearance.BackColor2")));
            this.btntemizle.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btntemizle.Appearance.Font")));
            this.btntemizle.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btntemizle.Appearance.ForeColor")));
            this.btntemizle.Appearance.Options.UseBackColor = true;
            this.btntemizle.Appearance.Options.UseFont = true;
            this.btntemizle.Appearance.Options.UseForeColor = true;
            this.btntemizle.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btntemizle.AppearanceHovered.BackColor")));
            this.btntemizle.AppearanceHovered.BackColor2 = ((System.Drawing.Color)(resources.GetObject("btntemizle.AppearanceHovered.BackColor2")));
            this.btntemizle.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btntemizle.AppearanceHovered.Font")));
            this.btntemizle.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btntemizle.AppearanceHovered.ForeColor")));
            this.btntemizle.AppearanceHovered.Options.UseBackColor = true;
            this.btntemizle.AppearanceHovered.Options.UseFont = true;
            this.btntemizle.AppearanceHovered.Options.UseForeColor = true;
            this.btntemizle.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btntemizle.Name = "btntemizle";
            this.btntemizle.Click += new System.EventHandler(this.btntemizle_Click);
            // 
            // btnkaydet
            // 
            resources.ApplyResources(this.btnkaydet, "btnkaydet");
            this.btnkaydet.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnkaydet.Appearance.BackColor")));
            this.btnkaydet.Appearance.BackColor2 = ((System.Drawing.Color)(resources.GetObject("btnkaydet.Appearance.BackColor2")));
            this.btnkaydet.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnkaydet.Appearance.Font")));
            this.btnkaydet.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnkaydet.Appearance.ForeColor")));
            this.btnkaydet.Appearance.Options.UseBackColor = true;
            this.btnkaydet.Appearance.Options.UseFont = true;
            this.btnkaydet.Appearance.Options.UseForeColor = true;
            this.btnkaydet.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnkaydet.AppearanceHovered.BackColor")));
            this.btnkaydet.AppearanceHovered.BackColor2 = ((System.Drawing.Color)(resources.GetObject("btnkaydet.AppearanceHovered.BackColor2")));
            this.btnkaydet.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnkaydet.AppearanceHovered.Font")));
            this.btnkaydet.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnkaydet.AppearanceHovered.ForeColor")));
            this.btnkaydet.AppearanceHovered.Options.UseBackColor = true;
            this.btnkaydet.AppearanceHovered.Options.UseFont = true;
            this.btnkaydet.AppearanceHovered.Options.UseForeColor = true;
            this.btnkaydet.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnkaydet.Name = "btnkaydet";
            this.btnkaydet.Click += new System.EventHandler(this.btnkaydet_Click);
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Name = "label4";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Name = "label2";
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DimGray;
            this.button2.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Orange;
            this.button1.FlatAppearance.BorderSize = 0;
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.label5);
            resources.ApplyResources(this.panel4, "panel4");
            this.panel4.Name = "panel4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.DimGray;
            this.label5.Name = "label5";
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.cevircagirDataSetBindingSource;
            resources.ApplyResources(this.gridControl1, "gridControl1");
            gridLevelNode1.RelationName = "FK_reports_tables";
            this.gridControl1.LevelTree.Nodes.AddRange(new DevExpress.XtraGrid.GridLevelNode[] {
            gridLevelNode1});
            this.gridControl1.MainView = this.tileView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.tileView1});
            // 
            // cevircagirDataSetBindingSource
            // 
            this.cevircagirDataSetBindingSource.DataSource = this.cevircagirDataSet;
            this.cevircagirDataSetBindingSource.Position = 0;
            // 
            // cevircagirDataSet
            // 
            this.cevircagirDataSet.DataSetName = "cevircagirDataSet";
            this.cevircagirDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tileView1
            // 
            this.tileView1.Appearance.EmptySpace.BackColor = ((System.Drawing.Color)(resources.GetObject("tileView1.Appearance.EmptySpace.BackColor")));
            this.tileView1.Appearance.EmptySpace.Options.UseBackColor = true;
            this.tileView1.Appearance.ItemSelected.Font = ((System.Drawing.Font)(resources.GetObject("tileView1.Appearance.ItemSelected.Font")));
            this.tileView1.Appearance.ItemSelected.Options.UseFont = true;
            this.tileView1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.tileView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.cltable_id1,
            this.clisactive});
            this.tileView1.GridControl = this.gridControl1;
            this.tileView1.Name = "tileView1";
            this.tileView1.OptionsTiles.ItemSize = new System.Drawing.Size(120, 40);
            this.tileView1.OptionsTiles.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tileView1.OptionsTiles.RowCount = 10;
            tileViewItemElement1.Appearance.Normal.BackColor = ((System.Drawing.Color)(resources.GetObject("resource.BackColor")));
            tileViewItemElement1.Appearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("resource.Font")));
            tileViewItemElement1.Appearance.Normal.ForeColor = ((System.Drawing.Color)(resources.GetObject("resource.ForeColor")));
            tileViewItemElement1.Appearance.Normal.Options.UseBackColor = true;
            tileViewItemElement1.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement1.Appearance.Normal.Options.UseForeColor = true;
            tileViewItemElement1.Column = this.cltable_id1;
            tileViewItemElement1.StretchHorizontal = true;
            tileViewItemElement1.StretchVertical = true;
            resources.ApplyResources(tileViewItemElement1, "tileViewItemElement1");
            tileViewItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.MiddleCenter;
            tileViewItemElement2.Column = this.clisactive;
            resources.ApplyResources(tileViewItemElement2, "tileViewItemElement2");
            this.tileView1.TileTemplate.Add(tileViewItemElement1);
            this.tileView1.TileTemplate.Add(tileViewItemElement2);
            this.tileView1.ItemClick += new DevExpress.XtraGrid.Views.Tile.TileViewItemClickEventHandler(this.tileView1_ItemClick);
            this.tileView1.ItemCustomize += new DevExpress.XtraGrid.Views.Tile.TileViewItemCustomizeEventHandler(this.tileView1_ItemCustomize_1);
            this.tileView1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.tileView1_MouseDown);
            // 
            // FormCihazKonfigurasyon
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormCihazKonfigurasyon";
            this.Load += new System.EventHandler(this.FormCihazKonfigürasyon_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tablesBindingSource1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tileView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label3;
        //private cevircagirDataSet cevircagirDataSet;
        private System.Windows.Forms.BindingSource tablesBindingSource1;
        //private cevircagirDataSetTableAdapters.tablesTableAdapter tablesTableAdapter1;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label5;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Tile.TileView tileView1;
        private DevExpress.XtraGrid.Columns.TileViewColumn cltable_id1;
        private DevExpress.XtraGrid.Columns.TileViewColumn clisactive;
        private DevExpress.XtraEditors.SimpleButton btnkaydet;
        private DevExpress.XtraEditors.SimpleButton btntemizle;
        private System.Windows.Forms.BindingSource cevircagirDataSetBindingSource;
        private cevircagirDataSet cevircagirDataSet;
    }
}