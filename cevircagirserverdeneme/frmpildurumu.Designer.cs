﻿namespace cevircagirserverdeneme
{
    partial class frmpildurumu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblgarsonbaslik = new System.Windows.Forms.Label();
            this.lblformname = new System.Windows.Forms.Label();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tablename = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
           // this.cevircagirDataSet = new cevircagirserverdeneme.cevircagirDataSet();
            this.tablesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            //this.tablesTableAdapter = new cevircagirserverdeneme.cevircagirDataSetTableAdapters.tablesTableAdapter();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            //((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablesBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.lblgarsonbaslik);
            this.panel3.Controls.Add(this.lblformname);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1100, 33);
            this.panel3.TabIndex = 20;
            // 
            // lblgarsonbaslik
            // 
            this.lblgarsonbaslik.AutoSize = true;
            this.lblgarsonbaslik.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblgarsonbaslik.ForeColor = System.Drawing.Color.Orange;
            this.lblgarsonbaslik.Location = new System.Drawing.Point(12, 10);
            this.lblgarsonbaslik.Name = "lblgarsonbaslik";
            this.lblgarsonbaslik.Size = new System.Drawing.Size(0, 19);
            this.lblgarsonbaslik.TabIndex = 13;
            // 
            // lblformname
            // 
            this.lblformname.AccessibleName = "";
            this.lblformname.AutoSize = true;
            this.lblformname.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblformname.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblformname.ForeColor = System.Drawing.Color.Orange;
            this.lblformname.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblformname.Location = new System.Drawing.Point(4, 7);
            this.lblformname.Name = "lblformname";
            this.lblformname.Size = new System.Drawing.Size(131, 19);
            this.lblformname.TabIndex = 12;
            this.lblformname.Text = "Cihaz Pil Durumları";
            this.lblformname.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.tablesBindingSource;
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 33);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(1100, 571);
            this.gridControl1.TabIndex = 21;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.tablename,
            this.gridColumn2});
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            // 
            // tablename
            // 
            this.tablename.Caption = "Cihaz Adı";
            this.tablename.FieldName = "table_name";
            this.tablename.Name = "tablename";
            this.tablename.Visible = true;
            this.tablename.VisibleIndex = 0;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "gridColumn2";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 1;
            // 
            // cevircagirDataSet
            // 
            //this.cevircagirDataSet.DataSetName = "cevircagirDataSet";
            //this.cevircagirDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tablesBindingSource
            // 
            this.tablesBindingSource.DataMember = "tables";
            //this.tablesBindingSource.DataSource = this.cevircagirDataSet;
            // 
            // tablesTableAdapter
            // 
            //this.tablesTableAdapter.ClearBeforeFill = true;
            // 
            // frmpildurumu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1100, 604);
            this.Controls.Add(this.gridControl1);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmpildurumu";
            this.Text = "frmpildurumu";
            this.Load += new System.EventHandler(this.frmpildurumu_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            //((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablesBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Label lblgarsonbaslik;
        public System.Windows.Forms.Label lblformname;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn tablename;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        //private cevircagirDataSet cevircagirDataSet;
        private System.Windows.Forms.BindingSource tablesBindingSource;
        //private cevircagirDataSetTableAdapters.tablesTableAdapter tablesTableAdapter;
    }
}