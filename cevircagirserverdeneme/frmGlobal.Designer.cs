﻿namespace cevircagirserverdeneme
{
    partial class frmGlobal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pbar = new DevExpress.XtraEditors.ProgressBarControl();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pbar.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // pbar
            // 
            this.pbar.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pbar.Location = new System.Drawing.Point(273, 493);
            this.pbar.Name = "pbar";
            this.pbar.Properties.Appearance.BorderColor = System.Drawing.Color.DimGray;
            this.pbar.Properties.Appearance.ForeColor = System.Drawing.Color.Orange;
            this.pbar.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.pbar.Properties.EndColor = System.Drawing.Color.Orange;
            this.pbar.Properties.ExportMode = DevExpress.XtraEditors.Repository.ExportMode.Value;
            this.pbar.Properties.LookAndFeel.Style = DevExpress.LookAndFeel.LookAndFeelStyle.Flat;
            this.pbar.Properties.LookAndFeel.UseDefaultLookAndFeel = false;
            this.pbar.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this.pbar.Properties.StartColor = System.Drawing.Color.Linen;
            this.pbar.Size = new System.Drawing.Size(555, 43);
            this.pbar.TabIndex = 0;
            this.pbar.EditValueChanged += new System.EventHandler(this.progressBarControl1_EditValueChanged);
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.button1.ForeColor = System.Drawing.Color.DimGray;
            this.button1.Image = global::cevircagirserverdeneme.Properties.Resources.orange_circle;
            this.button1.Location = new System.Drawing.Point(314, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(471, 426);
            this.button1.TabIndex = 1;
            this.button1.Text = "Lütfen Bekleyiniz..";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // frmGlobal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1100, 650);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pbar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmGlobal";
            this.Text = "frmGlobal";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmGlobal_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbar.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.ProgressBarControl pbar;
        private System.Windows.Forms.Button button1;
    }
}