﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cevircagirserverdeneme
{
    public partial class frmcihazekle : Form
    {
        public frmcihazekle()
        {
            InitializeComponent();
        }

        private void frmcihazekle_Load(object sender, EventArgs e)
        {
            //masa adı veya mac adreslerine göre cihaz kayıtlı değilse kontrolü yap.(Aramayı veri tabanında yap)
            txtmacid.Text = frmadmin.cihazmac;
            txtmasaadi.Text = frmadmin.masaadi;
        }
    }
}
