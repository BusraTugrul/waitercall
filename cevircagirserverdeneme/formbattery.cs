﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;

namespace cevircagirserverdeneme
{
    public partial class formbattery : Form
    {
        static string conString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\cevircagir.mdf';Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        //"Server=.;Database=cevircagir;Uid=sa;Password=bhappy_5;";
        SqlConnection baglanti;
        SqlCommand loadcommand, admincommand;
        ArrayList devicebattery = new ArrayList();
        TcpIpServer tisbattery;
        int devno = 0;

        private static readonly formbattery singletonfrmb = new formbattery();   //singleton nesne


        static formbattery()
        {

        }
        private formbattery()
        {
            InitializeComponent();
            baglanti = new SqlConnection(conString);
        }
        public static formbattery singletonfrmbattery
        {
            get
            {
                return singletonfrmb;
            }
        }
        public void addLabel(string mac, string bat)
        {
            Label l = new Label();
            l.Name = "lbl" + mac;
            l.Text = "Masa " + mac + "\r\n" + bat;
            l.Font = new Font("Segoe UI Semibold",12,FontStyle.Bold);
            l.Width = 150;
            l.Height = 70;
            l.ForeColor = Color.White;
            l.BackColor = Color.DimGray;
            l.TextAlign = ContentAlignment.MiddleCenter;
            l.Margin = new Padding(5);
            Console.WriteLine("***"+ mac + "***" + bat);
            //pnllayout.Controls.Add(l);
            //if (devno < devicebattery.Count)
            //{
            //    sendData(devicebattery[devno].ToString() + "01010D");
            //    devno++;
            //    //burada diziye ekle
            //}
            //else
            //{
            //    devno = 0;
            //    Console.WriteLine("batarya sorguları bitti");
            //    //burada ekrana bas
            //}
        }

        private void formbattery_Load(object sender, EventArgs e)
        {
            //veri tabanından kaç cihaz olduğunu çek ve cihaz numaralarını listeye at
            tisbattery = TcpIpServer.singletontisobj;
        }
        public void getBattery()
        {
            baglanti.Open();
            devicebattery.Clear();
            loadcommand = new SqlCommand("SELECT DISTINCT mac_address FROM tables", baglanti);
            using (SqlDataReader dr = loadcommand.ExecuteReader())  //select sorgusundan gelen verileri döndürür.
            {
                while (dr.Read())//satır varsa
                {
                    devicebattery.Add(dr["mac_address"].ToString());
                }
            }
            baglanti.Close();
            //sendData(devicebattery[devno].ToString() + "01010D");
            //devno++;
        }

        public void deneme(string mac, string bat)
        {
            //Label l = addLabel(mac, Convert.ToInt16(bat));
            //pnllayout.Controls.Add(l);
            sendData(devicebattery[1].ToString() + "01010D");
        }
        private void lblformname_Click(object sender, EventArgs e)
        {
            //getBattery();
        }
        

        public void sendData(string data)
        {
            Console.WriteLine(data);
            tisbattery.ocd = data;
            tisbattery.sendData();
        }
    }
}
