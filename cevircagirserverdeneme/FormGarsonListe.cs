﻿using DevExpress.Utils.Menu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cevircagirserverdeneme
{

    public partial class FormGarsonListe : Form
    {
        static string conString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\cevircagir.mdf';Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        SqlConnection baglanti = new SqlConnection(conString);
        SqlCommand command;
        string komutgarson = "select waiter_name from waiter", komutmasa = "select table_name from tables";
        public DevExpress.XtraEditors.PanelControl pnlcntrl;
        public DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnedt;
        public DevExpress.XtraGrid.Views.Grid.GridView gview;
        public DevExpress.XtraEditors.SimpleButton btnyeni;
        string sorgu;
        bool dragging;
        Point offset;
        SqlTransaction myTransaction = null;
        string deleteRelationShip;
        //public DevExpress.XtraEditors.SimpleButton btnlist;

        private static readonly FormGarsonListe singletonfrmgarson = new FormGarsonListe();  //singleton nesne
        
        static FormGarsonListe()
        {

        }
        private FormGarsonListe()
        {
            btnyeni = btnyeniiliski;
            InitializeComponent();
            pnlcntrl = pnlislem;
            btnedt = btn_delete;
            gview = gridView1;
        }
        public static FormGarsonListe singletonfrmgar
        {
            get
            {
                return singletonfrmgarson;
            }
        }
        private void FormGarsonListe_Load(object sender, EventArgs e)
        {
            // TODO: Bu kod satırı 'cevircagirDataSet.waitersstables' tablosuna veri yükler. Bunu gerektiği şekilde taşıyabilir, veya kaldırabilirsiniz.
            // this.waitersstablesTableAdapter1.Fill(this.cevircagirDataSet.waitersstables);
            // TODO: Bu kod satırı 'cevircagirDataSet5.waitersstables' tablosuna veri yükler. Bunu gerektiği şekilde taşıyabilir, veya kaldırabilirsiniz.
            //this.waitersstablesTableAdapter.Fill(this.cevircagirDataSet5.waitersstables);

            deleteRelationShip = Localization.deleteRelationShip;
            labelControl1.Text = Localization.labelControl1;
            labelControl2.Text = Localization.labelControl2;
            btnlistele.Text = Localization.btnList;
            colwaiter_name.Caption = Localization.colwaiter_name;
            coltable_name.Caption = Localization.coltable_name;
            gridView1.GroupPanelText = Localization.gridPanelText;
            gridView1.Columns[0].OptionsColumn.AllowEdit = false;  //garson kolonunun düzenlenmesini önler.
            gridView1.Columns[1].OptionsColumn.AllowEdit = false;  //masa kolonunun düzenlenmesini önler.
            textautocompletegarson();
            textautocompletemasa();
            btnlistele_Click(sender, e);    //listeler
            btnyeniiliski.Text = Localization.btnyeniiliski;
            btnnewrecord.Text = Localization.btnNew;
        }
        private void btnkapat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btn_delete_Click(object sender, EventArgs e)
        {
            
        }

        private void btnyeniiliski_Click(object sender, EventArgs e)
        {
            FormGarsonlar frmgrsn = new FormGarsonlar();
            frmgrsn.pnlduzenle.Visible = true;
            frmgrsn.pnlyeni.Visible = false;
            frmgrsn.duzenle = 1;
            frmgrsn.Show();
            //this.Hide();
        }//btnyeniiliski_Click
        void textautocompletegarson()
        {
            SqlDataReader dr;
            AutoCompleteStringCollection colectiongarson = new AutoCompleteStringCollection();
            command = new SqlCommand(komutgarson, baglanti);
            try
            {
                baglanti.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    string garsonismi = dr["waiter_name"].ToString();
                    colectiongarson.Add(garsonismi);
                }
                txtgarson_ad.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txtgarson_ad.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txtgarson_ad.AutoCompleteCustomSource = colectiongarson;
                dr.Close();
                baglanti.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }//textautocompletegarson

        private void btn_delete_ButtonClick(object sender, DevExpress.XtraEditors.Controls.ButtonPressedEventArgs e)
        {
            //sil butonuna tıklandığında -->seçili satırı gridden kaldırıyor, waitersstables tablosundan ilişkiyi siliyor. 
            //tables tablosundan silinen masa için isselected=0 yapılıyor.
            baglanti.Open();
            DataRow row = gridView1.GetDataRow(gridView1.GetSelectedRows()[0]);
            string cellValue = row[3].ToString();
            string garson = row[2].ToString();
            string masa = row[4].ToString();
            formdelete form = new formdelete();
            form.txtdlt.Text =String.Format(Localization.deleteRelationShip,garson,masa);
            form.ShowDialog();
            if (form.DialogResult == DialogResult.Yes)
            {
                command = baglanti.CreateCommand();
                myTransaction = baglanti.BeginTransaction();
                command.Connection = baglanti;
                command.Transaction = myTransaction;
                string isselected = "update tables set isselected=0 where table_id=@tid";
                command.CommandText=isselected;
                command.Parameters.AddWithValue("@tid", Convert.ToInt32(cellValue));
                command.ExecuteNonQuery();
                string delete = "delete from waitersstables where waiter_name=@w_n and table_id=@t_id";
                command.CommandText=delete;
                command.Parameters.AddWithValue("@t_id", Convert.ToInt32(cellValue));
                command.Parameters.AddWithValue("@w_n", garson);
                command.ExecuteNonQuery();
                myTransaction.Commit();
                string selectnew = "select id,waiter_id,waiter_name,table_id,table_name from waitersstables";
                
                command = new SqlCommand(selectnew, baglanti);
                SqlDataAdapter da = new SqlDataAdapter(command);
                DataSet ds = new DataSet();
                da.Fill(ds);
                gridControl1.DataSource = null;
                gridControl1.DataSource = ds.Tables[0];
                gridControl1.ViewCollection.Clear();
                gridControl1.ViewCollection.Add(gridView1);
                gridControl1.MainView = gridView1;
                gridControl1.RefreshDataSource();
                //this.waitersstablesTableAdapter1.Fill(this.cevircagirDataSet.waitersstables);
                //gridControl1.DataSource = null;
                //command = new SqlCommand(selectsorgusu, baglanti);
                //da = new SqlDataAdapter(command);
                //ds = new DataSet();
                //da.Fill(ds);
                //gridControl1.DataSource = ds.Tables[0];
                //gridControl1.ViewCollection.Clear();
                //gridControl1.ViewCollection.Add(gridView1);
                //gridControl1.MainView = gridView1;
                //gridControl1.RefreshDataSource();
                //baglanti.Close();
                //this.waitersstablesBindingSource1.RemoveCurrent();
                //this.waitersstablesTableAdapter1.Update(cevircagirDataSet);
                //this.waitersstablesBindingSource.RemoveCurrent();
                //this.waitersstablesTableAdapter.Update(cevircagirDataSet5);
            }
            else
               form.Close();
            baglanti.Close();
        }//btn_delete_ButtonClick

        private void btnlistele_Click(object sender, EventArgs e)
        {
            string selectfrom = "select id,waiter_id,waiter_name,table_id,table_name from waitersstables";
            baglanti.Open();
            if(txtgarson_ad.Text!=""&&txtmasaad.Text=="")
            {
                sorgu = " where waiter_name='"+txtgarson_ad.Text+"'";
            }
            if(txtmasaad.Text!=""&&txtgarson_ad.Text!="")
            {
                sorgu = " where waiter_name='" + txtgarson_ad.Text + "' and table_name='"+txtmasaad.Text+"'";
            }
            if(txtmasaad.Text!=""&&txtgarson_ad.Text=="")
            {
                sorgu = " where table_name='" + txtmasaad.Text + "'";
            }
            
            selectfrom +=sorgu;
            sorgu = null;
            command = new SqlCommand(selectfrom, baglanti);
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gridControl1.DataSource = null;
            gridControl1.DataSource = ds.Tables[0];
            gridControl1.ViewCollection.Clear();
            gridControl1.ViewCollection.Add(gridView1);
            gridControl1.MainView = gridView1;
            gridControl1.RefreshDataSource();
            txtgarson_ad.Clear();
            txtmasaad.Clear();
            baglanti.Close();
            gridView1.Focus();
        }//btnlistele_Click

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            offset = e.Location;
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point currentScreenPos = PointToScreen(e.Location);
                Location = new
                Point(currentScreenPos.X - offset.X,
                currentScreenPos.Y - offset.Y);
            }
        }

        private void btnnewrecord_Click(object sender, EventArgs e)
        {
            FormGarsonlar frmyeni = new FormGarsonlar();
            frmyeni.pnlduzenle.Visible = false;
            frmyeni.pnlyeni.Visible = true;
            frmyeni.duzenle = 2;
            frmyeni.Show();
        }

        private void txtmasaad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtmasaad.Text.Length > 30)
            {
                e.Handled = !(e.KeyChar == (char)Keys.Back);
            }
            else
                e.Handled = !(char.IsLetter(e.KeyChar) || char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space);
        }

        private void txtgarson_ad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtgarson_ad.Text.Length > 30)
            {
                e.Handled = !(e.KeyChar == (char)Keys.Back);
            }
            else
                e.Handled = !(char.IsLetter(e.KeyChar) || char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space);
        }

        void textautocompletemasa()
        {
            SqlDataReader dr;
            AutoCompleteStringCollection colectiongarson = new AutoCompleteStringCollection();
            SqlCommand command = new SqlCommand(komutmasa, baglanti);
            try
            {
                baglanti.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    string masaismi = dr["table_name"].ToString();
                    colectiongarson.Add(masaismi);
                }
                txtmasaad.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txtmasaad.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txtmasaad.AutoCompleteCustomSource = colectiongarson;

                dr.Close();
                baglanti.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }//textautocompletemasa
    }
}
