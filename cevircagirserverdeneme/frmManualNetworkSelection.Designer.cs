﻿namespace cevircagirserverdeneme
{
    partial class frmManualNetworkSelection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmManualNetworkSelection));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            this.panel3 = new System.Windows.Forms.Panel();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.lblgarsonbaslik = new System.Windows.Forms.Label();
            this.lblformname = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPnl2Next = new DevExpress.XtraEditors.SimpleButton();
            this.cmbNetwork = new DevExpress.XtraEditors.ComboBoxEdit();
            this.pnlforwifi = new System.Windows.Forms.Panel();
            this.chkSerial = new System.Windows.Forms.CheckBox();
            this.chkwifi = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbNetwork.Properties)).BeginInit();
            this.pnlforwifi.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Orange;
            this.panel3.Controls.Add(this.simpleButton1);
            this.panel3.Controls.Add(this.lblgarsonbaslik);
            this.panel3.Controls.Add(this.lblformname);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1100, 40);
            this.panel3.TabIndex = 21;
            // 
            // simpleButton1
            // 
            this.simpleButton1.AllowFocus = false;
            this.simpleButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.simpleButton1.Appearance.BackColor = System.Drawing.Color.Transparent;
            this.simpleButton1.Appearance.BackColor2 = System.Drawing.Color.Transparent;
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton1.Image = global::cevircagirserverdeneme.Properties.Resources.icons8_Close_Window_Filled_30;
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(1056, 4);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 2, 11, 2);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(33, 30);
            this.simpleButton1.TabIndex = 29;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // lblgarsonbaslik
            // 
            this.lblgarsonbaslik.AutoSize = true;
            this.lblgarsonbaslik.Font = new System.Drawing.Font("Segoe UI Semibold", 7.8F, System.Drawing.FontStyle.Bold);
            this.lblgarsonbaslik.ForeColor = System.Drawing.Color.Orange;
            this.lblgarsonbaslik.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblgarsonbaslik.Location = new System.Drawing.Point(12, 10);
            this.lblgarsonbaslik.Name = "lblgarsonbaslik";
            this.lblgarsonbaslik.Size = new System.Drawing.Size(0, 19);
            this.lblgarsonbaslik.TabIndex = 13;
            // 
            // lblformname
            // 
            this.lblformname.AccessibleName = "";
            this.lblformname.AutoSize = true;
            this.lblformname.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblformname.Font = new System.Drawing.Font("Segoe UI Semibold", 14F, System.Drawing.FontStyle.Bold);
            this.lblformname.ForeColor = System.Drawing.Color.White;
            this.lblformname.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblformname.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lblformname.Location = new System.Drawing.Point(4, 3);
            this.lblformname.Name = "lblformname";
            this.lblformname.Size = new System.Drawing.Size(346, 32);
            this.lblformname.TabIndex = 12;
            this.lblformname.Text = "Ağ Seçimi / Network Selection";
            this.lblformname.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label17
            // 
            this.label17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.DimGray;
            this.label17.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label17.Location = new System.Drawing.Point(174, 31);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(196, 20);
            this.label17.TabIndex = 75;
            this.label17.Text = "Ağ Seçiniz / Select Network";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label1.Location = new System.Drawing.Point(160, 131);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 28);
            this.label1.TabIndex = 76;
            this.label1.Visible = false;
            // 
            // btnPnl2Next
            // 
            this.btnPnl2Next.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPnl2Next.Appearance.BackColor = System.Drawing.Color.Orange;
            this.btnPnl2Next.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.btnPnl2Next.Appearance.ForeColor = System.Drawing.Color.White;
            this.btnPnl2Next.Appearance.Options.UseBackColor = true;
            this.btnPnl2Next.Appearance.Options.UseFont = true;
            this.btnPnl2Next.Appearance.Options.UseForeColor = true;
            this.btnPnl2Next.AppearanceHovered.BackColor = System.Drawing.Color.White;
            this.btnPnl2Next.AppearanceHovered.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.btnPnl2Next.AppearanceHovered.ForeColor = System.Drawing.Color.DimGray;
            this.btnPnl2Next.AppearanceHovered.Options.UseBackColor = true;
            this.btnPnl2Next.AppearanceHovered.Options.UseFont = true;
            this.btnPnl2Next.AppearanceHovered.Options.UseForeColor = true;
            this.btnPnl2Next.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnPnl2Next.Location = new System.Drawing.Point(723, 373);
            this.btnPnl2Next.Name = "btnPnl2Next";
            this.btnPnl2Next.Size = new System.Drawing.Size(84, 32);
            this.btnPnl2Next.TabIndex = 77;
            this.btnPnl2Next.Text = "İleri / Next";
            this.btnPnl2Next.Click += new System.EventHandler(this.btnPnl2Next_Click);
            // 
            // cmbNetwork
            // 
            this.cmbNetwork.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.cmbNetwork.Location = new System.Drawing.Point(178, 78);
            this.cmbNetwork.Name = "cmbNetwork";
            this.cmbNetwork.Properties.AllowFocused = false;
            this.cmbNetwork.Properties.Appearance.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.cmbNetwork.Properties.Appearance.ForeColor = System.Drawing.Color.DimGray;
            this.cmbNetwork.Properties.Appearance.Options.UseFont = true;
            this.cmbNetwork.Properties.Appearance.Options.UseForeColor = true;
            this.cmbNetwork.Properties.AppearanceDropDown.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.cmbNetwork.Properties.AppearanceDropDown.Options.UseFont = true;
            this.cmbNetwork.Properties.AppearanceDropDown.Options.UseForeColor = true;
            this.cmbNetwork.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbNetwork.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, DevExpress.XtraEditors.ImageLocation.MiddleCenter, ((System.Drawing.Image)(resources.GetObject("cmbNetwork.Properties.Buttons"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, "", null, null, true)});
            this.cmbNetwork.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.cmbNetwork.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbNetwork.Size = new System.Drawing.Size(530, 34);
            this.cmbNetwork.TabIndex = 78;
            // 
            // pnlforwifi
            // 
            this.pnlforwifi.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.pnlforwifi.Controls.Add(this.label17);
            this.pnlforwifi.Controls.Add(this.cmbNetwork);
            this.pnlforwifi.Controls.Add(this.label1);
            this.pnlforwifi.Location = new System.Drawing.Point(100, 196);
            this.pnlforwifi.Name = "pnlforwifi";
            this.pnlforwifi.Size = new System.Drawing.Size(898, 171);
            this.pnlforwifi.TabIndex = 79;
            // 
            // chkSerial
            // 
            this.chkSerial.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.chkSerial.AutoSize = true;
            this.chkSerial.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.chkSerial.ForeColor = System.Drawing.Color.DimGray;
            this.chkSerial.Location = new System.Drawing.Point(535, 144);
            this.chkSerial.Name = "chkSerial";
            this.chkSerial.Size = new System.Drawing.Size(59, 24);
            this.chkSerial.TabIndex = 80;
            this.chkSerial.Text = "USB";
            this.chkSerial.UseVisualStyleBackColor = true;
            this.chkSerial.CheckedChanged += new System.EventHandler(this.chkSerial_CheckedChanged);
            // 
            // chkwifi
            // 
            this.chkwifi.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.chkwifi.AutoSize = true;
            this.chkwifi.Checked = true;
            this.chkwifi.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkwifi.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.chkwifi.ForeColor = System.Drawing.Color.DimGray;
            this.chkwifi.Location = new System.Drawing.Point(283, 144);
            this.chkwifi.Name = "chkwifi";
            this.chkwifi.Size = new System.Drawing.Size(67, 24);
            this.chkwifi.TabIndex = 81;
            this.chkwifi.Text = "Wi-Fi";
            this.chkwifi.UseVisualStyleBackColor = true;
            this.chkwifi.CheckedChanged += new System.EventHandler(this.chkwifi_CheckedChanged);
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold);
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.label2.Location = new System.Drawing.Point(274, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(413, 20);
            this.label2.TabIndex = 79;
            this.label2.Text = "Erişim Noktası Bağlantı Şekli/ Access Point Connection Type";
            // 
            // frmManualNetworkSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1100, 650);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.chkwifi);
            this.Controls.Add(this.btnPnl2Next);
            this.Controls.Add(this.chkSerial);
            this.Controls.Add(this.pnlforwifi);
            this.Controls.Add(this.panel3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "frmManualNetworkSelection";
            this.Text = "frmManualNetworkSelection";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.frmManualNetworkSelection_Load);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbNetwork.Properties)).EndInit();
            this.pnlforwifi.ResumeLayout(false);
            this.pnlforwifi.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        public System.Windows.Forms.Label lblgarsonbaslik;
        public System.Windows.Forms.Label lblformname;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton btnPnl2Next;
        private DevExpress.XtraEditors.ComboBoxEdit cmbNetwork;
        private System.Windows.Forms.Panel pnlforwifi;
        private System.Windows.Forms.CheckBox chkSerial;
        private System.Windows.Forms.CheckBox chkwifi;
        private System.Windows.Forms.Label label2;
    }
}