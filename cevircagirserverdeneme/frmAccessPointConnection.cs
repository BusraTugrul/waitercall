﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cevircagirserverdeneme
{
    public partial class frmAccessPointConnection : Form
    {
        Timer tmr;
        UdpBroadcast udpbr;
        public int counter = 0;
        public frmAccessPointConnection()
        {
            InitializeComponent();
            labelControl4.Text = Localization.apconnect;
        }

        private void frmAccessPointConnection_Load(object sender, EventArgs e)
        {
            Screen screen = Screen.FromControl(this);
            int x = screen.WorkingArea.X - screen.Bounds.X;
            int y = screen.WorkingArea.Y - screen.Bounds.Y;
            this.MaximizedBounds = new Rectangle(x, y,
                screen.WorkingArea.Width, screen.WorkingArea.Height);
            this.MaximumSize = screen.WorkingArea.Size;
            this.WindowState = FormWindowState.Maximized;

            udpbr = UdpBroadcast.singletonudpobject;
            tmr = new Timer();
            tmr.Interval = 300;
            tmr.Tick += new EventHandler(onTimeout);
            tmr.Start();

            pbar.Properties.Step = 1;
            pbar.Properties.PercentView = true;
            pbar.Properties.Maximum = Settings1.Default.devicenum;
            pbar.Properties.Minimum = 0;
        }

        public void onTimeout(Object myObject, EventArgs myEventArgs)
        {
            if (counter<udpbr.devcounter)
            {
                pbar.PerformStep();
                pbar.Update();
                counter++;
            }
            else if(counter ==Settings1.Default.devicenum)
            {
                tmr.Stop();
                this.Close();
            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            tmr.Stop();
            this.Close();
        }


        private void pbar_EditValueChanged(object sender, EventArgs e)
        {

        }

        private void labelControl4_Click(object sender, EventArgs e)
        {

        }
    }
}
