﻿using DevExpress.XtraGrid.Views.Tile;
using DevExpress.XtraGrid.Views.Tile.ViewInfo;
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace cevircagirserverdeneme
{
    public partial class FormGarsonlar : Form   //garson kaydı ve masa seçimi burdan yapılır
    {
        public Panel pnlyeni;
        public Panel pnlduzenle;
        public int duzenle = 0;
        ArrayList mad = new ArrayList();
        //private static readonly FormGarsonlar singletonfrmgars = new FormGarsonlar();
        static string conString=@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\cevircagir.mdf';Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        SqlConnection baglanti = new SqlConnection(conString);
        SqlCommand command;
        bool dragging;//asd
        Point offset;
        int id = 0;

        //static FormGarsonlar()
        //{

        //}
        public FormGarsonlar()
        {
            InitializeComponent();
            pnlyeni = pnlgarsonyeni;
            pnlduzenle = pnlgarsonduzenle;
        }
        //public static FormGarsonlar singletonfrmg
        //{
        //    get
        //    {
        //        return singletonfrmgars;
        //    }
        //}

        private void FormBolumler_Load(object sender, EventArgs e)
        {
            // TODO: Bu kod satırı 'cevircagirDataSet.waiter' tablosuna veri yükler. Bunu gerektiği şekilde taşıyabilir, veya kaldırabilirsiniz.
            this.waiterTableAdapter1.Fill(this.cevircagirDataSet.waiter);
            // TODO: Bu kod satırı 'cevircagirDataSet.tables' tablosuna veri yükler. Bunu gerektiği şekilde taşıyabilir, veya kaldırabilirsiniz.
            this.tablesTableAdapter.Fill(this.cevircagirDataSet.tables);
            //this.waiterTableAdapter.Fill(this.cevircagirDataSet5.waiter);
            btnmasasec.Text = Localization.btnmasasec;
            btnKaydet.Text = Localization.btnKaydet;
            labelControl2.Text = Localization.labelControl2_;
            labelControl1.Text = Localization.labelControl1_;
            lblad.Text = Localization.labelControl2_;
            colbool.Visible = false;  //veri tabanındaki isselected alanının tileviev kolon adı=colbool.
            cmbgarsonlar.Text = null;
            baglanti.Open();
            string idal = "SELECT TOP 1 waiter_id FROM waiter ORDER BY waiter_id desc";
            command = new SqlCommand(idal,baglanti);
            SqlDataReader sread = command.ExecuteReader();
            if (sread.Read())
            {
                id = Convert.ToInt32(sread["waiter_id"].ToString());
                id+=1;
                txtgarson_kod.Text = id.ToString();
            }
            else
            {
                id = 1;
                txtgarson_kod.Text = "1";
            }
            baglanti.Close();
        }//FormBolumler_Load

        private void btnmasasec_Click(object sender, EventArgs e)
        {
            mad.Clear();
            if(baglanti.State!=ConnectionState.Open)
                baglanti.Open();
            gridControl1.DataSource = null;
            string fillgrid = "select table_name,isselected from tables where isselected=0";
            command = new SqlCommand(fillgrid, baglanti);
            command.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gridControl1.DataSource = ds.Tables[0];   //Dataset yerine datatable olunca hata veren satır
            gridControl1.ViewCollection.Clear();
            gridControl1.ViewCollection.Add(tileView1);
            gridControl1.MainView = tileView1;
            gridControl1.RefreshDataSource();
            baglanti.Close();
            gridControl1.Visible = true;
            gridControl1.Focus();
            btnKaydet.Visible = true;
        }//btnmasasec_Click

        private void btnKaydet_Click(object sender, EventArgs e)
        {
            int a;
            if(baglanti.State != ConnectionState.Open)
                baglanti.Open();
            if (duzenle== 1 && cmbgarsonlar.Items.Count>0 && cmbgarsonlar.SelectedValue != null)   //garson işlem formunun yeni ilişki butonu tıklandıysa
            {
                string ad = cmbgarsonlar.SelectedValue.ToString();
                string kod = "select waiter_id from waiter where waiter_name=@name";
                command = new SqlCommand(kod, baglanti);
                command.Parameters.AddWithValue("@name",ad);
                SqlDataAdapter da = new SqlDataAdapter(command);
                SqlDataReader sread = command.ExecuteReader();
                if (sread.Read())
                {
                    a = Convert.ToInt32(sread["waiter_id"].ToString());
                    //kntrl = 0;
                    sread.Close();
                    kaydet(ad, a);
                }
                cmbgarsonlar.Text = null;
            }
            if(duzenle==2)   //yeni kayıt işlemi yapılıyorsa
            {
                if (txtgarson_ad.Text != "")
                {
                    string conwaiter = "insert into waiter(waiter_id,waiter_name)values(@waiter_id,@waitername)";
                    command = new SqlCommand(conwaiter, baglanti);
                    command.Parameters.AddWithValue("@waiter_id", txtgarson_kod.Text);
                    command.Parameters.AddWithValue("@waitername", txtgarson_ad.Text);
                    command.ExecuteNonQuery();
                    kaydet(txtgarson_ad.Text, Convert.ToInt32(txtgarson_kod.Text));
                }
                else
                    baglanti.Close();
            }
        }//btnKaydet_Click

        public void kaydet(string garsonad, int garsonkod)
        {
            foreach (object masaid in mad)
            {
                string contables = "update tables set isselected=1 where table_name=@mid";
                command = new SqlCommand(contables, baglanti);
                command.Parameters.AddWithValue("@mid", masaid.ToString());
                command.ExecuteNonQuery();
                Console.WriteLine(garsonkod);
                string conwaiterstable = "insert into waitersstables(waiter_id,waiter_name,table_id,table_name) select @mid1,@mid2, t.table_id,@table_name from tables t where t.table_name=@table_name";
                command = new SqlCommand(conwaiterstable, baglanti);
                command.Parameters.AddWithValue("@table_name", masaid.ToString());
                command.Parameters.AddWithValue("@mid1", garsonkod);
                command.Parameters.AddWithValue("@mid2", garsonad);
                command.ExecuteNonQuery();
                //string ax = "update waitersstables set waiter_id=@mid1, waiter_name=@mid2 where table_id=@tid";
                //command = new SqlCommand(ax, baglanti);
                //command.Parameters.AddWithValue("@tid", Convert.ToInt32(masaid));
                //command.ExecuteNonQuery();
            }
            mad.Clear();
            id += 1;
            txtgarson_kod.Text = id.ToString();
            txtgarson_ad.Text = "";
            //baglanti.Open();
            gridControl1.DataSource = null;
            string fillgrid = "select table_name,isselected from tables where isselected=0";
            command = new SqlCommand(fillgrid, baglanti);
            command.ExecuteNonQuery();
            SqlDataAdapter da = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            da.Fill(ds);
            gridControl1.DataSource = ds.Tables[0];   //Dataset yerine datatable olunca hata veren satır
            gridControl1.ViewCollection.Clear();
            gridControl1.ViewCollection.Add(tileView1);
            gridControl1.MainView = tileView1;
            gridControl1.RefreshDataSource();
            baglanti.Close();
        }//kaydet
        private void tileView1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Left)
            {
                TileView view = sender as TileView;
                TileViewHitInfo hitInfo = view.CalcHitInfo(e.Location);
                if (hitInfo.InItem)
                {
                    if (hitInfo.Item.Elements[0].Appearance.Normal.BackColor == Color.Orange)
                    {
                        int val = 1;
                        view.SetRowCellValue(hitInfo.RowHandle, colbool, val);
                        if(!mad.Contains(tileView1.GetRowCellDisplayText(hitInfo.Item.RowHandle, coltable_id)))
                            mad.Add(tileView1.GetRowCellDisplayText(hitInfo.Item.RowHandle, coltable_id));
                    }
                    else if (hitInfo.Item.Elements[0].Appearance.Normal.BackColor == Color.DimGray)
                    {
                        int val = 0;
                        view.SetRowCellValue(hitInfo.RowHandle, colbool, val);
                        mad.Remove(tileView1.GetRowCellDisplayText(hitInfo.Item.RowHandle, coltable_id));
                    }
                }
            }
        }//tileView1_MouseDown

        private void tileView1_ItemCustomize(object sender, TileViewItemCustomizeEventArgs e)
        {
            TileView view = sender as TileView;
            int val = Convert.ToInt32( view.GetRowCellValue(e.RowHandle, colbool));
            if (val==0)
            {
                e.Item.Elements[0].Appearance.Normal.BackColor = Color.Orange;
            }
            if (val==1)
            {
                e.Item.Elements[0].Appearance.Normal.BackColor = Color.DimGray;
            }
        }//tileView1_ItemCustomize
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            offset = e.Location;
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                Point currentScreenPos = PointToScreen(e.Location);
                Location = new Point(currentScreenPos.X - offset.X, currentScreenPos.Y - offset.Y);
            }
        }

        private void cmbgarsonlar_SelectedIndexChanged(object sender, EventArgs e)
        {
            panel1.Focus();
        }

        private void cmbgarsonlar_SelectedValueChanged(object sender, EventArgs e)
        {
            panel1.Focus();
        }

        private void txtgarson_ad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtgarson_ad.Text.Length > 30)
            {
                e.Handled = !(e.KeyChar == (char)Keys.Back);
            }
            else
                e.Handled = !(char.IsLetter(e.KeyChar) || char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space);
        }
    }
}
