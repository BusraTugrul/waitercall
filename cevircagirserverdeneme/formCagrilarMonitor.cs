﻿using DevExpress.XtraGrid.Views.Tile;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Collections;
using System.Diagnostics;

namespace cevircagirserverdeneme
{
    public partial class formCagrilarMonitor : Form  //bu form; eğer ikinci bir monitör varsa tam ekran yansıtılacak olan masalar çağrı formudur. 
    {
        public DevExpress.XtraGrid.GridControl gridcagrilarmonitor;
        public DevExpress.XtraGrid.Views.Tile.TileView tvmainvmonitor;
        static string conString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\cevircagir.mdf';Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        SqlConnection baglanti;
        SqlCommand loadcommand;
        formadminsettings frmset;
        Color r1;
        int sure1, sure2, sure3;
        string cname,dk_sn;
        int[] sureler = { 10, 20, 30, 40, 60, 80, 120, 180, 240, 300 };
        public DevExpress.XtraEditors.PanelControl pnl1;
        public ArrayList color = new ArrayList();
        Image immor ,imyesil,imkirmizi;
        private static readonly formCagrilarMonitor singletonfrmcm = new formCagrilarMonitor(); //singleton nesnesi
        static formCagrilarMonitor()
        {
            //static constructor
        }
        private formCagrilarMonitor()
        {
            InitializeComponent();
            gridcagrilarmonitor = this.gridControl1;
            tvmainvmonitor = this.tvMainv;
            pnl1 = panelControl1;
            frmset = new formadminsettings();
        }
        public static formCagrilarMonitor singletonfrmcagrilarm
        {
            get
            {
                return singletonfrmcm;
            }
        }
        public int adminsetingskaydet { get; set; }
        
        private void formCagrilarMonitor_Load(object sender, EventArgs e)
        {
            string a =Environment.CurrentDirectory;
            baglanti = new SqlConnection(conString);
            veritaboku();
            lblcompanyname.Text = cname;  //form açılırken firma adını üst panele ekle
            immor = Properties.Resources.akebossonmor150;
            imyesil = Properties.Resources.akebossonyesil150;
            imkirmizi = Properties.Resources.akebossonkirmizi150;
        }

        private void formCagrilarMonitor_Shown(object sender, EventArgs e)
        {
        }
        public void tvMainv_ItemCustomize(object sender, TileViewItemCustomizeEventArgs e)
        {
            if (adminsetingskaydet == 1)
            {
                veritaboku();
                lblcompanyname.Text = cname;
            }
            adminsetingskaydet = 0;
            if (color.Count >= 0)
            {
                if (!color.Contains(e))
                    color.Add(e);
            }
            TimeSpan nownow = DateTime.Now.TimeOfDay;
            TimeSpan ts = new TimeSpan();
            TileView view = sender as TileView;
            if (view.RowCount > 0)
            {
                string val = Convert.ToString(view.GetRowCellValue(e.RowHandle, clreqtime));
                ts = nownow - Convert.ToDateTime(val).TimeOfDay;
                int hesaplasn = ts.Hours * 3600 + ts.Minutes * 60 + ts.Seconds;
                int hesapladk = ts.Hours * 3600 + ts.Minutes;
                dk_sn = ts.Minutes.ToString("D2") + ":" + ts.Seconds.ToString("D2");
                e.Item.Elements[3].Text = dk_sn;
                if (sure1 != 0 && r1 != Color.Transparent && hesaplasn >= sure1 && hesaplasn < sure2 && hesaplasn < sure3)
                {
                    e.Item.Elements[0].StretchVertical = true;
                    e.Item.Elements[2].Image = imyesil;
                    e.Item.Elements[0].Appearance.Normal.ForeColor = Color.Green;
                    e.Item.Elements[1].Appearance.Normal.ForeColor = Color.Green;
                    e.Item.Elements[3].Appearance.Normal.ForeColor = Color.Green;
                }
                if (sure2 != 0 && r1 != Color.Transparent && hesaplasn >= sure2 && hesaplasn < sure3)
                {
                    e.Item.Elements[0].StretchVertical = true;
                    e.Item.Elements[2].Image = immor;
                    e.Item.Elements[0].Appearance.Normal.ForeColor = Color.Purple;
                    e.Item.Elements[1].Appearance.Normal.ForeColor = Color.Purple;
                    e.Item.Elements[3].Appearance.Normal.ForeColor = Color.Purple;
                }
                if (sure3 != 0 && r1 != Color.Transparent && hesaplasn >= sure3)
                {
                    e.Item.Elements[0].StretchVertical = true;
                    e.Item.Elements[2].Image = imkirmizi;
                    e.Item.Elements[0].Appearance.Normal.ForeColor = Color.Red;
                    e.Item.Elements[1].Appearance.Normal.ForeColor = Color.Red;
                    e.Item.Elements[3].Appearance.Normal.ForeColor = Color.Red;
                }
            }
        }
        public Color coloring()
        {
            int arraycountm, notredcountm;
            for (arraycountm = 0; arraycountm <= color.Count - 1;)
            {
                notredcountm = arraycountm;
                //Debug.WriteLine("tile eleman sayısı  " + color.Count.ToString());
                TileViewItemCustomizeEventArgs tl = (TileViewItemCustomizeEventArgs)color[arraycountm];
                arraycountm++;
                if (tl.Item.Elements[0].Appearance.Normal.ForeColor != Color.Red)
                {
                    notredcountm++;
                }
                //color.Remove(tl);
                if (notredcountm < arraycountm)
                {
                    return Color.Red;
                }
            }
            //arraycountm = 0;
            return Color.Orange;
        }
       
        public void veritaboku()  //veri tabanından adminsettings tablosunda olan süre ve renk değişikliğini çekmek için
        {
            if(baglanti.State!=ConnectionState.Open)
            baglanti.Open();
            loadcommand = new SqlCommand("select company_name,duration1,duration2,duration3,color1,color2,color3 from adminsettings", baglanti);
            using (SqlDataReader dr = loadcommand.ExecuteReader())  //select sorgusundan gelen verileri döndürür.
            {
                while (dr.Read())//satır varsa
                {
                    if (dr["company_name"].ToString() != "")
                    {
                        cname = dr["company_name"].ToString();
                    }
                    else
                    {
                        cname = "";
                    }
                    if (dr["duration1"].ToString() != "")
                    {
                        sure1 = sureler[Convert.ToInt16(dr["duration1"])];
                    }
                    else
                        sure1 = 0;
                    if (dr["duration2"].ToString() != "")
                    {
                        sure2 = sureler[Convert.ToInt16(dr["duration2"])];
                    }
                    else
                        sure2 = 0;
                    if (dr["duration3"].ToString() != "")
                    {
                        sure3 = sureler[Convert.ToInt16(dr["duration3"])];
                    }
                    else
                        sure3 = 0;
                    //if (dr["color1"].ToString() != "")
                    //{
                    //    color = dr["color1"].ToString();
                    //    r1 = whichcolor(color);
                    //}
                    //if (dr["color2"].ToString() != "")
                    //{
                    //    color = dr["color2"].ToString();
                    //    r2 = whichcolor(color);
                    //}
                    //if (dr["color3"].ToString() != "")
                    //{
                    //    color = dr["color3"].ToString();
                    //    r3 = whichcolor(color);
                    //}
                }
                dr.Close();
            }
            if(baglanti.State!=ConnectionState.Closed)
            baglanti.Close();
        }
        //public Color whichcolor(string renk)  //admin ayarlarında kaydedilen renklerin color tipinden karşılığını döndür.
        //{
        //    if (renk == "Mor")
        //    {
        //        return Color.Purple;
        //    }
        //    //else if (renk == "Gri")
        //    //{
        //    //    return Color.DimGray;
        //    //}
        //    else if (renk == "Mavi")
        //    {
        //        return Color.DodgerBlue;
        //    }
        //    else if (renk == "Kırmızı")
        //    {
        //        return Color.Red;
        //    }
        //    else if (renk == "Turuncu")
        //    {
        //        return Color.Orange;
        //    }
        //    else if (renk == "Yeşil")
        //    {
        //        return Color.LimeGreen;
        //    }
        //    else if (renk == "Siyah")
        //    {
        //        return Color.Black;
        //    }
        //    else
        //    {
        //        return Color.Transparent;
        //    }
        //}
    }
}

