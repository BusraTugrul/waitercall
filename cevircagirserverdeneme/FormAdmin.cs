﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Net;
using System.Data.SqlClient;
using System.Drawing;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraTab;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;
using DevExpress.XtraGrid.Views.Tile;
using DevExpress.XtraEditors;
using System.Linq;
using System.Management;
using DevExpress.LookAndFeel.Design;
using System.IO.Ports;
using System.Diagnostics;
using System.Security.Principal;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Globalization;
using System.Net.NetworkInformation;

namespace cevircagirserverdeneme
{
    public partial class frmadmin : Form   //Ana form
    {
        public string cihazmac, masaadi;
        //int statenummasa, statenumgarson;
        //DateTime dt1,dt2;
        //TimeSpan sonuc;
        //static string conString = "Server=.;Database=cevircagir;Uid=sa;Password=bhappy_5;";
       // SqlConnection baglanti;
        bool dragging;
        Point offset;
        FormCihazKonfigurasyon frmconf;
        FormRaporlar frmrap;
        formadminsettings frmadminset;
        formbattery frmbattery;
        DevExpress.XtraTab.XtraTabPage newPageRaporlar,newPageAdmin,newPageCagrilar,newPageKonf,newPageBattery,newPageGarson;
        formadminsettings frmset;
        public DevExpress.XtraTab.XtraTabControl tabpageadmin;
        FormGarsonIslem frmtanim = FormGarsonIslem.singletonfrmgars;
        formCagrilar frmcagrilar = formCagrilar.singletonfrmcagrilar;    //fromCagrilar singleton nesnesi
        formCagrilarMonitor frmcagrilarmonitor;
        frmErrorPage frmerr;
        public PanelControl pnlcntrl;
        public Color col,colm;
        static string conString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\cevircagir.mdf';Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        SqlConnection baglanti;
        SqlCommand loadcommand;
        //Thread serialread;
        //WifiHotSpot whs = null;
        TcpIpServer tis;
        UdpBroadcast udpclass;
        Serial ser;
        bool minimized = false;
        System.Windows.Forms.Timer tmrSerialFix;
        //[DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        //private static extern IntPtr CreateRoundRectRgn
        //(
        //  int nLeftRect, // x-coordinate of upper-left corner
        //  int nTopRect, // y-coordinate of upper-left corner
        //  int nRightRect, // x-coordinate of lower-right corner
        //  int nBottomRect, // y-coordinate of lower-right corner
        //  int nWidthEllipse, // height of ellipse
        //  int nHeightEllipse // width of ellipse
        //);
        private void simpleButton1_Click(object sender, EventArgs e)
        {
            //whs.stop();
            if(Settings1.Default.serialorwifi == 0)
                tis.closeserver();
            this.Close();
            System.Windows.Forms.Application.Exit();
        }

        private void pnladmin_Paint(object sender, PaintEventArgs e)
        {
            
        }
        public string[] sendtoclients()
        {
            string[] a= getssid.Split('+');
            string ssid=a[0];
            string pass=a[1];
            return a;
        }
        public int adminsetingskaydet { get; set; }
        public string  getssid{ get; set; }
        public int leds { get; set; }
        public int info { get; set; }
        

        public frmadmin()
        {            
            InitializeComponent();
            //Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 20, 20));    //form kenarları yuvarlama
            CheckForIllegalCrossThreadCalls = false;
            //if (Debugger.IsAttached)
            //    Settings1.Default.Reset();
            tabpageadmin = xtraTabControl1;
            frmset = new formadminsettings();
            pnlcntrl = panelControl1;
            baglanti = new SqlConnection(conString);

        }
        private void frmadmin_Load(object sender, EventArgs  e)
        {
            Screen screen = Screen.FromControl(this);
            int x = screen.WorkingArea.X - screen.Bounds.X;
            int y = screen.WorkingArea.Y - screen.Bounds.Y;
            this.MaximizedBounds = new Rectangle(x, y,
                screen.WorkingArea.Width, screen.WorkingArea.Height);
            this.MaximumSize = screen.WorkingArea.Size;
            this.WindowState = FormWindowState.Maximized;
            //CultureInfo ci = CultureInfo.InstalledUICulture;
            //Console.WriteLine("LANG "+ci.TwoLetterISOLanguageName);
            //Console.WriteLine("dil seçimi"+Settings1.Default.language);
            //if (Settings1.Default.language == "tr")
            //{
            //    Console.WriteLine("dil boş");
            //    if (ci.TwoLetterISOLanguageName == "en")
            //    {
            //        Console.WriteLine("dil ing");
            //        Localization.Culture = new CultureInfo("en-US");
            //        Settings1.Default.language = "en";
            //    }
            //    else
            //    {
            //        Console.WriteLine("dil tr");
            //        Localization.Culture = new CultureInfo("");
            //        Settings1.Default.language = "tr";
            //    }
            //}
            if (Settings1.Default.language=="en")
                Localization.Culture = new CultureInfo("en-US");
            btnadmin.Text = Localization.btnadmin;
            btntanimlar.Text = Localization.btntanimlar;
            btnmasaraporlar.Text = Localization.btnmasaraporlar;
            btncihazekle.Text = Localization.btncihazekle;
            //Serial ser = Serial.singletonterobj;

            baglanti.Open();     //silme işlemi için
            loadcommand = new SqlCommand("delete from reports where statenum = 2", baglanti);
            loadcommand.ExecuteNonQuery();
            baglanti.Close();

            frmerr = frmErrorPage.singletonep;

            //udpclass = UdpBroadcast.singletonudpobject;
            //udpclass.startUdpBroadcasting();


            cagrilartabekle(); //formcagrilar formunu başlangıçta tab olarak eklemek için
            //if (System.Windows.Forms.Screen.AllScreens.Length > 1)   //eğer ikinci bir monitor varsa formcagrilarmonitor show et
            //{
            //    frmcagrilarmonitor = formCagrilarMonitor.singletonfrmcagrilarm;
            //    frmcagrilarmonitor.Location = Screen.AllScreens[1].Bounds.Location;
            //    frmcagrilarmonitor.WindowState = FormWindowState.Maximized;
            //    frmcagrilarmonitor.Show();
            //}
            //foreach (Form frm in System.Windows.Forms.Application.OpenForms)
            //{
            //    if (frm.Name.Equals("formCagrilarMonitor"))//eğer formcagrilarmonitor açıldıysa timer başlat
            //    {
            //        timer2.Start();
            //    }
            //}
            leds = 900;
            timer1.Start();  //formcagrilar formundaki gridi kontrol edecek timer
            timer2.Start();

            if (Settings1.Default.serialorwifi == 0)/// alıcı cihaz wifi ile çalışıyorsa
            {
                tis = TcpIpServer.singletontisobj;
                NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
                bool isthatmac = false;
                bool nointerface = false;


                if (adapters == null || adapters.Length < 1)
                {
                    nointerface = true;
                }
                if (Settings1.Default.manualSelectedIP == "0")
                {
                    nointerface = true;
                }
                else
                {
                    foreach (NetworkInterface adapter in adapters)
                    {
                        if (adapter.GetPhysicalAddress().ToString() == Settings1.Default.manualSelectedIP && adapter.OperationalStatus == OperationalStatus.Up)
                        {
                            isthatmac = true;
                        }
                    }
                    if (!isthatmac)
                    {
                        nointerface = true;
                    }
                }
                if (nointerface)  //eğer ağ arayüzü yoksa veya settings de tutulan ip değeri boşsa
                {
                    //Console.WriteLine("*****"+Settings1.Default.manualSelectedIP);
                    frmManualNetworkSelection frmmanual = new frmManualNetworkSelection();
                    frmmanual.StartPosition = FormStartPosition.Manual;
                    frmmanual.Left = this.Left;
                    frmmanual.Top = this.Top;
                    frmmanual.Show();
                }
                else
                {
                    udpclass = UdpBroadcast.singletonudpobject;
                    udpclass.startUdpBroadcasting();
                    frmAccessPointConnection frmapc = new frmAccessPointConnection();
                    frmapc.StartPosition = FormStartPosition.Manual;
                    frmapc.Left = this.Left;
                    frmapc.Top = this.Top;
                    frmapc.Show();
                    
                }
            }
            else//seriportlu
            {
                ser = Serial.singletonterobj;
                ser.ComPortNames();
                if (ser.portnumm != null)   //cihaz varsa portu aç 
                    ser.startSerial();
                else
                {
                    //Console.WriteLine("com port açılamadı");
                    serialErrorFix();
                }
                tmrSerialFix = new System.Windows.Forms.Timer();
                tmrSerialFix.Interval = 5000;
                tmrSerialFix.Tick += new EventHandler(onTimeoutSerial);
                tmrSerialFix.Start();
                //seri port classı çağır okumaya başla 
            }


        }//frmadminload
        public static bool IsAdmin()    //Admin olarak izinli ise true döner
        {
            WindowsIdentity id = WindowsIdentity.GetCurrent();
            WindowsPrincipal p = new WindowsPrincipal(id);
            Debug.WriteLine(p.IsInRole(WindowsBuiltInRole.Administrator));
            return p.IsInRole(WindowsBuiltInRole.Administrator);
        }

        private static void RestartElevated()
        {
            if (!IsAdmin())
            { 
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.UseShellExecute = true;
            startInfo.CreateNoWindow = true;
            startInfo.WorkingDirectory = Environment.CurrentDirectory;
            startInfo.FileName =  Assembly.GetExecutingAssembly().CodeBase;  //System.Windows.Forms.Application.ExecutablePath;
            startInfo.Verb = "runas";
            try
            {
                Process.Start(startInfo);
                //Debug.WriteLine("ADMIN OLARAK ÇALIŞIYOR");
            }
            catch
            {
                //Debug.WriteLine("hataaa");
            }
                System.Windows.Forms.Application.Exit();
                //if (null == System.Windows.Application.Current)
                //{
                //    System.Windows.Application app= new System.Windows.Application();
                //    app.Shutdown();
                //}
                
        }   
        }
        //public void masailkkayit(object obj)
        //{
        //    frmconf = new FormCihazKonfigurasyon();
        //    frmconf.masaekle(obj.ToString());
        //}

        public void onTimeoutSerial(Object myObject, EventArgs myEventArgs)
        {
            serialErrorFix();
        }
        public void serialErrorFix()    //serial sınıfından port var mı ve açık mı kontrolü yapar --- uyarı ekranı verir
        {
            ser.serialfix();
            if (!ser.portok)
            {
                if (!frmerr.pageopen && this.WindowState != FormWindowState.Minimized) 
                {
                    frmerr.StartPosition = FormStartPosition.Manual;
                    frmerr.Left = this.Left;
                    frmerr.Top = this.Top;
                    frmerr.lblErrorMessage.Text = Localization.usbnotdetect;
                    frmerr.Show();
                }
            }
            else
            {
                 frmerr.Hide();                
            }
        }
        private void xtraTabControl1_CloseButtonClick(object sender, EventArgs e)  //tab görünürlüğü gizlendi bu fonk gerek kalmadı
        {
            ClosePageButtonEventArgs arg = e as ClosePageButtonEventArgs;
            xtraTabControl1.TabPages.Remove(arg.Page as XtraTabPage);   // close butonuna tıklanan tabpage i remove et.
        }//tab close butonu olayı

       

        private void simpleButton2_Click(object sender, EventArgs e)   //Logoya tıklayınca çağrılar ekranı aç
        {
            xtraTabControl1.SelectedTabPage = newPageCagrilar;
            //sendtoclients();
        }

        private void timer1_Tick(object sender, EventArgs e) //formcagrilar için olan timer
        {
            //wdc.frmAdmintimer1();
            lock (frmcagrilar.gridcagrilar)
            {
                frmcagrilar.gridcagrilar.BeginUpdate();
                try
                {
                    frmcagrilar.gridcagrilar.RefreshDataSource();
                    frmcagrilar.tvmainv.ItemCustomize += frmcagrilar.tvMainv_ItemCustomize;
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
                finally
                {
                    frmcagrilar.gridcagrilar.EndUpdate();
                    col = frmcagrilar.coloring();
                    frmcagrilar.color.Clear();
                    //col = wdc.colwdc;
                    panelControl1.Appearance.BackColor2 = col;
                    panelControl1.BackColor = col;
                    xtraTabControl1.BackColor = col;
                    xtraTabControl1.Appearance.BackColor2 = col;
                   
                }
            }
        }//timer1_Tick
        private void timer2_Tick(object sender, EventArgs e)  //formcagrilarmonitor için olan timer artık formcihazkonfigurasyon refresh için
        {
            if (xtraTabControl1.SelectedTabPage.Text.Equals("Cihaz Ayarları") && frmconf != null)
            {
                frmconf.gridrefresh();
            }
                    //lock (frmcagrilarmonitor.gridcagrilarmonitor)
                    //{
                    //    frmcagrilarmonitor.gridcagrilarmonitor.BeginUpdate();
                    //    try
                    //    {
                    //        frmcagrilarmonitor.gridcagrilarmonitor.RefreshDataSource();
                    //        frmcagrilarmonitor.tvmainvmonitor.ItemCustomize += frmcagrilarmonitor.tvMainv_ItemCustomize;
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //       // MessageBox.Show(ex.Message);
                    //    }
                    //    finally
                    //    {
                    //        frmcagrilarmonitor.gridcagrilarmonitor.EndUpdate();
                    //        colm = frmcagrilarmonitor.coloring();
                    //        frmcagrilarmonitor.color.Clear();
                    //        frmcagrilarmonitor.pnl1.Appearance.BackColor = colm;
                    //        frmcagrilarmonitor.pnl1.Appearance.BackColor2 = colm;
                    //    }
                    //}
        }//timer2_Tick

       

        private void panelControl1_MouseDown(object sender, MouseEventArgs e)
        {
            dragging = true;
            offset = e.Location;
        }

        private void panelControl1_MouseUp(object sender, MouseEventArgs e)
        {
            dragging = false;
            //Console.WriteLine(this.WindowState);
            if (this.WindowState == FormWindowState.Normal)
            {
                //if (System.Windows.Forms.Screen.AllScreens.Length > 1)   //eğer ikinci bir monitor varsa formcagrilarmonitor show et
                //{

                Form.ActiveForm.Location = Screen.FromControl(Form.ActiveForm).WorkingArea.Location;
                this.WindowState = FormWindowState.Maximized;

                //frmadmin.ActiveForm.Location = Screen.FromControl(frmadmin.ActiveForm).WorkingArea.Location;
                //frmadmin.ActiveForm.Width = Screen.FromControl(frmadmin.ActiveForm).WorkingArea.Width;
                //frmadmin.ActiveForm.Height = Screen.FromControl(frmadmin.ActiveForm).WorkingArea.Height;
                //}
            }
        }

        private void panelControl1_MouseMove(object sender, MouseEventArgs e)
        {
            if (dragging)
            {
                if (this.WindowState != FormWindowState.Normal)   //üzerine basılı iken yalnızca bir kere çalışması için
                {                    
                    this.Width -= 3;
                    this.Height -= 3;
                    this.WindowState = FormWindowState.Normal;
                }
                Point currentScreenPos = PointToScreen(e.Location);
                this.Location = new Point(currentScreenPos.X - offset.X,currentScreenPos.Y - offset.Y);
            }
        }

        public void simpleButton3_Click(object sender, EventArgs e)
        {
            //tis.starttcp();
        }
        
        private void btnadmin_Click(object sender, EventArgs e)
        {
            frmadminset = new formadminsettings();
            newPageAdmin = new DevExpress.XtraTab.XtraTabPage { Text = "Admin Ayarları" };
            foreach (XtraTabPage page in xtraTabControl1.TabPages)     //tab önceden varsa selected page yap.
            {
                if (page.Text.Equals("Admin Ayarları"))
                {
                    xtraTabControl1.SelectedTabPage = page;
                }
            }
            if (!xtraTabControl1.SelectedTabPage.Text.Equals("Admin Ayarları"))   //tab yeni var edilecekse
            {
                xtraTabControl1.TabPages.Add(newPageAdmin);
                xtraTabControl1.SelectedTabPage = newPageAdmin;
                frmadminset.TopLevel = false;
                frmadminset.Parent = newPageAdmin;
                frmadminset.Dock = DockStyle.Fill;
                xtraTabControl1.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.DimGray;
                xtraTabControl1.AppearancePage.HeaderActive.Font = new Font("Segoe UI Semibold", 9, FontStyle.Bold);
                xtraTabControl1.AppearancePage.Header.Font = new Font("Segoe UI Semibold", 9, FontStyle.Bold);
                newPageAdmin.Appearance.Header.ForeColor = Color.DimGray;
                newPageAdmin.TabPageWidth = 100;
                frmadminset.Show();
            }
        }//btnadmin_Click

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            minimized = true;
            this.WindowState = FormWindowState.Minimized;
        }

        private void simpleButton3_Click_1(object sender, EventArgs e)
        {
            frmbattery = formbattery.singletonfrmbattery; ;
            newPageBattery = new DevExpress.XtraTab.XtraTabPage { Text = "Battery" };  //newpageraporlar enable=true olduğu an
            foreach (XtraTabPage page in xtraTabControl1.TabPages)   //tab önceden varsa selected page yap.
            {
                if (page.Text.Equals("Battery"))
                {
                    xtraTabControl1.SelectedTabPage = page;
                    page.Focus();
                }
            }
            if (!xtraTabControl1.SelectedTabPage.Text.Equals("Battery"))    //tab yeni var edilecekse
            {
                xtraTabControl1.TabPages.Add(newPageBattery);
                xtraTabControl1.SelectedTabPage = newPageBattery;
                frmbattery.TopLevel = false;
                frmbattery.Parent = newPageBattery;
                frmbattery.Dock = DockStyle.Fill;
                xtraTabControl1.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.DimGray;
                xtraTabControl1.AppearancePage.HeaderActive.Font = new Font("Segoe UI Semibold", 9, FontStyle.Bold);
                xtraTabControl1.AppearancePage.Header.Font = new Font("Segoe UI Semibold", 9, FontStyle.Bold);
                newPageBattery.Appearance.Header.ForeColor = Color.DimGray;
                newPageBattery.TabPageWidth = 100;
                frmbattery.Show();
            }
        }

        private void btncihazekle_Click(object sender, EventArgs e)
        {
            frmconf = FormCihazKonfigurasyon.singletonfrmdevconf;
            newPageKonf = new DevExpress.XtraTab.XtraTabPage { Text = "Cihaz Ayarları" };  //newpageraporlar enable=true olduğu an
            foreach (XtraTabPage page in xtraTabControl1.TabPages)   //tab önceden varsa selected page yap.
            {
                if (page.Text.Equals("Cihaz Ayarları"))
                {
                    xtraTabControl1.SelectedTabPage = page;
                    page.Focus();
                }
            }
            if (!xtraTabControl1.SelectedTabPage.Text.Equals("Cihaz Ayarları"))    //tab yeni var edilecekse
            {
                xtraTabControl1.TabPages.Add(newPageKonf);
                xtraTabControl1.SelectedTabPage = newPageKonf;
                frmconf.TopLevel = false;
                frmconf.Parent = newPageKonf;
                frmconf.Dock = DockStyle.Fill;
                frmconf.Show();
            }
        }

        private void btnmasaraporlar_Click(object sender, EventArgs e)
        {
            frmrap = new FormRaporlar();
            newPageRaporlar = new DevExpress.XtraTab.XtraTabPage { Text = "Raporlar" };  //newpageraporlar enable=true olduğu an
            foreach (XtraTabPage page in xtraTabControl1.TabPages)   //tab önceden varsa selected page yap.
            {
                if (page.Text.Equals("Raporlar"))
                {
                    xtraTabControl1.SelectedTabPage = page;
                    page.Focus();
                }
               
            }
            if(!xtraTabControl1.SelectedTabPage.Text.Equals("Raporlar"))    //tab yeni var edilecekse
            {
                xtraTabControl1.TabPages.Add(newPageRaporlar);
                xtraTabControl1.SelectedTabPage = newPageRaporlar;
                frmrap.TopLevel = false;
                frmrap.Parent = newPageRaporlar;
                frmrap.Dock = DockStyle.Fill;
                xtraTabControl1.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.DimGray;
                xtraTabControl1.AppearancePage.HeaderActive.Font = new Font("Segoe UI Semibold", 9, FontStyle.Bold);
                xtraTabControl1.AppearancePage.Header.Font = new Font("Segoe UI Semibold", 9, FontStyle.Bold);
                newPageRaporlar.Appearance.Header.ForeColor = Color.DimGray;
                newPageRaporlar.TabPageWidth = 100;
                frmrap.Show();
            }
        }//btnmasaraporlar_Click
        public void cagrilartabekle()
        {
            foreach (XtraTabPage page in xtraTabControl1.TabPages)   //tab önceden varsa selected page yap.
            {
                if (page.Text.Equals("Çağrılar2"))
                {
                    xtraTabControl1.SelectedTabPage = page;
                    page.Focus();
                }
            }
            if (!xtraTabControl1.SelectedTabPage.Text.Equals("Çağrılar2"))    //tab yeni var edilecekse
            {
                newPageCagrilar = new DevExpress.XtraTab.XtraTabPage { Text = "Çağrılar2" };  //newpageraporlar enable=true olduğu an
                xtraTabControl1.TabPages.Add(newPageCagrilar);
                xtraTabControl1.SelectedTabPage = newPageCagrilar;
                frmcagrilar.TopLevel = false;
                frmcagrilar.Parent = newPageCagrilar;
                frmcagrilar.Dock = DockStyle.Fill;
                xtraTabControl1.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.DimGray;
                xtraTabControl1.AppearancePage.HeaderActive.Font = new Font("Segoe UI Semibold", 9, FontStyle.Bold);
                xtraTabControl1.AppearancePage.Header.Font = new Font("Segoe UI Semibold", 9, FontStyle.Bold);
                newPageCagrilar.Appearance.Header.ForeColor = Color.DimGray;
                newPageCagrilar.TabPageWidth = 100;
                frmcagrilar.fradmin = this;
                frmcagrilar.Show();
            }
        }//cagrilartabekle
        public void btncihazcikar_Click(object sender, EventArgs e)
        {
        }
        
        public void btntanimlar_Click(object sender, EventArgs e)
        {
            FormGarsonListe frmduzen = FormGarsonListe.singletonfrmgar;
            frmduzen.lblgarsonbaslik.Text =Localization.waiterTable;
            frmduzen.pnlcntrl.Visible = true;
            frmduzen.gview.Columns[2].Visible = true;
            frmduzen.gview.Focus();
            foreach (XtraTabPage page in xtraTabControl1.TabPages)   //tab önceden varsa selected page yap.
            {
                if (page.Text.Equals(Localization.editWaiter))
                {
                    //garson listele page kapat
                    //Debug.WriteLine("///////////////"+page.Text);
                    xtraTabControl1.SelectedTabPage = page;
                    page.Focus();
                    //frmduzen.btnlist.PerformClick();
                }
            }
            if (!xtraTabControl1.SelectedTabPage.Text.Equals(Localization.editWaiter))    //tab yeni var edilecekse
            {
                newPageGarson = new DevExpress.XtraTab.XtraTabPage { Text = Localization.editWaiter };  //newpagegarson enable=true olduğu an
                                                                                            //Debug.WriteLine("tab yok");
                xtraTabControl1.TabPages.Add(newPageGarson);
                xtraTabControl1.SelectedTabPage = newPageGarson;
                frmduzen.TopLevel = false;
                frmduzen.Parent = newPageGarson;
                frmduzen.Dock = DockStyle.Fill;
                xtraTabControl1.AppearancePage.HeaderActive.ForeColor = System.Drawing.Color.DimGray;
                xtraTabControl1.AppearancePage.HeaderActive.Font = new Font("Segoe UI Semibold", 9, FontStyle.Bold);
                xtraTabControl1.AppearancePage.Header.Font = new Font("Segoe UI Semibold", 9, FontStyle.Bold);
                newPageGarson.Appearance.Header.ForeColor = Color.DimGray;
                newPageGarson.TabPageWidth = 100;
                frmduzen.Show();
            }


            //frmtanim.Show();
            //frmtanim.TopMost=true;
            //frmtanim.tab(tabpageadmin);
        }//btntanimlar_Click

    }//main class
    public class StateObject
    {
        public Socket workSocket = null;
        public const int BufferSize = 1024;
        public byte[] buffer = new byte[BufferSize];
    }//endclass
    

}
