﻿namespace cevircagirserverdeneme
{
    partial class frmcihazekle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtmacid = new System.Windows.Forms.TextBox();
            this.lblmac = new System.Windows.Forms.Label();
            this.lblcihazid = new System.Windows.Forms.Label();
            this.txtcihazid = new System.Windows.Forms.TextBox();
            this.txtmasaadi = new System.Windows.Forms.TextBox();
            this.lblmasaadi = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtmacid
            // 
            this.txtmacid.BackColor = System.Drawing.SystemColors.Window;
            this.txtmacid.Enabled = false;
            this.txtmacid.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtmacid.Location = new System.Drawing.Point(120, 20);
            this.txtmacid.Name = "txtmacid";
            this.txtmacid.Size = new System.Drawing.Size(149, 22);
            this.txtmacid.TabIndex = 3;
            // 
            // lblmac
            // 
            this.lblmac.AutoSize = true;
            this.lblmac.Location = new System.Drawing.Point(12, 23);
            this.lblmac.Name = "lblmac";
            this.lblmac.Size = new System.Drawing.Size(92, 17);
            this.lblmac.TabIndex = 2;
            this.lblmac.Text = "Cihaz Mac Id:";
            // 
            // lblcihazid
            // 
            this.lblcihazid.AutoSize = true;
            this.lblcihazid.Location = new System.Drawing.Point(12, 51);
            this.lblcihazid.Name = "lblcihazid";
            this.lblcihazid.Size = new System.Drawing.Size(66, 17);
            this.lblcihazid.TabIndex = 4;
            this.lblcihazid.Text = "Cihaz Id :";
            // 
            // txtcihazid
            // 
            this.txtcihazid.BackColor = System.Drawing.SystemColors.Window;
            this.txtcihazid.Enabled = false;
            this.txtcihazid.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtcihazid.Location = new System.Drawing.Point(120, 48);
            this.txtcihazid.Name = "txtcihazid";
            this.txtcihazid.Size = new System.Drawing.Size(149, 22);
            this.txtcihazid.TabIndex = 5;
            // 
            // txtmasaadi
            // 
            this.txtmasaadi.BackColor = System.Drawing.SystemColors.Window;
            this.txtmasaadi.Enabled = false;
            this.txtmasaadi.ForeColor = System.Drawing.SystemColors.MenuText;
            this.txtmasaadi.Location = new System.Drawing.Point(120, 77);
            this.txtmasaadi.Name = "txtmasaadi";
            this.txtmasaadi.Size = new System.Drawing.Size(149, 22);
            this.txtmasaadi.TabIndex = 7;
            // 
            // lblmasaadi
            // 
            this.lblmasaadi.AutoSize = true;
            this.lblmasaadi.Location = new System.Drawing.Point(12, 80);
            this.lblmasaadi.Name = "lblmasaadi";
            this.lblmasaadi.Size = new System.Drawing.Size(70, 17);
            this.lblmasaadi.TabIndex = 6;
            this.lblmasaadi.Text = "Masa Adı:";
            // 
            // frmcihazekle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.txtmasaadi);
            this.Controls.Add(this.lblmasaadi);
            this.Controls.Add(this.txtcihazid);
            this.Controls.Add(this.lblcihazid);
            this.Controls.Add(this.txtmacid);
            this.Controls.Add(this.lblmac);
            this.Name = "frmcihazekle";
            this.Text = "frmcihazekle";
            this.Load += new System.EventHandler(this.frmcihazekle_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtmacid;
        private System.Windows.Forms.Label lblmac;
        private System.Windows.Forms.Label lblcihazid;
        private System.Windows.Forms.TextBox txtcihazid;
        private System.Windows.Forms.TextBox txtmasaadi;
        private System.Windows.Forms.Label lblmasaadi;
    }
}