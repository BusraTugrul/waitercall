﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace cevircagirserverdeneme
{
    class MasterSettings
    {
        public string SSID = "", pass = "";
        TcpClient client = new TcpClient();
        string pcip="";
        string espip = "192.168.101.1";
        public bool cannotconnectesp = false;
        public void startClient()
        {
            //Console.WriteLine("SSID "+SSID+" pass "+pass);
            //TcpClient client = new TcpClient();
            //client.Connect("192.168.101.1", 8213);
            //if(client.Connected)
            //{
            //    NetworkStream serverStream = client.GetStream();
            //    byte[] outStream = Encoding.ASCII.GetBytes("$SP#" + SSID + "?" + pass + "\r\n");
            //    serverStream.Write(outStream, 0, outStream.Length);
            //    serverStream.Flush();
            //    serverStream.Close();
            //    client.Close();
            //    Console.WriteLine("client esp ye veri gönderdi");
            //}




            try
            {
                IPAddress ipAddress = IPAddress.Parse(espip);
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, 8213);
                //IPHostEntry host = Dns.GetHostEntry("192.168.101.1");
                //IPAddress ipAddress = host.AddressList[0];
                //IPEndPoint remoteEP = new IPEndPoint(ipAddress, 8213);
                Socket sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                try
                {
                    string host = Dns.GetHostName();
                    IPHostEntry ip = Dns.GetHostEntry(host);
                    foreach (IPAddress ips in ip.AddressList)
                    {
                        if (ips.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            //System.Diagnostics.Debug.WriteLine("LocalIPadress: " + ip);
                            pcip = ips.ToString();
                        }
                    }
                    String[] arr = pcip.Split('.');
                    String[] arr2 = espip.Split('.');
                    string ip1 = arr[0] + arr[1] + arr[2];
                    string ip2 = arr2[0] + arr2[1] + arr2[2];
                    //Console.WriteLine(ip1 + " " + ip2);
                    if (ip1 == ip2)
                    {
                        cannotconnectesp = true;
                        //Console.WriteLine("esp ile aynı ağdayım");
                        sender.Connect(remoteEP);
                        //Console.WriteLine("Socket connected to {0}", sender.Connected + " " + sender.RemoteEndPoint.ToString());

                        // Encode the data string into a byte array.    
                        byte[] msg = Encoding.ASCII.GetBytes("$SP#" + SSID + "?" + pass + "}");  //StringToByteArray("$SP#" + SSID + "?" + pass + "\\r");

                        if (IsConnected(sender))
                        {
                            // Send the data through the socket.    
                            int a = sender.Send(msg);
                            //Console.WriteLine("gönderilen byte: " + a);

                            // Receive the response from the remote device.    
                            //int bytesRec = sender.Receive(bytes);
                            //Console.WriteLine("Echoed test = {0}",
                            //    Encoding.ASCII.GetString(bytes, 0, bytesRec));

                            // Release the socket.  
                            sender.Shutdown(SocketShutdown.Both);
                            sender.Close();
                        }
                    }
                    else
                        cannotconnectesp = false;

                }
                catch (Exception ex)
                {
                    //Console.WriteLine("veri atma hata mesajı " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("ip oluşturma hata mesajı " + ex.Message);
            }

        }
        public static bool IsConnected(Socket socket)
        {
            try
            {
                return !(socket.Poll(1, SelectMode.SelectRead) && socket.Available == 0);
            }
            catch (SocketException) { return false; }
            catch (System.ObjectDisposedException) { return false; }
        }
        public byte[] StringToByteArray(String hex)
        {
            //Console.WriteLine("gönderilecek "+ hex);
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }

    }
}
