﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace cevircagirserverdeneme
{
    public partial class frmGlobal : Form
    {
        Timer tmr, tmrpbar;
        TcpIpServer tis;
        Serial ser;
        public frmGlobal()
        {
            InitializeComponent();
            if (Settings1.Default.serialorwifi == 0)
                tis = TcpIpServer.singletontisobj;
            else
                ser = Serial.singletonterobj;
            button1.Text = Localization.pleasewait;
        }

        private void progressBarControl1_EditValueChanged(object sender, EventArgs e)
        {
            
        }

        private void frmGlobal_Load(object sender, EventArgs e)
        {
            Screen screen = Screen.FromControl(this);
            int x = screen.WorkingArea.X - screen.Bounds.X;
            int y = screen.WorkingArea.Y - screen.Bounds.Y;
            this.MaximizedBounds = new Rectangle(x, y,
                screen.WorkingArea.Width, screen.WorkingArea.Height);
            this.MaximumSize = screen.WorkingArea.Size;
            this.WindowState = FormWindowState.Maximized;

            tmr = new Timer();
            if (Settings1.Default.serialorwifi == 0)
                tmr.Interval = tis.globalTimer;
            else
                tmr.Interval = 5000;
            tmr.Tick += new EventHandler(onTimeout);
            tmr.Start();

            tmrpbar = new Timer();
            tmrpbar.Interval = 100;
            tmrpbar.Tick += new EventHandler(onTimeoutPbar);
            tmrpbar.Start();

            pbar.Properties.Step = 100;
            pbar.Properties.PercentView = true;
            if (Settings1.Default.serialorwifi == 0)
                pbar.Properties.Maximum = tis.globalTimer;
            else pbar.Properties.Maximum = 5000;
            pbar.Properties.Minimum = 0;
        }
        public void onTimeout(Object myObject,EventArgs myEventArgs)
        {
            this.Close();
        }
        public void onTimeoutPbar(Object myObject, EventArgs myEventArgs)
        {
            pbar.PerformStep();
            pbar.Update();
        }
    }
}
