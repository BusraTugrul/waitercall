﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid;
using DevExpress.XtraEditors;
using DevExpress.XtraCharts;
using DevExpress.XtraGrid.Views.Base;
using System.Threading.Tasks;

namespace cevircagirserverdeneme
{
    public partial class FormRaporlar : Form
    {
      
        static string conString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename='|DataDirectory|\cevircagir.mdf';Integrated Security=True;Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        SqlConnection baglanti = new SqlConnection(conString);
        string komutgarson ="select waiter_name from waiter", komutmasa = "select table_name from tables";
        SqlCommand command;
        string commandfirst,commandgridfirst;
        DataTable dt = new DataTable();
        //string selectfrom = "select table_name,req_time,reply_time,reply_duration,req_date,waiter_name from reports where";
        string araliksorgu, bastarihsorgu,bittarihsorgu, masasorgu, garsonsorgu;
        SqlDataAdapter da;
        DataSet ds;
        Series series;
        String reportsSum;
        ChartTitle chartTitle1 = new ChartTitle();
        int chrt,val1=0,val2=0;
        string selectfrom2;
        string last1day, last1week, last1month, last1year;
        string chartLine1, chartLine2, chartLine3, chartLine4;
        string averageResponse;
        public DevExpress.XtraGrid.GridControl control;
        
        public FormRaporlar()
        {
            InitializeComponent();
            //System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en-EN");
            control = gridControl1;
        }

        private void FormRaporlar_Load(object sender, EventArgs e)
        {
            // TODO: Bu kod satırı 'cevircagirDataSet.reports' tablosuna veri yükler. Bunu gerektiği şekilde taşıyabilir, veya kaldırabilirsiniz.
            //this.reportsTableAdapter1.Fill(this.cevircagirDataSet.reports);
            //burada yalnızca cevap verilen çağrılara ait raporlar görüntülensin.
            // TODO: Bu kod satırı 'cevircagirDataSet5.reports' tablosuna veri yükler. Bunu gerektiği şekilde taşıyabilir, veya kaldırabilirsiniz.
            localizationSettings();

            //baglanti.Open();     //silime işlemi için
            //command = new SqlCommand("delete from reports where statenum = 2", baglanti);
            //command.ExecuteNonQuery();
            //baglanti.Close();

            //baglanti.Open();     //silime işlemi için
            //command = new SqlCommand("delete from tables", baglanti);
            //command.ExecuteNonQuery();
            //baglanti.Close();

            //baglanti.Open();     //silime işlemi için
            //command = new SqlCommand("update tables set isselected=0", baglanti);
            //command.ExecuteNonQuery();
            //baglanti.Close();

            //baglanti.Open();     //silime işlemi için
            //command = new SqlCommand("delete from reports", baglanti);
            //command.ExecuteNonQuery();
            //baglanti.Close();

            //baglanti.Open();     //silime işlemi için
            //command = new SqlCommand("delete from waitersstables", baglanti);
            //command.ExecuteNonQuery();
            //baglanti.Close();

            //baglanti.Open();     //silime işlemi için
            //command = new SqlCommand("delete from waiter", baglanti);
            //command.ExecuteNonQuery();
            //baglanti.Close();

            //baglanti.Open();     //silime işlemi için
            //command = new SqlCommand("delete from adminsettings", baglanti);
            //command.ExecuteNonQuery();
            //baglanti.Close();

            //baglanti.Open();
            //string kod = "select * from waiter";
            //command = new SqlCommand(kod, baglanti);
            //SqlDataAdapter da = new SqlDataAdapter(command);
            //SqlDataReader sread = command.ExecuteReader();
            //while (sread.Read())
            //{
            //    Console.WriteLine(sread["waiter_id"].ToString()+" " + sread["waiter_id"].ToString());
            //}
            //sread.Close();
            //baglanti.Close();

            reportsSum = Localization.reportsSum;
            xtraTabControl1.TabPages[0].Text = Localization.btnmasaraporlar;
            xtraTabControl1.TabPages[1].Text = Localization.chart;
            commandfirst = "Select * from reports where statenum=1";
            string songun = (DateTime.Now.Date).ToString("yyyy-MM-dd HH:mm:ss");
           
            //this.reportsTableAdapter.Fill(this.cevircagirDataSet5.reports);
            textautocompletegarson();
            textautocompletemasa();
            //await Task.Factory.StartNew(() =>
            //{
            //    colreply_dur.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Average;
            //    colreply_dur.SummaryItem.DisplayFormat = "Ortalama Yanıt Süresi = {0:n2}";
            //    coltable_name.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
            //    coltable_name.SummaryItem.DisplayFormat = "Toplam İşlem Sayısı = {0}";
            //    var a = gridView1.Columns["table_name"].SummaryItem.SummaryValue;
            //    double b = Convert.ToDouble(gridView1.Columns["reply_duration"].SummaryItem.SummaryValue.ToString());
            //    txtsum.Text = "Toplam İşlem Sayısı : " + a.ToString() + "   Ortalama Cevap Süresi : " + b.ToString("0.##") + " sn";
            //}); 
            baglanti.Open();
            series = new Series("Series1", ViewType.Area);
            chartControl1.Series.Add(series);
            //Console.WriteLine(songun + "     "+ (DateTime.Now.Date.AddDays(1)).ToString("yyyy-MM-dd HH:mm:ss"));
            commandgridfirst = "Select * from reports where statenum=1 and req_date>='" + songun + "' and req_date<'" + (DateTime.Now.Date.AddDays(1)).ToString("yyyy-MM-dd HH:mm:ss") + "'";
            griddoldur(commandfirst);  //başlangıçta yalnızca işlem görmüş masalarla doldur.
            baglanti.Open();
            chrt = 1;
            chartdoldur(commandgridfirst);
            gridControl1.Focus();
        }
        private void xtraTabControl1_Selected(object sender, DevExpress.XtraTab.TabPageEventArgs e)
        {
            this.reportsTableAdapter1.Fill(this.cevircagirDataSet.reports);
        }

        private void xtraTabControl1_Click(object sender, EventArgs e)
        {

        }

        private void xtraTabControl1_SelectedPageChanged(object sender, DevExpress.XtraTab.TabPageChangedEventArgs e)
        {
            if(xtraTabControl1.SelectedTabPage.Name=="tabraporlar")
            {
                dateedtgrafik.EditValue = null;
                txtsum.Visible = true;
                panelControl1.Visible = true;
            }
            if (xtraTabControl1.SelectedTabPage.Name == "tabgrafikler")
            {
                panelControl1.Visible = false;
                //chartControl1.Titles.Clear();
                //string selectfrom = "select table_name,req_time,reply_time,reply_duration,req_date,waiter_name from reports";
                //baglanti.Open();
                //gridControl1.DataSource = null;
                //chartdoldur(selectfrom);
                txtsum.Visible = false;

            }
        }//xtraTabControl1_SelectedPageChanged

        private void gridView1_CustomRowFilter(object sender, RowFilterEventArgs e)
        {
        //    ColumnView view = sender as ColumnView;
        //    string a = view.GetListSourceRowCellValue(e.ListSourceRow, "statenum").ToString();
        //    if (a == "1")
        //    {
        //        // Make the current row visible.
        //        e.Visible = false;
        //        // Prevent default processing, so the row will be visible 
        //        // regardless of the view's filter.
        //        e.Handled = true;
        //    }
        }

        private void btnfiltrele_Click(object sender, EventArgs e)
        {
            txtgarsonismi.Enabled = true;
            txtmasaismi.Enabled = true;
            string selectfrom = "select table_name,req_time,reply_time,reply_duration,req_date,waiter_name from reports";
            //chartTitle1.Text = "";
            griddoldur(selectfrom);
            string selectfrommasa=null,selectfromgarson=null;
            baglanti.Open();
            //gridControl1.DataSource = null;
            //if (cmbraporaralik.SelectedIndex > -1) //aralık seçildiyse
            //{
            //    selectfrom += AralikHesapla();
            //}//if aralık seçildiyse
            if (edtbaslangictarih.ContainsFocus||edtbaslangictarih.EditValue!=null)//edtbaslangictarih.DateTime.ToString()!="01.01.0001 00:00:00")  //başlangıç tarihi seçildiyse
            {
                //chrt = 0;
                val1 = 0;
                val2 = 0;
                DateTime dtbaslangic = edtbaslangictarih.DateTime; //dd.MM.yyyy hh.mm.ss
                string baslangicsqlformat = dtbaslangic.ToString("yyyy-MM-dd HH:mm:ss");
                bastarihsorgu = " where req_date>='"+baslangicsqlformat+"'";
                selectfrom += bastarihsorgu;
            }//if başlangıç tarihi seçildiyse

            if (edtbitistarih.ContainsFocus||edtbitistarih.EditValue!=null)// edtbitistarih.DateTime.ToString() != "01.01.0001 00:00:00") //bitiş tarihi seçildiyse
            {
                val1 = 0;
                val2 = 0;
                //chrt = 0;
                DateTime dtbitis = edtbitistarih.DateTime;
                string bitissqlformat = dtbitis.ToString("yyyy-MM-dd HH:mm:ss");
                if (edtbaslangictarih.DateTime.ToString() == "01.01.0001 00:00:00")
                {
                    bittarihsorgu = null;
                    bittarihsorgu = " where req_date<='" + bitissqlformat + "'";
                }
                if (edtbaslangictarih.DateTime.ToString() != "01.01.0001 00:00:00")  //başlangıç ve bitiş seçildiyse
                {
                    bittarihsorgu = null;
                    bittarihsorgu +=" and req_date <= '"+bitissqlformat+"'";
                    if (edtbaslangictarih.EditValue != null && edtbitistarih.EditValue != null)
                    {
                        if (edtbaslangictarih.EditValue.ToString() == edtbitistarih.EditValue.ToString()) //mesela 22.06.2017 sorgulanmak istenirse(tek gün)
                        {
                            bittarihsorgu = null;
                            string dt = dtbitis.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                            bittarihsorgu = " and req_date<'" + dt + "'";
                        }
                    }
                }
                
                
                selectfrom += bittarihsorgu;
                bastarihsorgu = null;
                
            }//if bitiş tarihi seçildiyse

            if (txtgarsonismi.ContainsFocus||txtgarsonismi.Text!="")  //garson seçildiyse
            {
                
                garsonsorgu = " where waiter_name like '"+txtgarsonismi.Text+"%'";
                if (edtbaslangictarih.DateTime.ToString() != "01.01.0001 00:00:00" || edtbitistarih.DateTime.ToString() != "01.01.0001 00:00:00") //tarih ve garson ismi seçildiyse
                {
                    garsonsorgu = null;
                    garsonsorgu += " and waiter_name like '" + txtgarsonismi.Text + "%'";
                    selectfrom += garsonsorgu;
                   
                }
                if (val1!=0||val2!=0)//aralık ve garson seçildiyse
                {
                    selectfromgarson = selectfrom2;
                    garsonsorgu = null;
                    garsonsorgu += " and waiter_name like '" + txtgarsonismi.Text + "%'";
                    selectfromgarson += garsonsorgu;
                    griddoldur(selectfromgarson);
                    val1 = 0;
                    val2 = 0;
                }
                else
                {
                    selectfrom += garsonsorgu;
                    griddoldur(selectfrom);
                }
                txtmasaismi.Enabled = true;
            }//if garson seçildiyse

            if (txtmasaismi.ContainsFocus || txtmasaismi.Text != "")   //masa seçildiyse
            {
                
                masasorgu = " where table_name like '" + txtmasaismi.Text + "%'";
                if (edtbaslangictarih.DateTime.ToString() != "01.01.0001 00:00:00" || edtbitistarih.DateTime.ToString() != "01.01.0001 00:00:00") //tarih ve masa ismi seçildiyse
                {
                    masasorgu = null;
                    masasorgu += " and table_name like '" + txtmasaismi.Text + "%'";
                   // selectfrom += masasorgu;
                }

                if (val1 != 0 || val2 != 0)//aralık ve masa seçildiyse
                {
                    selectfrommasa = selectfrom2;
                    masasorgu = null;
                    masasorgu += " and table_name like '" + txtmasaismi.Text + "%'";
                    selectfrommasa += masasorgu;
                    griddoldur(selectfrommasa);
                    val1 = 0;
                    val2 = 0;
                }
                else
                {
                    selectfrom += masasorgu;
                    griddoldur(selectfrom);
                }
                
                //selectfrom +=masasorgu;
                //griddoldur(selectfrom2);
                //selectfrom2 = "select table_name,req_time,reply_time,reply_duration,req_date,waiter_name from reports";
            }//if masa seçildiyse

            //else
            //{
            //    chartTitle1.Text = "";
            //    griddoldur(selectfrom);
            //   // chartdoldur(selectfrom);
            //}

            if (selectfrommasa == null && selectfromgarson == null)  //aralik ve masa seçimi ile aralık ve garson seçimi için griddoldur fonk. burda çalısın.
            {
                griddoldur(selectfrom);
            }
            if (txtcevapsure.Text != "")   //cevap süresi seçildiyse; süreden büyük olan satırları boyar.
            {
                gridView1.FormatRules.Clear();
                GridView view = new GridView();
                GridFormatRule gridFormatRule = new GridFormatRule();
                FormatConditionRuleValue formatConditionRuleValue = new FormatConditionRuleValue();
                gridFormatRule.Column = colreply_dur;
                formatConditionRuleValue.Appearance.BackColor = Color.DodgerBlue;
                formatConditionRuleValue.Appearance.BackColor2 = Color.DodgerBlue;
                formatConditionRuleValue.Appearance.ForeColor = Color.White;
                formatConditionRuleValue.Condition = FormatCondition.GreaterOrEqual;
                formatConditionRuleValue.Value1 = Convert.ToInt32(txtcevapsure.Text);
                gridFormatRule.Rule = formatConditionRuleValue;
                gridView1.FormatRules.Add(gridFormatRule);
                gridView1.FormatRules[0].ApplyToRow = true;
            }//if cevap süresi girildiyse

            if (txtcevapsure.Text == "")  //cevap süresinde bişey yazmıyorsa formatlamayı sıfırla.
            {
                gridView1.FormatRules.Clear();
            }
            gridView1.Focus();
            edtbaslangictarih.EditValue = null;
            edtbitistarih.EditValue = null;
            txtmasaismi.Clear();
            txtgarsonismi.Clear();
            txtgarsonismi.Enabled = true;
            txtmasaismi.Enabled = true;
        }//btnfiltrele_Click
        
       public void griddoldur(string selectsorgusu)
       {
            gridControl1.DataSource = null;
            command = new SqlCommand(selectsorgusu, baglanti);
            da = new SqlDataAdapter(command);
            ds = new DataSet();
            da.Fill(ds);
            gridControl1.DataSource = ds.Tables[0];
            gridControl1.ViewCollection.Clear();
            gridControl1.ViewCollection.Add(gridView1);
            gridControl1.MainView = gridView1;
            gridControl1.RefreshDataSource();
            baglanti.Close();
            //toplam satır sayısı ve ortalama//
            if (ds.Tables[0].Rows.Count == 0)
            {
                txtsum.Text = "";
            }
            if (ds.Tables[0].Rows.Count != 0)
            {
                colreply_dur.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Average;
                //colreply_dur.SummaryItem.DisplayFormat = "Ortalama Yanıt Süresi = {0:n2}";
                coltable_name.SummaryItem.SummaryType = DevExpress.Data.SummaryItemType.Count;
                //coltable_name.SummaryItem.DisplayFormat = "Toplam İşlem Sayısı = {0}";
                var a = coltable_name.SummaryItem.SummaryValue.ToString();
                double b = Convert.ToDouble(colreply_dur.SummaryItem.SummaryValue.ToString());
                txtsum.Text = String.Format(reportsSum, a.ToString(), b.ToString("0.##"));
            }
            //toplam satır sayısı ve ortalama//
        }//griddoldur

        void chartdoldur(string selectsorguch)
        {
            //gridControl1.DataSource = null;
            command = new SqlCommand(selectsorguch, baglanti);
            da = new SqlDataAdapter(command);
            ds = new DataSet();
            da.Fill(ds);
            baglanti.Close();
            if (chrt == 0)
            {
                chartControl1.ClearCache();
                chartControl1.DataSource = null;
                series.DataSource = null;
                series.ArgumentDataMember = null;
                chartControl1.Series.Clear();
                chartControl1.Series.Add(series);
                chartControl1.RefreshData();
                series.ArgumentDataMember = "req_date";
                series.ArgumentScaleType = ScaleType.DateTime;
                ((XYDiagram)chartControl1.Diagram).AxisX.DateTimeScaleOptions.MeasureUnit = DateTimeMeasureUnit.Day;
            }
            if (chrt == 1)//son gün seçiliyse
            {
                chartControl1.ClearCache();
                chartControl1.DataSource = null;
                series.DataSource = null;
                series.ArgumentDataMember = null;
                chartControl1.Series.Clear();
                chartControl1.Series.Add(series);
                chartControl1.RefreshData();
                series.ArgumentDataMember = "req_time";
                series.ArgumentScaleType = ScaleType.Auto;
            }
            series.SeriesPointsSorting = SortingMode.Ascending;
            chartControl1.DataSource = ds.Tables[0];
            series.ValueScaleType = ScaleType.Numerical;
            series.ValueDataMembers.AddRange(new string[] { "reply_duration" });
            ((XYDiagram)chartControl1.Diagram).AxisY.Visibility = DevExpress.Utils.DefaultBoolean.True;
            chartControl1.Legend.Visibility = DevExpress.Utils.DefaultBoolean.False;
            series.CrosshairLabelPattern = "{A:dd.MM.yyyy }\r\n"+ averageResponse+" : {V:F1}";
            series.Label.Font = new Font("Segoe UI Semibold", 9, FontStyle.Regular);
            series.Label.TextColor = Color.DimGray;
        }//chartdoldur

        private void txtrapgunluk_Click(object sender, EventArgs e)
        {
            val1 = 1;
            selectfrom2 = "select table_name,req_time,reply_time,reply_duration,req_date,waiter_name from reports";
            baglanti.Open();
            //gridControl1.DataSource = null;
            selectfrom2 += AralikHesapla(val1);
            griddoldur(selectfrom2);
        }//txtrapgunluk_Click
        private void txtraphaftalik_Click(object sender, EventArgs e)
        {
            val1 = 2;
            selectfrom2 = "select table_name,req_time,reply_time,reply_duration,req_date,waiter_name from reports";
            baglanti.Open();
            //gridControl1.DataSource = null;
            selectfrom2 += AralikHesapla(val1);
            griddoldur(selectfrom2);
        }//txtraphaftalik_Click
        private void txtrapaylık_Click(object sender, EventArgs e)
        {
            val1 = 3;
            selectfrom2 = "select table_name,req_time,reply_time,reply_duration,req_date,waiter_name from reports";
            baglanti.Open();
            //gridControl1.DataSource = null;
            selectfrom2 += AralikHesapla(val1);
            griddoldur(selectfrom2);
        }//txtrapaylık_Click
        
        private void txtrapyillik_Click(object sender, EventArgs e)
        {
            val1 = 4;
            selectfrom2 = "select table_name,req_time,reply_time,reply_duration,req_date,waiter_name from reports";
            baglanti.Open();
            //gridControl1.DataSource = null;
            selectfrom2 += AralikHesapla(val1);
            griddoldur(selectfrom2);
        }//txtrapyillik_Click
        
        //grafikte mouse ile üzerine gelinen noktanın detaylarını gösterir.
        private void chartControl1_CustomDrawCrosshair(object sender, CustomDrawCrosshairEventArgs e)
        {
            foreach (CrosshairElementGroup group in e.CrosshairElementGroups)
            {
                foreach (CrosshairElement element in group.CrosshairElements)
                {
                    element.LabelElement.TextColor = Color.DimGray;
                    element.LabelElement.Font = new Font("Segoe UI Semibold", 9, FontStyle.Regular);
                    SeriesPoint currentPoint = element.SeriesPoint;
                    if (currentPoint.Tag.GetType() == typeof(DataRowView))
                    {
                        DataRowView rowView = (DataRowView)currentPoint.Tag;
                        string s = String.Format("{0}", rowView["req_date"]) +
                            "\r\n"+chartLine1 + rowView["waiter_name"].ToString()+
                            "\r\n" + chartLine2 + rowView["table_name"].ToString() +
                            "\r\n"+ chartLine3 + rowView["req_time"].ToString() +
                            "\r\n" +chartLine4 + rowView["reply_duration"].ToString()+" sn";
                        element.LabelElement.TextColor = Color.DimGray;
                        element.LabelElement.Font = new Font("Segoe UI Semibold", 9, FontStyle.Regular);
                        element.LabelElement.Text = s;
                    }
                }
            }
        }//chartControl1_CustomDrawCrosshair

        private void txtcevapsure_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtcevapsure.Text.Length > 3)
            {
                e.Handled = !(e.KeyChar == (char)Keys.Back);
            }
            else
                e.Handled = !(char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back);
        }

        private void txtgarsonismi_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtgarsonismi.Text.Length > 30)
            {
                e.Handled = !(e.KeyChar == (char)Keys.Back);
            }
            else
                e.Handled = !(char.IsLetter(e.KeyChar) || char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space);
        }

        private void txtmasaismi_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtmasaismi.Text.Length > 30)
            {
                e.Handled = !(e.KeyChar == (char)Keys.Back);
            }
            else
                e.Handled = !(char.IsLetter(e.KeyChar) || char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space);
        }

        private void dateedtgrafik_EditValueChanged(object sender, EventArgs e)
        {
            if (dateedtgrafik.EditValue != null)
            {
                chrt = 1;
                baglanti.Open();
                DateTime dtbaslangic = dateedtgrafik.DateTime; //dd.MM.yyyy hh.mm.ss
                string grafiktarihbas = dtbaslangic.ToString("yyyy-MM-dd 00:00:00");
                string grafiktarihson = dtbaslangic.ToString("yyyy-MM-dd 23:59:59");
                string chartgunluk = "select table_name,req_time,reply_time,reply_duration,req_date,waiter_name from reports where req_date>= '" + grafiktarihbas + "' and req_date<='" + grafiktarihson + "'";
                chartTitle1.TextColor = Color.DimGray;
                chartTitle1.Font = new Font("Segoe UI Semibold", 12, FontStyle.Bold);
                chartTitle1.Text = dateedtgrafik.DateTime.ToString("dd.MM.yyyy dddd");
                chartControl1.Titles.Clear();
                chartControl1.Titles.Add(chartTitle1);
                chartdoldur(chartgunluk);
            }
            chartControl1.Focus();
        }//dateedtgrafik_EditValueChanged

        private void xtraTabControl1_Click_1(object sender, EventArgs e)
        {

        }

        private void simpleButton4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtgarsonismi_TextChanged(object sender, EventArgs e)
        {
                //txtmasaismi.Text = null;
                txtmasaismi.Enabled = false;
            
        }

        private void txtmasaismi_TextChanged(object sender, EventArgs e)
        {
                //txtgarsonismi.Text = null;
                txtgarsonismi.Enabled = false;
            
        }

        private void separatorControl1_Click(object sender, EventArgs e)
        {

        }

        private void txtgrgunluk_Click(object sender, EventArgs e)
        {
            val2 = 1;
            string selectfrom = "select table_name,req_time,reply_time,reply_duration,req_date,waiter_name from reports";
            baglanti.Open();
            //gridControl1.DataSource = null;
            selectfrom += AralikHesapla(val2);
            chartdoldur(selectfrom);
            chartTitle1.Text = last1day;
            chartControl1.Titles.Clear();
            chartControl1.Titles.Add(chartTitle1);
        }//txtgrgunluk_Click
        private void txtgrhaftalik_Click(object sender, EventArgs e)
        {
            val2 = 2;
            string selectfrom = "select table_name,req_time,reply_time,reply_duration,req_date,waiter_name from reports";
            baglanti.Open();
            //gridControl1.DataSource = null;
            selectfrom += AralikHesapla(val2);
            chartdoldur(selectfrom);
            chartTitle1.Text = last1week;
            chartControl1.Titles.Clear();
            chartControl1.Titles.Add(chartTitle1);
        }//txtgrhaftalik_Click
        private void txtgraylik_Click(object sender, EventArgs e)
        {
            val2 = 3;
            string selectfrom = "select table_name,req_time,reply_time,reply_duration,req_date,waiter_name from reports";
            baglanti.Open();
            //gridControl1.DataSource = null;
            selectfrom += AralikHesapla(val2);
            chartdoldur(selectfrom);
            chartTitle1.Text = last1month;
            chartControl1.Titles.Clear();
            chartControl1.Titles.Add(chartTitle1);
        }//txtgraylik_Click
        private void txtgryillik_Click(object sender, EventArgs e)
        {
            val2 = 4;
            string selectfrom = "select table_name,req_time,reply_time,reply_duration,req_date,waiter_name from reports";
            baglanti.Open();
            //gridControl1.DataSource = null;
            selectfrom += AralikHesapla(val2);
            chartdoldur(selectfrom);
            chartTitle1.Text = last1year;
            chartControl1.Titles.Clear();
            chartControl1.Titles.Add(chartTitle1);
        }//txtgryillik_Click
        string  AralikHesapla(int val)
        {
            string bugun = (DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
            //string selectfrom = "select table_name,req_time,reply_time,reply_duration,req_date,waiter_name from reports where";
            // command = new SqlCommand(selectfrom, baglanti);
            //string aralik = cmbraporaralik.SelectedItem.ToString();
            chartTitle1.TextColor = Color.DimGray;
            chartTitle1.Font=new Font("Segoe UI Semibold", 12, FontStyle.Bold);
            if (val == 2)
            {
                chrt = 0;
                string sonhafta = (DateTime.Now.Date.AddDays(-7)).ToString("yyyy-MM-dd HH:mm:ss");
                araliksorgu = " where req_date>='"+sonhafta+"' and req_date<='"+bugun+"'";
                return araliksorgu;
            }
            else if (val == 3)
            {
                chrt = 0;
                string sonay = (DateTime.Now.Date.AddMonths(-1)).ToString("yyyy-MM-dd HH:mm:ss");
                araliksorgu = " where req_date>='" + sonay + "' and req_date<='" + bugun + "'";
                return araliksorgu;
            }
            else if (val == 4)
            {
                chrt = 0;
                string sonyil = (DateTime.Now.Date.AddYears(-1)).ToString("yyyy-MM-dd HH:mm:ss");
                araliksorgu = " where req_date>='" + sonyil + "' and req_date<='" + bugun + "'";
                return araliksorgu;
            }
            else if (val == 1)
            {
                chrt = 1;
                string songun = (DateTime.Now.Date).ToString("yyyy-MM-dd");
                araliksorgu = " where req_date>='" + songun + "' and req_date<'"+ (DateTime.Now.Date.AddDays(1)).ToString("yyyy-MM-dd HH:mm:ss") + "'";
                return araliksorgu;
            }
           return null;
        }//AralikHesapla  
        
        void textautocompletegarson()
        {
            SqlDataReader dr;
            AutoCompleteStringCollection colectiongarson = new AutoCompleteStringCollection();
            command = new SqlCommand(komutgarson, baglanti);
            try
            {
                baglanti.Open();
                dr = command.ExecuteReader();
                    while (dr.Read())
                    {
                        string garsonismi = dr["waiter_name"].ToString();
                        colectiongarson.Add(garsonismi);
                    }
                txtgarsonismi.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txtgarson.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txtgarsonismi.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txtgarson.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txtgarsonismi.AutoCompleteCustomSource = colectiongarson;
                txtgarson.AutoCompleteCustomSource = colectiongarson;

                dr.Close();
                baglanti.Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }//textautocompletegarson
        
        void textautocompletemasa()
        {
            SqlDataReader dr;
            AutoCompleteStringCollection colectionmasa = new AutoCompleteStringCollection();
            command = new SqlCommand(komutmasa, baglanti);
            try
            {
                baglanti.Open();
                dr = command.ExecuteReader();
                while (dr.Read())
                {
                    string garsonismi = dr["table_name"].ToString();
                    colectionmasa.Add(garsonismi);

                }
                txtmasaismi.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                txtmasaismi.AutoCompleteSource = AutoCompleteSource.CustomSource;
                txtmasaismi.AutoCompleteCustomSource = colectionmasa;

                dr.Close();
                baglanti.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }//textautocompletemasa
        public void localizationSettings()
        {
            averageResponse = Localization.averageResponse;
            lblformname.Text = Localization.lblformname;
            labelControl2.Text = Localization.labelControl2__;
            labelControl3.Text = Localization.labelControl3;
            labelControl4.Text = Localization.labelControl4;
            labelControl5.Text = Localization.labelControl5;
            labelControl6.Text = Localization.labelControl6;
            label1.Text = Localization.label1;
            btnfiltrele.Text = Localization.btnfiltrele;
            txtrapgunluk.Text = Localization.txtgrgunluk;
            txtraphaftalik.Text = Localization.txtraphaftalik;
            txtrapaylık.Text = Localization.txtrapaylik;
            txtrapyillik.Text = Localization.txtrapyillik;
            txtgrgunluk.Text = Localization.txtgrgunluk;
            txtgrhaftalik.Text = Localization.txtraphaftalik;
            txtgraylik.Text = Localization.txtrapaylik;
            txtgryillik.Text = Localization.txtrapyillik;
            colreq_date.Caption = Localization.label3_;
            coltable_name.Caption = Localization.coltable_name_;
            colwaiter_name.Caption = Localization.colwaiter_name_;
            colreq_time.Caption = Localization.colreq_time;
            colreply_time.Caption = Localization.colreply_time;
            colreply_dur.Caption = Localization.colreply_dur;
            gridView1.GroupPanelText = Localization.gridPanelText;
            label3.Text = Localization.label3_;
            last1day = Localization.last1day;
            last1week = Localization.last1week;
            last1month = Localization.last1month;
            last1year = Localization.last1year;
            chartLine1 = Localization.chartLine1;
            chartLine2 = Localization.chartLine2;
            chartLine3 = Localization.chartLine3;
            chartLine4 = Localization.chartLine4;
        }


    }
}
