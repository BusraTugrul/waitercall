﻿using NativeWifi;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cevircagirserverdeneme
{
    public partial class frmMasterSettings : Form
    {
        MasterSettings masterset;
        public frmMasterSettings()
        {
            InitializeComponent();
        }

        private void btnPnl1Next_Click(object sender, EventArgs e)
        {
            pnl2.Visible = true;
        }

        private void btnPnl2Next_Click(object sender, EventArgs e)
        {
            pnl3.Visible = true;
            int i = 0;
            try
            {
                WlanClient client = new WlanClient();
                foreach (WlanClient.WlanInterface wlanIface in client.Interfaces)
                {
                    // Lists all available networks
                    Wlan.WlanAvailableNetwork[] networks = wlanIface.GetAvailableNetworkList(0);
                    foreach (Wlan.WlanAvailableNetwork network in networks)
                    {
                        if (i == 0)
                        {
                            i++;
                            cmbssid.Text = GetStringForSSID(network.dot11Ssid);
                            // Console.WriteLine("Found network with SSID {0}.", GetStringForSSID(network.dot11Ssid));
                        }
                        cmbssid.Items.Add(GetStringForSSID(network.dot11Ssid));
                    }
                }
            }
            catch(Exception ex)
            {

            }
        }
        public string GetStringForSSID(Wlan.Dot11Ssid ssid)
        {
            return Encoding.ASCII.GetString(ssid.SSID, 0, (int)ssid.SSIDLength);
        }

        private void btnPnl3Next_Click(object sender, EventArgs e)
        {
            pnl4.Visible = true;
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            this.Close();
            System.Windows.Forms.Application.Exit();
        }

        private void labelControl1_Click(object sender, EventArgs e)
        {

        }

        private void btnPnl4Next_Click(object sender, EventArgs e)
        {
            this.Close();
            System.Windows.Forms.Application.Exit();
        }

        private void btnMasterSend_Click(object sender, EventArgs e)
        {
            //Console.WriteLine( cmbssid.Text);
            if (cmbssid.Text != "" && txtPass.Text != "")
            {
                masterset.SSID = cmbssid.Text;
                masterset.pass = txtPass.Text;
                masterset.startClient();
            }
            btnPnl3Next.Focus();
            if (!masterset.cannotconnectesp)
            {
                lblerror.Visible = true;
            }
            else
            {
                lblerror.Visible = false;
            }
            //btnPnl3Next.Focus();
            //txtSSID.Text = "";
            //txtPass.Text = "";
        }

        private void frmMasterSettings_Load(object sender, EventArgs e)
        {
            Screen screen = Screen.FromControl(this);
            int x = screen.WorkingArea.X - screen.Bounds.X;
            int y = screen.WorkingArea.Y - screen.Bounds.Y;
            this.MaximizedBounds = new Rectangle(x, y,
                screen.WorkingArea.Width, screen.WorkingArea.Height);
            this.MaximumSize = screen.WorkingArea.Size;
            this.WindowState = FormWindowState.Maximized;

            masterset = new MasterSettings();
            localizationSettings();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbssid_DropDownClosed(object sender, EventArgs e)
        {
            txtPass.Focus();
        }
        public void localizationSettings()
        {
            lblformname.Text = Localization.apdevice;
            btnPnl1Next.Text = Localization.next;
            btnPnl2Next.Text = Localization.next;
            btnPnl3Next.Text = Localization.next;
            btnPnl4Next.Text = Localization.next;
            btnMasterSend.Text = Localization.btnSend;
            label17.Text = Localization.ssid;
            label18.Text = Localization.pass;
            lblerror.Text = Localization.apseterror;
            labelControl1.Text = Localization.set1;
            labelControl2.Text = Localization.set2;
            labelControl3.Text = Localization.set3;
            labelControl4.Text = Localization.set4;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
        
    }
}
