﻿namespace cevircagirserverdeneme
{
    partial class formadminsettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formadminsettings));
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject10 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject1 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject2 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject3 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject4 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject5 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject6 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject7 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject8 = new DevExpress.Utils.SerializableAppearanceObject();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.separatorControl1 = new DevExpress.XtraEditors.SeparatorControl();
            this.btnadminkaydet = new DevExpress.XtraEditors.SimpleButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.lblgarsonbaslik = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lblformname = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.txtssidparola = new DevExpress.XtraEditors.TextEdit();
            this.cmbssid = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbsurea = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbsureb = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbsurec = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbrenka = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbrenkb = new DevExpress.XtraEditors.ComboBoxEdit();
            this.cmbrenkc = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnredlight = new DevExpress.XtraEditors.SimpleButton();
            this.btnlightoff = new DevExpress.XtraEditors.SimpleButton();
            this.btnderinuyku = new DevExpress.XtraEditors.SimpleButton();
            this.separatorControl4 = new DevExpress.XtraEditors.SeparatorControl();
            this.label14 = new System.Windows.Forms.Label();
            this.btnwakeup = new DevExpress.XtraEditors.SimpleButton();
            this.btncvrcgr = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.tmrtmr = new System.Windows.Forms.Timer(this.components);
            this.lblinfo = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtinfo = new System.Windows.Forms.TextBox();
            this.btnsimpleFlash = new DevExpress.XtraEditors.SimpleButton();
            this.cmbDevNo = new DevExpress.XtraEditors.ComboBoxEdit();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton3 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton4 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton5 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton6 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton7 = new DevExpress.XtraEditors.SimpleButton();
            this.cmbSettings = new DevExpress.XtraEditors.ComboBoxEdit();
            this.btnSend = new DevExpress.XtraEditors.SimpleButton();
            this.btnDefault = new DevExpress.XtraEditors.SimpleButton();
            this.txtrenk3 = new DevExpress.XtraEditors.TextEdit();
            this.txtrenk2 = new DevExpress.XtraEditors.TextEdit();
            this.txtrenk1 = new DevExpress.XtraEditors.TextEdit();
            this.label16 = new System.Windows.Forms.Label();
            this.btnMasterStart = new DevExpress.XtraEditors.SimpleButton();
            this.separatorControl6 = new DevExpress.XtraEditors.SeparatorControl();
            this.label17 = new System.Windows.Forms.Label();
            this.simpleButton8 = new DevExpress.XtraEditors.SimpleButton();
            this.label13 = new System.Windows.Forms.Label();
            this.chkYes = new System.Windows.Forms.CheckBox();
            this.chkNo = new System.Windows.Forms.CheckBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.btnTr = new System.Windows.Forms.Button();
            this.btnEn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label9 = new System.Windows.Forms.Label();
            this.simpleButton10 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton11 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton12 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton13 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton14 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton15 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton16 = new DevExpress.XtraEditors.SimpleButton();
            this.separatorControl7 = new DevExpress.XtraEditors.SeparatorControl();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.simpleButton18 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton19 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton20 = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton21 = new DevExpress.XtraEditors.SimpleButton();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.simpleButton9 = new DevExpress.XtraEditors.SimpleButton();
            this.separatorControl3 = new DevExpress.XtraEditors.SeparatorControl();
            this.label24 = new System.Windows.Forms.Label();
            this.lstChn = new System.Windows.Forms.ListBox();
            this.btnnet = new DevExpress.XtraEditors.SimpleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.separatorControl5 = new DevExpress.XtraEditors.SeparatorControl();
            this.txtfirmaad = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.separatorControl2 = new DevExpress.XtraEditors.SeparatorControl();
            this.label8 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtssidparola.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbssid.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbsurea.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbsureb.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbsurec.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbrenka.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbrenkb.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbrenkc.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDevNo.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSettings.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrenk3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrenk2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrenk1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl3)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl2)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Name = "label4";
            // 
            // separatorControl1
            // 
            this.separatorControl1.LineAlignment = DevExpress.XtraEditors.Alignment.Near;
            this.separatorControl1.LineColor = System.Drawing.Color.Orange;
            resources.ApplyResources(this.separatorControl1, "separatorControl1");
            this.separatorControl1.Name = "separatorControl1";
            // 
            // btnadminkaydet
            // 
            this.btnadminkaydet.AllowFocus = false;
            resources.ApplyResources(this.btnadminkaydet, "btnadminkaydet");
            this.btnadminkaydet.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnadminkaydet.Appearance.BackColor")));
            this.btnadminkaydet.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnadminkaydet.Appearance.Font")));
            this.btnadminkaydet.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnadminkaydet.Appearance.ForeColor")));
            this.btnadminkaydet.Appearance.Options.UseBackColor = true;
            this.btnadminkaydet.Appearance.Options.UseFont = true;
            this.btnadminkaydet.Appearance.Options.UseForeColor = true;
            this.btnadminkaydet.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnadminkaydet.AppearanceHovered.BackColor")));
            this.btnadminkaydet.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnadminkaydet.AppearanceHovered.Font")));
            this.btnadminkaydet.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnadminkaydet.AppearanceHovered.ForeColor")));
            this.btnadminkaydet.AppearanceHovered.Options.UseBackColor = true;
            this.btnadminkaydet.AppearanceHovered.Options.UseFont = true;
            this.btnadminkaydet.AppearanceHovered.Options.UseForeColor = true;
            this.btnadminkaydet.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnadminkaydet.Name = "btnadminkaydet";
            this.btnadminkaydet.Click += new System.EventHandler(this.btnadminkaydet_Click);
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.ForeColor = System.Drawing.Color.DimGray;
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.ForeColor = System.Drawing.Color.DimGray;
            this.label6.Name = "label6";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Name = "label7";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.label26);
            this.panel3.Controls.Add(this.lblgarsonbaslik);
            this.panel3.Controls.Add(this.label25);
            this.panel3.Controls.Add(this.lblformname);
            resources.ApplyResources(this.panel3, "panel3");
            this.panel3.Name = "panel3";
            // 
            // label26
            // 
            resources.ApplyResources(this.label26, "label26");
            this.label26.ForeColor = System.Drawing.Color.DimGray;
            this.label26.Name = "label26";
            // 
            // lblgarsonbaslik
            // 
            resources.ApplyResources(this.lblgarsonbaslik, "lblgarsonbaslik");
            this.lblgarsonbaslik.ForeColor = System.Drawing.Color.Orange;
            this.lblgarsonbaslik.Name = "lblgarsonbaslik";
            // 
            // label25
            // 
            resources.ApplyResources(this.label25, "label25");
            this.label25.ForeColor = System.Drawing.Color.DimGray;
            this.label25.Name = "label25";
            // 
            // lblformname
            // 
            resources.ApplyResources(this.lblformname, "lblformname");
            this.lblformname.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblformname.ForeColor = System.Drawing.Color.Orange;
            this.lblformname.Name = "lblformname";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.DimGray;
            this.label11.Name = "label11";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.ForeColor = System.Drawing.Color.DimGray;
            this.label12.Name = "label12";
            // 
            // txtssidparola
            // 
            resources.ApplyResources(this.txtssidparola, "txtssidparola");
            this.txtssidparola.Name = "txtssidparola";
            this.txtssidparola.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtssidparola.Properties.Appearance.Font")));
            this.txtssidparola.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtssidparola.Properties.Appearance.ForeColor")));
            this.txtssidparola.Properties.Appearance.Options.UseFont = true;
            this.txtssidparola.Properties.Appearance.Options.UseForeColor = true;
            this.txtssidparola.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            // 
            // cmbssid
            // 
            resources.ApplyResources(this.cmbssid, "cmbssid");
            this.cmbssid.Name = "cmbssid";
            this.cmbssid.Properties.AllowFocused = false;
            this.cmbssid.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cmbssid.Properties.Appearance.Font")));
            this.cmbssid.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("cmbssid.Properties.Appearance.ForeColor")));
            this.cmbssid.Properties.Appearance.Options.UseFont = true;
            this.cmbssid.Properties.Appearance.Options.UseForeColor = true;
            this.cmbssid.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbssid.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cmbssid.Properties.Buttons"))), resources.GetString("cmbssid.Properties.Buttons1"), ((int)(resources.GetObject("cmbssid.Properties.Buttons2"))), ((bool)(resources.GetObject("cmbssid.Properties.Buttons3"))), ((bool)(resources.GetObject("cmbssid.Properties.Buttons4"))), ((bool)(resources.GetObject("cmbssid.Properties.Buttons5"))), ((DevExpress.XtraEditors.ImageLocation)(resources.GetObject("cmbssid.Properties.Buttons6"))), ((System.Drawing.Image)(resources.GetObject("cmbssid.Properties.Buttons7"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject10, resources.GetString("cmbssid.Properties.Buttons8"), ((object)(resources.GetObject("cmbssid.Properties.Buttons9"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("cmbssid.Properties.Buttons10"))), ((bool)(resources.GetObject("cmbssid.Properties.Buttons11"))))});
            this.cmbssid.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.cmbssid.Properties.Items.AddRange(new object[] {
            resources.GetString("cmbssid.Properties.Items"),
            resources.GetString("cmbssid.Properties.Items1"),
            resources.GetString("cmbssid.Properties.Items2"),
            resources.GetString("cmbssid.Properties.Items3"),
            resources.GetString("cmbssid.Properties.Items4"),
            resources.GetString("cmbssid.Properties.Items5"),
            resources.GetString("cmbssid.Properties.Items6"),
            resources.GetString("cmbssid.Properties.Items7"),
            resources.GetString("cmbssid.Properties.Items8")});
            this.cmbssid.SelectedIndexChanged += new System.EventHandler(this.cmbssid_SelectedIndexChanged);
            // 
            // cmbsurea
            // 
            resources.ApplyResources(this.cmbsurea, "cmbsurea");
            this.cmbsurea.Name = "cmbsurea";
            this.cmbsurea.Properties.AllowFocused = false;
            this.cmbsurea.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cmbsurea.Properties.Appearance.Font")));
            this.cmbsurea.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("cmbsurea.Properties.Appearance.ForeColor")));
            this.cmbsurea.Properties.Appearance.Options.UseFont = true;
            this.cmbsurea.Properties.Appearance.Options.UseForeColor = true;
            this.cmbsurea.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbsurea.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cmbsurea.Properties.Buttons"))), resources.GetString("cmbsurea.Properties.Buttons1"), ((int)(resources.GetObject("cmbsurea.Properties.Buttons2"))), ((bool)(resources.GetObject("cmbsurea.Properties.Buttons3"))), ((bool)(resources.GetObject("cmbsurea.Properties.Buttons4"))), ((bool)(resources.GetObject("cmbsurea.Properties.Buttons5"))), ((DevExpress.XtraEditors.ImageLocation)(resources.GetObject("cmbsurea.Properties.Buttons6"))), ((System.Drawing.Image)(resources.GetObject("cmbsurea.Properties.Buttons7"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject1, resources.GetString("cmbsurea.Properties.Buttons8"), ((object)(resources.GetObject("cmbsurea.Properties.Buttons9"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("cmbsurea.Properties.Buttons10"))), ((bool)(resources.GetObject("cmbsurea.Properties.Buttons11"))))});
            this.cmbsurea.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.cmbsurea.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            // 
            // cmbsureb
            // 
            resources.ApplyResources(this.cmbsureb, "cmbsureb");
            this.cmbsureb.Name = "cmbsureb";
            this.cmbsureb.Properties.AllowFocused = false;
            this.cmbsureb.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cmbsureb.Properties.Appearance.Font")));
            this.cmbsureb.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("cmbsureb.Properties.Appearance.ForeColor")));
            this.cmbsureb.Properties.Appearance.Options.UseFont = true;
            this.cmbsureb.Properties.Appearance.Options.UseForeColor = true;
            this.cmbsureb.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbsureb.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cmbsureb.Properties.Buttons"))), resources.GetString("cmbsureb.Properties.Buttons1"), ((int)(resources.GetObject("cmbsureb.Properties.Buttons2"))), ((bool)(resources.GetObject("cmbsureb.Properties.Buttons3"))), ((bool)(resources.GetObject("cmbsureb.Properties.Buttons4"))), ((bool)(resources.GetObject("cmbsureb.Properties.Buttons5"))), ((DevExpress.XtraEditors.ImageLocation)(resources.GetObject("cmbsureb.Properties.Buttons6"))), ((System.Drawing.Image)(resources.GetObject("cmbsureb.Properties.Buttons7"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject2, resources.GetString("cmbsureb.Properties.Buttons8"), ((object)(resources.GetObject("cmbsureb.Properties.Buttons9"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("cmbsureb.Properties.Buttons10"))), ((bool)(resources.GetObject("cmbsureb.Properties.Buttons11"))))});
            this.cmbsureb.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.cmbsureb.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbsureb.Properties.UseReadOnlyAppearance = false;
            // 
            // cmbsurec
            // 
            resources.ApplyResources(this.cmbsurec, "cmbsurec");
            this.cmbsurec.Name = "cmbsurec";
            this.cmbsurec.Properties.AllowFocused = false;
            this.cmbsurec.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cmbsurec.Properties.Appearance.Font")));
            this.cmbsurec.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("cmbsurec.Properties.Appearance.ForeColor")));
            this.cmbsurec.Properties.Appearance.Options.UseFont = true;
            this.cmbsurec.Properties.Appearance.Options.UseForeColor = true;
            this.cmbsurec.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbsurec.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cmbsurec.Properties.Buttons"))), resources.GetString("cmbsurec.Properties.Buttons1"), ((int)(resources.GetObject("cmbsurec.Properties.Buttons2"))), ((bool)(resources.GetObject("cmbsurec.Properties.Buttons3"))), ((bool)(resources.GetObject("cmbsurec.Properties.Buttons4"))), ((bool)(resources.GetObject("cmbsurec.Properties.Buttons5"))), ((DevExpress.XtraEditors.ImageLocation)(resources.GetObject("cmbsurec.Properties.Buttons6"))), ((System.Drawing.Image)(resources.GetObject("cmbsurec.Properties.Buttons7"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject3, resources.GetString("cmbsurec.Properties.Buttons8"), ((object)(resources.GetObject("cmbsurec.Properties.Buttons9"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("cmbsurec.Properties.Buttons10"))), ((bool)(resources.GetObject("cmbsurec.Properties.Buttons11"))))});
            this.cmbsurec.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.cmbsurec.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbsurec.Properties.UseReadOnlyAppearance = false;
            // 
            // cmbrenka
            // 
            resources.ApplyResources(this.cmbrenka, "cmbrenka");
            this.cmbrenka.Name = "cmbrenka";
            this.cmbrenka.Properties.AllowFocused = false;
            this.cmbrenka.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cmbrenka.Properties.Appearance.Font")));
            this.cmbrenka.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("cmbrenka.Properties.Appearance.ForeColor")));
            this.cmbrenka.Properties.Appearance.Options.UseFont = true;
            this.cmbrenka.Properties.Appearance.Options.UseForeColor = true;
            this.cmbrenka.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbrenka.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cmbrenka.Properties.Buttons"))), resources.GetString("cmbrenka.Properties.Buttons1"), ((int)(resources.GetObject("cmbrenka.Properties.Buttons2"))), ((bool)(resources.GetObject("cmbrenka.Properties.Buttons3"))), ((bool)(resources.GetObject("cmbrenka.Properties.Buttons4"))), ((bool)(resources.GetObject("cmbrenka.Properties.Buttons5"))), ((DevExpress.XtraEditors.ImageLocation)(resources.GetObject("cmbrenka.Properties.Buttons6"))), ((System.Drawing.Image)(resources.GetObject("cmbrenka.Properties.Buttons7"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject4, resources.GetString("cmbrenka.Properties.Buttons8"), ((object)(resources.GetObject("cmbrenka.Properties.Buttons9"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("cmbrenka.Properties.Buttons10"))), ((bool)(resources.GetObject("cmbrenka.Properties.Buttons11"))))});
            this.cmbrenka.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.cmbrenka.SelectedValueChanged += new System.EventHandler(this.cmbrenka_SelectedValueChanged);
            // 
            // cmbrenkb
            // 
            resources.ApplyResources(this.cmbrenkb, "cmbrenkb");
            this.cmbrenkb.Name = "cmbrenkb";
            this.cmbrenkb.Properties.AllowFocused = false;
            this.cmbrenkb.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cmbrenkb.Properties.Appearance.Font")));
            this.cmbrenkb.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("cmbrenkb.Properties.Appearance.ForeColor")));
            this.cmbrenkb.Properties.Appearance.Options.UseFont = true;
            this.cmbrenkb.Properties.Appearance.Options.UseForeColor = true;
            this.cmbrenkb.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbrenkb.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cmbrenkb.Properties.Buttons"))), resources.GetString("cmbrenkb.Properties.Buttons1"), ((int)(resources.GetObject("cmbrenkb.Properties.Buttons2"))), ((bool)(resources.GetObject("cmbrenkb.Properties.Buttons3"))), ((bool)(resources.GetObject("cmbrenkb.Properties.Buttons4"))), ((bool)(resources.GetObject("cmbrenkb.Properties.Buttons5"))), ((DevExpress.XtraEditors.ImageLocation)(resources.GetObject("cmbrenkb.Properties.Buttons6"))), ((System.Drawing.Image)(resources.GetObject("cmbrenkb.Properties.Buttons7"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject5, resources.GetString("cmbrenkb.Properties.Buttons8"), ((object)(resources.GetObject("cmbrenkb.Properties.Buttons9"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("cmbrenkb.Properties.Buttons10"))), ((bool)(resources.GetObject("cmbrenkb.Properties.Buttons11"))))});
            this.cmbrenkb.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.cmbrenkb.SelectedIndexChanged += new System.EventHandler(this.cmbrenkb_SelectedIndexChanged);
            // 
            // cmbrenkc
            // 
            resources.ApplyResources(this.cmbrenkc, "cmbrenkc");
            this.cmbrenkc.Name = "cmbrenkc";
            this.cmbrenkc.Properties.AllowFocused = false;
            this.cmbrenkc.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cmbrenkc.Properties.Appearance.Font")));
            this.cmbrenkc.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("cmbrenkc.Properties.Appearance.ForeColor")));
            this.cmbrenkc.Properties.Appearance.Options.UseFont = true;
            this.cmbrenkc.Properties.Appearance.Options.UseForeColor = true;
            this.cmbrenkc.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbrenkc.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cmbrenkc.Properties.Buttons"))), resources.GetString("cmbrenkc.Properties.Buttons1"), ((int)(resources.GetObject("cmbrenkc.Properties.Buttons2"))), ((bool)(resources.GetObject("cmbrenkc.Properties.Buttons3"))), ((bool)(resources.GetObject("cmbrenkc.Properties.Buttons4"))), ((bool)(resources.GetObject("cmbrenkc.Properties.Buttons5"))), ((DevExpress.XtraEditors.ImageLocation)(resources.GetObject("cmbrenkc.Properties.Buttons6"))), ((System.Drawing.Image)(resources.GetObject("cmbrenkc.Properties.Buttons7"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject6, resources.GetString("cmbrenkc.Properties.Buttons8"), ((object)(resources.GetObject("cmbrenkc.Properties.Buttons9"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("cmbrenkc.Properties.Buttons10"))), ((bool)(resources.GetObject("cmbrenkc.Properties.Buttons11"))))});
            this.cmbrenkc.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.cmbrenkc.SelectedValueChanged += new System.EventHandler(this.cmbrenkc_SelectedValueChanged_1);
            // 
            // btnredlight
            // 
            this.btnredlight.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnredlight.Appearance.BackColor")));
            this.btnredlight.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnredlight.Appearance.Font")));
            this.btnredlight.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnredlight.Appearance.ForeColor")));
            this.btnredlight.Appearance.Options.UseBackColor = true;
            this.btnredlight.Appearance.Options.UseFont = true;
            this.btnredlight.Appearance.Options.UseForeColor = true;
            this.btnredlight.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnredlight.AppearanceHovered.BackColor")));
            this.btnredlight.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnredlight.AppearanceHovered.Font")));
            this.btnredlight.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnredlight.AppearanceHovered.ForeColor")));
            this.btnredlight.AppearanceHovered.Options.UseBackColor = true;
            this.btnredlight.AppearanceHovered.Options.UseFont = true;
            this.btnredlight.AppearanceHovered.Options.UseForeColor = true;
            this.btnredlight.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnredlight, "btnredlight");
            this.btnredlight.Name = "btnredlight";
            this.btnredlight.Click += new System.EventHandler(this.btnredlight_Click);
            // 
            // btnlightoff
            // 
            this.btnlightoff.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnlightoff.Appearance.BackColor")));
            this.btnlightoff.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnlightoff.Appearance.Font")));
            this.btnlightoff.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnlightoff.Appearance.ForeColor")));
            this.btnlightoff.Appearance.Options.UseBackColor = true;
            this.btnlightoff.Appearance.Options.UseFont = true;
            this.btnlightoff.Appearance.Options.UseForeColor = true;
            this.btnlightoff.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnlightoff.AppearanceHovered.BackColor")));
            this.btnlightoff.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnlightoff.AppearanceHovered.Font")));
            this.btnlightoff.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnlightoff.AppearanceHovered.ForeColor")));
            this.btnlightoff.AppearanceHovered.Options.UseBackColor = true;
            this.btnlightoff.AppearanceHovered.Options.UseFont = true;
            this.btnlightoff.AppearanceHovered.Options.UseForeColor = true;
            this.btnlightoff.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnlightoff, "btnlightoff");
            this.btnlightoff.Name = "btnlightoff";
            this.btnlightoff.Click += new System.EventHandler(this.btnlightoff_Click);
            // 
            // btnderinuyku
            // 
            resources.ApplyResources(this.btnderinuyku, "btnderinuyku");
            this.btnderinuyku.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnderinuyku.Appearance.BackColor")));
            this.btnderinuyku.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnderinuyku.Appearance.Font")));
            this.btnderinuyku.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnderinuyku.Appearance.ForeColor")));
            this.btnderinuyku.Appearance.Options.UseBackColor = true;
            this.btnderinuyku.Appearance.Options.UseFont = true;
            this.btnderinuyku.Appearance.Options.UseForeColor = true;
            this.btnderinuyku.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnderinuyku.AppearanceHovered.BackColor")));
            this.btnderinuyku.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnderinuyku.AppearanceHovered.Font")));
            this.btnderinuyku.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnderinuyku.AppearanceHovered.ForeColor")));
            this.btnderinuyku.AppearanceHovered.Options.UseBackColor = true;
            this.btnderinuyku.AppearanceHovered.Options.UseFont = true;
            this.btnderinuyku.AppearanceHovered.Options.UseForeColor = true;
            this.btnderinuyku.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnderinuyku.Name = "btnderinuyku";
            this.btnderinuyku.Click += new System.EventHandler(this.btnderinuyku_Click);
            // 
            // separatorControl4
            // 
            this.separatorControl4.LineAlignment = DevExpress.XtraEditors.Alignment.Near;
            this.separatorControl4.LineColor = System.Drawing.Color.Orange;
            resources.ApplyResources(this.separatorControl4, "separatorControl4");
            this.separatorControl4.Name = "separatorControl4";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.ForeColor = System.Drawing.Color.DimGray;
            this.label14.Name = "label14";
            // 
            // btnwakeup
            // 
            resources.ApplyResources(this.btnwakeup, "btnwakeup");
            this.btnwakeup.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnwakeup.Appearance.BackColor")));
            this.btnwakeup.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnwakeup.Appearance.Font")));
            this.btnwakeup.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnwakeup.Appearance.ForeColor")));
            this.btnwakeup.Appearance.Options.UseBackColor = true;
            this.btnwakeup.Appearance.Options.UseFont = true;
            this.btnwakeup.Appearance.Options.UseForeColor = true;
            this.btnwakeup.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnwakeup.AppearanceHovered.BackColor")));
            this.btnwakeup.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnwakeup.AppearanceHovered.Font")));
            this.btnwakeup.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnwakeup.AppearanceHovered.ForeColor")));
            this.btnwakeup.AppearanceHovered.Options.UseBackColor = true;
            this.btnwakeup.AppearanceHovered.Options.UseFont = true;
            this.btnwakeup.AppearanceHovered.Options.UseForeColor = true;
            this.btnwakeup.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnwakeup.Name = "btnwakeup";
            this.btnwakeup.Click += new System.EventHandler(this.btnwakeup_Click);
            // 
            // btncvrcgr
            // 
            resources.ApplyResources(this.btncvrcgr, "btncvrcgr");
            this.btncvrcgr.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btncvrcgr.Appearance.BackColor")));
            this.btncvrcgr.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btncvrcgr.Appearance.Font")));
            this.btncvrcgr.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btncvrcgr.Appearance.ForeColor")));
            this.btncvrcgr.Appearance.Options.UseBackColor = true;
            this.btncvrcgr.Appearance.Options.UseFont = true;
            this.btncvrcgr.Appearance.Options.UseForeColor = true;
            this.btncvrcgr.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btncvrcgr.AppearanceHovered.BackColor")));
            this.btncvrcgr.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btncvrcgr.AppearanceHovered.Font")));
            this.btncvrcgr.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btncvrcgr.AppearanceHovered.ForeColor")));
            this.btncvrcgr.AppearanceHovered.Options.UseBackColor = true;
            this.btncvrcgr.AppearanceHovered.Options.UseFont = true;
            this.btncvrcgr.AppearanceHovered.Options.UseForeColor = true;
            this.btncvrcgr.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btncvrcgr.Name = "btncvrcgr";
            this.btncvrcgr.Click += new System.EventHandler(this.btncvrcgr_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.AllowFocus = false;
            resources.ApplyResources(this.simpleButton1, "simpleButton1");
            this.simpleButton1.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton1.Appearance.BackColor")));
            this.simpleButton1.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton1.Appearance.Font")));
            this.simpleButton1.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton1.Appearance.ForeColor")));
            this.simpleButton1.Appearance.Options.UseBackColor = true;
            this.simpleButton1.Appearance.Options.UseFont = true;
            this.simpleButton1.Appearance.Options.UseForeColor = true;
            this.simpleButton1.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton1.AppearanceHovered.BackColor")));
            this.simpleButton1.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton1.AppearanceHovered.Font")));
            this.simpleButton1.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton1.AppearanceHovered.ForeColor")));
            this.simpleButton1.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton1.AppearanceHovered.Options.UseFont = true;
            this.simpleButton1.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // tmrtmr
            // 
            this.tmrtmr.Interval = 3000;
            this.tmrtmr.Tick += new System.EventHandler(this.tmrtmr_Tick);
            // 
            // lblinfo
            // 
            resources.ApplyResources(this.lblinfo, "lblinfo");
            this.lblinfo.ForeColor = System.Drawing.Color.DimGray;
            this.lblinfo.Name = "lblinfo";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.ForeColor = System.Drawing.Color.DimGray;
            this.label15.Name = "label15";
            // 
            // txtinfo
            // 
            this.txtinfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.txtinfo, "txtinfo");
            this.txtinfo.ForeColor = System.Drawing.Color.DimGray;
            this.txtinfo.Name = "txtinfo";
            // 
            // btnsimpleFlash
            // 
            this.btnsimpleFlash.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnsimpleFlash.Appearance.BackColor")));
            this.btnsimpleFlash.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnsimpleFlash.Appearance.Font")));
            this.btnsimpleFlash.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnsimpleFlash.Appearance.ForeColor")));
            this.btnsimpleFlash.Appearance.Options.UseBackColor = true;
            this.btnsimpleFlash.Appearance.Options.UseFont = true;
            this.btnsimpleFlash.Appearance.Options.UseForeColor = true;
            this.btnsimpleFlash.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnsimpleFlash.AppearanceHovered.BackColor")));
            this.btnsimpleFlash.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnsimpleFlash.AppearanceHovered.Font")));
            this.btnsimpleFlash.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnsimpleFlash.AppearanceHovered.ForeColor")));
            this.btnsimpleFlash.AppearanceHovered.Options.UseBackColor = true;
            this.btnsimpleFlash.AppearanceHovered.Options.UseFont = true;
            this.btnsimpleFlash.AppearanceHovered.Options.UseForeColor = true;
            this.btnsimpleFlash.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnsimpleFlash, "btnsimpleFlash");
            this.btnsimpleFlash.Name = "btnsimpleFlash";
            this.btnsimpleFlash.Click += new System.EventHandler(this.btnsimpleFlash_Click);
            // 
            // cmbDevNo
            // 
            resources.ApplyResources(this.cmbDevNo, "cmbDevNo");
            this.cmbDevNo.Name = "cmbDevNo";
            this.cmbDevNo.Properties.AllowFocused = false;
            this.cmbDevNo.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cmbDevNo.Properties.Appearance.Font")));
            this.cmbDevNo.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("cmbDevNo.Properties.Appearance.ForeColor")));
            this.cmbDevNo.Properties.Appearance.Options.UseFont = true;
            this.cmbDevNo.Properties.Appearance.Options.UseForeColor = true;
            this.cmbDevNo.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbDevNo.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cmbDevNo.Properties.Buttons"))), resources.GetString("cmbDevNo.Properties.Buttons1"), ((int)(resources.GetObject("cmbDevNo.Properties.Buttons2"))), ((bool)(resources.GetObject("cmbDevNo.Properties.Buttons3"))), ((bool)(resources.GetObject("cmbDevNo.Properties.Buttons4"))), ((bool)(resources.GetObject("cmbDevNo.Properties.Buttons5"))), ((DevExpress.XtraEditors.ImageLocation)(resources.GetObject("cmbDevNo.Properties.Buttons6"))), ((System.Drawing.Image)(resources.GetObject("cmbDevNo.Properties.Buttons7"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject7, resources.GetString("cmbDevNo.Properties.Buttons8"), ((object)(resources.GetObject("cmbDevNo.Properties.Buttons9"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("cmbDevNo.Properties.Buttons10"))), ((bool)(resources.GetObject("cmbDevNo.Properties.Buttons11"))))});
            this.cmbDevNo.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.cmbDevNo.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbDevNo.SelectedIndexChanged += new System.EventHandler(this.cmbDevNo_SelectedIndexChanged);
            this.cmbDevNo.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.cmbDevNo_Closed);
            this.cmbDevNo.MouseDown += new System.Windows.Forms.MouseEventHandler(this.cmbDevNo_MouseDown);
            // 
            // simpleButton2
            // 
            this.simpleButton2.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton2.Appearance.BackColor")));
            this.simpleButton2.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton2.Appearance.Font")));
            this.simpleButton2.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton2.Appearance.ForeColor")));
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseFont = true;
            this.simpleButton2.Appearance.Options.UseForeColor = true;
            this.simpleButton2.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton2.AppearanceHovered.BackColor")));
            this.simpleButton2.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton2.AppearanceHovered.Font")));
            this.simpleButton2.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton2.AppearanceHovered.ForeColor")));
            this.simpleButton2.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton2.AppearanceHovered.Options.UseFont = true;
            this.simpleButton2.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton2, "simpleButton2");
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Click += new System.EventHandler(this.simpleButton2_Click_1);
            // 
            // simpleButton3
            // 
            this.simpleButton3.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton3.Appearance.BackColor")));
            this.simpleButton3.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton3.Appearance.Font")));
            this.simpleButton3.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton3.Appearance.ForeColor")));
            this.simpleButton3.Appearance.Options.UseBackColor = true;
            this.simpleButton3.Appearance.Options.UseFont = true;
            this.simpleButton3.Appearance.Options.UseForeColor = true;
            this.simpleButton3.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton3.AppearanceHovered.BackColor")));
            this.simpleButton3.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton3.AppearanceHovered.Font")));
            this.simpleButton3.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton3.AppearanceHovered.ForeColor")));
            this.simpleButton3.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton3.AppearanceHovered.Options.UseFont = true;
            this.simpleButton3.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton3.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton3, "simpleButton3");
            this.simpleButton3.Name = "simpleButton3";
            this.simpleButton3.Click += new System.EventHandler(this.simpleButton3_Click);
            // 
            // simpleButton4
            // 
            this.simpleButton4.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton4.Appearance.BackColor")));
            this.simpleButton4.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton4.Appearance.Font")));
            this.simpleButton4.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton4.Appearance.ForeColor")));
            this.simpleButton4.Appearance.Options.UseBackColor = true;
            this.simpleButton4.Appearance.Options.UseFont = true;
            this.simpleButton4.Appearance.Options.UseForeColor = true;
            this.simpleButton4.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton4.AppearanceHovered.BackColor")));
            this.simpleButton4.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton4.AppearanceHovered.Font")));
            this.simpleButton4.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton4.AppearanceHovered.ForeColor")));
            this.simpleButton4.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton4.AppearanceHovered.Options.UseFont = true;
            this.simpleButton4.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton4.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton4, "simpleButton4");
            this.simpleButton4.Name = "simpleButton4";
            this.simpleButton4.Click += new System.EventHandler(this.simpleButton4_Click);
            // 
            // simpleButton5
            // 
            resources.ApplyResources(this.simpleButton5, "simpleButton5");
            this.simpleButton5.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton5.Appearance.BackColor")));
            this.simpleButton5.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton5.Appearance.Font")));
            this.simpleButton5.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton5.Appearance.ForeColor")));
            this.simpleButton5.Appearance.Options.UseBackColor = true;
            this.simpleButton5.Appearance.Options.UseFont = true;
            this.simpleButton5.Appearance.Options.UseForeColor = true;
            this.simpleButton5.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton5.AppearanceHovered.BackColor")));
            this.simpleButton5.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton5.AppearanceHovered.Font")));
            this.simpleButton5.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton5.AppearanceHovered.ForeColor")));
            this.simpleButton5.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton5.AppearanceHovered.Options.UseFont = true;
            this.simpleButton5.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton5.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton5.Name = "simpleButton5";
            this.simpleButton5.Click += new System.EventHandler(this.simpleButton5_Click);
            // 
            // simpleButton6
            // 
            resources.ApplyResources(this.simpleButton6, "simpleButton6");
            this.simpleButton6.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton6.Appearance.BackColor")));
            this.simpleButton6.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton6.Appearance.Font")));
            this.simpleButton6.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton6.Appearance.ForeColor")));
            this.simpleButton6.Appearance.Options.UseBackColor = true;
            this.simpleButton6.Appearance.Options.UseFont = true;
            this.simpleButton6.Appearance.Options.UseForeColor = true;
            this.simpleButton6.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton6.AppearanceHovered.BackColor")));
            this.simpleButton6.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton6.AppearanceHovered.Font")));
            this.simpleButton6.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton6.AppearanceHovered.ForeColor")));
            this.simpleButton6.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton6.AppearanceHovered.Options.UseFont = true;
            this.simpleButton6.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton6.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton6.Name = "simpleButton6";
            this.simpleButton6.Click += new System.EventHandler(this.simpleButton6_Click);
            // 
            // simpleButton7
            // 
            resources.ApplyResources(this.simpleButton7, "simpleButton7");
            this.simpleButton7.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton7.Appearance.BackColor")));
            this.simpleButton7.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton7.Appearance.Font")));
            this.simpleButton7.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton7.Appearance.ForeColor")));
            this.simpleButton7.Appearance.Options.UseBackColor = true;
            this.simpleButton7.Appearance.Options.UseFont = true;
            this.simpleButton7.Appearance.Options.UseForeColor = true;
            this.simpleButton7.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton7.AppearanceHovered.BackColor")));
            this.simpleButton7.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton7.AppearanceHovered.Font")));
            this.simpleButton7.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton7.AppearanceHovered.ForeColor")));
            this.simpleButton7.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton7.AppearanceHovered.Options.UseFont = true;
            this.simpleButton7.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton7.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton7.Name = "simpleButton7";
            this.simpleButton7.Click += new System.EventHandler(this.simpleButton7_Click);
            // 
            // cmbSettings
            // 
            resources.ApplyResources(this.cmbSettings, "cmbSettings");
            this.cmbSettings.Name = "cmbSettings";
            this.cmbSettings.Properties.AllowFocused = false;
            this.cmbSettings.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("cmbSettings.Properties.Appearance.Font")));
            this.cmbSettings.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("cmbSettings.Properties.Appearance.ForeColor")));
            this.cmbSettings.Properties.Appearance.Options.UseFont = true;
            this.cmbSettings.Properties.Appearance.Options.UseForeColor = true;
            this.cmbSettings.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.cmbSettings.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(((DevExpress.XtraEditors.Controls.ButtonPredefines)(resources.GetObject("cmbSettings.Properties.Buttons"))), resources.GetString("cmbSettings.Properties.Buttons1"), ((int)(resources.GetObject("cmbSettings.Properties.Buttons2"))), ((bool)(resources.GetObject("cmbSettings.Properties.Buttons3"))), ((bool)(resources.GetObject("cmbSettings.Properties.Buttons4"))), ((bool)(resources.GetObject("cmbSettings.Properties.Buttons5"))), ((DevExpress.XtraEditors.ImageLocation)(resources.GetObject("cmbSettings.Properties.Buttons6"))), ((System.Drawing.Image)(resources.GetObject("cmbSettings.Properties.Buttons7"))), new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject8, resources.GetString("cmbSettings.Properties.Buttons8"), ((object)(resources.GetObject("cmbSettings.Properties.Buttons9"))), ((DevExpress.Utils.SuperToolTip)(resources.GetObject("cmbSettings.Properties.Buttons10"))), ((bool)(resources.GetObject("cmbSettings.Properties.Buttons11"))))});
            this.cmbSettings.Properties.ButtonsStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.cmbSettings.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbSettings.SelectedIndexChanged += new System.EventHandler(this.cmbSettings_SelectedIndexChanged);
            this.cmbSettings.Closed += new DevExpress.XtraEditors.Controls.ClosedEventHandler(this.cmbSettings_Closed);
            // 
            // btnSend
            // 
            this.btnSend.AllowFocus = false;
            this.btnSend.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnSend.Appearance.BackColor")));
            this.btnSend.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnSend.Appearance.Font")));
            this.btnSend.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnSend.Appearance.ForeColor")));
            this.btnSend.Appearance.Options.UseBackColor = true;
            this.btnSend.Appearance.Options.UseFont = true;
            this.btnSend.Appearance.Options.UseForeColor = true;
            this.btnSend.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnSend.AppearanceHovered.BackColor")));
            this.btnSend.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnSend.AppearanceHovered.Font")));
            this.btnSend.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnSend.AppearanceHovered.ForeColor")));
            this.btnSend.AppearanceHovered.Options.UseBackColor = true;
            this.btnSend.AppearanceHovered.Options.UseFont = true;
            this.btnSend.AppearanceHovered.Options.UseForeColor = true;
            this.btnSend.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnSend, "btnSend");
            this.btnSend.Name = "btnSend";
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnDefault
            // 
            this.btnDefault.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnDefault.Appearance.BackColor")));
            this.btnDefault.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnDefault.Appearance.Font")));
            this.btnDefault.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnDefault.Appearance.ForeColor")));
            this.btnDefault.Appearance.Options.UseBackColor = true;
            this.btnDefault.Appearance.Options.UseFont = true;
            this.btnDefault.Appearance.Options.UseForeColor = true;
            this.btnDefault.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnDefault.AppearanceHovered.BackColor")));
            this.btnDefault.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnDefault.AppearanceHovered.Font")));
            this.btnDefault.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnDefault.AppearanceHovered.ForeColor")));
            this.btnDefault.AppearanceHovered.Options.UseBackColor = true;
            this.btnDefault.AppearanceHovered.Options.UseFont = true;
            this.btnDefault.AppearanceHovered.Options.UseForeColor = true;
            this.btnDefault.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnDefault, "btnDefault");
            this.btnDefault.Name = "btnDefault";
            this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
            // 
            // txtrenk3
            // 
            resources.ApplyResources(this.txtrenk3, "txtrenk3");
            this.txtrenk3.Name = "txtrenk3";
            this.txtrenk3.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtrenk3.Properties.Appearance.Font")));
            this.txtrenk3.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtrenk3.Properties.Appearance.ForeColor")));
            this.txtrenk3.Properties.Appearance.Options.UseFont = true;
            this.txtrenk3.Properties.Appearance.Options.UseForeColor = true;
            this.txtrenk3.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtrenk3.Properties.ReadOnly = true;
            this.txtrenk3.Click += new System.EventHandler(this.txtrenk1_Click);
            // 
            // txtrenk2
            // 
            resources.ApplyResources(this.txtrenk2, "txtrenk2");
            this.txtrenk2.Name = "txtrenk2";
            this.txtrenk2.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtrenk2.Properties.Appearance.Font")));
            this.txtrenk2.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtrenk2.Properties.Appearance.ForeColor")));
            this.txtrenk2.Properties.Appearance.Options.UseFont = true;
            this.txtrenk2.Properties.Appearance.Options.UseForeColor = true;
            this.txtrenk2.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtrenk2.Properties.ReadOnly = true;
            this.txtrenk2.Click += new System.EventHandler(this.txtrenk1_Click);
            // 
            // txtrenk1
            // 
            resources.ApplyResources(this.txtrenk1, "txtrenk1");
            this.txtrenk1.Name = "txtrenk1";
            this.txtrenk1.Properties.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("txtrenk1.Properties.Appearance.Font")));
            this.txtrenk1.Properties.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("txtrenk1.Properties.Appearance.ForeColor")));
            this.txtrenk1.Properties.Appearance.Options.UseFont = true;
            this.txtrenk1.Properties.Appearance.Options.UseForeColor = true;
            this.txtrenk1.Properties.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.txtrenk1.Properties.ReadOnly = true;
            this.txtrenk1.Click += new System.EventHandler(this.txtrenk1_Click);
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.ForeColor = System.Drawing.Color.DimGray;
            this.label16.Name = "label16";
            // 
            // btnMasterStart
            // 
            this.btnMasterStart.AllowFocus = false;
            this.btnMasterStart.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnMasterStart.Appearance.BackColor")));
            this.btnMasterStart.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnMasterStart.Appearance.Font")));
            this.btnMasterStart.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnMasterStart.Appearance.ForeColor")));
            this.btnMasterStart.Appearance.Options.UseBackColor = true;
            this.btnMasterStart.Appearance.Options.UseFont = true;
            this.btnMasterStart.Appearance.Options.UseForeColor = true;
            this.btnMasterStart.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnMasterStart.AppearanceHovered.BackColor")));
            this.btnMasterStart.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnMasterStart.AppearanceHovered.Font")));
            this.btnMasterStart.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnMasterStart.AppearanceHovered.ForeColor")));
            this.btnMasterStart.AppearanceHovered.Options.UseBackColor = true;
            this.btnMasterStart.AppearanceHovered.Options.UseFont = true;
            this.btnMasterStart.AppearanceHovered.Options.UseForeColor = true;
            this.btnMasterStart.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnMasterStart, "btnMasterStart");
            this.btnMasterStart.Name = "btnMasterStart";
            this.btnMasterStart.Click += new System.EventHandler(this.btnMasterStart_Click);
            // 
            // separatorControl6
            // 
            this.separatorControl6.LineAlignment = DevExpress.XtraEditors.Alignment.Near;
            this.separatorControl6.LineColor = System.Drawing.Color.Orange;
            resources.ApplyResources(this.separatorControl6, "separatorControl6");
            this.separatorControl6.Name = "separatorControl6";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.ForeColor = System.Drawing.Color.DimGray;
            this.label17.Name = "label17";
            this.label17.Click += new System.EventHandler(this.label17_Click);
            // 
            // simpleButton8
            // 
            this.simpleButton8.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton8.Appearance.BackColor")));
            this.simpleButton8.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton8.Appearance.Font")));
            this.simpleButton8.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton8.Appearance.ForeColor")));
            this.simpleButton8.Appearance.Options.UseBackColor = true;
            this.simpleButton8.Appearance.Options.UseFont = true;
            this.simpleButton8.Appearance.Options.UseForeColor = true;
            this.simpleButton8.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton8.AppearanceHovered.BackColor")));
            this.simpleButton8.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton8.AppearanceHovered.Font")));
            this.simpleButton8.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton8.AppearanceHovered.ForeColor")));
            this.simpleButton8.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton8.AppearanceHovered.Options.UseFont = true;
            this.simpleButton8.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton8.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton8, "simpleButton8");
            this.simpleButton8.Name = "simpleButton8";
            this.simpleButton8.Click += new System.EventHandler(this.simpleButton8_Click);
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.ForeColor = System.Drawing.Color.DimGray;
            this.label13.Name = "label13";
            // 
            // chkYes
            // 
            resources.ApplyResources(this.chkYes, "chkYes");
            this.chkYes.Checked = true;
            this.chkYes.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkYes.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.chkYes.FlatAppearance.CheckedBackColor = System.Drawing.Color.Orange;
            this.chkYes.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.chkYes.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.chkYes.ForeColor = System.Drawing.Color.DimGray;
            this.chkYes.Name = "chkYes";
            this.chkYes.UseVisualStyleBackColor = true;
            this.chkYes.CheckedChanged += new System.EventHandler(this.chkYes_CheckedChanged);
            this.chkYes.CheckStateChanged += new System.EventHandler(this.chkYes_CheckStateChanged);
            // 
            // chkNo
            // 
            resources.ApplyResources(this.chkNo, "chkNo");
            this.chkNo.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
            this.chkNo.FlatAppearance.CheckedBackColor = System.Drawing.Color.Orange;
            this.chkNo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.chkNo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.chkNo.ForeColor = System.Drawing.Color.DimGray;
            this.chkNo.Name = "chkNo";
            this.chkNo.UseVisualStyleBackColor = true;
            this.chkNo.CheckedChanged += new System.EventHandler(this.chkNo_CheckedChanged);
            this.chkNo.CheckStateChanged += new System.EventHandler(this.chkNo_CheckStateChanged);
            // 
            // pictureBox2
            // 
            resources.ApplyResources(this.pictureBox2, "pictureBox2");
            this.pictureBox2.Image = global::cevircagirserverdeneme.Properties.Resources.help;
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // btnTr
            // 
            resources.ApplyResources(this.btnTr, "btnTr");
            this.btnTr.BackColor = System.Drawing.Color.Transparent;
            this.btnTr.FlatAppearance.BorderSize = 0;
            this.btnTr.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnTr.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnTr.Image = global::cevircagirserverdeneme.Properties.Resources.turkey32;
            this.btnTr.Name = "btnTr";
            this.btnTr.UseVisualStyleBackColor = false;
            this.btnTr.Click += new System.EventHandler(this.btnTr_Click);
            // 
            // btnEn
            // 
            resources.ApplyResources(this.btnEn, "btnEn");
            this.btnEn.BackColor = System.Drawing.Color.Transparent;
            this.btnEn.FlatAppearance.BorderSize = 0;
            this.btnEn.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.btnEn.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.btnEn.Image = global::cevircagirserverdeneme.Properties.Resources.united_kingdom2;
            this.btnEn.Name = "btnEn";
            this.btnEn.UseVisualStyleBackColor = false;
            this.btnEn.Click += new System.EventHandler(this.btnEn_Click);
            // 
            // pictureBox1
            // 
            resources.ApplyResources(this.pictureBox1, "pictureBox1");
            this.pictureBox1.Image = global::cevircagirserverdeneme.Properties.Resources.modem2akesiz;
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.TabStop = false;
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.ForeColor = System.Drawing.Color.DimGray;
            this.label9.Name = "label9";
            // 
            // simpleButton10
            // 
            this.simpleButton10.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton10.Appearance.BackColor")));
            this.simpleButton10.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton10.Appearance.Font")));
            this.simpleButton10.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton10.Appearance.ForeColor")));
            this.simpleButton10.Appearance.Options.UseBackColor = true;
            this.simpleButton10.Appearance.Options.UseFont = true;
            this.simpleButton10.Appearance.Options.UseForeColor = true;
            this.simpleButton10.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton10.AppearanceHovered.BackColor")));
            this.simpleButton10.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton10.AppearanceHovered.Font")));
            this.simpleButton10.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton10.AppearanceHovered.ForeColor")));
            this.simpleButton10.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton10.AppearanceHovered.Options.UseFont = true;
            this.simpleButton10.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton10.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton10, "simpleButton10");
            this.simpleButton10.Name = "simpleButton10";
            this.simpleButton10.Click += new System.EventHandler(this.simpleButton10_Click);
            // 
            // simpleButton11
            // 
            this.simpleButton11.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton11.Appearance.BackColor")));
            this.simpleButton11.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton11.Appearance.Font")));
            this.simpleButton11.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton11.Appearance.ForeColor")));
            this.simpleButton11.Appearance.Options.UseBackColor = true;
            this.simpleButton11.Appearance.Options.UseFont = true;
            this.simpleButton11.Appearance.Options.UseForeColor = true;
            this.simpleButton11.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton11.AppearanceHovered.BackColor")));
            this.simpleButton11.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton11.AppearanceHovered.Font")));
            this.simpleButton11.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton11.AppearanceHovered.ForeColor")));
            this.simpleButton11.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton11.AppearanceHovered.Options.UseFont = true;
            this.simpleButton11.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton11.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton11, "simpleButton11");
            this.simpleButton11.Name = "simpleButton11";
            this.simpleButton11.Click += new System.EventHandler(this.simpleButton11_Click);
            // 
            // simpleButton12
            // 
            this.simpleButton12.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton12.Appearance.BackColor")));
            this.simpleButton12.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton12.Appearance.Font")));
            this.simpleButton12.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton12.Appearance.ForeColor")));
            this.simpleButton12.Appearance.Options.UseBackColor = true;
            this.simpleButton12.Appearance.Options.UseFont = true;
            this.simpleButton12.Appearance.Options.UseForeColor = true;
            this.simpleButton12.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton12.AppearanceHovered.BackColor")));
            this.simpleButton12.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton12.AppearanceHovered.Font")));
            this.simpleButton12.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton12.AppearanceHovered.ForeColor")));
            this.simpleButton12.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton12.AppearanceHovered.Options.UseFont = true;
            this.simpleButton12.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton12.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton12, "simpleButton12");
            this.simpleButton12.Name = "simpleButton12";
            this.simpleButton12.Click += new System.EventHandler(this.simpleButton12_Click);
            // 
            // simpleButton13
            // 
            this.simpleButton13.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton13.Appearance.BackColor")));
            this.simpleButton13.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton13.Appearance.Font")));
            this.simpleButton13.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton13.Appearance.ForeColor")));
            this.simpleButton13.Appearance.Options.UseBackColor = true;
            this.simpleButton13.Appearance.Options.UseFont = true;
            this.simpleButton13.Appearance.Options.UseForeColor = true;
            this.simpleButton13.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton13.AppearanceHovered.BackColor")));
            this.simpleButton13.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton13.AppearanceHovered.Font")));
            this.simpleButton13.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton13.AppearanceHovered.ForeColor")));
            this.simpleButton13.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton13.AppearanceHovered.Options.UseFont = true;
            this.simpleButton13.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton13.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton13, "simpleButton13");
            this.simpleButton13.Name = "simpleButton13";
            this.simpleButton13.Click += new System.EventHandler(this.simpleButton13_Click);
            // 
            // simpleButton14
            // 
            this.simpleButton14.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton14.Appearance.BackColor")));
            this.simpleButton14.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton14.Appearance.Font")));
            this.simpleButton14.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton14.Appearance.ForeColor")));
            this.simpleButton14.Appearance.Options.UseBackColor = true;
            this.simpleButton14.Appearance.Options.UseFont = true;
            this.simpleButton14.Appearance.Options.UseForeColor = true;
            this.simpleButton14.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton14.AppearanceHovered.BackColor")));
            this.simpleButton14.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton14.AppearanceHovered.Font")));
            this.simpleButton14.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton14.AppearanceHovered.ForeColor")));
            this.simpleButton14.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton14.AppearanceHovered.Options.UseFont = true;
            this.simpleButton14.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton14.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton14, "simpleButton14");
            this.simpleButton14.Name = "simpleButton14";
            this.simpleButton14.Click += new System.EventHandler(this.simpleButton14_Click);
            // 
            // simpleButton15
            // 
            this.simpleButton15.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton15.Appearance.BackColor")));
            this.simpleButton15.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton15.Appearance.Font")));
            this.simpleButton15.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton15.Appearance.ForeColor")));
            this.simpleButton15.Appearance.Options.UseBackColor = true;
            this.simpleButton15.Appearance.Options.UseFont = true;
            this.simpleButton15.Appearance.Options.UseForeColor = true;
            this.simpleButton15.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton15.AppearanceHovered.BackColor")));
            this.simpleButton15.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton15.AppearanceHovered.Font")));
            this.simpleButton15.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton15.AppearanceHovered.ForeColor")));
            this.simpleButton15.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton15.AppearanceHovered.Options.UseFont = true;
            this.simpleButton15.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton15.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton15, "simpleButton15");
            this.simpleButton15.Name = "simpleButton15";
            this.simpleButton15.Click += new System.EventHandler(this.simpleButton15_Click);
            // 
            // simpleButton16
            // 
            this.simpleButton16.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton16.Appearance.BackColor")));
            this.simpleButton16.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton16.Appearance.Font")));
            this.simpleButton16.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton16.Appearance.ForeColor")));
            this.simpleButton16.Appearance.Options.UseBackColor = true;
            this.simpleButton16.Appearance.Options.UseFont = true;
            this.simpleButton16.Appearance.Options.UseForeColor = true;
            this.simpleButton16.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton16.AppearanceHovered.BackColor")));
            this.simpleButton16.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton16.AppearanceHovered.Font")));
            this.simpleButton16.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton16.AppearanceHovered.ForeColor")));
            this.simpleButton16.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton16.AppearanceHovered.Options.UseFont = true;
            this.simpleButton16.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton16.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton16, "simpleButton16");
            this.simpleButton16.Name = "simpleButton16";
            this.simpleButton16.Click += new System.EventHandler(this.simpleButton16_Click);
            // 
            // separatorControl7
            // 
            this.separatorControl7.LineAlignment = DevExpress.XtraEditors.Alignment.Near;
            this.separatorControl7.LineColor = System.Drawing.Color.Orange;
            resources.ApplyResources(this.separatorControl7, "separatorControl7");
            this.separatorControl7.Name = "separatorControl7";
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.ForeColor = System.Drawing.Color.DimGray;
            this.label18.Name = "label18";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.ForeColor = System.Drawing.Color.DimGray;
            this.label19.Name = "label19";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.ForeColor = System.Drawing.Color.DimGray;
            this.label20.Name = "label20";
            // 
            // simpleButton18
            // 
            this.simpleButton18.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton18.Appearance.BackColor")));
            this.simpleButton18.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton18.Appearance.Font")));
            this.simpleButton18.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton18.Appearance.ForeColor")));
            this.simpleButton18.Appearance.Options.UseBackColor = true;
            this.simpleButton18.Appearance.Options.UseFont = true;
            this.simpleButton18.Appearance.Options.UseForeColor = true;
            this.simpleButton18.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton18.AppearanceHovered.BackColor")));
            this.simpleButton18.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton18.AppearanceHovered.Font")));
            this.simpleButton18.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton18.AppearanceHovered.ForeColor")));
            this.simpleButton18.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton18.AppearanceHovered.Options.UseFont = true;
            this.simpleButton18.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton18.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton18, "simpleButton18");
            this.simpleButton18.Name = "simpleButton18";
            this.simpleButton18.Click += new System.EventHandler(this.simpleButton18_Click);
            // 
            // simpleButton19
            // 
            this.simpleButton19.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton19.Appearance.BackColor")));
            this.simpleButton19.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton19.Appearance.Font")));
            this.simpleButton19.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton19.Appearance.ForeColor")));
            this.simpleButton19.Appearance.Options.UseBackColor = true;
            this.simpleButton19.Appearance.Options.UseFont = true;
            this.simpleButton19.Appearance.Options.UseForeColor = true;
            this.simpleButton19.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton19.AppearanceHovered.BackColor")));
            this.simpleButton19.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton19.AppearanceHovered.Font")));
            this.simpleButton19.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton19.AppearanceHovered.ForeColor")));
            this.simpleButton19.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton19.AppearanceHovered.Options.UseFont = true;
            this.simpleButton19.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton19.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton19, "simpleButton19");
            this.simpleButton19.Name = "simpleButton19";
            this.simpleButton19.Click += new System.EventHandler(this.simpleButton19_Click);
            // 
            // simpleButton20
            // 
            this.simpleButton20.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton20.Appearance.BackColor")));
            this.simpleButton20.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton20.Appearance.Font")));
            this.simpleButton20.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton20.Appearance.ForeColor")));
            this.simpleButton20.Appearance.Options.UseBackColor = true;
            this.simpleButton20.Appearance.Options.UseFont = true;
            this.simpleButton20.Appearance.Options.UseForeColor = true;
            this.simpleButton20.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton20.AppearanceHovered.BackColor")));
            this.simpleButton20.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton20.AppearanceHovered.Font")));
            this.simpleButton20.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton20.AppearanceHovered.ForeColor")));
            this.simpleButton20.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton20.AppearanceHovered.Options.UseFont = true;
            this.simpleButton20.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton20.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton20, "simpleButton20");
            this.simpleButton20.Name = "simpleButton20";
            this.simpleButton20.Click += new System.EventHandler(this.simpleButton20_Click);
            // 
            // simpleButton21
            // 
            this.simpleButton21.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton21.Appearance.BackColor")));
            this.simpleButton21.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton21.Appearance.Font")));
            this.simpleButton21.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton21.Appearance.ForeColor")));
            this.simpleButton21.Appearance.Options.UseBackColor = true;
            this.simpleButton21.Appearance.Options.UseFont = true;
            this.simpleButton21.Appearance.Options.UseForeColor = true;
            this.simpleButton21.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton21.AppearanceHovered.BackColor")));
            this.simpleButton21.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton21.AppearanceHovered.Font")));
            this.simpleButton21.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton21.AppearanceHovered.ForeColor")));
            this.simpleButton21.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton21.AppearanceHovered.Options.UseFont = true;
            this.simpleButton21.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton21.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton21, "simpleButton21");
            this.simpleButton21.Name = "simpleButton21";
            this.simpleButton21.Click += new System.EventHandler(this.simpleButton21_Click);
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.ForeColor = System.Drawing.Color.DimGray;
            this.label21.Name = "label21";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.ForeColor = System.Drawing.Color.DimGray;
            this.label22.Name = "label22";
            // 
            // label23
            // 
            resources.ApplyResources(this.label23, "label23");
            this.label23.ForeColor = System.Drawing.Color.DimGray;
            this.label23.Name = "label23";
            // 
            // simpleButton9
            // 
            this.simpleButton9.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton9.Appearance.BackColor")));
            this.simpleButton9.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton9.Appearance.Font")));
            this.simpleButton9.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton9.Appearance.ForeColor")));
            this.simpleButton9.Appearance.Options.UseBackColor = true;
            this.simpleButton9.Appearance.Options.UseFont = true;
            this.simpleButton9.Appearance.Options.UseForeColor = true;
            this.simpleButton9.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton9.AppearanceHovered.BackColor")));
            this.simpleButton9.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("simpleButton9.AppearanceHovered.Font")));
            this.simpleButton9.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("simpleButton9.AppearanceHovered.ForeColor")));
            this.simpleButton9.AppearanceHovered.Options.UseBackColor = true;
            this.simpleButton9.AppearanceHovered.Options.UseFont = true;
            this.simpleButton9.AppearanceHovered.Options.UseForeColor = true;
            this.simpleButton9.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.simpleButton9, "simpleButton9");
            this.simpleButton9.Name = "simpleButton9";
            this.simpleButton9.Click += new System.EventHandler(this.simpleButton9_Click_1);
            // 
            // separatorControl3
            // 
            this.separatorControl3.LineAlignment = DevExpress.XtraEditors.Alignment.Near;
            this.separatorControl3.LineColor = System.Drawing.Color.Orange;
            resources.ApplyResources(this.separatorControl3, "separatorControl3");
            this.separatorControl3.Name = "separatorControl3";
            // 
            // label24
            // 
            resources.ApplyResources(this.label24, "label24");
            this.label24.ForeColor = System.Drawing.Color.DimGray;
            this.label24.Name = "label24";
            // 
            // lstChn
            // 
            this.lstChn.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this.lstChn, "lstChn");
            this.lstChn.ForeColor = System.Drawing.Color.DimGray;
            this.lstChn.FormattingEnabled = true;
            this.lstChn.Name = "lstChn";
            // 
            // btnnet
            // 
            this.btnnet.AllowFocus = false;
            this.btnnet.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnnet.Appearance.BackColor")));
            this.btnnet.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnnet.Appearance.Font")));
            this.btnnet.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnnet.Appearance.ForeColor")));
            this.btnnet.Appearance.Options.UseBackColor = true;
            this.btnnet.Appearance.Options.UseFont = true;
            this.btnnet.Appearance.Options.UseForeColor = true;
            this.btnnet.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnnet.AppearanceHovered.BackColor")));
            this.btnnet.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnnet.AppearanceHovered.Font")));
            this.btnnet.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnnet.AppearanceHovered.ForeColor")));
            this.btnnet.AppearanceHovered.Options.UseBackColor = true;
            this.btnnet.AppearanceHovered.Options.UseFont = true;
            this.btnnet.AppearanceHovered.Options.UseForeColor = true;
            this.btnnet.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnnet, "btnnet");
            this.btnnet.Name = "btnnet";
            this.btnnet.Click += new System.EventHandler(this.btnnet_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.cmbssid);
            this.panel1.Controls.Add(this.separatorControl6);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            // 
            // separatorControl5
            // 
            this.separatorControl5.LineAlignment = DevExpress.XtraEditors.Alignment.Near;
            this.separatorControl5.LineColor = System.Drawing.Color.Orange;
            resources.ApplyResources(this.separatorControl5, "separatorControl5");
            this.separatorControl5.Name = "separatorControl5";
            // 
            // txtfirmaad
            // 
            resources.ApplyResources(this.txtfirmaad, "txtfirmaad");
            this.txtfirmaad.ForeColor = System.Drawing.Color.DimGray;
            this.txtfirmaad.Name = "txtfirmaad";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.ForeColor = System.Drawing.Color.DimGray;
            this.label10.Name = "label10";
            // 
            // separatorControl2
            // 
            this.separatorControl2.LineAlignment = DevExpress.XtraEditors.Alignment.Near;
            this.separatorControl2.LineColor = System.Drawing.Color.Orange;
            resources.ApplyResources(this.separatorControl2, "separatorControl2");
            this.separatorControl2.Name = "separatorControl2";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.ForeColor = System.Drawing.Color.DimGray;
            this.label8.Name = "label8";
            // 
            // formadminsettings
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.txtfirmaad);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.separatorControl2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.separatorControl5);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnnet);
            this.Controls.Add(this.lstChn);
            this.Controls.Add(this.separatorControl3);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.simpleButton9);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.simpleButton18);
            this.Controls.Add(this.simpleButton19);
            this.Controls.Add(this.simpleButton20);
            this.Controls.Add(this.simpleButton21);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.separatorControl7);
            this.Controls.Add(this.simpleButton13);
            this.Controls.Add(this.simpleButton14);
            this.Controls.Add(this.simpleButton15);
            this.Controls.Add(this.simpleButton16);
            this.Controls.Add(this.simpleButton12);
            this.Controls.Add(this.simpleButton11);
            this.Controls.Add(this.simpleButton10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.chkNo);
            this.Controls.Add(this.chkYes);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.simpleButton8);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.btnMasterStart);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtrenk1);
            this.Controls.Add(this.txtrenk2);
            this.Controls.Add(this.txtrenk3);
            this.Controls.Add(this.btnDefault);
            this.Controls.Add(this.btnSend);
            this.Controls.Add(this.cmbSettings);
            this.Controls.Add(this.simpleButton5);
            this.Controls.Add(this.simpleButton6);
            this.Controls.Add(this.simpleButton7);
            this.Controls.Add(this.simpleButton4);
            this.Controls.Add(this.simpleButton3);
            this.Controls.Add(this.simpleButton2);
            this.Controls.Add(this.cmbDevNo);
            this.Controls.Add(this.btnsimpleFlash);
            this.Controls.Add(this.btnEn);
            this.Controls.Add(this.btnTr);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.lblinfo);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.txtinfo);
            this.Controls.Add(this.btncvrcgr);
            this.Controls.Add(this.btnwakeup);
            this.Controls.Add(this.separatorControl4);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.btnderinuyku);
            this.Controls.Add(this.btnlightoff);
            this.Controls.Add(this.btnredlight);
            this.Controls.Add(this.cmbrenkc);
            this.Controls.Add(this.cmbrenkb);
            this.Controls.Add(this.cmbrenka);
            this.Controls.Add(this.cmbsurec);
            this.Controls.Add(this.cmbsureb);
            this.Controls.Add(this.cmbsurea);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtssidparola);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnadminkaydet);
            this.Controls.Add(this.separatorControl1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "formadminsettings";
            this.Load += new System.EventHandler(this.formadminsettings_Load);
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl1)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtssidparola.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbssid.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbsurea.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbsureb.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbsurec.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbrenka.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbrenkb.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbrenkc.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDevNo.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSettings.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrenk3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrenk2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtrenk1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl3)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.separatorControl2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private DevExpress.XtraEditors.SeparatorControl separatorControl1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Label lblgarsonbaslik;
        public System.Windows.Forms.Label lblformname;
        public DevExpress.XtraEditors.SimpleButton btnadminkaydet;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private DevExpress.XtraEditors.TextEdit txtssidparola;
        private DevExpress.XtraEditors.ComboBoxEdit cmbssid;
        private DevExpress.XtraEditors.ComboBoxEdit cmbsurea;
        private DevExpress.XtraEditors.ComboBoxEdit cmbsureb;
        private DevExpress.XtraEditors.ComboBoxEdit cmbsurec;
        private DevExpress.XtraEditors.ComboBoxEdit cmbrenka;
        private DevExpress.XtraEditors.ComboBoxEdit cmbrenkb;
        private DevExpress.XtraEditors.ComboBoxEdit cmbrenkc;
        public DevExpress.XtraEditors.SimpleButton btnredlight;
        public DevExpress.XtraEditors.SimpleButton btnlightoff;
        public DevExpress.XtraEditors.SimpleButton btnderinuyku;
        private DevExpress.XtraEditors.SeparatorControl separatorControl4;
        private System.Windows.Forms.Label label14;
        public DevExpress.XtraEditors.SimpleButton btnwakeup;
        public DevExpress.XtraEditors.SimpleButton btncvrcgr;
        public DevExpress.XtraEditors.SimpleButton simpleButton1;
        private System.Windows.Forms.Timer tmrtmr;
        private System.Windows.Forms.Label lblinfo;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnTr;
        private System.Windows.Forms.Button btnEn;
        private System.Windows.Forms.TextBox txtinfo;
        public DevExpress.XtraEditors.SimpleButton btnsimpleFlash;
        private DevExpress.XtraEditors.ComboBoxEdit cmbDevNo;
        public DevExpress.XtraEditors.SimpleButton simpleButton2;
        public DevExpress.XtraEditors.SimpleButton simpleButton3;
        public DevExpress.XtraEditors.SimpleButton simpleButton4;
        public DevExpress.XtraEditors.SimpleButton simpleButton5;
        public DevExpress.XtraEditors.SimpleButton simpleButton6;
        public DevExpress.XtraEditors.SimpleButton simpleButton7;
        private DevExpress.XtraEditors.ComboBoxEdit cmbSettings;
        public DevExpress.XtraEditors.SimpleButton btnSend;
        public DevExpress.XtraEditors.SimpleButton btnDefault;
        private DevExpress.XtraEditors.TextEdit txtrenk3;
        private DevExpress.XtraEditors.TextEdit txtrenk2;
        private DevExpress.XtraEditors.TextEdit txtrenk1;
        private System.Windows.Forms.Label label16;
        public DevExpress.XtraEditors.SimpleButton btnMasterStart;
        private System.Windows.Forms.PictureBox pictureBox1;
        private DevExpress.XtraEditors.SeparatorControl separatorControl6;
        private System.Windows.Forms.Label label17;
        public DevExpress.XtraEditors.SimpleButton simpleButton8;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox chkYes;
        private System.Windows.Forms.CheckBox chkNo;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label9;
        public DevExpress.XtraEditors.SimpleButton simpleButton10;
        public DevExpress.XtraEditors.SimpleButton simpleButton11;
        public DevExpress.XtraEditors.SimpleButton simpleButton12;
        public DevExpress.XtraEditors.SimpleButton simpleButton13;
        public DevExpress.XtraEditors.SimpleButton simpleButton14;
        public DevExpress.XtraEditors.SimpleButton simpleButton15;
        public DevExpress.XtraEditors.SimpleButton simpleButton16;
        private DevExpress.XtraEditors.SeparatorControl separatorControl7;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        public DevExpress.XtraEditors.SimpleButton simpleButton18;
        public DevExpress.XtraEditors.SimpleButton simpleButton19;
        public DevExpress.XtraEditors.SimpleButton simpleButton20;
        public DevExpress.XtraEditors.SimpleButton simpleButton21;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        public DevExpress.XtraEditors.SimpleButton simpleButton9;
        private DevExpress.XtraEditors.SeparatorControl separatorControl3;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ListBox lstChn;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        public DevExpress.XtraEditors.SimpleButton btnnet;
        private System.Windows.Forms.Panel panel1;
        private DevExpress.XtraEditors.SeparatorControl separatorControl5;
        private System.Windows.Forms.TextBox txtfirmaad;
        private System.Windows.Forms.Label label10;
        private DevExpress.XtraEditors.SeparatorControl separatorControl2;
        private System.Windows.Forms.Label label8;
    }
}