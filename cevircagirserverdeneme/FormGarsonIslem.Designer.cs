﻿namespace cevircagirserverdeneme
{
    partial class FormGarsonIslem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormGarsonIslem));
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnislemclose = new DevExpress.XtraEditors.SimpleButton();
            this.pnlduzenle = new System.Windows.Forms.Panel();
            this.btnediticon = new DevExpress.XtraEditors.SimpleButton();
            this.btnduzenle = new DevExpress.XtraEditors.SimpleButton();
            this.pnlyeni = new System.Windows.Forms.Panel();
            this.btnyeniicon = new DevExpress.XtraEditors.SimpleButton();
            this.btnyenikayit = new DevExpress.XtraEditors.SimpleButton();
            this.pnlliste = new System.Windows.Forms.Panel();
            this.btnlistele = new DevExpress.XtraEditors.SimpleButton();
            this.btnlisticon = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.pnlduzenle.SuspendLayout();
            this.pnlyeni.SuspendLayout();
            this.pnlliste.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("panelControl1.Appearance.BackColor")));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.panel1);
            this.panelControl1.Controls.Add(this.pnlduzenle);
            this.panelControl1.Controls.Add(this.pnlyeni);
            this.panelControl1.Controls.Add(this.pnlliste);
            resources.ApplyResources(this.panelControl1, "panelControl1");
            this.panelControl1.Name = "panelControl1";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.btnislemclose);
            resources.ApplyResources(this.panel1, "panel1");
            this.panel1.Name = "panel1";
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Name = "label1";
            // 
            // btnislemclose
            // 
            this.btnislemclose.AllowFocus = false;
            resources.ApplyResources(this.btnislemclose, "btnislemclose");
            this.btnislemclose.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnislemclose.Appearance.BackColor")));
            this.btnislemclose.Appearance.Options.UseBackColor = true;
            this.btnislemclose.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnislemclose.Image = ((System.Drawing.Image)(resources.GetObject("btnislemclose.Image")));
            this.btnislemclose.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.btnislemclose.Name = "btnislemclose";
            this.btnislemclose.Click += new System.EventHandler(this.btnislemclose_Click);
            // 
            // pnlduzenle
            // 
            this.pnlduzenle.Controls.Add(this.btnediticon);
            this.pnlduzenle.Controls.Add(this.btnduzenle);
            resources.ApplyResources(this.pnlduzenle, "pnlduzenle");
            this.pnlduzenle.Name = "pnlduzenle";
            // 
            // btnediticon
            // 
            this.btnediticon.AllowFocus = false;
            this.btnediticon.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnediticon.Appearance.BackColor")));
            this.btnediticon.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnediticon.Appearance.Font")));
            this.btnediticon.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnediticon.Appearance.ForeColor")));
            this.btnediticon.Appearance.Options.UseBackColor = true;
            this.btnediticon.Appearance.Options.UseFont = true;
            this.btnediticon.Appearance.Options.UseForeColor = true;
            this.btnediticon.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnediticon.Image = global::cevircagirserverdeneme.Properties.Resources.icons8_Edit_User_Male_30__1_;
            resources.ApplyResources(this.btnediticon, "btnediticon");
            this.btnediticon.Name = "btnediticon";
            this.btnediticon.Click += new System.EventHandler(this.btnduzenle_Click);
            this.btnediticon.MouseLeave += new System.EventHandler(this.btnediticon_MouseLeave);
            this.btnediticon.MouseHover += new System.EventHandler(this.btnediticon_MouseHover);
            // 
            // btnduzenle
            // 
            this.btnduzenle.AllowFocus = false;
            this.btnduzenle.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnduzenle.Appearance.BackColor")));
            this.btnduzenle.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnduzenle.Appearance.Font")));
            this.btnduzenle.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnduzenle.Appearance.ForeColor")));
            this.btnduzenle.Appearance.Options.UseBackColor = true;
            this.btnduzenle.Appearance.Options.UseFont = true;
            this.btnduzenle.Appearance.Options.UseForeColor = true;
            this.btnduzenle.Appearance.Options.UseTextOptions = true;
            this.btnduzenle.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnduzenle.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btnduzenle.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnduzenle.AppearanceHovered.Font")));
            this.btnduzenle.AppearanceHovered.Options.UseFont = true;
            this.btnduzenle.AppearanceHovered.Options.UseTextOptions = true;
            this.btnduzenle.AppearanceHovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnduzenle.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnduzenle, "btnduzenle");
            this.btnduzenle.Name = "btnduzenle";
            this.btnduzenle.Click += new System.EventHandler(this.btnduzenle_Click);
            this.btnduzenle.MouseLeave += new System.EventHandler(this.btnduzenle_MouseLeave);
            this.btnduzenle.MouseHover += new System.EventHandler(this.btnduzenle_MouseHover);
            // 
            // pnlyeni
            // 
            this.pnlyeni.Controls.Add(this.btnyeniicon);
            this.pnlyeni.Controls.Add(this.btnyenikayit);
            resources.ApplyResources(this.pnlyeni, "pnlyeni");
            this.pnlyeni.Name = "pnlyeni";
            this.pnlyeni.MouseLeave += new System.EventHandler(this.btnyenikayit_MouseLeave);
            this.pnlyeni.MouseHover += new System.EventHandler(this.btnyenikayit_MouseHover);
            // 
            // btnyeniicon
            // 
            this.btnyeniicon.AllowFocus = false;
            this.btnyeniicon.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnyeniicon.Appearance.BackColor")));
            this.btnyeniicon.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnyeniicon.Appearance.Font")));
            this.btnyeniicon.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnyeniicon.Appearance.ForeColor")));
            this.btnyeniicon.Appearance.Options.UseBackColor = true;
            this.btnyeniicon.Appearance.Options.UseFont = true;
            this.btnyeniicon.Appearance.Options.UseForeColor = true;
            this.btnyeniicon.Appearance.Options.UseTextOptions = true;
            this.btnyeniicon.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnyeniicon.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnyeniicon.Image = global::cevircagirserverdeneme.Properties.Resources.icons8_Add_User_Male_30;
            resources.ApplyResources(this.btnyeniicon, "btnyeniicon");
            this.btnyeniicon.Name = "btnyeniicon";
            this.btnyeniicon.Click += new System.EventHandler(this.btnyenikayit_Click);
            this.btnyeniicon.MouseLeave += new System.EventHandler(this.btnyeniicon_MouseLeave);
            this.btnyeniicon.MouseHover += new System.EventHandler(this.btnyeniicon_MouseHover);
            // 
            // btnyenikayit
            // 
            this.btnyenikayit.AllowFocus = false;
            this.btnyenikayit.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnyenikayit.Appearance.BackColor")));
            this.btnyenikayit.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnyenikayit.Appearance.Font")));
            this.btnyenikayit.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnyenikayit.Appearance.ForeColor")));
            this.btnyenikayit.Appearance.Options.UseBackColor = true;
            this.btnyenikayit.Appearance.Options.UseFont = true;
            this.btnyenikayit.Appearance.Options.UseForeColor = true;
            this.btnyenikayit.Appearance.Options.UseTextOptions = true;
            this.btnyenikayit.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnyenikayit.Appearance.TextOptions.VAlignment = DevExpress.Utils.VertAlignment.Center;
            this.btnyenikayit.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("btnyenikayit.AppearanceHovered.BackColor")));
            this.btnyenikayit.AppearanceHovered.BorderColor = ((System.Drawing.Color)(resources.GetObject("btnyenikayit.AppearanceHovered.BorderColor")));
            this.btnyenikayit.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnyenikayit.AppearanceHovered.Font")));
            this.btnyenikayit.AppearanceHovered.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnyenikayit.AppearanceHovered.ForeColor")));
            this.btnyenikayit.AppearanceHovered.Options.UseBackColor = true;
            this.btnyenikayit.AppearanceHovered.Options.UseBorderColor = true;
            this.btnyenikayit.AppearanceHovered.Options.UseFont = true;
            this.btnyenikayit.AppearanceHovered.Options.UseForeColor = true;
            this.btnyenikayit.AppearanceHovered.Options.UseTextOptions = true;
            this.btnyenikayit.AppearanceHovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnyenikayit.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnyenikayit, "btnyenikayit");
            this.btnyenikayit.Name = "btnyenikayit";
            this.btnyenikayit.ShowFocusRectangle = DevExpress.Utils.DefaultBoolean.False;
            this.btnyenikayit.Click += new System.EventHandler(this.btnyenikayit_Click);
            this.btnyenikayit.MouseLeave += new System.EventHandler(this.btnyenikayit_MouseLeave);
            this.btnyenikayit.MouseHover += new System.EventHandler(this.btnyenikayit_MouseHover);
            // 
            // pnlliste
            // 
            this.pnlliste.Controls.Add(this.btnlistele);
            this.pnlliste.Controls.Add(this.btnlisticon);
            resources.ApplyResources(this.pnlliste, "pnlliste");
            this.pnlliste.Name = "pnlliste";
            // 
            // btnlistele
            // 
            this.btnlistele.AllowFocus = false;
            this.btnlistele.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnlistele.Appearance.BackColor")));
            this.btnlistele.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnlistele.Appearance.Font")));
            this.btnlistele.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnlistele.Appearance.ForeColor")));
            this.btnlistele.Appearance.Options.UseBackColor = true;
            this.btnlistele.Appearance.Options.UseFont = true;
            this.btnlistele.Appearance.Options.UseForeColor = true;
            this.btnlistele.Appearance.Options.UseTextOptions = true;
            this.btnlistele.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnlistele.AppearanceHovered.Font = ((System.Drawing.Font)(resources.GetObject("btnlistele.AppearanceHovered.Font")));
            this.btnlistele.AppearanceHovered.Options.UseFont = true;
            this.btnlistele.AppearanceHovered.Options.UseTextOptions = true;
            this.btnlistele.AppearanceHovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.btnlistele.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            resources.ApplyResources(this.btnlistele, "btnlistele");
            this.btnlistele.Name = "btnlistele";
            this.btnlistele.Click += new System.EventHandler(this.btnlistele_Click);
            this.btnlistele.MouseLeave += new System.EventHandler(this.btnlistele_MouseLeave);
            this.btnlistele.MouseHover += new System.EventHandler(this.btnlistele_MouseHover);
            // 
            // btnlisticon
            // 
            this.btnlisticon.AllowFocus = false;
            this.btnlisticon.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("btnlisticon.Appearance.BackColor")));
            this.btnlisticon.Appearance.Font = ((System.Drawing.Font)(resources.GetObject("btnlisticon.Appearance.Font")));
            this.btnlisticon.Appearance.ForeColor = ((System.Drawing.Color)(resources.GetObject("btnlisticon.Appearance.ForeColor")));
            this.btnlisticon.Appearance.Options.UseBackColor = true;
            this.btnlisticon.Appearance.Options.UseFont = true;
            this.btnlisticon.Appearance.Options.UseForeColor = true;
            this.btnlisticon.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.btnlisticon.Image = global::cevircagirserverdeneme.Properties.Resources.icons8_Bulleted_List_30beyaz;
            resources.ApplyResources(this.btnlisticon, "btnlisticon");
            this.btnlisticon.Name = "btnlisticon";
            this.btnlisticon.Click += new System.EventHandler(this.btnlistele_Click);
            this.btnlisticon.MouseLeave += new System.EventHandler(this.btnlistele_MouseLeave);
            this.btnlisticon.MouseHover += new System.EventHandler(this.btnlistele_MouseHover);
            // 
            // FormGarsonIslem
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormGarsonIslem";
            this.Load += new System.EventHandler(this.FormGarsonIslem_Load);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.FormGarsonIslem_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.FormGarsonIslem_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.FormGarsonIslem_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.pnlduzenle.ResumeLayout(false);
            this.pnlyeni.ResumeLayout(false);
            this.pnlliste.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraEditors.SimpleButton btnislemclose;
        private System.Windows.Forms.Panel pnlduzenle;
        private DevExpress.XtraEditors.SimpleButton btnediticon;
        private DevExpress.XtraEditors.SimpleButton btnduzenle;
        private System.Windows.Forms.Panel pnlyeni;
        private DevExpress.XtraEditors.SimpleButton btnyeniicon;
        private DevExpress.XtraEditors.SimpleButton btnyenikayit;
        private System.Windows.Forms.Panel pnlliste;
        private DevExpress.XtraEditors.SimpleButton btnlistele;
        private DevExpress.XtraEditors.SimpleButton btnlisticon;
    }
}