﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;

namespace cevircagirserverdeneme
{
    class TcpIpServer
    {
        static Socket clientsocket,s;
        public static String outcomedata;
        static WhenDataComes wdctip;
        private static readonly TcpIpServer singletontis = new TcpIpServer();
        private static readonly object _lock = new object();
        private static readonly object _lockflag = new object();
        private static readonly List<Socket> clients = new List<Socket>();
        private static readonly List<String> chanel = new List<String>();    //bu dizide verileri kanal - ip olarak tut
        private static readonly List<String> vers = new List<String>();
        public bool tcpstartok = false;
        public int globalTimer = 0;
        public string senin_ip;
        private static ManualResetEvent recvDone = new ManualResetEvent(false);
        public static int portnum = 8751;
        public static Socket handler;
        public static AsyncCallback asyncallback;
        public static StateObject state;
        public static bool firstconnect = true;
        public int globalcounter = 0;
        public int globalclientcount = 0;
        System.Timers.Timer tmr,tmr2;
        static TcpIpServer()
        {
            
        }
        private TcpIpServer()   //constructor
        {
            wdctip = new WhenDataComes();
            tmr = new System.Timers.Timer();
            tmr2 = new System.Timers.Timer();
            tmr.Interval = 5000;
            tmr.Elapsed += onTimeout;
            tmr2.Interval = 5000;
            tmr2.Elapsed += onTimeout2;
            outcomedata = "";
            
            //try
            //{
            //    clientsocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            //    //string host = Dns.GetHostName();
            //    //IPHostEntry ip = Dns.GetHostByName(host);
            //    //string senin_ip = ip.AddressList[0].ToString();
            //    //clientsocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, 1);
            //    clientsocket.Bind(new IPEndPoint(IPAddress.Parse("192.168.5.2"), 8500));    //hotspot static ip=192.168.5.3
            //    clientsocket.Listen(100);  //kaç client                                                                                                                       
            //    clientsocket.BeginAccept(new AsyncCallback(whentableconnect), clientsocket);
            //    //sp.SetTcpKeepAlive(true,1000,10000);        //tcp keep alive tanımı(tcp kurulduktan 1 sn sonra, her 10 sn de keep alive paketi gönderilir.)
            //    Debug.WriteLine("192.168.5.2 ile tcp server açıldı");
            //    wdctip = new WhenDataComes();
            //    outcomedata = "$0#";
            //}
            //catch (Exception ex)
            //{
            //    Debug.WriteLine("HATA");
            //    Debug.WriteLine(ex.Message);
            //}
        }//constructor
        public static TcpIpServer singletontisobj
        {
            get
            {
                return singletontis;
            }
        }

        public static Socket[] GetClients()
        {
            lock (_lock) return clients.ToArray();
        }

        public static int GetClientCount()
        {
            lock (_lock) return clients.Count;
        }

        public static void RemoveClient(Socket client)
        {
            lock (_lock) clients.Remove(client);
        }

        public static String[] GetChanels()
        {
            lock (_lock) return chanel.ToArray();
        }

        public static int GetChanelCount()
        {
            lock (_lock) return chanel.Count;
        }

        public static void RemoveChanel(String chan)
        {
            lock (_lock) chanel.Remove(chan);
        }
        public static String[] GetVersion()
        {
            lock (_lock) return vers.ToArray();
        }
        public static void RemoveVersion(String vrs)
        {
            lock (_lock) vers.Remove(vrs);
        }        
        public String[] getCh  
        {
            get
            {
                return vers.ToArray();
            }
        }
        public String ocd   //outcomedata get set method
        {
            get
            {
                return outcomedata;
            }
            set
            {
                outcomedata = value;
            }
        }

        //public void accept()
        //{
        //    while(true)
        //    {
        //        allDone.Reset();
        //        clientsocket.BeginAccept(new AsyncCallback(whentableconnect), clientsocket);
        //        allDone.WaitOne();
        //    }
        //}
        /////////////////////accept callback
        public void starttcp()
        {
            try
            {
                //string host = Dns.GetHostName();
                ////IPHostEntry ip = Dns.GetHostByName(host);
                ////senin_ip = ip.AddressList[0].ToString();
                //IPHostEntry ip = Dns.GetHostEntry(host);
                //foreach (IPAddress ips in ip.AddressList)
                //{
                //    if (ips.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                //    {
                //        senin_ip = ips.ToString();
                //    }
                //}
                //Debug.WriteLine(host+"  "+senin_ip);

                senin_ip = Settings1.Default.udptcpIP;

                clientsocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                SetKeepAliveValues(clientsocket, true, 30000, 1000);   //1 sn de bir keep alive 30 sn ye ayarla
                clientsocket.Bind(new IPEndPoint(IPAddress.Parse(senin_ip), portnum));    //senin_ip --> hostname to ip
                clientsocket.Listen(10);  //kaç client dinleyeceği 
                clientsocket.BeginAccept(new AsyncCallback(whentableconnect), clientsocket);
                tcpstartok = true;
            }
            catch (Exception ex)
            {
                //Debug.WriteLine("HATA");
                Debug.WriteLine(ex.Message);
            }
        }
        public int SetKeepAliveValues(System.Net.Sockets.Socket Socket,bool On_Off,uint KeepaLiveTime,uint KeepaLiveInterval)
        {
            int Result = -1;
            
            unsafe
            {
                TcpKeepAlive KeepAliveValues = new TcpKeepAlive();

                KeepAliveValues.On_Off = Convert.ToUInt32(On_Off);
                KeepAliveValues.KeepaLiveTime = KeepaLiveTime;
                KeepAliveValues.KeepaLiveInterval = KeepaLiveInterval;

                byte[] InValue = new byte[12];

                for (int I = 0; I < 12; I++)
                    InValue[I] = KeepAliveValues.Bytes[I];

                Result = Socket.IOControl(IOControlCode.KeepAliveValues, InValue, null);
            }

            return Result;
        }
        public static void whentableconnect(IAsyncResult iar) //porttan veri geldiğinde yani bir client bağlandığında  
        {
            //allDone.Reset();
            //Debug.WriteLine("----------whentableconnect----------");
            Socket s = iar.AsyncState as Socket;
            Socket handler = s.EndAccept(iar);
            StateObject state = new StateObject();
            clients.Add(handler);

            foreach (Socket sck in GetClients())    //bağlı olmayan cihaz varsa listeden temizle  
            {
                if (!IsConnected(sck))
                {
                    RemoveClient(sck);
                }
            }

            //Debug.WriteLine("biri geldi"+handler.RemoteEndPoint);

            //if (!firstconnect)     //cihaz kopup yeniden bağlanmışsa
            //{
            //    //Console.WriteLine("cihaz kopup bağlandı, hepsini beyaz yap");
            //    outcomedata = "FA07030D";    //234324
            //    singletontisobj.sendData();
            //    wdctip.clearAllReds(1);
            //}
            //firstconnect = false;

            state.workSocket = handler;
            //Debug.WriteLine("\r\n");
            Receivex(state.workSocket);
            //Debug.WriteLine("receiveden döndüm");

            s.BeginAccept(new AsyncCallback(whentableconnect), s);        
        }//whentableconnect
         ////////////////////////accept callback

        public static void Receivex(Socket handler)
        {
            try
            {
                //Console.WriteLine("Receivex");
                StateObject connection = new StateObject();
                connection.workSocket = handler;
                //if (asyncallback == null)
                //{
                //    asyncallback = new AsyncCallback(On_Receive);
                //}
                //handler.BeginReceive(connection.buffer, 0, StateObject.BufferSize, 0, asyncallback, connection);
                handler.BeginReceive(connection.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(On_Receive), connection);
            }
            catch (Exception e)
            {
               // Debug.WriteLine("receive hata "+e.Message);
            }

        }
        public static void On_Receive(IAsyncResult asyn)   
        {
            //kanal iki byte + 23 43 24 (#C$) ekleniyor + id komut değer
            //gelen veri formatı #C$ ile başlıyor sonrasında id komut değer --> && esp den veriler gelirken başında kanal id var

            //Debug.WriteLine("----------gettablemessage----------");
            StateObject connection = (StateObject)asyn.AsyncState;
            Socket handler = connection.workSocket;
            //using (Socket handler = connection.workSocket)
            //{
            if (IsConnected(handler))
            {
                try
                {
                    int size = handler.EndReceive(asyn);
                    if (size > 0)
                    {
                        //Debug.WriteLine("mesaj geldi" + handler.RemoteEndPoint);
                        //incomedata = Encoding.UTF8.GetString(connection.buffer, 0, size);
                        String incomedata = ByteArrayToString(connection.buffer);
                        //Console.WriteLine("gelen veri " + incomedata);
                        if (incomedata.Contains("43484e"))    //"CHN"
                        {
                            //Console.WriteLine("kanal verisi geldi");
                            lock (chanel)
                            {
                                if (GetChanelCount() > 0)
                                {
                                    for (int i = 0; i < GetChanelCount(); i++) //foreach (String chn in GetChanels())
                                    {
                                        //Console.WriteLine("dizide eleman var " +  i + " " + chanel[i]);
                                        if (chanel[i].Contains(incomedata.Substring(6, 4)))
                                        {
                                            //Console.WriteLine("bu kanala ait cihaz önceden bağlanmış clients dizisini düzenle " +handler.RemoteEndPoint +" "+ chanel[i]);
                                            firstconnect = false;
                                            String[] arr = chanel[i].Split('-');
                                            RemoveChanel(chanel[i]);
                                            foreach (Socket sck in GetClients())
                                            {
                                                //Console.WriteLine("soket dizisi elemanı " + sck.RemoteEndPoint + " " + GetClientCount());
                                                if (sck.RemoteEndPoint.ToString() == arr[1])
                                                {
                                                    RemoveClient(sck);
                                                    sck.Shutdown(SocketShutdown.Both);
                                                    sck.Close();
                                                    break;
                                                }
                                            }
                                            foreach(String vrs in GetVersion())
                                            {
                                                if (vrs.Contains(arr[0]))
                                                {
                                                    RemoveVersion(vrs);
                                                    break;
                                                }
                                            }
                                            break;   //burada olcak
                                        }
                                    }
                                }
                                chanel.Add(incomedata.Substring(6, 4) + "-" + handler.RemoteEndPoint);
                                //Console.WriteLine("kanal dizisi sayısı : " + GetChanelCount());
                            }
                            //if (!firstconnect)
                            //{
                            //    outcomedata = incomedata.Substring(6, 4) + "-FA07030D";
                            //    singletontisobj.sendDatabyChanel();
                            //    wdctip.clearAllReds(1, incomedata.Substring(6, 4));
                            //}
                            if(firstconnect)   // cihaz ilk defa bağlanıyorsa kırmızı olan cihazlar kim sorgusu at
                            {
                                outcomedata = incomedata.Substring(6, 4) + "-FA070E0D";
                                singletontisobj.sendDatabyChanel();
                            }
                            firstconnect = true;
                        }
                        else if (incomedata.Contains("5252"))  //versiyon yolluyor
                        {
                            //Console.WriteLine("versiyon : "+ incomedata.Substring(4, 4) + "-" + (Convert.ToInt32( incomedata.Substring(8,2)) -30) + "-" + Settings1.Default.setupversion + "-" + incomedata.Substring(11, 1)+ "." + incomedata.Substring(13, 1));
                            vers.Add(incomedata.Substring(4, 4) + "-" + (Convert.ToInt32(incomedata.Substring(8, 2)) - 30) + "-" + incomedata.Substring(11, 1) + "." + incomedata.Substring(13, 1));
                        }
                        else
                        {
                            //incomedata = incomedata.Substring(0, 2) + ":" + incomedata.Substring(2, 2) + ":" + incomedata.Substring(4, 2);
                            incomedata = incomedata.Substring(0, 2) + "" + incomedata.Substring(2, 2) + "-" + incomedata.Substring(10, 2) + ":" + incomedata.Substring(12, 2) + ":" + incomedata.Substring(14, 2);
                            //Console.WriteLine("gelen veri" + incomedata);
                            wdctip.GetData(incomedata);
                        }
                    }
                }
                catch (Exception ex)
                {
                    //Debug.WriteLine("end receive hata mesajı" + ex.Message);
                }
                Receivex(handler);
            }
        }//On_Receive
        

        /// <summary>
        /// GLOBAL VERİ GÖNDERİRKEN KULLANILACAK METOD --> sendData
        /// </summary>
        public void sendData()   //veri gönderirken herşeyin başına 234324 eklendi -> #C$    
        {
            int a = GetClientCount();
            //listedeki tüm soketleri tara ve gönder 
            if (a > 0)
            {
                try
                {
                    globalTimer = a * 5000;
                    frmGlobal frmglo = new frmGlobal();
                    frmglo.StartPosition = FormStartPosition.Manual;
                    frmglo.Left = frmadmin.ActiveForm.Left;
                    frmglo.Top = frmadmin.ActiveForm.Top;
                    frmglo.Show();
                    if (a > 1)   //cihaz sayısı 1 ise progressbar a gerek yok
                    {
                        Socket sck = (GetClients())[globalcounter];
                        globalcounter++;
                        globalclientcount = a;
                        send(sck);
                        tmr.Start();
                    }
                    else
                    {
                        foreach (Socket sck in GetClients())    //bağlı olan her cihaza gönder  
                        {
                            if (!IsConnected(sck))   //cihaz bağlı değilse listeden sil
                            {
                                RemoveClient(sck);
                            }
                            else   //cihaz bağlı ise veri gönder
                            {
                                send(sck);
                                tmr2.Start();
                            }
                        }
                        outcomedata = "";
                    }
                }
                catch (Exception ex)
                {
                    //Console.WriteLine("senddata"+ex.Message);
                }
            }
        }
        public void onTimeout(object sender, EventArgs e)
        {            
            if (globalcounter < globalclientcount)
            {
                //Console.WriteLine("timer");
                Socket sck = (GetClients())[globalcounter];
                if (IsConnected(sck))   //cihaz bağlı değilse listeden sil
                {
                    send(sck);
                }
                globalcounter++;
            }
            else
            {
                tmr.Stop();
                globalcounter = 0;
                globalclientcount = 0;
            }
        }
        public void onTimeout2(object sender, EventArgs e)
        {
            tmr2.Stop();
        }
        public void send(Socket sck)
        {
            //Console.WriteLine("outcomedata = " + outcomedata + " " + sck.RemoteEndPoint);
            string snd = "234324";
            snd += outcomedata;
            Object objData = snd;
            byte[] byData = StringToByteArray(objData.ToString());     //System.Text.Encoding.ASCII.GetBytes(objData.ToString());
            sck.Send(byData);
        }
        /// <summary>
        /// SEÇİLEN MASAYA VERİ GÖNDERMEK İÇİN KULLANILACAK METOD --> sendDatabyChanel
        /// </summary>
        public void sendDatabyChanel()
        {
            //Console.WriteLine("sendDatabyChanel outcomedata = " + outcomedata);
            try
            {
                String[] splitarr = null;
                splitarr = outcomedata.Split('-');
                string chanel = splitarr[0];
                string command = splitarr[1];
                lock (chanel)
                {
                    foreach (String chn in GetChanels())
                    {
                        //Console.WriteLine("dizide eleman var "+ chn + " "+ (chn.Split('-'))[1]+ " "+chanel );
                        if (chn.Contains(chanel))
                        {
                            foreach (Socket sck in GetClients())
                            {
                                //Console.WriteLine("dizide eleman var " + sck.RemoteEndPoint.ToString());
                                if (sck.RemoteEndPoint.ToString() == (chn.Split('-'))[1])
                                {
                                    if (!IsConnected(sck))   //cihaz bağlı değilse listeden sil
                                    {
                                        RemoveClient(sck);
                                    }
                                    else   //cihaz bağlı ise veri gönder
                                    {
                                        //Console.WriteLine("outcomedata = " + outcomedata + " " + sck.RemoteEndPoint);
                                        string snd = "234324";
                                        snd += command;
                                        Object objData = snd;
                                        byte[] byData = StringToByteArray(objData.ToString());
                                        sck.Send(byData);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                }
                outcomedata = "";
            }
            catch(Exception ex)
            {
                //Console.WriteLine("sendDatabyChanel hata "+ ex.Message);
            }
        }

        public void closeserver()
        {
            //handler.Dispose();
            try {    //sorun try-catch ile çözüldü şimdilik
                handler.Shutdown(SocketShutdown.Both);
                handler.Close();
                handler = null;
            } 
            catch (Exception ex)
            {
                //Console.WriteLine("hata var"+ex.Message);
            }
        }    
        
        public static bool IsConnected(Socket socket)
        {
            try
            {
                return !(socket.Poll(1, SelectMode.SelectRead) && socket.Available == 0);
            }
            catch (SocketException) { return false; }
            catch (System.ObjectDisposedException) { return false; }
        }
        public static string ByteArrayToString(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
                hex.AppendFormat("{0:x2}", b);
            //Console.WriteLine(hex.ToString());
            return hex.ToString();
        }
        public static byte[] StringToByteArray(String hex)
        {
            int NumberChars = hex.Length;
            byte[] bytes = new byte[NumberChars / 2];
            for (int i = 0; i < NumberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
        //public void Send(Socket handlersend, String data)
        //{
        //    // Convert the string data to byte data using ASCII encoding.
        //    byte[] byteData = Encoding.ASCII.GetBytes(data);

        //    // Begin sending the data to the remote device.
        //    handlersend.BeginSend(byteData, 0, byteData.Length, 0,
        //        new AsyncCallback(SendCallback), handlersend);
        //}//Send

        //public void SendCallback(IAsyncResult ar)
        //{
        //    try
        //    {
        //        // Retrieve the socket from the state object.
        //        Socket handlerscallbck = (Socket)ar.AsyncState;
        //        // Complete sending the data to the remote device.
        //        int bytesSent = handlerscallbck.EndSend(ar);
        //        Console.WriteLine("Sent {0} bytes to client.", bytesSent);

        //        handlerscallbck.Shutdown(SocketShutdown.Both);
        //        handlerscallbck.Close();
        //        Debug.WriteLine("socket kapatıldı");
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e.ToString());
        //    }
        //}//SendCallback

    }//end class
}
public class StateObject
{
    public Socket workSocket = null;
    public const int BufferSize = 1024;
    public byte[] buffer = new byte[BufferSize];
}//endclass
struct tcp_keepalive
{
    ulong onoff;
    ulong keepalivetime;
    ulong keepaliveinterval;
};
[
   System.Runtime.InteropServices.StructLayout
   (
       System.Runtime.InteropServices.LayoutKind.Explicit
   )
]
unsafe struct TcpKeepAlive
{
    [System.Runtime.InteropServices.FieldOffset(0)]
    [
          System.Runtime.InteropServices.MarshalAs
           (
               System.Runtime.InteropServices.UnmanagedType.ByValArray,
               SizeConst = 12
           )
    ]
    public fixed byte Bytes[12];

    [System.Runtime.InteropServices.FieldOffset(0)]
    public uint On_Off;

    [System.Runtime.InteropServices.FieldOffset(4)]
    public uint KeepaLiveTime;

    [System.Runtime.InteropServices.FieldOffset(8)]
    public uint KeepaLiveInterval;
}

