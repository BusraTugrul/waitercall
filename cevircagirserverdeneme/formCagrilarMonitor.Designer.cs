﻿namespace cevircagirserverdeneme
{
    partial class formCagrilarMonitor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(formCagrilarMonitor));
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement1 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement2 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement3 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            DevExpress.XtraGrid.Views.Tile.TileViewItemElement tileViewItemElement4 = new DevExpress.XtraGrid.Views.Tile.TileViewItemElement();
            this.cltableid = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.clwaiter = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.cldk_sn = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.clreqtime = new DevExpress.XtraGrid.Columns.TileViewColumn();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.lblcompanyname = new System.Windows.Forms.Label();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.tablesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cevircagirDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.cevircagirDataSet = new cevircagirserverdeneme.cevircagirDataSet();
            this.tvMainv = new DevExpress.XtraGrid.Views.Tile.TileView();
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.reportsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tablesTableAdapter = new cevircagirserverdeneme.cevircagirDataSetTableAdapters.tablesTableAdapter();
            this.gridSplitContainer1 = new DevExpress.XtraGrid.GridSplitContainer();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSetBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tvMainv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).BeginInit();
            this.gridSplitContainer1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cltableid
            // 
            resources.ApplyResources(this.cltableid, "cltableid");
            this.cltableid.FieldName = "table_id";
            this.cltableid.Name = "cltableid";
            // 
            // clwaiter
            // 
            resources.ApplyResources(this.clwaiter, "clwaiter");
            this.clwaiter.FieldName = "waiter_name";
            this.clwaiter.Name = "clwaiter";
            // 
            // cldk_sn
            // 
            resources.ApplyResources(this.cldk_sn, "cldk_sn");
            this.cldk_sn.FieldName = "cldk_sn";
            this.cldk_sn.Name = "cldk_sn";
            this.cldk_sn.UnboundType = DevExpress.Data.UnboundColumnType.String;
            // 
            // clreqtime
            // 
            resources.ApplyResources(this.clreqtime, "clreqtime");
            this.clreqtime.FieldName = "req_time";
            this.clreqtime.Name = "clreqtime";
            // 
            // panelControl1
            // 
            this.panelControl1.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("panelControl1.Appearance.BackColor")));
            this.panelControl1.Appearance.BackColor2 = ((System.Drawing.Color)(resources.GetObject("panelControl1.Appearance.BackColor2")));
            this.panelControl1.Appearance.Options.UseBackColor = true;
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.lblcompanyname);
            this.panelControl1.Controls.Add(this.simpleButton2);
            resources.ApplyResources(this.panelControl1, "panelControl1");
            this.panelControl1.Name = "panelControl1";
            // 
            // lblcompanyname
            // 
            resources.ApplyResources(this.lblcompanyname, "lblcompanyname");
            this.lblcompanyname.BackColor = System.Drawing.Color.Transparent;
            this.lblcompanyname.ForeColor = System.Drawing.Color.White;
            this.lblcompanyname.Name = "lblcompanyname";
            // 
            // simpleButton2
            // 
            this.simpleButton2.AllowFocus = false;
            this.simpleButton2.Appearance.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton2.Appearance.BackColor")));
            this.simpleButton2.Appearance.BorderColor = ((System.Drawing.Color)(resources.GetObject("simpleButton2.Appearance.BorderColor")));
            this.simpleButton2.Appearance.Options.UseBackColor = true;
            this.simpleButton2.Appearance.Options.UseBorderColor = true;
            this.simpleButton2.Appearance.Options.UseTextOptions = true;
            this.simpleButton2.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.simpleButton2.AppearanceHovered.BackColor = ((System.Drawing.Color)(resources.GetObject("simpleButton2.AppearanceHovered.BackColor")));
            this.simpleButton2.AppearanceHovered.Options.UseBackColor = true;
            resources.ApplyResources(this.simpleButton2, "simpleButton2");
            this.simpleButton2.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.simpleButton2.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton2.Image")));
            this.simpleButton2.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleLeft;
            this.simpleButton2.Name = "simpleButton2";
            // 
            // gridControl1
            // 
            this.gridControl1.AllowRestoreSelectionAndFocusedRow = DevExpress.Utils.DefaultBoolean.False;
            this.gridControl1.DataSource = this.tablesBindingSource;
            resources.ApplyResources(this.gridControl1, "gridControl1");
            this.gridControl1.MainView = this.tvMainv;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.tvMainv,
            this.gridView2,
            this.gridView1});
            // 
            // tablesBindingSource
            // 
            this.tablesBindingSource.DataMember = "tables";
            this.tablesBindingSource.DataSource = this.cevircagirDataSetBindingSource;
            // 
            // cevircagirDataSetBindingSource
            // 
            this.cevircagirDataSetBindingSource.DataSource = this.cevircagirDataSet;
            this.cevircagirDataSetBindingSource.Position = 0;
            // 
            // cevircagirDataSet
            // 
            this.cevircagirDataSet.DataSetName = "cevircagirDataSet";
            this.cevircagirDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // tvMainv
            // 
            this.tvMainv.Appearance.EmptySpace.BackColor = ((System.Drawing.Color)(resources.GetObject("tvMainv.Appearance.EmptySpace.BackColor")));
            this.tvMainv.Appearance.EmptySpace.BackColor2 = ((System.Drawing.Color)(resources.GetObject("tvMainv.Appearance.EmptySpace.BackColor2")));
            this.tvMainv.Appearance.EmptySpace.Options.UseBackColor = true;
            this.tvMainv.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.tvMainv.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.cldk_sn,
            this.cltableid,
            this.clreqtime,
            this.clwaiter});
            this.tvMainv.FocusBorderColor = System.Drawing.Color.Transparent;
            this.tvMainv.GridControl = this.gridControl1;
            this.tvMainv.Name = "tvMainv";
            this.tvMainv.OptionsTiles.HorizontalContentAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.tvMainv.OptionsTiles.ItemBorderVisibility = DevExpress.XtraEditors.TileItemBorderVisibility.Never;
            this.tvMainv.OptionsTiles.ItemSize = new System.Drawing.Size(200, 200);
            this.tvMainv.OptionsTiles.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.tvMainv.OptionsTiles.Padding = new System.Windows.Forms.Padding(20, 30, 20, 30);
            this.tvMainv.OptionsTiles.RowCount = 7;
            this.tvMainv.OptionsTiles.ShowGroupText = false;
            this.tvMainv.OptionsTiles.VerticalContentAlignment = DevExpress.Utils.VertAlignment.Top;
            tileViewItemElement1.Appearance.Normal.BackColor = ((System.Drawing.Color)(resources.GetObject("resource.BackColor")));
            tileViewItemElement1.Appearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("resource.Font")));
            tileViewItemElement1.Appearance.Normal.ForeColor = ((System.Drawing.Color)(resources.GetObject("resource.ForeColor")));
            tileViewItemElement1.Appearance.Normal.Options.UseBackColor = true;
            tileViewItemElement1.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement1.Appearance.Normal.Options.UseForeColor = true;
            tileViewItemElement1.Column = this.cltableid;
            resources.ApplyResources(tileViewItemElement1, "tileViewItemElement1");
            tileViewItemElement1.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.TopCenter;
            tileViewItemElement1.TextLocation = new System.Drawing.Point(4, -17);
            tileViewItemElement2.Appearance.Normal.BackColor = ((System.Drawing.Color)(resources.GetObject("resource.BackColor1")));
            tileViewItemElement2.Appearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("resource.Font1")));
            tileViewItemElement2.Appearance.Normal.ForeColor = ((System.Drawing.Color)(resources.GetObject("resource.ForeColor1")));
            tileViewItemElement2.Appearance.Normal.Options.UseBackColor = true;
            tileViewItemElement2.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement2.Appearance.Normal.Options.UseForeColor = true;
            tileViewItemElement2.Column = this.clwaiter;
            resources.ApplyResources(tileViewItemElement2, "tileViewItemElement2");
            tileViewItemElement2.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileViewItemElement2.TextLocation = new System.Drawing.Point(4, -12);
            tileViewItemElement3.Image = global::cevircagirserverdeneme.Properties.Resources.akebossongri150;
            tileViewItemElement3.ImageLocation = new System.Drawing.Point(-11, 14);
            resources.ApplyResources(tileViewItemElement3, "tileViewItemElement3");
            tileViewItemElement4.Appearance.Normal.Font = ((System.Drawing.Font)(resources.GetObject("resource.Font2")));
            tileViewItemElement4.Appearance.Normal.ForeColor = ((System.Drawing.Color)(resources.GetObject("resource.ForeColor2")));
            tileViewItemElement4.Appearance.Normal.Options.UseFont = true;
            tileViewItemElement4.Appearance.Normal.Options.UseForeColor = true;
            tileViewItemElement4.Column = this.cldk_sn;
            resources.ApplyResources(tileViewItemElement4, "tileViewItemElement4");
            tileViewItemElement4.TextAlignment = DevExpress.XtraEditors.TileItemContentAlignment.BottomCenter;
            tileViewItemElement4.TextLocation = new System.Drawing.Point(4, 13);
            this.tvMainv.TileTemplate.Add(tileViewItemElement1);
            this.tvMainv.TileTemplate.Add(tileViewItemElement2);
            this.tvMainv.TileTemplate.Add(tileViewItemElement3);
            this.tvMainv.TileTemplate.Add(tileViewItemElement4);
            // 
            // gridView2
            // 
            this.gridView2.GridControl = this.gridControl1;
            this.gridView2.Name = "gridView2";
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            // 
            // reportsBindingSource
            // 
            this.reportsBindingSource.DataMember = "reports";
            // 
            // tablesTableAdapter
            // 
            this.tablesTableAdapter.ClearBeforeFill = true;
            // 
            // gridSplitContainer1
            // 
            resources.ApplyResources(this.gridSplitContainer1, "gridSplitContainer1");
            this.gridSplitContainer1.Grid = this.gridControl1;
            this.gridSplitContainer1.Name = "gridSplitContainer1";
            this.gridSplitContainer1.Panel1.Controls.Add(this.gridControl1);
            // 
            // formCagrilarMonitor
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.gridSplitContainer1);
            this.Controls.Add(this.panelControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "formCagrilarMonitor";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.formCagrilarMonitor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tablesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSetBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cevircagirDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tvMainv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reportsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridSplitContainer1)).EndInit();
            this.gridSplitContainer1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        public DevExpress.XtraGrid.GridControl gridControl1;
        public DevExpress.XtraGrid.Views.Tile.TileView tvMainv;
        private DevExpress.XtraGrid.Columns.TileViewColumn clreqtime;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        //private cevircagirDataSet5 cevircagirDataSet5;
        private System.Windows.Forms.BindingSource reportsBindingSource;
        //private cevircagirDataSet5TableAdapters.reportsTableAdapter reportsTableAdapter;
        private System.Windows.Forms.Label lblcompanyname;
        private DevExpress.XtraGrid.Columns.TileViewColumn clwaiter;
        private DevExpress.XtraGrid.Columns.TileViewColumn cltableid;
        private DevExpress.XtraGrid.Columns.TileViewColumn cldk_sn;
        private System.Windows.Forms.BindingSource cevircagirDataSetBindingSource;
        private cevircagirDataSet cevircagirDataSet;
        private System.Windows.Forms.BindingSource tablesBindingSource;
        private cevircagirDataSetTableAdapters.tablesTableAdapter tablesTableAdapter;
        private DevExpress.XtraGrid.GridSplitContainer gridSplitContainer1;
    }
}